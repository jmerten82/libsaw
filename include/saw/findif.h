/***
findif.h: header file of the libsaw library
Author: Julian Merten
Institution: ITA Heidelberg
Mail: jmerten@ita.uni-heidelberg.de
***/

#ifndef   	FIN_DIF_H_
# define   	FIN_DIF_H_


#include <iostream>
#include <cmath>
#include <vector>
#include <gsl/gsl_matrix.h>
#include <stdexcept>

using namespace std;

/**
   This class represents a non-masked, non-adaptive Cartesian grid. 
   Several finite differencing operations can be performed, the
   according matrices can be returned and complete multiplications
   can be performed on that grid.
**/

class FinDifGrid
{

 protected:

  int x, y;
  /*
    The x and the y dimension of the grid in pixels.
  */

  int numpix;
  /*
    The number of pixels in the grid. FOr this simple grid just x*y.
  */

  int order;
  /*
    The order of the findif operations. In the case of lensing order 1 means
    delfection angle operations, 2 refers to shear and convergence and 3 to
    flexion.
  */

  double size;
  /*
    The phyical size of the x-dimension of the field. This is needed to 
    give the derivatives the right dimensions. Can also be set to 1.0 
    which is the standard behaviour. 
  */

  vector<bool> allocationtable;
  //  gsl_vector_bool *allocationtable;  
  /*
    Vector showing which quantities are indeed allocated. The order is 
    the following. indexmap, a, s/c, f/g , a_inverted, s/c_inverted,
    f/g_inverted and mask.
  */ 

  gsl_matrix_int *typemap;
  /*
    Of dimension (y,x). Defines each individual pixel as corner, edge or
    central pixel. Type 1 is lower-left corner, 2 is lower-right, 3 is 
    upper-left, 4 is upper-right. 5 is lower edge, 6 is upper edge, 7 is
    left edge and 8 is right edge. 0 is a central pixel.
    3  6  4
    7  0  8
    1  5  2
    For grids of order >3 the layout is:
    33 6 44
    33 6 44
    77 0 88
    11 5 22
    11 5 11
  */

  gsl_matrix_int *indexmap;
  /*
    Of dimensions (y,x). Gives the index of each individual pixel if the
    grid is represented by a vector. Counting starts in the lower-left corner
    and goes row-wise. Indices run from 0 to x*y-1. x*y-1 is upper-right corner
    pixel.
  */

  gsl_matrix_int *mask;
  /*
    The mask of the field, where all masked pixels are set to 1 all others
    to zero. This mask is only created if it's asked for by show_data("mask").
  */

  gsl_matrix *a1;
  /*
    Of dimension (numpix,3). Contains all non-zero coefficients for each grid
    pixel for a findif operation refering to the first component of the 
    deflection angle.
  */
  gsl_matrix_int *a1pos;
  /*
    Of dimension (numpix,3). Contains the index-positions of the a1 coeff.
    Saved in the same order as a1.
  */

  gsl_matrix *a2;
  /*
    As above but for the second deflection angle component.
  */

  gsl_matrix_int *a2pos;
  /*
    Refers to a2.
  */

  gsl_matrix *s1;
  /*
    As above but for the first shear component.
  */

  gsl_matrix_int *s1pos;
  /*
    Refers to s1.
  */

  gsl_matrix *s2;
  /*
    As above but for the second shear component.
  */

  gsl_matrix_int *s2pos;
  /*
    Refers to s2.
  */

  gsl_matrix *c;
  /*
    As above but for the convergence.
  */

  gsl_matrix_int *cpos;
  /*
    Refers to c.
  */

  gsl_matrix *f1;
  /*
    As above but for the first F flexion component.
  */

  gsl_matrix_int *f1pos;
  /*
    Refers to f1.
  */

  gsl_matrix *f2;
  /*
    As above but for the second F flexion component.
  */

  gsl_matrix_int *f2pos;
  /*
    Refers to f2.
  */

  gsl_matrix *g1;
  /*
    As above but for the first G flexion component.
  */

  gsl_matrix_int *g1pos;
  /*
    Refers to g1.
  */

  gsl_matrix *g2;
  /*
    As above but for the second G flexion component.
  */

  gsl_matrix_int *g2pos;
  /*
    Refers to g2.
  */

  /*
    In the following all quantities are repeated in the inverted form, which
    allows for inverted matrix-multiplication operations. The method
    to produce this data is described later.
  */

  gsl_matrix *inv_a1;
  /*
    Of dimension (numpix,3). Contains all non-zero coefficients for each grid
    pixel for an inverted findif operation refering to the first component 
    of the deflection angle.
  */
  gsl_matrix_int *inv_a1pos;
  /*
    Of dimension (numpix,3). Contains the index-positions of the inv_a1 coeff.
    Saved in the same order as inv_a1.
  */

  gsl_matrix *inv_a2;
  /*
    As above but for the second deflection angle component.
  */

  gsl_matrix_int *inv_a2pos;
  /*
    Refers to a2.
  */

  gsl_matrix *inv_s1;
  /*
    As above but for the first shear component.
  */

  gsl_matrix_int *inv_s1pos;
  /*
    Refers to s1.
  */

  gsl_matrix *inv_s2;
  /*
    As above but for the second shear component.
  */

  gsl_matrix_int *inv_s2pos;
  /*
    Refers to s2.
  */

  gsl_matrix *inv_c;
  /*
    As above but for the convergence.
  */

  gsl_matrix_int *inv_cpos;
  /*
    Refers to c.
  */

  gsl_matrix *inv_f1;
  /*
    As above but for the first F flexion component.
  */

  gsl_matrix_int *inv_f1pos;
  /*
    Refers to f1.
  */

  gsl_matrix *inv_f2;
  /*
    As above but for the second F flexion component.
  */

  gsl_matrix_int *inv_f2pos;
  /*
    Refers to f2.
  */

  gsl_matrix *inv_g1;
  /*
    As above but for the first G flexion component.
  */

  gsl_matrix_int *inv_g1pos;
  /*
    Refers to g1.
  */

  gsl_matrix *inv_g2;
  /*
    As above but for the second G flexion component.
  */

  gsl_matrix_int *inv_g2pos;
  /*
    Refers to g2.
  */

 public:

  FinDifGrid(int x_dim, int y_dim, double size, int fin_dif_order);
  /*
    Standard constructor for a non-masked_field. Size is the physical
    field size of the x-dimension. Fin_dif_order can be 1,2 or 3.
  */

  FinDifGrid(gsl_matrix_int *mask, double size, int fin_dif_order);
  /*
    Standard constructor in the masked case. Should only be used if
    field is really masked since more runtime expensive. Masked pixels
    must be indicated by a integer value -1 in mask.
  */

  ~FinDifGrid();
  /*
    Standard destructor. Get de-allocation info from the allocation table.
  */

  int show_int(string selection);
  /*
    Returns int valued grid parameters, selections are: 
    x_dim, y_dim,  numpix and the grid order.
  */

  double show_double(string selection);
  /*
    Returns the double valued  grid parameters. Selections are:
    size.
  */

  gsl_matrix_int* show_data(string selection);
  /*
    Gives direct access to some the essential grid definition. Selections are:
    typemap, indexmap and mask. Mask is only created if this function
    is called.
  */

  gsl_matrix* coeffs(string selection);
  /*
    Returns the non-zero coefficients of the grid's findif operation
    in a vector which has the same order as the according position vector.
    Selection are: a1, a2, s1, s2, c, f1, f2, g1, g2, inv_a1, inv_a2, inv_s1,
    inv_s2, inv_c, inv_f1, inv_f2, inv_g1, inv_g2
  */

  gsl_matrix_int* positions(string selection);
  /*
    Returns the grid positions of the non-zero coefficients
    in a vector which has the same order as the according coeff vector.
    Selection are: a1, a2, s1, s2, c, f1, f2, g1, g2, inv_a1, inv_a2, inv_s1,
    inv_s2, inv_c, inv_f1, inv_f2, inv_g1, inv_g2 
  */

  int findif_length(string selection);
  /*
    Returns the maximum number of non-zero elements for a findif operation.
    Selections are: a1, a2, s1, s2, c, f1, f2, g1, g2, inv_a1, inv_a2, inv_s1,
    inv_s2, inv_c, inv_f1, inv_f2, inv_g1, inv_g2  
  */

  void findif_matrix(string selection, gsl_matrix* out);
  /*
    Writes a full findif operation matrix  to out.
    Selections are:  a1, a2, s1, s2, c, f1, f2, g1, g2.
  */

  void invert();
  /*
    Inverts the findif operations and writes the coefficients to the
    according matrices. This is tailored for SaWLens.
  */

  void mult_vec(string selection, gsl_vector *in, gsl_vector *out);
  /*
    Performs a full findif operation on in and writes the result to out.
    Selections are:  a1, a2, s1, s2, c, f1, f2, g1, g2.
  */
  void mult_vec(string selection, gsl_vector_float *in, gsl_vector_float *out);
  /*
    Performs a full findif operation on in and writes the result to out.
    Selections are:  a1, a2, s1, s2, c, f1, f2, g1, g2.
  */

  void convert(gsl_matrix *in, gsl_vector *out);
  /* 
     Converts a quanity given as a matrix map into a vector of size numpix
  */
  void convert(gsl_vector *in, gsl_matrix *out);
  /*
    Inverse process of what is described above.
  */
  void convert(gsl_matrix_int *in, gsl_vector_int *out);
  /*
    Same as above but for integer data.
  */
  void convert(gsl_vector_int *in, gsl_matrix_int *out);
  /*
    Inverse process of what is described above.
  */

  
};    


#endif 	    /* !FIN_DIF_H_ */
