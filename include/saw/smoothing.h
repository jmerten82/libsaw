#ifndef SMOOTHING_H
#define SMOOTHING_H



#include <iostream>
#include <cmath>
#include <vector>
#include <gsl/gsl_matrix.h>
#include <stdexcept>

using namespace std;


void gauss_smooth(gsl_matrix* inmap, gsl_matrix* outmap, double sigma);

#endif //SMOOTHING_H








