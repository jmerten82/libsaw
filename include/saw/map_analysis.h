/***
Header included in the SaWLens reconstruction package.
Developed by Julian Merten (2010)
at ITA Heidelberg and OA Bologna
Not to be used without permission
of the author.
jmerten@ita.uni-heidelberg.de
***/


#ifndef MAP_ANALYSIS_H_
#define MAP_ANALYSIS_H_

#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <astro/cosmology.h>

using namespace std;

/**
   Some useful numbers.
**/

const double cH = 2997.92458; //In Mpc/h
const double PI = 3.141592654;
const double speed_of_light = 2.99792458e10; //In cm/s
const double NewtonG = 6.673e-8; //In cm^3 g^-1 s^-2
const double MPCtoCM = 3.08568e24; //1Mpc = MPCtoCM cm
const double MSOLtoG = 1.989e33; //1M_{\odot} = MSOLtoG g 


class Convergence_Map
{
  
  /**
     This class provides the functionality to derive density profiles and
     mass estimates from a given convergence map as input.
  **/

 protected:

  gsl_matrix *orig_convergence;
  /*
    This matrix is the initial input convergence and will stay untouched 
    if the analysis should be reset.
  */
  gsl_matrix *redshifts;
  /*
    To convert to a physical surface density, the distribution of constraint
    redshifts is needed. It's possible that this map is not provided.
  */
  int sampling_factor;
  /*
    The oversampling factor for the convergence map.
  */
  gsl_matrix *overs_convergence;
  /*
    The oversampled convergence map.
  */
  gsl_matrix *overs_redshifts;
  /*
    The oversampled redshift map.
  */
  gsl_matrix_int *overs_mask;
  /*
    Allows to mask certain areas ouf of the analysis. Must have the same
    dimension as orig_convergence. Every pixel set to 0 in this map will
    be ignored. All other values switch the pixel on.
  */
  gsl_matrix *density_map;
  /*
    The oversampled surface mass density map. This can only calculated 
    if redshifts are provided.
  */
  double map_redshift;
  /*
    The redshift of the surface density plane.
  */
  double fieldsize;
  /*
    The extension of the field along the x-axis in arcsec.
  */
  double pixel_size;
  /*
    The oversampled pixel size in arcsec.
  */
  double pixel_size_o;
  /*
    The pixel size of the non-oversampled map.
  */
  double centre_x;
  /*
    The oversampled pixel centre of the coordinate frame.
  */
  double centre_x_o;
  /*
    The non-oversampled centre of the coordinate frame.
  */
  double centre_y;
  /*
    The oversampled pixel centre of the coordinate frame.
  */
  double centre_y_o;
  /*
    The non-oversampled centre of the coordinate frame.
  */
  double physical_pixel_size;
  /*
    The oversampled pixel size in Mpc.
  */
  double physical_pixel_size_o;
  /*
    The non-oversampled pixel size in Mpc.
  */
  int normal_x;
  /*
    The original pixel x-dimension.
  */
  int normal_y;
  int over_x;
  /*
    The oversampled x-dimension.
  */
  int over_y;
  bool density;
  /*
    Flag, indicating if a density is to be calculated.
  */

  bool scaled;
  /*
    Flag indicating if the convergence was already scaled to a certain redshift.
  */

  bool masked;
  /*
    Indicating if the map is masked.
  */

  astro::cosmology *cosmo;
  /*
    The underlying cosmology, which mostly does distance conversions. 
  */

 public:
  
  Convergence_Map(gsl_matrix *inmap, double givenfieldsize, gsl_matrix *zmap, double mapredshift, double centrex, double centrey, int oversampling, astro::cosmology *cosmo, double standard_redshift);
  /*
    The standard constructor. Needs the original convergence map, together
    with the fieldsize of the map in arcsec, the redshift map of same
    dimensionality, the redshift of the map plane. Defining the centre 
    is non-trivial. We assume zero being the first pixel coordinate and
    x/y-dim -1 as the last coordinate.
    The centre of a pixel determines its coordinates and the origin of 
    the frame has to be given wrt the not oversampled map.
    The oversampling factor determines by how much the field is oversampled
    for smooth curves. Conversion is the factor to translate one arcsec into
    Mpc. The cosmology comes from libastro.
  */
  Convergence_Map(gsl_matrix *inmap, gsl_matrix_int *mask, double givenfieldsize, gsl_matrix *zmap, double mapredshift, double centrex, double centrey, int oversampling, astro::cosmology *cosmo, double standard_redshift);
  /*
    As above but with the possibility to retain certain errors of the field from
    the analysis as defined in mask. Mask must have the same dimensions as
    inmap and pixels the should disregarded must be set to 0 in this map. All
    other values in mask will be interpreted as that the pixel is "switched"
    on.
  */
  Convergence_Map(gsl_matrix *inmap, double givenfieldsize,double mapredshift, double centrex, double centrey, int oversampling, astro::cosmology *cosmo); 
  /*
    As above but without redshift map, this does not allow for 
    deriving physical densities or masses then.
  */
  Convergence_Map(gsl_matrix *inmap, gsl_matrix_int *mask,double givenfieldsize,double mapredshift, double centrex, double centrey, int oversampling, astro::cosmology *cosmo); 
  /*
    As above but with the possibility of masking.
  */

  ~Convergence_Map();

  /*
    Standard destructor, free memory.
  */

  double x_c(string selection, int x);
    /*
      Returns the physical x-coordinate of a pixel. Selection are:
      left_arcsec, right_arcsec, centre_arcsec,
      left_mpc, right_mpc, centre_mpc.
    */

  double x_c_o(string selection, int x);
    /*
      As above but for the original, non-oversampled map.
    */

  double y_c(string selection, int y);
    /*
      Returns the physical y-coordinate of a pixel. Selection are:
      bottom_arcsec, top_arcsec, centre_arcsec,
      bottom_mpc, top_mpc, centre_mpc.
    */

  double y_c_o(string selection, int y);
    /*
      As above but for the original, non-oversampled map.
    */

  int show_xpixel(string selection, double x_c);
  /*
    Selection are mpc or arcsec. Writes the pixel x-coordinate physical 
    x-coordinate x_c. CAREFUL: Give x_c in Mpc h^-1.
  */

  int show_xpixel_o(string selection, double x_c);
  /*
    As above but for the original, non-oversampled map.
  */

  int show_ypixel(string selection, double y_c);
  /*
    The same for the y-coordinate.
  */

  int show_ypixel_o(string selection, double y_c);
  /*
    As above but for the original, non-oversampled map.
  */

  gsl_matrix* data(string selection);
  /*
    Returns the data of the class. Selections are "orig_convgergence",
    "overs_convergence", "redshifts", "overs_redshifts" and "density".
  */

  gsl_matrix_int* mask();
  /*
    Returns the oversampled mask.
  */

  double double_data(string selection);
  /*
    Returns double values. Selections are: physical_pixel_size(_o), 
    pixel_size(_o), fieldsize, map_redshift.
  */

  int int_data(string selection);
  /*
    Returns integer values. Selections are: normal_x, normal_y, over_x, over_y,
    sampling_factor, centre_x(_o) and centre_y(_o).
  */

  bool bool_data(string selection);
  /*
    Returns the bool attributes. Selections are: density, scaled.
  */

  void set_bool(string selection, bool value);
  /*
    Sets the bool attributes to value. Selections are: density, scaled.
  */
 
  void masssheet_normalise(string selection, double value);
  /*
    Masssheet transforms the convergence map, selections are "min", where
    the coldest pixel is transformed to value, or "max" where the hottest 
    pixel is transformed to value. Another selection is "border" where the
    value fraction of the borders is transformed to 0.0 on average.
  */

  void scaling(double redshift_S_old, double redshift_S_new, double redshift_D);
  /*
    Rescales the convergence map given at an old unique source redshift
    to a new unique source redshift. Redshift of lens has also to be given.
    This should be extended to take into account initial redshift map.
  */

  void convergence_profile(string selection, double x, double y, double radius, int sampling, bool discard, string filename);
  /*
    Creates a radial profile around x/y by sampling radius into the given
    number of bins. The result is written as two-cloumn ASCII to filename.
    Selections are arcsec and mpc which define the coordinate system of 
    centre and radius, together with the output. If the discard flag is set, 
    underdense regions (negative kappa) will be disregarded.
  */

  double convergence(string selection, double x, double y, double radius, bool discard);
  /*
    Returns the radial convergence around the centre x/y at a given radius.
    If the discard flag is set underdense (negative kappa) regions will be
    disregarded.
  */

  void density_profile(string selection, double x, double y, double radius, int sampling, bool discard, string filename);
  /*
    As above, but creates a density profile.
  */

  double density_shell(string selection, double x, double y, bool discard, double radius);
  /*
    As for the convergence above.
  */

  double mass(string selection, double x, double y, double radius, bool discard);
  /*
    As above, but return the total mass within radius around x/y to filename.
  */
  double mass(string selection, double x, double y, double x_extension, double y_extension, bool discard);
  /*
    Returns the mass within a rectangular field, centred on x/y and sidelengths
    x_extension and y_extension.
  */
  double annulus_mass(string selection, double x, double y, double inner_radius, double outer_radius, bool discard);
  /*
    Calculates the mass within an annulus, so mass within outer_radius - mass 
    within inner_radius.
  */
  double mass();
  /*
    Returns the total mass of the field in units of M_sol / h.
  */

  double surface_mass(string selection, double x, double y, double radius, string selection2, bool discard);
  /*
    Returns the average surface-mass density within a radius around x/y in 
    M_sol / Mpc^2 * h (selection2 = cosmo) or in g/cm^2 * h (selection2 = cgm)
  */

  void find_peaks(string selection, string method, bool maxpeak, bool pixel_coord, string outfile);
  /*
    Finds peaks in the (original, not over-sampled!) 
    field and writes their coordinates according to 
    selection which allows for (left/right/centre_)arcsec or 
    (left/right/centre_)mpc. Method is either maximum,
    which searches for highest convergence pixel value or neighbours which 
    asks for a pixel to be brighten then all its neighbours. If maxpeak 
    is set only the brightest peak is written to ASCII outfile.
    If pixel coord is set, also the pixel position is written to file. 
    This is obsolete if method is maximum.
  */

  void find_peaks(string selection, string method, int xmin, int xmax, int ymin, int ymax, bool maxpeak, bool pixel_coord, string output); 
   
  /*
    As before but peaks are only returned if they lie within the window
    defined by xmin/max and ymin/max, depending on selection.
  */

};  



#endif //MAP_ANALYSIS_H_
