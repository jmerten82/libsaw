/***
Julian Merten
JPL/Caltech/University of Heidelberg

Some little timing utilities which give you very precise 
timing data. This is derived from a CUDA course
at HLRS.

***/

#ifndef   	TIMING_H_
# define   	TIMING_H_


#ifndef __USE_POSIX199309
#define __USE_POSIX199309
#include <time.h>
#else
#include <time.h>
#endif

typedef struct timespec Timer;

static inline void initTimer(Timer* timer)
{

  /* 
     Inits a timer. 
  */
  clock_gettime(CLOCK_MONOTONIC,timer);
}
  
static inline double getTimer(Timer* timer)

{
  /*
    Reads and prints the timer.
  */

  struct timespec endTimespec;
  clock_gettime(CLOCK_MONOTONIC, &endTimespec);
  return (endTimespec.tv_sec - timer->tv_sec) + (endTimespec.tv_nsec - timer->tv_nsec)*1e-9;
  }


static inline double getAndResetTimer(Timer* timer)

{
  /*
    Reads, prints and resets the timer to zero.
  */

  struct timespec endTimespec;
  clock_gettime(CLOCK_MONOTONIC, &endTimespec);
  return (endTimespec.tv_sec - timer->tv_sec) + (endTimespec.tv_nsec - timer->tv_nsec)*1e-9;
  *timer = endTimespec;
}
  
static inline double getTimerDifference(Timer* timerStart, Timer* timerEnd)
{
  /*
    Outputs the diference between two timers.
  */

  return (timerEnd->tv_sec - timerStart->tv_sec) + (timerEnd->tv_nsec - timerStart->tv_nsec)*1e-9;
}

#endif 	    /* !TIMING_H_ */
