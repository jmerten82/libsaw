/***
    Coordinate grid class, moderating between a pixelised grid 
    representation and underlying physical coordinates. Allows for
    masking within DS9.

    Julian Merten
    JPL / Caltech
    jmerten@caltech.edu
    Dec. 2011

***/

#ifndef   COORD_GRID_H_
#define   COORD_GRID_H_

#include <iostream>
#include <stdexcept>
#include <vector>
#include <cmath>
#include <fstream>
#include <saw/util.h>
#include <gsl/gsl_matrix.h>
#include <saw/rw_fits.h>

using namespace std;

class coordinate_grid
{

  /**
     Base class providing transformation, masking and information abilities.
  **/

 protected:

  /**
    Basic grid quantities. 0 component is always x, 1 is y direction.
  **/

  /*
    Pixel dimensions
  */

  vector<int> pix_dim;

  /*
    Physical dimensions
  */

  vector<int> phys_dim;

  /*
    Flag identifying the physical coordinate system. True if J2000, false if
    it is something else.
  */

  bool J2000;

  /*
    Flag indicating if the grid is masked.
  */

  bool masked;

  /*
    The origin of the coordinate frame in pixel space.
  */

  vector<double> pix_origin;

  /*
    The physical origin.
  */

  vector<double> phys_origin;

  /*
    The coordinate transformation matrix.
  */

  vector<double> M;

  /*
    The position information of all rectangular masks in physical coordinates.
  */

  vector<double> rect_masks;

  /*
   The position information of all circular masks in physical coordinates.
  */

  vector<double> circ_masks;

  /*
    The position information of all annuli masks in physical coordinates.
  */

  vector<double> annuli_masks;


 public:

  /*
    Constructor creating basic grid of size X x Y, without any physical 
    coordinate system. This will just be set to a continuous representation
    of the pixel coordinates.
  */
  coordinate_grid(int x, int y);

  /*
    Constructor creating a grid with physical coordinates.
  */ 
  coordinate_grid(int x, int y, double pix_orig_x, double pix_orig_y, double phys_orig_x, double phys_orig_y,double m00, double m01, double m10, double m11, bool J2000);

  /*
    Destructor, frees memory.
  */
  ~coordinate_grid();

  /*
    Adds a WCS to an existing grid. If grid had an WCS, this will
    be overwritten. All masks will be reset. 
  */
  void add_WCS(double pix_orig_x, double pix_orig_y, double phys_orig_x, double phys_orig_y,double m00, double m01, double m10, double m11, bool J2000in);

  /*
    Resets the WCS of a grid to a simple one representing continuous 
    pixel coordinates.
  */

  void reset_WCS();

  /*
    Adds rectangular masks to the WCS depending on the length of the 
    input DS9 file. Every line with the text tag "rect_mask", "circ_mask",
    or "annuli_mask" will be interpreted as mask. Note that rect_masks
    can also be rotated.
  */

  void add_masks(string ds9_file);

  /*
    Return basic pixel quantities. Selection are x_dim, y_dim, 
    field size.
  */

  int return_pixel_data(string selection);
  
  /*
    Return basic physical quantities. Selection are x_dim, y_dim, 
    field size.
  */

  int return_phys_data(string selection);
  
  /*
    Returns the WCS data. Order is pix_x, pix_y, phys_x, phys_y, 
    M00, M01, M10, M11.
  */
  
  vector<double> return_WCS();
  
  /*
    Return the pixel coordindate when you give physical one. Selections
      are "x" and "y".
  */

  double return_pix_coord(double phys_coord_x,double phys_coord_y, string selection);

  /*
    Return the physical coordindate when you give the pixel one. Selections
    are "x" and "y".
  */

  double return_phys_coord(double pix_coord_x, double pix_coord_y, string selection);

  /*
    Returns true or false depending if pixel coordinate is masked.
  */
  bool is_pix_masked(double pix_coord_x, double pix_coord_y);

  /*
    Returns true or false depending if physical coordinate is masked.
  */
  bool is_phys_masked(double phys_coord_x, double phys_coord_y);

  /*
    Put the visualisation of the grid into GSL output. Masked
    pixels will be set to 1. Rest is set to 0.
  */
  void visualise(gsl_matrix_int *output);

  /*
    Write the grid to fits.  Masked pixels will be set to 1. 
    Rest is set to 0. WCS is written to FITS header.
  */
  void visualise(string filename);

};


#endif  /*!COORD_GRID_H_*/
