/***
    Class providing very precise and fast finite differencing routines
    for unmasked grids and up to second order. The derivative coefficients
    are based on Fongberg88.

    Julian Merten
    JPL / Caltech 
    jmerten@caltech.edu
    Dec. 2011

***/

#ifndef    FINDIF_FAST_H
#define    FINDIF_FAST_H

#include <vector>
#include <stdexcept>
#include <cmath>
#include <gsl/gsl_matrix.h>
#include <saw/util.h>
#include <sstream>

using namespace std;

class findif_grid_fast
{

 protected:

  /*
    Pixel dimensions of the grid. Length of vector is 2.
  */

  vector<int> dim;

  /*
    Order information, first entry is maximum derivative order,
    second one is the accurazy of the scheme. This has become redundant.
    Order is now always 8.
  */

  vector<int> order;

  /*
    If the grid has a length unit, the pixel size can be set here.
    Initially this quantity is set such that the total grid size is 1.
  */

  double grid_scale;

  /*
    Vectors holding the central and forward coefficients for
    first order differencing.
  */

  vector<double> coeff_c1_8;
  vector<double> coeff_c1_6;
  vector<double> coeff_c1_4;
  vector<double> coeff_c1_2; 
  vector<double> coeff_f1_8; 
  vector<double> coeff_f1_6;
  vector<double> coeff_f1_4;
  vector<double> coeff_f1_2;

  /*
    Vectors holding the central and forward coefficients for
    second order differencing.
  */

  vector<double> coeff_c2_8; 
  vector<double> coeff_c2_6;
  vector<double> coeff_c2_4;
  vector<double> coeff_c2_2;
  vector<double> coeff_f2_8; 
  vector<double> coeff_f2_6;
  vector<double> coeff_f2_4;
  vector<double> coeff_f2_2;
  /*
    Vectors holding the central and forward coefficients for
    third order differencing.
  */

  vector<double> coeff_c3_6;
  vector<double> coeff_c3_4;
  vector<double> coeff_c3_2; 
  vector<double> coeff_f3_6;
  vector<double> coeff_f3_4;
  vector<double> coeff_f3_2;

 public:

  /*
    Standard constructor. Needs x and y-dimension of the grid. 
    The initial derivative order is set to one. 
    Can be changed with method below.
    The side length  of the grid is initially set to 1.0. Can
    be set differently with set_scale below. A general suggestion
    though: Scale all length dimensions to unity in the first place.
  */

  findif_grid_fast(int x, int y);

  /*
    Standard destructor. Empyting coefficients. 
  */
  
  ~findif_grid_fast();

  /*
    Returns the dimensions of the grid. Selection are "x" and "y".
  */

  int show_dim(string selection);

  /*
    Returns the order of the grid. Selections are "derivative" and
    "accuracy".
  */

  int show_order(string selection);

  /*
    Depending on the accurasy order, this function returns an identifier
    showing what finite differencing scheme has to be used. Selections
    are "x" and "y" return values are -1 for forward scheme, 1 for
    backward scheme and 0 for a central scheme.
  */

  int define_type(int index, string selection);

  /*
    Changes the derivative order of the grid. Selections are "first", "second"
    and "third".
  */

  void switch_order(string selection);

  /*
    Sets the size of the grid in x-direction. This normalises the finite
    differencing. Usually the scale should be chosen as 1. 
    This is also the default. 
  */

  void set_scale(double scale);

  /*
    Applies the finite differencing of the currently set derivative order
    to the input vector and overwrites it with the result. Selections are
    "x" and "y".
  */

  void differentiate(gsl_vector *input, string selection);

  /*
    As above but the input vector stays untouched.
  */

  void differentiate(gsl_vector *input, gsl_vector *output, string selection);

  /*
    As the two methods above but with a limited accuracy in return 
    for speed. The accuracy determines the maximum accuracy order 
    of derivatives taken on the grid. In the default case this number
    would be 8, here it can be set to 8, 6, 4 and 2.
  */

  void differentiate(gsl_vector *input, string selection, int acc_order);
  void differentiate(gsl_vector *input, gsl_vector *output, string selection, int acc_order);

  /*
    Performs consecutive derivatives on input. The selection format is:
    Each individual operation must me within #-symbols. The format
    of operations is #%<scaling>%<order>%<direction>%<action>%#. Scaling
    is a number with which the result is scaled. Order is the derivative
    order, direction is the spatial direction of the derivative. Action
    describes the relation to the results before, e.g. to be added, subtracted
    or multiplied. Multiplied means consecutive derivatives on same object. 
    An example would be the shears in lensing:
    gamma[0] = #%0.5%second%x%add%#%0.5%second%y%subtract%#
    gamma[1] = #1.0%first%x%add%#1.0%first%y%multiply%#
  */

  void derivative_parser(gsl_vector *input, gsl_vector *output, string selection);


};



#endif    /*!FINDIF_FAST_H*/
