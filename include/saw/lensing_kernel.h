#ifndef    LENSING_KERN_H
#define    LENSING_KERN_H


/***
    This class represents a typical 2D lensing kernel, describing the
    relation between convergence and shear in Fourier space. 

    Julian Merten
    JPL/Caltech
    jmerten@caltech.edu
    Dec. 2011

***/


#include <fftw3.h>
#include <gsl/gsl_matrix.h>
#include <cmath>
#include <saw/rw_fits.h>

class lensing_kernel
{

 protected:

  /*
    Represents the basic frequency grid on which the kernel is defined.
  */

  //The frequncy order follows the typical in-order scheme used by e.g. 
  //FFTW3. For an example see Chapter 12 of Numerical Recipes. 
 
  gsl_matrix *real_base_grid;
  gsl_matrix *imaginary_base_grid;

 public:

  /*
    Standard constructor. Needs x and y dimension of the grid.
  */

  lensing_kernel(int x, int y);

  /*
    Standard destrcutor.
  */
  ~lensing_kernel();

  /*
    Return x_dimension of the underlying map.
  */

  int show_x();

  /*
    Return y_dimension of the underlying map.
  */

  int show_y();

  /*
    Multiply this kernel with another grid-based map. Set inversion if
    you wanna reverse the process e.g. by inputing shear and obtaining
    convergence.
  */

  void convolve(gsl_matrix *in_re, gsl_matrix *in_im, gsl_matrix *out_re, gsl_matrix *out_im, bool inversion = 0);

  /*
    Writes the two kernel components into a FITS files. If the real space
    flag is set it writes the Fourier transform of the kernel.
  */

  void visualise(string filename, bool real_space = 0);

};

  

#endif  /*!LENSING_KERN_H!*/




