/***
    Julian Merten
    JPL / Caltech
    November 2011
    jmerten@caltech.edu

    This simple header provides basic FITS binary table I/O routines.
***/

#ifndef   	RW_FITS_H_
# define   	RW_FITS_H_


#include <CCfits/CCfits>
#include <valarray>
#include <stdexcept>
#include <gsl/gsl_matrix.h>
#include <iostream>
#include <stdexcept>
#include <cstring>
#include <fitsio.h>
#include <string>
#include <vector>
#include <saw/util.h>

using namespace std;

/**
   Routines which handle the complete read/write tasks for FITS-files.
**/


void write_pimg(string filename, gsl_matrix *data);
/* 
   Writes a double-valued gsl_matrix data to the primary image of a FITS file
   called filename.
*/

void write_pimgint(string filename, gsl_matrix_int *data);
/* 
   Writes a integer-valued gsl_matrix data to the primary image of a FITS file
   called filename.
*/
void write_pimgfloat(string filename, gsl_matrix_float *data);
/* 
   Writes a float-valued gsl_matrix data to the primary image of a FITS file
   called filename.
*/

void write_pimg(string filename, int x_dim, int y_dim, gsl_vector *data);
/* 
   Writes a double-valued gsl_vector data to the primary image of a FITS file
   called filename. The dimensions of this image are defined by x_dim and
   y_dim, so the vector should have length  (x_dim*y_dim).
*/

void write_pimg(string filename, int x_dim, int y_dim, std::valarray<double> data);
/*
  Same to write valarrays directly.
*/

void write_pimgint(string filename, int x_dim, int y_dim, gsl_vector_int *data);
/* 
   Writes a integer-valued gsl_vector data to the primary image of a FITS file
   called filename. The dimensions of this image are defined by x_dim and
   y_dim, so the vector should have length  (x_dim*y_dim).
*/
void write_pimgint(string filename, int x_dim, int y_dim, std::valarray<int> data);
/*
  Same to write valarrays directly.
*/
void write_pimgfloat(string filename, int x_dim, int y_dim, gsl_vector_float *data);
/* 
   Writes a float-valued gsl_vector data to the primary image of a FITS file
   called filename. The dimensions of this image are defined by x_dim and
   y_dim, so the vector should have length  (x_dim*y_dim).
*/
void write_pimgfloat(string filename, int x_dim, int y_dim, std::valarray<float> data);
/*
  Same to write valarrays directly.
*/

void write_imge(string filename, string extname, gsl_matrix *data);
/* 
   Writes a double-valued gsl_matrix data to the image extension extname
   of a FITS file called filename.
*/


void write_imgeint(string filename, string extname, gsl_matrix_int *data);
/* 
   Writes a integer-valued gsl_matrix data to the image extension extname
   of a FITS file called filename.
*/

void write_imgefloat(string filename, string extname, gsl_matrix_float *data);
/* 
   Writes a float-valued gsl_matrix data to the image extension extname
   of a FITS file called filename.
*/

void write_imge(string filename, string extname, int x_dim, int y_dim, gsl_vector *data);
/* 
   Writes a double-valued gsl_vector data to an image extension extname 
   of a FITS file called filename. The dimensions of this image are defined 
   by x_dim and y_dim, so the vector should have length  (x_dim*y_dim).
*/

void write_imge(string filename, string extname, int x_dim, int y_dim, std::valarray<double> data);
/*
  Same for valarrays.
*/

void write_imgeint(string filename, string extname, int x_dim, int y_dim, gsl_vector_int *data);
/* 
   Writes a integer-valued gsl_vector data to an image extension extname 
   of a FITS file called filename. The dimensions of this image are defined 
   by x_dim and y_dim, so the vector should have length  (x_dim*y_dim).
*/

void write_imgeint(string filename, string extname, int x_dim, int y_dim, std::valarray<int> data);
/*
  Same for valarrays.
*/

void write_imgefloat(string filename, string extname, int x_dim, int y_dim, gsl_vector_float *data);
/* 
   Writes a float-valued gsl_vector data to an image extension extname 
   of a FITS file called filename. The dimensions of this image are defined 
   by x_dim and y_dim, so the vector should have length  (x_dim*y_dim).
*/

void write_imgefloat(string filename, string extname, int x_dim, int y_dim, std::valarray<float> data);
/*
  Same for valarrays.
*/

void read_pimg(string filename, gsl_matrix *data);
/*
  Reads a double-valued primary image of a FITS file filename into an 
  extenal gsl_matrix data.
*/

void read_pimgint(string filename, gsl_matrix_int *data);
/*
  Reads an integer-valued primary image of a FITS file filename into an 
  extenal gsl_matrix data.
*/
void read_pimgfloat(string filename, gsl_matrix_float *data);
/*
  Reads an float-valued primary image of a FITS file filename into an 
  extenal gsl_matrix data.
*/

void read_pimg(string filename, gsl_vector *data);
/*
  Reads a double-valued primary image of a FITS file filename into an 
  extenal gsl_vector data.
*/

void read_pimgint(string filename, gsl_vector_int *data);
/*
  Reads a integer-valued primary image of a FITS file filename into an 
  extenal gsl_vector data.
*/
void read_pimgfloat(string filename, gsl_vector_float *data);
/*
  Reads a float-valued primary image of a FITS file filename into an 
  extenal gsl_vector data.
*/

void read_imge(string filename, string extname, gsl_matrix *data);
/*
  Reads a double-valued image extension extname of a FITS file filename
  into an extenal gsl_matrix data.
*/

void read_imgeint(string filename, string extname, gsl_matrix_int *data);
/*
  Reads a integer-valued image extension extname of a FITS file filename
  into an extenal gsl_matrix data.
*/
void read_imgefloat(string filename, string extname, gsl_matrix_float *data);
/*
  Reads a float-valued image extension extname of a FITS file filename
  into an extenal gsl_matrix data.
*/

void read_imge(string filename, string extname, gsl_vector *data);
/*
  Reads a double-valued image extension extname of a FITS file filename
  into an extenal gsl_vector data.
*/
void read_imgeint(string filename, string extname, gsl_vector_int *data);
/*
  Reads a integer-valued image extension extname of a FITS file filename
  into an extenal gsl_vector data.
*/
void read_imgefloat(string filename, string extname, gsl_vector_float *data);
/*
  Reads a float-valued image extension extname of a FITS file filename
  into an extenal gsl_vector data.
*/

void write_header(string filename, string name, double value, string description);
/*
  Writes a double-valued header with its name to the pHDU of a FITS file 
  filename with a description.
*/

void write_header(string filename, string name, int value, string description);
/*
  Writes a integer-valued header with its name to the pHDU of a FITS file 
  filename with a description.
*/
void write_header(string filename, string name, float value, string description);
/*
  Writes a float-valued header with its name to the pHDU of a FITS file 
  filename with a description.
*/
void write_header(string filename, string name, string value, string description);
/*
  Writes a string-valued header with its name to the pHDU of a FITS file 
  filename with a description.
*/

void write_header_ext(string filename,string extenion, string name, double value, string description);
/*
  Writes a double-valued header with its name to the pHDU of a FITS file 
  filename with a description.
*/

void write_header_ext(string filename,string extenion, string name, int value, string description);
/*
  Writes a integer-valued header with its name to the pHDU of a FITS file 
  filename with a description.
*/
void write_header_ext(string filename,string extenion, string name, float value, string description);
/*
  Writes a float-valued header with its name to the pHDU of a FITS file 
  filename with a description.
*/
void write_header_ext(string filename,string extenion, string name, string value, string description);
/*
  Writes a string-valued header with its name to the pHDU of a FITS file 
  filename with a description.
*/

double read_doubleheader(string filename,string name);
/* 
   Reads a double-valued header name from the pHDU of FITS file filename.
*/
double read_doubleheaderext(string filename, string extension, string name);
/*
  Reads a double-valued header name from en extension of FITS file filename.
*/

int read_intheader(string filename,string name);
/* 
   Reads a integer-valued header name from the pHDU of FITS file filename.
*/
int read_intheaderext(string filename, string extension, string name);
/*
  Reads a int-valued header name from en extension of FITS file filename.
*/
int read_intheaderext(string filename, string name);
/*
  Reads a int-valued header name from the first extension  of FITS file filename.
*/
float read_floatheader(string filename,string name);
/* 
   Reads a float-valued header name from the pHDU of FITS file filename.
*/
float read_floatheaderext(string filename, string extension, string name);
/*
  Reads a float-valued header name from en extension of FITS file filename.
*/

string read_stringheader(string filename,string name);
/* 
   Reads a string-valued header name from the pHDU of FITS file filename.
*/
string read_stringheaderext(string filename, string extension, string name);
/*
  Reads a string-valued header name from en extension of FITS file filename.
*/

void check_table_dims(string filename, string extension, long *rows, int *cols);
 
/*
  This routine reads the ith column (+1 offset due to FITS convention
  of  a float binary table with extension name extension from a 
  FITS file filename to a gsl vector output. 
  Conversion from float to double is done. 
*/

void read_float_tablecolumn(string filename, string extension, int i_col, gsl_vector_float *output);

/*
  Reads a double valued header keyword from a table extension of a FITS file
  filename. 
*/

double read_double_table_header(string filename, string extension, string keyword);

/*
  Same as above for integer.
*/

int read_int_table_header(string filename, string extension, string keyword);

/* 
  Add a WCS system to a FITS primary image. Selections are linear or J2000.
*/

void add_WCS(string filename, vector<double> WCS, string selection);

/* 
  Add a WCS system to a FITS extension image. Selections are linear or J2000.
*/

void add_WCS(string filename, string extension, vector<double> WCS, string selection);

/*
  Writes a 3D FITS datacube image into filename. You have to give the x,y
  and z dimensions which must match with the total length of the data vector.
*/

void write_3D_datacube(string filename, int x, int y, int z, gsl_vector *data); 

/*
  As above but writing a 3D datacube extension.
*/

void write_rgb(string filename, gsl_matrix *r, gsl_matrix *g, gsl_matrix *b);

/*
  Write a false-colour image into a single FITS file, following the 
  RGB FITS standard. 
*/

void read_rgb(string filename, gsl_matrix *r, gsl_matrix *g, gsl_matrix *b);

/*
  Reads and RGB FITS image.
*/ 



#endif 	    /* !RW_FITS_H_ */
