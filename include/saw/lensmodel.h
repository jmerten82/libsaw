/***
lensmodel.h: header file of the libsaw library
Author: Julian Merten
Institution: ITA Heidelberg
Mail: jmerten@ita.uni-heidelberg.de
***/

#ifndef   	LENS_MODEL_H_
# define   	LENS_MODEL_H_

#include <iostream>
#include <cmath>
#include <vector>
#include <stdexcept>
#include <saw/rw_fits.h>
#include <saw/findif.h>
#include <astro/cosmology.h>

using namespace std;

/**
   This class represents a typical lens model as it is derived from a 
   set of finite differences operators and a given lensing potential.
   It performs all necessary operations to derive lensing quantities 
   and is able to write them into a FITS file. Also WCS coordinate 
   information can be provided.
**/

class LensModel
{

 protected:

  FinDifGrid *lensgrid;
  /*
    The underlying finite differences grid, which basically defines 
    all numerical dimensions of the lens in pixel space and provides
    the necesssary differtiation operators. 
  */

  gsl_vector *pot;
  /*
    The lensing potential of the lens from which all other quantities
    will be derived. Dimension have to match the finite differencing grid.
  */

  gsl_vector *a1;
  /*
    The first component of the deflection angle: (\nabla \psi)_1.
  */

  gsl_vector *a2;
  /*
    The second component of the deflection angle: (\nabla \psi)_2.
  */

  gsl_vector *c;
  /*
    The convergence of the lens. 2 \laplace \psi.
  */

  gsl_vector *s1;
  /*
    The first component of the shear. 2 (dx^2 - dy^2)\psi.
  */

  gsl_vector *s2;
  /*
    The second component of the shear. (dxdy)\psi.
  */

  gsl_vector *g1;
  /*
    The first component of the reduced shear s1 / (1-c)
    of the lens.
  */

  gsl_vector *g2;
  /*
    The second component of the reduced shear s2 / (1-c)
    of the lens.
  */

  gsl_vector *jacdet;
  /*
    The Jacobian determinant of the lens system:
    (1-c)^2 - (s1^2+s2^2)
    Be careful because of the redshift dependence.
  */

  gsl_vector *mu;
  /*
    The magnification of the lens system.
    1/jacdet
    Be careful because of the redshift dependence. 
  */

  gsl_vector *F1;
  /*
    The first component of the F flexion.
  */

  gsl_vector *F2;
  /*
    The second component of the F flexion.
  */

  gsl_vector *G1;
  /*
    The first component of the G flexion.
  */

  gsl_vector *G2;
  /*
    The second component of the G flexion.
  */

  vector<bool> allocationtable;
  /*
    For memory allocation book keeping.
  */

  bool coordinates;
  /*
    Little flag stating if the lens field has a coordinate system attached.
  */

  bool J2000;
  /*
    Another flag indicating of the coordinate system is J2000.
  */ 

  vector<double> origin;
  /*
    Vector of length 4 which saves the pixel coordinates (first two
    components) and physical coordinates of the origin of the WCS.
  */

  vector<double> scale;
  /*
    Vector of length 4 which saves the elements of the propagation
    matrix of the coordinate system.
  */


 public:

  LensModel(FinDifGrid*);
  /*
    A very basic constructor which just defines a basic lens field,
    allocates memory and where the lensing potential is given manually
    later on.
  */

  LensModel(FinDifGrid*, gsl_vector *potin);
  /*
    Another constructor already reading in the lensing potential and
    constructing all other quantities. This may take some runtime on
    large fields.
  */

  LensModel(FinDifGrid*, vector<double> originin, vector<double> scalein, bool J2000flag);
  /*
    More complex constructor which also constructs a WCS around the lens.
    Like above, no potential needed. Originin is a vector of length 4, where
    the first two entires define the pixel position of the origin 
    and the last two the according physical position. 
    Scalein defines WCS and the vector
    can have length 4 which complete defines the trafo matrix m11, m12, m21
    m22. Length 2 is a non-rotated system for x and y. Length 1 assumes
    x-scale = y-scale where when J2000flag is set m11 is set to -scale.
  */

  LensModel(FinDifGrid*, gsl_vector *potin, vector<double> originin, vector<double> scalein, bool J2000flag);
  /*
    More complex constructor which also constructs a WCS around the lens.
    This version takes a potential already.
  */

  ~LensModel();
  /*
    Standard destructor. Frees memory.
  */

  int show_int(string selection);
  /*
    Returns some of the interesting integer quantities of the field.
    Selections are: x_dim, y_dim, numpix (non-masked pixels)
  */

  double show_double(string selection);
  /*
    Returns some of the interesting double quantities of the field.
    Selections are: pixel_origin_x/y, physical_origin_x/y, 
    m11, m12, m21, m22.
  */

  void build_lens_pot();
  /*
    Constructs all derived quantities from the potential. Also if
    no potential was given.
  */

  void build_lens_nopot();
  /*
    Derives quantities which are constructed from other basic quantities.
    To be precise, g1/2, jacdet and mu.
  */

  void rescale(gsl_matrix *zmap, double lens_z, double target_z);
  /*
    Rescales all quantities of the lens at redshift lens_z
    to a given uniform redshift target_z. The original redshift of each 
    pixel has to be given by the zmap. 
    Assuming a standard LCDM comoslogy 0.3,0.7,-1.0
  */

  gsl_vector* data(string selection);
  /*
    Direct pointer to the data vectors of the lens.
    Selections are: pot, a1, a2, c,...
  */

  void read_vector(string selection, gsl_vector *input);
  /*
    Reads an external vector directly into the data of the lens.
    Selections are as above. Dimensions must fit the number of
    pixels in the underlying findif grid.
  */

  void read_map(string selection, gsl_matrix *input);
  /*
    Reads an external map (matrix) directly into the data of the lens.
    Selections are as above. Dimensions must fit the x -and y-dimension
    of the underlying findif grid.
  */

  void write_vector(string selection, gsl_vector *output);
  /*
    Writes the data of the lens directly into an external vector.
    Selections are as above. Dimensions must fit the number of
    pixels in the underlying findif grid.
  */

  void write_map(string selection, gsl_matrix *output);
  /*
    Writes the data of the lens directly into an external map (matrix).
    Selections are as above. Dimensions must fit the x -and y-dimension
    of the underlying findif grid.
  */

  void write_fits(string filename, int verboselevel);
  /*
    Writes the data of the lens to a FITS file with filename.
    Selctions for verboselevel are: 0 if just potential, 1 if
    also first order lensing quantities, 2 if also second order
    lensing quantities and 3 if also third order lensing quantities
    should be written to the file.
  */ 

};

#endif 	    /* !LENS_MODEL_H_ */
