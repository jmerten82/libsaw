/***
    Template Selection 1.0
    Julian Merten
    JPL/Caltech 2013
    jmerten@caltech.edu

    This little tool allows to cut an area out of an existing
    SaWLens reconstruction and construct a template that can be 
    used in a following SaWLens reconstruction. 
***/

#include <iostream>
#include <vector>
#include <gsl/gsl_matrix.h>
#include <saw/rw_fits.h>
#include <tclap/CmdLine.h>

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Template Selection", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The parent SaWLens reconstruction.",true,"./","string",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The filename of the output template.",true,"./","string",cmd);
  TCLAP::MultiArg<int> XArg("x","Xcuts","The two 0-offset pixel coordinates in the x dimension which define the cut out region.",true,"int",cmd);
  TCLAP::MultiArg<int> YArg("y","Ycuts","The two 0-offset pixel coordinates in the y dimension which define the cut out region.",true,"int",cmd);
  cmd.parse( argc, argv );

  string inputfile = inputArg.getValue();
  string outputfile = outputArg.getValue();
  vector<int> xcuts = XArg.getValue();
  vector<int> ycuts = YArg.getValue();

  gsl_matrix *inputPot, *inputConv, *inputShear1, *inputShear2,  *inputF1, *inputF2,  *inputG1, *inputG2, *inputjac;
  gsl_matrix *outputPot, *outputConv, *outputShear1, *outputShear2, *outputF1, *outputF2, *outputG1, *outputG2, *outputjac;

  gsl_matrix_int *inputmask, *outputmask;

  int x_dim = read_intheader(inputfile,"NAXIS1");
  int y_dim =  read_intheader(inputfile,"NAXIS2");
  int x_dim_out = xcuts[1] - xcuts[0];
  int y_dim_out = ycuts[1] - ycuts[0];

  inputPot = gsl_matrix_calloc(y_dim,x_dim);
  inputConv = gsl_matrix_calloc(y_dim,x_dim);
  inputShear1 = gsl_matrix_calloc(y_dim,x_dim);
  inputShear2 = gsl_matrix_calloc(y_dim,x_dim);
  inputF1 = gsl_matrix_calloc(y_dim,x_dim);
  inputF2 = gsl_matrix_calloc(y_dim,x_dim);
  inputG1 = gsl_matrix_calloc(y_dim,x_dim);
  inputG2 = gsl_matrix_calloc(y_dim,x_dim);
  inputjac = gsl_matrix_calloc(y_dim,x_dim);

  outputPot = gsl_matrix_calloc(y_dim_out,x_dim_out);
  outputConv = gsl_matrix_calloc(y_dim_out,x_dim_out);
  outputShear1 = gsl_matrix_calloc(y_dim_out,x_dim_out);
  outputShear2 = gsl_matrix_calloc(y_dim_out,x_dim_out);
  outputF1 = gsl_matrix_calloc(y_dim_out,x_dim_out);
  outputF2 = gsl_matrix_calloc(y_dim_out,x_dim_out);
  outputG1 = gsl_matrix_calloc(y_dim_out,x_dim_out);
  outputG2 = gsl_matrix_calloc(y_dim_out,x_dim_out);
  outputjac = gsl_matrix_calloc(y_dim_out,x_dim_out);

  inputmask = gsl_matrix_int_calloc(y_dim,x_dim);
  outputmask = gsl_matrix_int_calloc(y_dim_out,x_dim_out);

  read_pimg(inputfile,inputPot);
  read_imge(inputfile,"convergence",inputConv);
  read_imge(inputfile,"shear1",inputShear1);
  read_imge(inputfile,"shear2",inputShear2);
  read_imge(inputfile,"f1",inputF1);
  read_imge(inputfile,"f2",inputF2);
  read_imge(inputfile,"g1",inputG1);
  read_imge(inputfile,"g2",inputG2);
  read_imge(inputfile,"jacdet",inputjac);
  read_imgeint(inputfile,"field_mask",inputmask);

  int xcounter, ycounter;

  ycounter = 0;
  for(int i = 0; i < y_dim; i++)
    {
      if(i >= ycuts[0] && i < ycuts[1])
	{
	  xcounter = 0;
	  for(int j = 0; j < x_dim; j++)
	    {
	      if(j >= xcuts[0] && j < xcuts[1])
		{
		  gsl_matrix_set(outputPot,ycounter,xcounter,gsl_matrix_get(inputPot,i,j));
		  gsl_matrix_set(outputConv,ycounter,xcounter,gsl_matrix_get(inputConv,i,j));
		  gsl_matrix_set(outputShear1,ycounter,xcounter,gsl_matrix_get(inputShear1,i,j));
		  gsl_matrix_set(outputShear2,ycounter,xcounter,gsl_matrix_get(inputShear2,i,j));
		  gsl_matrix_set(outputF1,ycounter,xcounter,gsl_matrix_get(inputF1,i,j));
		  gsl_matrix_set(outputF2,ycounter,xcounter,gsl_matrix_get(inputF2,i,j));
		  gsl_matrix_set(outputG1,ycounter,xcounter,gsl_matrix_get(inputG1,i,j));
		  gsl_matrix_set(outputG2,ycounter,xcounter,gsl_matrix_get(inputG2,i,j));
		  gsl_matrix_set(outputjac,ycounter,xcounter,gsl_matrix_get(inputjac,i,j));
		  gsl_matrix_int_set(outputmask,ycounter,xcounter,gsl_matrix_int_get(inputmask,i,j));
		  xcounter++;
		}
	    }
	  ycounter++;
	}
    }

  write_pimg(outputfile,outputPot);
  write_imge(outputfile,"convergence",outputConv);
  write_imge(outputfile,"shear1",outputShear1);
  write_imge(outputfile,"shear2",outputShear2);
  write_imge(outputfile,"f1",outputF1);
  write_imge(outputfile,"f2",outputF2);
  write_imge(outputfile,"g1",outputG1);
  write_imge(outputfile,"g2",outputG2);
  write_imge(outputfile,"jacdet",outputjac);
  write_imgeint(outputfile,"field_mask",outputmask);
  
  return 0;
}
