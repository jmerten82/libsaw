/***
    g ellipses 1.0
    Creates ds9 region files from a given ASCII shear catalogue
    These regions are ellipses, representing the measured shapes
    in the catalogue

    Julian Merten
    University of Oxford 
    julian.merten@physics.ox.ac.uk
    March 2015

    This routine is based on a small Python script by
    Peter Melchior. 
***/

#include <vector>
#include <stdexcept>
#include <cmath>
#include <tclap/CmdLine.h>
#include <fstream>
#include <saw/cat_reader.h>
#include <saw/cat_writer.h>

using namespace std;


int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("g_ellipses", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The input ASCII file.",true,"dummy","string",cmd);
  TCLAP::MultiArg<int> posArg("p","position","The two position columns for the objects in the catalogue.",true,"int",cmd);
  TCLAP::MultiArg<int> functionArg("f","function","The two functional  columns for g1 and g2.",true,"int",cmd);
  TCLAP::SwitchArg j2000Arg("j","j2000","Indicates that positions are J2000 WCS. The region output will be adjusted accordingly.",cmd);
  TCLAP::ValueArg<double> scaleArg("s","scale","An additional factor by which the sizes of the objects are to be scaled.",false,1.0,"double",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output DS9 region file.",true,"dummy","string",cmd);
  cmd.parse( argc, argv );

  //Setting the stage

  string infile = inputArg.getValue();
  vector<int> pos = posArg.getValue();
  vector<int> function = functionArg.getValue();
  bool J2000 = j2000Arg.getValue();
  double scale = scaleArg.getValue();
  string outfile = outputArg.getValue();

  if(pos.size() < 2)
    {
      throw invalid_argument("Two few columns for source position given.");
    }
  if(function.size() < 2)
    {
      throw invalid_argument("Two few columns for functional information given.");
    }

  //Reading the input catalog

  CatReader reader1(infile);

  vector<double> ellip, theta;
  double g1, g2, size;

  double pi = acos(-1.);
  for(int i = 0; i < reader1.show_rowdim(); i++)
    {
      g1 = gsl_matrix_get(reader1.show_data(),i,function[0]);
      g2 = gsl_matrix_get(reader1.show_data(),i,function[1]);
      ellip.push_back(sqrt(g1*g1+g2*g2)*scale);
      theta.push_back(180.*0.5*atan2(g2,g1)/pi);
    }

  CatWriter writer1(&reader1);

  string selection = "image";
  if(J2000)
    {
      selection = "J2000";
    }

  writer1.write_DS9_sticks(outfile, pos[0], pos[1], &ellip, &theta, selection);

  return 0;
}
