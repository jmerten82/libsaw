/***
    SaWCombine 0.1
    Part of the CLASH pipeline. Combine multi-resolution lensing maps from SaWLens 
    Julian Merten
    JPL/Caltech 
    August 2013
***/

#include <iostream>
#include <vector>
#include <stdexcept>
#include <tclap/CmdLine.h>
#include <gsl/gsl_matrix.h>
#include <saw/rw_fits.h>
#include <saw/interpol.h>
#include <astro/cosmology.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("SaWCombine", ' ', "0.1");
  TCLAP::ValueArg<std::string> lowresArg("l","lowres","The low resolution input map as a FITS file with 'convergence' extension.",true," ","string",cmd);
  TCLAP::ValueArg<int> interpolationArg("i","interpol","The target pixel resolution the map needs to be inteprolated to.",false,0,"int",cmd);
  TCLAP::MultiArg<int> finalArg("f","factor","The factor by which the lowres map (first argument) and the highres map (second argument) needs to be over-sampled.",true,"int",cmd);
  TCLAP::ValueArg<double> zArg("z","redshift","The target redshift the input lowres map needs to be scaled to.",true,20000.0,"double",cmd);
  TCLAP::ValueArg<int> xArg("x","x_insert","The x-insertion point for the highres map.",true,180,"int",cmd);
  TCLAP::ValueArg<int> yArg("y","y_insert","The y-insertion point for the highres map.",true,180,"int",cmd);
  TCLAP::ValueArg<std::string> highresArg("r","highres","The FITS file containing the highres field stack.",true," ","string",cmd);
  TCLAP::MultiArg<double> wcsArg("w","wcs","Offers the possibility to add a WCS system to your final output maps. Expects 8 numbers, the pixel centre, the WCS centre and the 4 matrix elements. If you give only six elements it assumes M12 and M21 to be 0.",false,"int",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output FITS file.",true,"./output.fits","string",cmd);
  TCLAP::SwitchArg specialArg("s","special","Toggle this if the highres map is coming from a medres map instead of a highres map.",cmd);

  cmd.parse( argc, argv );

  //Reading initial map

  string lowresfile = lowresArg.getValue();
  int x_lowres = read_intheader(lowresfile,"NAXIS1");
  int y_lowres = read_intheader(lowresfile,"NAXIS2");
  gsl_matrix *lowres_in = gsl_matrix_calloc(y_lowres,x_lowres);
  read_imge(lowresfile,"convergence",lowres_in);

  //Checking if we need to interpolate
  int target_pixels = interpolationArg.getValue();

  gsl_matrix *lowres_inter;

  if(target_pixels != 0)
    {
      lowres_inter = gsl_matrix_calloc(target_pixels,target_pixels);
      cspline_mapinterpol_smooth(lowres_in,lowres_inter);
    }
  else
    {
      lowres_inter = gsl_matrix_calloc(y_lowres,x_lowres);
      gsl_matrix_memcpy(lowres_inter,lowres_in);
    }

  //Over-sampling the input map
  vector<int> oversample = finalArg.getValue();
  gsl_matrix *lowres = gsl_matrix_calloc(lowres_inter->size1*oversample[0],lowres_inter->size2*oversample[0]);
  gsl_matrix *control = gsl_matrix_calloc(lowres_inter->size1*oversample[0],lowres_inter->size2*oversample[0]);

  for(int i = 0; i < lowres_inter->size2; i++)
    {
      for(int j = 0; j < lowres_inter->size1; j++)
	{
	  for(int l = 0; l < oversample[0]; l++)
	    {
	      for(int k = 0; k < oversample[0]; k++)
		{
		  gsl_matrix_set(lowres,i*oversample[0]+l,j*oversample[0]+k,gsl_matrix_get(lowres_inter,i,j));
		}
	    }
	}
    }

  //Rescaling map to target redshift
  double clusterz_low = read_doubleheader(lowresfile,"CLUSTERZ");
  double mapz_low = read_doubleheader(lowresfile,"SOURCEZ");
  double low_mapz_new = zArg.getValue();
  astro::cosmology cosmo1(0.27,0.73,0.7,-1.0,0.04);
  double scaling = cosmo1.angularDist(clusterz_low,low_mapz_new) / cosmo1.angularDist(0.0,low_mapz_new) * cosmo1.angularDist(0.0,mapz_low) / cosmo1.angularDist(clusterz_low,mapz_low);
  gsl_matrix_scale(lowres,scaling);

  //Reading the highres input field stack

  string highresfile = highresArg.getValue();
  int x_highres = read_intheader(highresfile,"NAXIS1");
  int y_highres = read_intheader(highresfile,"NAXIS2");
  gsl_matrix *highres_in = gsl_matrix_calloc(y_highres,x_highres);
  gsl_matrix *control_in = gsl_matrix_calloc(y_highres,x_highres);
  read_pimg(highresfile,highres_in);
  read_imge(highresfile,"conv_sd",control_in);

  //Oversampling the highres map
  gsl_matrix *highres = gsl_matrix_calloc(y_highres*oversample[1],x_highres*oversample[1]);
  gsl_matrix *control_high = gsl_matrix_calloc(y_highres*oversample[1],x_highres*oversample[1]);

  for(int i = 0; i < highres_in->size2; i++)
    {
      for(int j = 0; j < highres_in->size1; j++)
	{
	  for(int l = 0; l < oversample[1]; l++)
	    {
	      for(int k = 0; k < oversample[1]; k++)
		{
		  gsl_matrix_set(highres,i*oversample[1]+l,j*oversample[1]+k,gsl_matrix_get(highres_in,i,j));
		  gsl_matrix_set(control_high,i*oversample[1]+l,j*oversample[1]+k,gsl_matrix_get(control_in,i,j));
		}
	    }
	}
    }

  //Finally merging the two maps into the final map

  int x_cut = xArg.getValue();
  int y_cut = yArg.getValue();

  double value,value2;
  for(int i = 0; i < highres->size2; i++)
    {
      for(int j = 0; j < highres->size1; j++)
	{
	  value = gsl_matrix_get(highres,i,j);
	  value2 = gsl_matrix_get(control_high,i,j);
	  
	  if(i < 1*oversample[1] || (highres->size2 - i) < 1*oversample[1] || j < 1*oversample[1] || (highres->size1 - j) < 1*oversample[1])
	    {
	      value = (3.*gsl_matrix_get(lowres,y_cut+i,x_cut+j)+value)/4.;
	    }
	  else if(i < 2*oversample[1] || (highres->size2 - i) < 2*oversample[1] || j < 2*oversample[1] || (highres->size1 - j) < 2*oversample[1])
	    {
	      value = (gsl_matrix_get(lowres,y_cut+i,x_cut+j)+value)/2.;
	    }
	  else if(i < 3*oversample[1] || (highres->size2 - i) < 3*oversample[1] || j < 3*oversample[1] || (highres->size1 - j) < 3*oversample[1])
	    {
	      value = (gsl_matrix_get(lowres,y_cut+i,x_cut+j)+3.*value)/4.;
	    }
	  
	  gsl_matrix_set(lowres,y_cut+i,x_cut+j,value);
	  gsl_matrix_set(control,y_cut+i,x_cut+j,value2);
	}
    }

  //Writing the output
  string outputfile = outputArg.getValue();

  write_pimg(outputfile,lowres);
  write_imge(outputfile,"control",control);

  //Checking for WCS

  vector<double> WCS = wcsArg.getValue();

  if(WCS.size() > 0)
    {
      add_WCS(outputfile,WCS,"J2000");
      add_WCS(outputfile,"control",WCS,"J2000");
    }


  /*





  double clusterz_high = read_doubleheader(files[1],"Z_L");
  double clusterz_low = read_doubleheader(files[0],"CLUSTERZ");
  double mapz_high = read_doubleheader(files[1],"Z_S");
  double mapz_low = read_doubleheader(files[0],"SOURCEZ");

  if(abs(clusterz_low/clusterz_high-1.) > 0.01)
    {
      throw invalid_argument("SaWComb: The lens redshifts of your maps seem incompatible.");
    }

  if(abs(clusterz_low/clusterz_high-1.) != 0.0)
    {
      cout <<"SaWComb Warning: The lens redshifts of your input maps differ slightly." <<endl;
    }

  vector<int> xcuts = cutxArg.getValue();
  vector<int> ycuts = cutyArg.getValue();

  if(xcuts.size() < 1 || ycuts.size() < 1)
    {
      throw invalid_argument("SaWComb: Not enough cutting points defined.");
    }

  if((x_highres%(xcuts[1]-xcuts[0]+1) != 0) || (y_highres%(ycuts[1]-ycuts[0]+1) != 0))
    {
      throw invalid_argument("SaWComb: Cutting points are not fitting the highres map.");
    }

  int factor = x_highres/(xcuts[1]-xcuts[0]+1); 
  
  gsl_matrix *lowconv = gsl_matrix_calloc(y_lowres,x_lowres);
  gsl_matrix *lowshear1 = gsl_matrix_calloc(y_lowres,x_lowres);
  gsl_matrix *lowshear2 = gsl_matrix_calloc(y_lowres,x_lowres);

  gsl_matrix *highconv = gsl_matrix_calloc(y_highres,x_highres);
  gsl_matrix *highshear1 = gsl_matrix_calloc(y_highres,x_highres);
  gsl_matrix *highshear2 = gsl_matrix_calloc(y_highres,x_highres);

  read_imge(files[0],"convergence",lowconv);
  read_imge(files[0],"shear1",lowshear1);
  read_imge(files[0],"shear2",lowshear2);

  read_pimg(files[1],highconv);
  read_imge(files[1],"shear1",highshear1);
  read_imge(files[1],"shear2",highshear2);

  //Checking if we wanna rescale maps and if so, rescale them.

  vector<double> redshifts = zArg.getValue();

  if(redshifts.size() > 0)
    {

      astro::cosmology cosmo1(0.27,0.73,0.7,-1.0,0.04);
      double scaling = cosmo1.angularDist(clusterz_high,redshifts[0]) / cosmo1.angularDist(0.0,redshifts[0]) * cosmo1.angularDist(0.0,mapz_low) / cosmo1.angularDist(clusterz_high,mapz_low);
      gsl_matrix_scale(lowconv,scaling);
      gsl_matrix_scale(lowshear1,scaling);
      gsl_matrix_scale(lowshear2,scaling);
      mapz_low = redshifts[0];

      if(redshifts.size() > 1)
	{
	  double scaling = cosmo1.angularDist(clusterz_high,redshifts[1]) / cosmo1.angularDist(0.0,redshifts[1]) * cosmo1.angularDist(0.0,mapz_high) / cosmo1.angularDist(clusterz_high,mapz_high);
	  gsl_matrix_scale(highconv,scaling);
	  gsl_matrix_scale(highshear1,scaling);
	  gsl_matrix_scale(highshear2,scaling);
	  mapz_high = redshifts[1];
	}
    }

  if(abs(mapz_high/mapz_low - 1.) > 0.01)
    {
      throw invalid_argument("SaWComb: You are trying to combine maps which are incompatible in redshift.");
    }
  if(abs(mapz_high/mapz_low - 1.) != 0.0)
    {
      cout <<"SaWComb Warning: The redshift of your combined maps differ slghtly." <<endl;
    }

  //Putting the maps together

  gsl_matrix *outconv = gsl_matrix_calloc(factor*y_lowres,factor*x_lowres);
  gsl_matrix *outshear1 = gsl_matrix_calloc(factor*y_lowres,factor*x_lowres);
  gsl_matrix *outshear2 = gsl_matrix_calloc(factor*y_lowres,factor*x_lowres);
  gsl_matrix *outjac = gsl_matrix_calloc(factor*y_lowres,factor*x_lowres);
  gsl_matrix *outmag = gsl_matrix_calloc(factor*y_lowres,factor*x_lowres);

  double jac;
  for(int i = 0; i < y_lowres; i++)
    {
      for(int j = 0; j < x_lowres; j++)
	{
	  for(int l = 0; l < factor; l++)
	    {
	      for(int k = 0; k < factor; k++)
		{
		  jac = pow(1.-gsl_matrix_get(lowconv,i,j),2.) - (pow(gsl_matrix_get(lowshear1,i,j),2.)+pow(gsl_matrix_get(lowshear2,i,j),2.));
		  gsl_matrix_set(outconv,i*factor+l,j*factor+k,gsl_matrix_get(lowconv,i,j));
		  gsl_matrix_set(outshear1,i*factor+l,j*factor+k,gsl_matrix_get(lowshear1,i,j));
		  gsl_matrix_set(outshear2,i*factor+l,j*factor+k,gsl_matrix_get(lowshear2,i,j));
		  gsl_matrix_set(outjac,i*factor+l,j*factor+k,jac);
		  gsl_matrix_set(outmag,i*factor+l,j*factor+k,1./jac);
		}
	    }
	}
    }

  //Rescaling map to new redshift



  double correction = 1.0;
  double conv, shear1, shear2;
  for(int i = 0; i < y_highres; i++)
    {
      for(int j = 0; j < x_highres; j++)
	{
	  //This is still fairly crude...we should use some adaptive smoothing kernel here, instead. 
	  if(i < factor-1 || (y_highres - i) < factor || j < factor-1 || (x_highres - j) < factor)
	    {
	      conv = (3.*gsl_matrix_get(outconv,ycuts[0]*factor+i,xcuts[0]*factor+j)+gsl_matrix_get(highconv,i,j))/4.;
	      shear1 = (3.*gsl_matrix_get(outshear1,ycuts[0]*factor+i,xcuts[0]*factor+j)+gsl_matrix_get(highshear1,i,j))/4.;
	      shear2 = (3.*gsl_matrix_get(outshear2,ycuts[0]*factor+i,xcuts[0]*factor+j)+gsl_matrix_get(highshear2,i,j))/4.;
	    }
	  else if(i < 2*factor-1 || (y_highres - i) < 2*factor || j < 2*factor-1 || (x_highres - j) < 2*factor)
	    {
	      conv = (gsl_matrix_get(outconv,ycuts[0]*factor+i,xcuts[0]*factor+j)+gsl_matrix_get(highconv,i,j))/2.;
	      shear1 = (gsl_matrix_get(outshear1,ycuts[0]*factor+i,xcuts[0]*factor+j)+gsl_matrix_get(highshear1,i,j))/2.;
	      shear2 = (gsl_matrix_get(outshear2,ycuts[0]*factor+i,xcuts[0]*factor+j)+gsl_matrix_get(highshear2,i,j))/2.;
	    }
	  else
	    {
	      conv = gsl_matrix_get(highconv,i,j);
	      shear1 = gsl_matrix_get(highshear1,i,j);
	      shear2 = gsl_matrix_get(highshear2,i,j);
	    }

	  jac = pow(1.-correction*conv,2.) - (pow(correction*shear1,2.)+pow(correction*shear2,2.));
	  gsl_matrix_set(outconv,ycuts[0]*factor+i,xcuts[0]*factor+j,correction*conv);
	  gsl_matrix_set(outshear1,ycuts[0]*factor+i,xcuts[0]*factor+j,correction*shear1);
	  gsl_matrix_set(outshear2,ycuts[0]*factor+i,xcuts[0]*factor+j,correction*shear2);
	  gsl_matrix_set(outjac,ycuts[0]*factor+i,xcuts[0]*factor+j,jac);
	  gsl_matrix_set(outmag,ycuts[0]*factor+i,xcuts[0]*factor+j,1./jac);
	  
	}
    }

  vector<double> wcs = wcsArg.getValue();
  string outfile = outputArg.getValue();

 
  write_pimg(outfile,outconv);
  write_imge(outfile,"shear1",outshear1);
  write_imge(outfile,"shear2",outshear2);
  write_imge(outfile,"jacdet",outjac);
  write_imge(outfile,"magnification",outmag);

  write_header(outfile,"Z_L",clusterz_high,"Redshift of the lens.");
  write_header(outfile,"Z_S",mapz_high,"Redshift of the map.");
  write_header_ext(outfile,"shear1","Z_L",clusterz_high,"Redshift of the lens.");
  write_header_ext(outfile,"shear1","Z_S",mapz_high,"Redshift of the map.");
  write_header_ext(outfile,"shear2","Z_L",clusterz_high,"Redshift of the lens.");
  write_header_ext(outfile,"shear2","Z_S",mapz_high,"Redshift of the map.");
  write_header_ext(outfile,"jacdet","Z_L",clusterz_high,"Redshift of the lens.");
  write_header_ext(outfile,"jacdet","Z_S",mapz_high,"Redshift of the map.");
  write_header_ext(outfile,"magnification","Z_L",clusterz_high,"Redshift of the lens.");
  write_header_ext(outfile,"magnification","Z_S",mapz_high,"Redshift of the map.");

  if(wcs.size() > 0)
    {
      add_WCS(outfile,wcs,"J2000");
      add_WCS(outfile,"shear1",wcs,"J2000");
      add_WCS(outfile,"shear2",wcs,"J2000");
      add_WCS(outfile,"jacdet",wcs,"J2000");
      add_WCS(outfile,"magnification",wcs,"J2000");
    }
  */
  return 0;

}




					      


