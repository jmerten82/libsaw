/***
    Map combination 1.0
    Julian Merten
    JPL/Caltech 2012
    jmerten@caltech.edu

    This tool combines the bootstrap analyses on two different resolution
    into one unique map from which e.g. profiles can be measured etc.
***/

#include <iostream>
#include <vector>
#include <cmath>
#include <stdexcept>
#include <tclap/CmdLine.h>
#include <saw/rw_fits.h>
#include <saw/interpol.h>
#include <saw/util.h>

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Map Combination", ' ', "1.0");
  TCLAP::MultiArg<std::string> inputArg("i","input","The two fits files to be combined. Lower resolution is the first file, larger resolution the second.",true,"string",cmd);
  TCLAP::MultiArg<int> interArg("s","interpolation","Two arguments: The first is the target x dimension of the first map, the second of the second map.",false,"int",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output filename.",true,"./stack.fits","string",cmd);
  TCLAP::ValueArg<double> threshArg("t","threshold","Defines the value of signal to noise from which it is switched to the lowres map.",true,0.002,"double",cmd);
  cmd.parse( argc, argv );

  vector<std::string> input = inputArg.getValue();
  vector<int> new_dims = interArg.getValue();
  string outfilename = outputArg.getValue();
  double thresh = threshArg.getValue();

  bool interpolate = new_dims.size() > 0;
  bool interpolate_low, interpolate_high; 
  vector<int> old_dims;
  old_dims.resize(4);

  vector<double> FITS_header;

  //Determining inputdims
  int x_low_original, x_high_original, y_low_original, y_high_original;

  if(input.size() < 2)
    {
      throw invalid_argument("Not enough input arguments.");
    }

  old_dims[0] =  read_intheader(input[0],"NAXIS1");
  old_dims[2] =  read_intheader(input[0],"NAXIS2");
  old_dims[1] =  read_intheader(input[1],"NAXIS1");
  old_dims[3] =  read_intheader(input[1],"NAXIS2");

  //Reading some important info

  FITS_header.push_back(read_doubleheader(input[0],"NUM_BS"));
  FITS_header.push_back(read_doubleheader(input[1],"NUM_BS"));
  FITS_header.push_back(read_doubleheader(input[0],"Z_S"));
  FITS_header.push_back(read_doubleheader(input[1],"Z_S"));
  FITS_header.push_back(read_doubleheader(input[0],"Z_L"));
  FITS_header.push_back(read_doubleheader(input[1],"Z_L"));
  FITS_header.push_back(read_doubleheader(input[1],"S_SCALE"));
  FITS_header.push_back(thresh);

  if(FITS_header[2] != FITS_header[3] || FITS_header[4] != FITS_header[5])
    {
      throw invalid_argument("The maps you are trying to combine are not consistent.");
    }


  if(!interpolate)
    {
      new_dims.resize(4);
      new_dims = old_dims;
    }

  //Determining output dims

  if(new_dims[1]%new_dims[0] != 0)
    {
      throw invalid_argument("The chosen dimension do not allow for combination.");
    }
  if(interpolate)
    {
      double ratio_low = old_dims[0]/old_dims[2];
      double ratio_high = old_dims[1]/old_dims[3];
      new_dims.push_back(floor((double) new_dims[0] * ratio_low +0.5));
      new_dims.push_back(floor((double) new_dims[1] * ratio_high +0.5));
    }


  if(new_dims[3]%new_dims[2] != 0)
    {
      throw invalid_argument("The chosen dimension do not allow for combination.");
    }

  cout <<"Old low dims: " <<old_dims[0] <<" x " <<old_dims[2] <<endl;
  cout <<"Old high_dims: " <<old_dims[1] <<" x " <<old_dims[3] <<endl;

  cout <<"New low dims: " <<new_dims[0] <<" x " <<new_dims[2] <<endl;
  cout <<"New high_dims: " <<new_dims[1] <<" x " <<new_dims[3] <<endl;

  if(old_dims[0] == new_dims[0] && old_dims[2] == new_dims[2])
    {
      interpolate_low = false;
    }
  else
    {
      interpolate_low = true;
    }
  if(old_dims[1] == new_dims[1] && old_dims[3] == new_dims[3])
    {
      interpolate_high = false;
    }
  else
    {
      interpolate_high = true;
    }



  //Getting the masks for the interpolation
  gsl_matrix_int *low_mask_int;
  gsl_matrix_int *high_mask_int;
  gsl_matrix *low_mask;
  gsl_matrix *high_mask;
  gsl_matrix *low_mask_inter;
  gsl_matrix *high_mask_inter;

  if(interpolate)
    {
      low_mask_int = gsl_matrix_int_calloc(old_dims[2],old_dims[0]);
      high_mask_int = gsl_matrix_int_calloc(old_dims[3],old_dims[1]);
      low_mask = gsl_matrix_calloc(old_dims[2],old_dims[0]);
      high_mask = gsl_matrix_calloc(old_dims[3],old_dims[1]);
      low_mask_inter = gsl_matrix_calloc(new_dims[2],new_dims[0]);
      high_mask_inter = gsl_matrix_calloc(new_dims[3],new_dims[1]);

      for(int i = 0; i < old_dims[2]; i++)
	{
	  for(int j = 0; j < old_dims[0]; j++)
	    {
	      if(gsl_matrix_get(low_mask,i,j) != 1.0)
		{
		  gsl_matrix_set(low_mask,i,j,0.0);
		}
	    }
	}
      for(int i = 0; i < old_dims[3]; i++)
	{
	  for(int j = 0; j < old_dims[1]; j++)
	    {
	      if(gsl_matrix_get(high_mask,i,j) != 1.0)
		{
		  gsl_matrix_set(high_mask,i,j,0.0);
		}
	    }
	}
      
      read_imgeint(input[0],"field_mask",low_mask_int);
      read_imgeint(input[1],"field_mask",high_mask_int);
      read_imge(input[0],"field_mask",low_mask);
      read_imge(input[1],"field_mask",high_mask);
      cspline_mapinterpol_smooth(low_mask,low_mask_inter);
      cspline_mapinterpol_smooth(high_mask,high_mask_inter);

      for(int i = 0; i < old_dims[2]; i++)
	{
	  for(int j = 0; j < old_dims[0]; j++)
	    {
	      if(gsl_matrix_get(low_mask,i,j) > 0.6)
		{
		  gsl_matrix_set(low_mask,i,j,1.0);
		}
	      else
		{
		  gsl_matrix_set(low_mask,i,j,0.0);
		}
	    }
	}
      for(int i = 0; i < old_dims[3]; i++)
	{
	  for(int j = 0; j < old_dims[1]; j++)
	    {
	      if(gsl_matrix_get(high_mask,i,j) > 0.6)
		{
		  gsl_matrix_set(high_mask,i,j,1.0);
		}
	      else
		{
		  gsl_matrix_set(high_mask,i,j,0.0);
		}
	    }
	}
    }

  //Creating a main loop, going through all quantities

  vector<std::string> extensions;
  extensions.resize(20);
  extensions[0] = " ";
  extensions[1] = "conv_sd";
  extensions[2] = "conv_min";
  extensions[3] = "conv_max";
  extensions[4] = "shear1";
  extensions[5] = "shear1_sd";
  extensions[6] = "shear1_min";
  extensions[7] = "shear1_max";
  extensions[8] = "shear2";
  extensions[9] = "shear2_sd";
  extensions[10] = "shear2_min";
  extensions[11] = "shear2_max";
  extensions[12] = "jacdet";
  extensions[13] = "jacdet_sd";
  extensions[14] = "jacdet_min";
  extensions[15] = "jacdet_max";
  extensions[16] = "magnification";
  extensions[17] = "magnification_sd";
  extensions[18] = "magnification_min";
  extensions[19] = "magnification_max";

  gsl_matrix *low_reference = gsl_matrix_calloc(new_dims[3],new_dims[1]);
  gsl_matrix *high_reference = gsl_matrix_calloc(new_dims[3],new_dims[1]); 
  gsl_matrix *low_map_read = gsl_matrix_calloc(old_dims[2],old_dims[0]);
  gsl_matrix *low_map_inter = gsl_matrix_calloc(new_dims[2],new_dims[0]);
  gsl_matrix *high_map_read = gsl_matrix_calloc(old_dims[3],old_dims[1]);
  gsl_matrix *high_map_inter = gsl_matrix_calloc(new_dims[3],new_dims[1]);
  gsl_matrix *selected_map = gsl_matrix_calloc(new_dims[3],new_dims[1]);

  //Creating a reference...
  //Reading the maps of interest 

  cout <<"Creating reference..." <<flush;
  read_imge(input[0],"conv_sd",low_map_read);
  read_imge(input[1],"conv_sd",high_map_read);
  cout <<"Done" <<endl;

  if(interpolate_low)
    {

      cspline_smooth_morecleverinterpol(low_map_read,low_mask_int,low_map_inter);
      for(int l = 0; l < low_map_inter->size1; l++)
	{
	  for(int k = 0; k < low_map_inter->size2; k++)
	    {
	      if(gsl_matrix_get(low_mask_inter,l,k) == 0.0)
		{
		  gsl_matrix_set(low_map_inter,l,k,0.0);
		}
	    }
	}
    }
  else
    {
      gsl_matrix_memcpy(low_map_inter,low_map_read);
    }

  if(interpolate_high)
    {
      cspline_smooth_morecleverinterpol(high_map_read,high_mask_int,high_map_inter);

      for(int l = 0; l < high_map_inter->size1; l++)
	{
	  for(int k = 0; k < high_map_inter->size2; k++)
	    {
	      if(gsl_matrix_get(high_mask_inter,l,k) == 0.0)
		{
		  gsl_matrix_set(high_map_inter,l,k,0.0);
		}
	    }
	}
    }
  else
    {
      gsl_matrix_memcpy(high_map_inter,high_map_read);
    }

      //Binning up the low_res map to target resolution

      int up_scale = high_map_inter->size2 / low_map_inter->size2;

      for(int l = 0; l <  low_map_inter->size1; l++)
	{
	  for(int k = 0; k < low_map_inter->size2; k++)
	    {
	      for(int m = 0; m < up_scale; m++)
		{
		  for(int n = 0; n < up_scale; n++)
		    {
		      gsl_matrix_set(low_reference,l*up_scale+m,k*up_scale+n,gsl_matrix_get(low_map_inter,l,k));
		    }
		}
	    }
	}
      gsl_matrix_memcpy(high_reference,high_map_inter);

      //write_pimg("./debug.fits",high_map_inter);
      //write_imge("./debug.fits","lowres",low_map_inter);

      //Processing all different components

  for(int i = 0; i < extensions.size(); i++)
    {
      if(i == 0)
	{
	  cout <<"Processing convergence..." <<flush;
	}
      else if(i == 3)
	{
	  cout <<"Processing shear1..." <<flush;
	}
      else if(i == 6)
	{
	  cout <<"Processing shear2..." <<flush;
	}
      else if(i == 9)
	{
	  cout <<"Processing Jacobian..." <<flush;
	}
      else if(i == 12)
	{
	  cout <<"Processing Magnification..." <<flush;
	}

      //Reading the maps of interest
      if(i == 0)
	{
	  read_pimg(input[0],low_map_read);
	  read_pimg(input[1],high_map_read);
	}
      else
	{
	  read_imge(input[0],extensions[i],low_map_read);
	  read_imge(input[1],extensions[i],high_map_read);
	}
      //If necessary interpolate
      if(interpolate_low)
	{
	  cspline_smooth_morecleverinterpol(low_map_read,low_mask_int,low_map_inter);
	  for(int l = 0; l < low_map_inter->size1; l++)
	    {
	      for(int k = 0; k < low_map_inter->size2; k++)
		{
		  if(gsl_matrix_get(low_mask_inter,l,k) == 0.0)
		    {
		      gsl_matrix_set(low_map_inter,l,k,0.0);
		    }
		}
	    }
	}
      else
	{
	  gsl_matrix_memcpy(low_map_inter,low_map_read);
	}

      if(interpolate_high)
	{
	  cspline_smooth_morecleverinterpol(high_map_read,high_mask_int,high_map_inter);

	  for(int l = 0; l < high_map_inter->size1; l++)
	    {
	      for(int k = 0; k < high_map_inter->size2; k++)
		{
		  if(gsl_matrix_get(high_mask_inter,l,k) == 0.0)
		    {
		      gsl_matrix_set(high_map_inter,l,k,0.0);
		    }
		}
	    }
	}
      else
	{
	  gsl_matrix_memcpy(high_map_inter,high_map_read);
	}

      //Binning up the low_res map to target resolution     
      for(int l = 0; l <  low_map_inter->size1; l++)
	{
	  for(int k = 0; k < low_map_inter->size2; k++)
	    {
	      for(int m = 0; m < up_scale; m++)
		{
		  for(int n = 0; n < up_scale; n++)
		    {
		      if(gsl_matrix_get(high_reference,l*up_scale+m,k*up_scale+n) <= thresh)//gsl_matrix_get(low_reference,l*up_scale+m,k*up_scale+n))
			{ 
			  gsl_matrix_set(selected_map,l*up_scale+m,k*up_scale+n,gsl_matrix_get(low_map_inter,l,k));
			}
		      else
			{
			  gsl_matrix_set(selected_map,l*up_scale+m,k*up_scale+n,gsl_matrix_get(high_map_inter,l*up_scale+m,k*up_scale+n));
			}
		    }
		}
	    }
	}
      if(i == 0)
	{
	  write_pimg(outfilename,selected_map);
	  write_header(outfilename,"N_BS_L",FITS_header[0],"The number of lowres bootstraps");
	  write_header(outfilename,"N_BS_H",FITS_header[1],"The number of highres bootstraps");
	  write_header(outfilename,"Z_S",FITS_header[2],"The source redshift used to create this map.");
	  write_header(outfilename,"Z_L",FITS_header[4],"The lens redshift used to create this map.");
	  write_header(outfilename,"S_SCALE",FITS_header[6],"The smoothing scale of the highres map.");
	  write_header(outfilename,"S_N_T",FITS_header[7],"The sd threshold used to distinguish the regimes.");
	}
      else
	{
	  write_imge(outfilename,extensions[i],selected_map);
	  write_header_ext(outfilename,extensions[i],"N_BS_L",FITS_header[0],"The number of lowres bootstraps");
	  write_header_ext(outfilename,extensions[i],"N_BS_H",FITS_header[1],"The number of highres bootstraps");
	  write_header_ext(outfilename,extensions[i],"Z_S",FITS_header[2],"The source redshift used to create this map.");
	  write_header_ext(outfilename,extensions[i],"Z_L",FITS_header[4],"The lens redshift used to create this map.");
	  write_header_ext(outfilename,extensions[i],"S_SCALE",FITS_header[6],"The smoothing scale of the highres map.");
	  write_header_ext(outfilename,extensions[i],"S_N_T",FITS_header[7],"The sd threshold used to distinguish the regimes.");
	}

      if(i%3 == 0)
	{
	  cout <<"done." <<endl;
	}

      //Finally creating a field mask for binning later
      if(i == extensions.size() - 1)
	{  
	  gsl_matrix_int *final_mask = gsl_matrix_int_calloc(selected_map->size1,selected_map->size2);
	  for(int l = 0; l < selected_map->size1; l++)
	    {
	      for(int k = 0; k < selected_map->size2; k++)
		{
		  if(gsl_matrix_get(selected_map,l,k) == 0.0)
		    {
		      gsl_matrix_int_set(final_mask,l,k,1.0);
		    }
		}
	    }
	  write_imgeint(outfilename,"field_mask",final_mask);
	  write_header_ext(outfilename,"field_mask","N_BS_L",FITS_header[0],"The number of lowres bootstraps");
	  write_header_ext(outfilename,"field_mask","N_BS_H",FITS_header[1],"The number of highres bootstraps");
	  write_header_ext(outfilename,"field_mask","Z_S",FITS_header[2],"The source redshift used to create this map.");
	  write_header_ext(outfilename,"field_mask","Z_L",FITS_header[4],"The lens redshift used to create this map.");
	  write_header_ext(outfilename,"field_mask","S_SCALE",FITS_header[6],"The smoothing scale of the highres map.");
	  write_header_ext(outfilename,"field_mask","S_N_T",FITS_header[7],"The sd threshold used to distinguish the regimes.");
	}//Last round (field mask)
    } //Extension loop
    
       
  return 0;

}
