/***
    SaWMultiscaleBS 0.1
    Part of the CLASH pipeline. Create multi-resolution bootstraps.
    Julian Merten
    JPL/Caltech 
    August 2013
***/

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>
#include <tclap/CmdLine.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_rng.h>
#include <saw/rw_fits.h>
#include <astro/cosmology.h>

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("SaWMultiscaleBS", ' ', "0.1");
  TCLAP::MultiArg<std::string> inputArg("i","input","The three input FITS roots. This will be follower by the bootstrap ID as typical in the CLASH pipeline.",true,"string",cmd);
  TCLAP::MultiArg<double> fieldArg("f","field-size","The two field sizes in x-dimension for the lowres map (first arg) and the two highres maps (second arg)",true,"double",cmd);
  TCLAP::MultiArg<int> samplingArg("s","over-sampling","The first argument is the oversampling factor for the medres map, the second the factor for highres map.",true,"int",cmd);
  TCLAP::ValueArg<int> cutxArg("x","x-cuts","The x insertion point.",true,0,"int",cmd);
  TCLAP::ValueArg<int> cutyArg("y","y-cuts","The y insertion point.",true,0,"int",cmd);
  TCLAP::MultiArg<double> wcsArg("w","wcs","Offers the possibility to add a WCS system to your final output maps. Expects 4 numbers, the pixel centre and the WCS centre.",false,"double",cmd);
  TCLAP::MultiArg<int> highresArg("a","h_field","Expects the four highres insert points where it will be switched from the medres to highres map. Give in oversampled pixel coordinates.",false,"double",cmd);
  TCLAP::MultiArg<int> numberArg("n","bs-num","Four numbers are needed, the first three are the total number of available low/med/highres bootstraps, the fourth number is the number of output bootstraps.",false,"double",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output root structure which will be followed by the bootstrap ID and the FITS extension.",true,"./output.fits","string",cmd);
  TCLAP::SwitchArg meanArg("m","mean_map","Toogle if you want a map containing the mean of all bootstraps created.",cmd);

  cmd.parse( argc, argv );

  //Parsing and checking the input parameters.

  bool create_mean = meanArg.getValue();

  vector<std::string> file_roots = inputArg.getValue();

  if(file_roots.size() < 3)
    {
      throw invalid_argument("SawBS: Not enough input filenames given.");
    }

  vector<double> fields = fieldArg.getValue();

  if(fields[0] < fields[1] || fields.size() < 2)
    {
      throw invalid_argument("SaWBS: The fields you are trying to combine are incompatible");
    }

  vector<int> o_sampling = samplingArg.getValue();

  if(o_sampling.size() < 2)
    {
      throw invalid_argument("SaWBS: Not enough over-sampling parameters given.");
    }

  vector<int> cuts;
  cuts.resize(2);
  cuts[0] = cutxArg.getValue();
  cuts[1] = cutyArg.getValue();

  if((cuts[0] == 0 || cuts[1] == 0) && cuts[0] != cuts[1])
    {
      throw invalid_argument("SaWComb: Invalid entry points.");
    }

  vector<double> WCS = wcsArg.getValue();

  if(WCS.size() < 4 && WCS.size() != 0)
    {
      throw invalid_argument("SaWComb: Invalid WCS information given.");
    }

  vector<int> high_cuts = highresArg.getValue();
  if(high_cuts.size() < 4)
    {
      throw invalid_argument("SaWComb: Invlid highresolution insertion points.");
    }

  vector<int> number = numberArg.getValue();
  if(number.size() < 4)
    {
      throw invalid_argument("SaWComb: Invalid bootstrap number defintion.");
    }

  string outroot = outputArg.getValue();


  //Initialising the random number generator

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  //Seeding with randome value

  unsigned int seed = random();
  gsl_rng_set(r,seed);

  //Starting the outer loop

  vector<std::string> files;
  files.resize(3);
  string outfile;

  int BS1;
  int BS2;
  int BS3;

  //Creating maps for the total mean

  gsl_matrix *mean_conv;
  gsl_matrix *mean_shear1;
  gsl_matrix *mean_shear2;
  gsl_matrix *mean_jacdet;
  gsl_matrix *mean_magnification;

  cout <<"Processing realisation: " <<flush;

  for(int n = 1; n <= number[3]; n++)
    {

      cout <<left <<setw(6);
      cout  <<n <<flush;
      CURSORLEFT(6);

      //Constructing filenames
      BS1 = 1+floor(gsl_rng_uniform(r)*number[0]);
      BS2 = 1+floor(gsl_rng_uniform(r)*number[1]);
      BS3 = 1+floor(gsl_rng_uniform(r)*number[2]);

      ostringstream in1,in2,in3,out;
      in1 <<BS1;
      in2 <<BS2;
      in3 <<BS3;
      out <<n;

      files[0] = file_roots[0]+"_BS"+in1.str()+".fits";
      files[1] = file_roots[1]+"_BS"+in2.str()+".fits";
      files[2] = file_roots[2]+"_BS"+in3.str()+".fits";
      outfile = outroot+"_BS"+out.str()+".fits";

      
      //Getting pixel information from the input maps
      int x_lowres = read_intheader(files[0],"NAXIS1");
      int y_lowres = read_intheader(files[0],"NAXIS2");
      int x_medres = read_intheader(files[1],"NAXIS1");
      int y_medres = read_intheader(files[1],"NAXIS2");
      int x_highres = read_intheader(files[2],"NAXIS1");
      int y_highres = read_intheader(files[2],"NAXIS2");
      
      //Checking input maps for redshift compatibility
      
      int z_map_low = read_doubleheader(files[0],"Z_S"); 
      int z_map_med = read_doubleheader(files[1],"Z_S"); 
      int z_map_high = read_doubleheader(files[2],"Z_S");
      int z_lens_low = read_doubleheader(files[0],"Z_L"); 
      int z_lens_med = read_doubleheader(files[1],"Z_L"); 
      int z_lens_high = read_doubleheader(files[2],"Z_L");
      
      if(z_map_low != z_map_med || z_lens_low != z_lens_med)
	{
	  throw invalid_argument("SaWBS: Redsifts of input maps incompatible.");
	}
      else if(z_map_high != z_map_med || z_lens_high != z_lens_med)
	{
	  throw invalid_argument("SaWBS: Redsifts of input maps incompatible.");
	}
      //Figuring out oversampling
      
      int x_medres_new = o_sampling[0]*x_medres;
      int y_medres_new = o_sampling[0]*y_medres;
      int x_highres_new = o_sampling[1]*x_highres;
      int y_highres_new = o_sampling[1]*y_highres;
      
      double lowres_res = fields[0]/x_lowres;
      double highres_res = fields[1]/x_highres_new;
      int field_factor = floor(lowres_res / highres_res + 0.5);

      if(x_medres_new != x_highres_new || y_medres_new != y_highres_new)
	{
	  throw invalid_argument("SaWBS: Invalid numbers for perfect over-sampling.");
	}
      
      int x_lowres_new = field_factor*x_lowres;
      int y_lowres_new = field_factor*y_lowres;

      if(create_mean && n == 1)
	{
	  mean_conv = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
	  mean_shear1 = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
	  mean_shear2 = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
	  mean_jacdet = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
	  mean_magnification = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
	}


  //Reading input maps

      gsl_matrix *in_conv_low = gsl_matrix_calloc(y_lowres,x_lowres);
      gsl_matrix *in_conv_med = gsl_matrix_calloc(y_medres,x_medres);
      gsl_matrix *in_conv_high = gsl_matrix_calloc(y_highres,x_highres);
      read_pimg(files[0],in_conv_low);
      read_pimg(files[1],in_conv_med);
      read_pimg(files[2],in_conv_high);
      
      gsl_matrix *in_shear1_low = gsl_matrix_calloc(y_lowres,x_lowres);
      gsl_matrix *in_shear1_med = gsl_matrix_calloc(y_medres,x_medres);
      gsl_matrix *in_shear1_high = gsl_matrix_calloc(y_highres,x_highres);
      read_imge(files[0],"shear1",in_shear1_low);
      read_imge(files[1],"shear1",in_shear1_med);
      read_imge(files[2],"shear1",in_shear1_high);

      gsl_matrix *in_shear2_low = gsl_matrix_calloc(y_lowres,x_lowres);
      gsl_matrix *in_shear2_med = gsl_matrix_calloc(y_medres,x_medres);
      gsl_matrix *in_shear2_high = gsl_matrix_calloc(y_highres,x_highres);
      read_imge(files[0],"shear2",in_shear2_low);
      read_imge(files[1],"shear2",in_shear2_med);
      read_imge(files[2],"shear2",in_shear2_high);
      
      gsl_matrix *in_jacdet_low = gsl_matrix_calloc(y_lowres,x_lowres);
      gsl_matrix *in_jacdet_med = gsl_matrix_calloc(y_medres,x_medres);
      gsl_matrix *in_jacdet_high = gsl_matrix_calloc(y_highres,x_highres);
      read_imge(files[0],"jacdet",in_jacdet_low);
      read_imge(files[1],"jacdet",in_jacdet_med);
      read_imge(files[2],"jacdet",in_jacdet_high);
      
      gsl_matrix *in_magnification_low = gsl_matrix_calloc(y_lowres,x_lowres);
      gsl_matrix *in_magnification_med = gsl_matrix_calloc(y_medres,x_medres);
      gsl_matrix *in_magnification_high = gsl_matrix_calloc(y_highres,x_highres);
      read_imge(files[0],"magnification",in_magnification_low);
      read_imge(files[1],"magnification",in_magnification_med);
      read_imge(files[2],"magnification",in_magnification_high);
      

      //Oversampling the maps
      
      gsl_matrix *conv_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
      gsl_matrix *shear1_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
      gsl_matrix *shear2_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
      gsl_matrix *jacdet_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
      gsl_matrix *magnification_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
      
      for(int i = 0; i < y_lowres; i++)
	{
	  for(int j = 0; j < x_lowres; j++)
	    {
	      for(int l = 0; l < field_factor; l++)
		{
		  for(int k = 0; k < field_factor; k++)
		    {
		      gsl_matrix_set(conv_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_conv_low,i,j));
		      gsl_matrix_set(shear1_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_shear1_low,i,j));
		      gsl_matrix_set(shear2_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_shear2_low,i,j));
		      gsl_matrix_set(jacdet_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_jacdet_low,i,j));
		      gsl_matrix_set(magnification_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_magnification_low,i,j));
		    }
		}
	    }
	}
      
      gsl_matrix *conv_med = gsl_matrix_calloc(y_medres_new,x_medres_new);
      gsl_matrix *shear1_med = gsl_matrix_calloc(y_medres_new,x_medres_new);
      gsl_matrix *shear2_med = gsl_matrix_calloc(y_medres_new,x_medres_new);
      gsl_matrix *jacdet_med = gsl_matrix_calloc(y_medres_new,x_medres_new);
      gsl_matrix *magnification_med = gsl_matrix_calloc(y_medres_new,x_medres_new);
      
      for(int i = 0; i < y_medres; i++)
	{
	  for(int j = 0; j < x_medres; j++)
	    {
	      for(int l = 0; l < o_sampling[0]; l++)
		{
		  for(int k = 0; k < o_sampling[0]; k++)
		    {
		      gsl_matrix_set(conv_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_conv_med,i,j));
		      gsl_matrix_set(shear1_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_shear1_med,i,j));
		      gsl_matrix_set(shear2_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_shear2_med,i,j));
		      gsl_matrix_set(jacdet_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_jacdet_med,i,j));
		      gsl_matrix_set(magnification_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_magnification_med,i,j));
		    }
		}
	    }
	}
      
      gsl_matrix *conv_high = gsl_matrix_calloc(y_highres_new,x_highres_new);
      gsl_matrix *shear1_high = gsl_matrix_calloc(y_highres_new,x_highres_new);
      gsl_matrix *shear2_high = gsl_matrix_calloc(y_highres_new,x_highres_new);
      gsl_matrix *jacdet_high = gsl_matrix_calloc(y_highres_new,x_highres_new);
      gsl_matrix *magnification_high = gsl_matrix_calloc(y_highres_new,x_highres_new);
      
      for(int i = 0; i < y_highres; i++)
	{
	  for(int j = 0; j < x_highres; j++)
	    {
	      for(int l = 0; l < o_sampling[1]; l++)
		{
		  for(int k = 0; k < o_sampling[1]; k++)
		    {
		      gsl_matrix_set(conv_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_conv_high,i,j));
		      gsl_matrix_set(shear1_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_shear1_high,i,j));
		      gsl_matrix_set(shear2_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_shear2_high,i,j));
		      gsl_matrix_set(jacdet_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_jacdet_high,i,j));
		      gsl_matrix_set(magnification_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_magnification_high,i,j));
		    }
		}
	    }
	}

      if(cuts[0] < 0 || cuts[0] >= x_lowres_new || cuts[1] < 0 || cuts[1] >= y_lowres_new)
	{
	  throw invalid_argument("SaWComb: Invalid inset locations.");
	}

      double value;
      
      for(int i = 0; i < y_highres_new; i++)
	{
	  for(int j = 0; j < x_highres_new; j++)
	    {
	      if(i >= high_cuts[2] && i <= high_cuts[3] && j >= high_cuts[0] && j <= high_cuts[1])
		{
		  value = gsl_matrix_get(conv_high,i,j);		
		  //NEW STUFF COMING HERE!
		  if(i == high_cuts[2] || i == high_cuts[3] || j == high_cuts[0] || j == high_cuts[1])
		    {
		      value = (3.0*gsl_matrix_get(conv_med,i,j)+value)/4.;
		    }
		  else if(i == high_cuts[2]+1 || i == high_cuts[3]-1 || j == high_cuts[0]+1 || j == high_cuts[1]-1)
		    {
		      value = (gsl_matrix_get(conv_med,i,j)+value)/2.;
		    }
		  else if(i == high_cuts[2]+2 || i == high_cuts[3]-2 || j == high_cuts[0]+2 || j == high_cuts[1]-2)
		    {
		      value = (gsl_matrix_get(conv_med,i,j)+3.0*value)/4.;
		    }
		}
	      else
		{
		  value = gsl_matrix_get(conv_med,i,j);
		}
	      if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
		{
		  value = (3.*gsl_matrix_get(conv_low,cuts[1]+i,cuts[0]+j)+value)/4.;
		}
	      else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
		{
		  value = (gsl_matrix_get(conv_low,cuts[1]+i,cuts[0]+j)+value)/2.;
		}
	      else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
		{
		  value = (gsl_matrix_get(conv_low,cuts[1]+i,cuts[0]+j)+3.0*value)/4.;
		}
	      gsl_matrix_set(conv_low,cuts[1]+i,cuts[0]+j,value);
	    
	    

	      if(i >= high_cuts[2] && i <= high_cuts[3] && j >= high_cuts[0] && j <= high_cuts[1])
		{
		  value = gsl_matrix_get(shear1_high,i,j);
		}
	      else
		{
		  value = gsl_matrix_get(shear1_med,i,j);
		}
	      if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
		{
		  value = (3.*gsl_matrix_get(shear1_low,cuts[1]+i,cuts[0]+j)+value)/4.;
		}
	      else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
		{
		  value = (2.*gsl_matrix_get(shear1_low,cuts[1]+i,cuts[0]+j)+value)/3.;
		}
	      else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
		{
	      value = (gsl_matrix_get(shear1_low,cuts[1]+i,cuts[0]+j)+value)/2.;
		}
	      gsl_matrix_set(shear1_low,cuts[1]+i,cuts[0]+j,value);  
	     
	
	      
	      if(i >= high_cuts[2] && i <= high_cuts[3] && j >= high_cuts[0] && j <= high_cuts[1])
		{
		  value = gsl_matrix_get(shear2_high,i,j);
		}
	      
	      else
		{
		  value = gsl_matrix_get(shear2_med,i,j);
		}
	      if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
		{
		  value = (3.*gsl_matrix_get(shear2_low,cuts[1]+i,cuts[0]+j)+value)/4.;
		}
	      else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
		{
		  value = (2.*gsl_matrix_get(shear2_low,cuts[1]+i,cuts[0]+j)+value)/3.;
		}
	      else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
		{
		  value = (gsl_matrix_get(shear2_low,cuts[1]+i,cuts[0]+j)+value)/2.;
		}
		gsl_matrix_set(shear2_low,cuts[1]+i,cuts[0]+j,value); 
		


	  
		if(i >= high_cuts[2] && i <= high_cuts[3] && j >= high_cuts[0] && j <= high_cuts[1])
		  {
		    value = gsl_matrix_get(jacdet_high,i,j);
		  }
		else
		  {
		    value = gsl_matrix_get(jacdet_med,i,j);
		  }
		if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
		  {
		    value = (3.*gsl_matrix_get(jacdet_low,cuts[1]+i,cuts[0]+j)+value)/4.;
		  }
		else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
		  {
		    value = (2.*gsl_matrix_get(jacdet_low,cuts[1]+i,cuts[0]+j)+value)/3.;
		  }
		else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
		  {
		    value = (gsl_matrix_get(jacdet_low,cuts[1]+i,cuts[0]+j)+value)/2.;
		  }
		gsl_matrix_set(jacdet_low,cuts[1]+i,cuts[0]+j,value);  


	  
		if(i >= high_cuts[2] && i <= high_cuts[3] && j >= high_cuts[0] && j <= high_cuts[1])
		  {
		    value = gsl_matrix_get(magnification_high,i,j);
		  }
		else
		  {
		    value = gsl_matrix_get(magnification_med,i,j);
		  }
		if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
		  {
		    value = (3.*gsl_matrix_get(magnification_low,cuts[1]+i,cuts[0]+j)+value)/4.;
		  }
		else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
		  {
		    value = (2.*gsl_matrix_get(magnification_low,cuts[1]+i,cuts[0]+j)+value)/3.;
		  }
		
		else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
		  {
		    value = (gsl_matrix_get(magnification_low,cuts[1]+i,cuts[0]+j)+value)/2.;
		  }
		gsl_matrix_set(magnification_low,cuts[1]+i,cuts[0]+j,value);   
		
	      
	    } //j loop
	} // i loop



      //Writing out the maps
	  
      write_pimg(outfile,conv_low);
      write_imge(outfile,"shear1",shear1_low);
      write_imge(outfile,"shear2",shear2_low);
      write_imge(outfile,"jacdet",jacdet_low);
      write_imge(outfile,"magnification",magnification_low);


      //Writing some redshift information to header

      write_header(outfile,"Z_L",z_lens_low,"Lens redshift");
      write_header(outfile,"Z_S",z_map_low,"Source redshift");
      write_header_ext(outfile,"shear1","Z_L",z_lens_low,"Lens redshift");
      write_header_ext(outfile,"shear1","Z_S",z_map_low,"Source redshift");
      write_header_ext(outfile,"shear2","Z_L",z_lens_low,"Lens redshift");
      write_header_ext(outfile,"shear2","Z_S",z_map_low,"Source redshift");
      write_header_ext(outfile,"jacdet","Z_L",z_lens_low,"Lens redshift");
      write_header_ext(outfile,"jacdet","Z_S",z_map_low,"Source redshift");
      write_header_ext(outfile,"magnification","Z_L",z_lens_low,"Lens redshift");
      write_header_ext(outfile,"magnification","Z_S",z_map_low,"Source redshift");
      	  
      //Checking for and adding WCS
      
      if(WCS.size() > 0)
	{
	  WCS.resize(8);
	  double pixel_size = fields[0]/(x_lowres_new*3600.0);
	  WCS[4] = -pixel_size;
	  WCS[5] = 0.;
	  WCS[6] = 0.;
	  WCS[7] = pixel_size;
	  
	  add_WCS(outfile,WCS,"J2000");
	  add_WCS(outfile,"shear1",WCS,"J2000");
	  add_WCS(outfile,"shear2",WCS,"J2000");
	  add_WCS(outfile,"jacdet",WCS,"J2000");
	  add_WCS(outfile,"magnification",WCS,"J2000");
	}

      //Writing BS information to header

      write_header(outfile,"BS1",BS1,"The initial lowres bootstrap index.");
      write_header(outfile,"BS2",BS2,"The initial medres bootstrap index.");
      write_header(outfile,"BS3",BS3,"The initial highres bootstrap index.");
      write_header_ext(outfile,"shear1","BS1",BS1,"The initial lowres bootstrap index.");
      write_header_ext(outfile,"shear1","BS2",BS2,"The initial medres bootstrap index.");
      write_header_ext(outfile,"shear1","BS3",BS3,"The initial highres bootstrap index.");
      write_header_ext(outfile,"shear2","BS1",BS1,"The initial lowres bootstrap index.");
      write_header_ext(outfile,"shear2","BS2",BS2,"The initial medres bootstrap index.");
      write_header_ext(outfile,"shear2","BS3",BS3,"The initial highres bootstrap index.");
      write_header_ext(outfile,"jacdet","BS1",BS1,"The initial lowres bootstrap index.");
      write_header_ext(outfile,"jacdet","BS2",BS2,"The initial medres bootstrap index.");
      write_header_ext(outfile,"jacdet","BS3",BS3,"The initial highres bootstrap index.");
      write_header_ext(outfile,"magnification","BS1",BS1,"The initial lowres bootstrap index.");
      write_header_ext(outfile,"magnification","BS2",BS2,"The initial medres bootstrap index.");
      write_header_ext(outfile,"magnification","BS3",BS3,"The initial highres bootstrap index.");

      if(create_mean)
	{
	  gsl_matrix_add(mean_conv,conv_low);
	  gsl_matrix_add(mean_shear1,shear1_low);
	  gsl_matrix_add(mean_shear2,shear2_low);
	  gsl_matrix_add(mean_jacdet,jacdet_low);
	  gsl_matrix_add(mean_magnification,magnification_low);
	}


    }// main BS loop

  if(create_mean)
    {
      string mean_out = outroot+"_mean.fits";

      gsl_matrix_scale(mean_conv,1./number[3]);
      gsl_matrix_scale(mean_shear1,1./number[3]);
      gsl_matrix_scale(mean_shear2,1./number[3]);
      gsl_matrix_scale(mean_jacdet,1./number[3]);
      gsl_matrix_scale(mean_magnification,1./number[3]);

      write_pimg(mean_out,mean_conv);
      write_imge(mean_out,"shear1",mean_shear1);
      write_imge(mean_out,"shear2",mean_shear2);
      write_imge(mean_out,"jacdet",mean_jacdet);
      write_imge(mean_out,"magnification",mean_magnification);

      if(WCS.size() > 0)
	{
	  add_WCS(mean_out,WCS,"J2000");
	  add_WCS(mean_out,"shear1",WCS,"J2000");
	  add_WCS(mean_out,"shear2",WCS,"J2000");
	  add_WCS(mean_out,"jacdet",WCS,"J2000");
	  add_WCS(mean_out,"magnification",WCS,"J2000");
	}

    }

  cout <<endl;

  return 0;

}



