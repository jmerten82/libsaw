/***
    R_g_profile 1.0 
    Julian Merten
    JPL/Caltech 2013
    This tool derives a radial tangential and cross shear 
    profile from an input catalogue and reference centre
***/

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <saw/cat_reader.h>
#include <astro/utilities.h>
#include <tclap/CmdLine.h>

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Radial shear profiles", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The input shear catalogue. First two columns must be coordinates, the second two shear values.",true,"./","string",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output binned table.",true,"./","string",cmd);
  TCLAP::ValueArg<int> binArg("n","numBINS","The number of bins to be used.",true,1,"int",cmd);
  TCLAP::SwitchArg logArg("l","low","If set, logarithmic binning is used.",cmd);
  TCLAP::MultiArg<double> refArg("r","ref","The reference x and y coordinate.",false,"double",cmd);
  TCLAP::SwitchArg j2000Arg("j","j2000","If set, RA-DEC coordinates are assumed.",cmd);
  TCLAP::ValueArg<int> bootstrapArg("b","numBS","The number of bootstraps per bin for error estimation.",true,0,"int",cmd);
  TCLAP::MultiArg<double> FactorArg("f","factor","A constant factor with which either the output radial distance or the average value can be multiplied.",false,"double",cmd);

  cmd.parse( argc, argv );

  string infile = inputArg.getValue();
  string outfile = outputArg.getValue();
  int bins = binArg.getValue();
  bool logarithmic = logArg.getValue();
  vector<double> reference = refArg.getValue();
  bool j2000 = j2000Arg.getValue();
  vector<double> f = FactorArg.getValue();
  int BS = bootstrapArg.getValue();
  if(f.size() == 0)
    {
      f.push_back(1.0);
      f.push_back(1.0);
    }
  else if(f.size() == 1)
    {
      f.push_back(1.0);
    }

  double PI = acos(-1.);
  double radec_corr = cos(reference[1]/180.*PI);

  //Setting up RNG

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);


  //Reading the input catalogue

  CatReader reader1(infile);
  int size = reader1.show_rowdim();

  //Calculating distances and position angles relative to reference

  gsl_vector *distances = gsl_vector_calloc(size);
  gsl_vector *angles = gsl_vector_calloc(size);
  gsl_vector *tang_shear = gsl_vector_calloc(size);
  gsl_vector *cross_shear = gsl_vector_calloc(size);
  double x,y;

  for(int i = 0; i < size; i++)
    {
      x = gsl_matrix_get(reader1.show_data(),i,0)-reference[0];
      if(j2000)
	{
	  x *= -1.0*radec_corr;
	}
      y = gsl_matrix_get(reader1.show_data(),i,1)-reference[1];
      gsl_vector_set(distances,i,sqrt(x*x+y*y));
      gsl_vector_set(angles,i,atan2(y,x));
    
      //Calculating tangential and cross shears. 
      gsl_vector_set(tang_shear,i,-1.*(gsl_matrix_get(reader1.show_data(),i,2)*cos(2.*gsl_vector_get(angles,i)) + gsl_matrix_get(reader1.show_data(),i,3)*sin(2.*gsl_vector_get(angles,i))));
      gsl_vector_set(cross_shear,i,-1.*(gsl_matrix_get(reader1.show_data(),i,3)*cos(2.*gsl_vector_get(angles,i)) - gsl_matrix_get(reader1.show_data(),i,2)*sin(2.*gsl_vector_get(angles,i))));
    }


  //Creating the bins

  double max = gsl_vector_max(distances);
  double min = gsl_vector_min(distances);
  vector<double> bin_vector;
  vector<double> bin_centres;
		     

  for(int i = 0; i < bins+1; i++)
    {
      if(logarithmic)
	{
	  bin_vector.push_back(astro::x_logarithmic(i, bins+1,min,max));
	}
      else
	{
	  bin_vector.push_back(astro::x_linear(i,bins+1,min,max));
	}
    }


  for(int i = 1; i < bins+1; i++)
    {

      if(logarithmic)
	{
	  double value = exp((log(bin_vector[i])-log(bin_vector[i-1])) / 2.0+log(bin_vector[i-1]));
	  bin_centres.push_back(value);

	}
      else
	{
	  bin_centres.push_back((bin_vector[i-1]+bin_vector[i])/2.0);
	}
    }

  //Performing averages in the bins

  ofstream output(outfile.c_str());
  output <<scientific;
  output <<setprecision(7);
  output <<showpoint; 

  if(BS==0)
    {
      output <<"# bin_centre(1)  bin_lower_edge(2)  bin_upper_edge(3)  data_points_in_bin(4)  mean_tang_shear(5)   variance_tang_shear(6)  mean_radial_shear(7)  variance_radial_shear(8)" <<endl;
    }
  else
    {
      output <<"# bin_centre(1)  bin_lower_edge(2)  bin_upper_edge(3)  data_points_in_bin(4)  mean_tang_shear(5)  BS_mean_tang_shear(6)   variance_tang_shear(7)  BS_variance_tang_shear(8)  mean_radial_shear(9)  BS_mean_radial_shear(10)  variance_radial_shear(11)  BS_variance_radial_shear(12)  num_bootstraps_per_bin(13)" <<endl;
    }

  double upper,lower,centre,tang_avg,cross_avg,tang_sd,cross_sd;

  for(int i = 1; i < bin_vector.size(); i++)
    {
      upper = bin_vector[i];
      lower = bin_vector[i-1];
      centre = bin_centres[i-1];

      output <<f[0]*centre <<"\t" <<f[0]*lower <<"\t" <<f[0]*upper <<"\t" <<flush; \

      vector<double> tang_sample;
      vector<double> cross_sample;
      vector<double> BS_means_tang;
      vector<double> BS_means_cross;

      for(int j = 0; j < size; j++)
	{
	  if(gsl_vector_get(distances,j) <= upper && gsl_vector_get(distances,j)> lower)
	    {
	      tang_sample.push_back(gsl_vector_get(tang_shear,j));
	      cross_sample.push_back(gsl_vector_get(cross_shear,j));
	    }
	}

      //Running bootstrap analysis
      int sample_size = tang_sample.size();
      int pos;
      for(int l = 0; l < BS; l++)
	{
	  vector<double> BS_tang_sample;
	  vector<double> BS_cross_sample;
	  for(int k = 0; k < sample_size; k++)
	    {
	      pos = floor(gsl_rng_uniform(r)*sample_size+0.5);
	      BS_tang_sample.push_back(tang_sample[pos]);
	      BS_cross_sample.push_back(cross_sample[pos]);
	    }
	  BS_means_tang.push_back(gsl_stats_mean(&BS_tang_sample[0],1.0,BS_tang_sample.size()));
	  BS_means_cross.push_back(gsl_stats_mean(&BS_cross_sample[0],1.0,BS_cross_sample.size()));
	}
      if(BS == 0)
	{
	  output <<tang_sample.size() <<"\t" <<f[1]*gsl_stats_mean(&tang_sample[0],1,tang_sample.size()) <<"\t" <<f[1]*gsl_stats_variance(&tang_sample[0],1,tang_sample.size()) <<"\t" <<f[1]*gsl_stats_mean(&cross_sample[0],1,tang_sample.size()) <<"\t" <<f[1]*gsl_stats_variance(&cross_sample[0],1,tang_sample.size()) <<endl;
	}
      else
	{
	  output <<tang_sample.size() <<"\t" <<f[1]*gsl_stats_mean(&tang_sample[0],1,tang_sample.size()) <<"\t" <<f[1]*gsl_stats_mean(&BS_means_tang[0],1.0,BS_means_tang.size()) <<"\t"  <<f[1]*gsl_stats_variance(&tang_sample[0],1,tang_sample.size()) <<"\t" <<f[1]*gsl_stats_variance(&BS_means_tang[0],1.0,BS_means_tang.size()) <<"\t" <<f[1]*gsl_stats_mean(&cross_sample[0],1,tang_sample.size()) <<"\t" <<f[1]*gsl_stats_mean(&BS_means_cross[0],1.0,BS_means_cross.size()) <<"\t" <<f[1]*gsl_stats_variance(&cross_sample[0],1,tang_sample.size()) <<"\t" <<f[1]*gsl_stats_variance(&BS_means_cross[0],1.0,BS_means_cross.size()) <<"\t" <<BS_means_tang.size() <<endl;
	}
    }

  output.close();

  return 0;

}
 






