/***
    Redshift-Lensing scaling 1.0 
    Julian Merten
    JPL/Caltech 2013
    Given a lens redshift, this routine calculates
    the correction factor to scale an input redshift 
    lensing quantity to a target redshift. 
***/

#include <iostream>
#include <cmath>
#include <gsl/gsl_matrix.h>
#include <astro/cosmology.h>
#include <tclap/CmdLine.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Lensing Redshift scaling", ' ', "1.0");
  TCLAP::ValueArg<double> lensArg("l","lens","The redshift of the lens.",true,0.5,"double",cmd);
  TCLAP::ValueArg<double> inputArg("i","initial","The original redshift.",true,1.0,"double",cmd);
  TCLAP::ValueArg<double> outputArg("o","output","The wanted output redshift.",true,1.0,"double",cmd);
  cmd.parse( argc, argv );

  double lens = lensArg.getValue();
  double in = inputArg.getValue();
  double out = outputArg.getValue();

  astro::cosmology cosmo1(0.3,0.7,0.7,-1.0,0.04);

  double factor = cosmo1.angularDist(lens,out)/cosmo1.angularDist(0.0,out) *cosmo1.angularDist(0.0,in) / cosmo1.angularDist(lens,in);

  cout <<factor <<endl;

  return 0;

}  

