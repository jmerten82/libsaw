#include <gsl/gsl_matrix.h>
#include <saw/rw_fits.h>

using namespace std;

int main()
{

  gsl_matrix_int *mask = gsl_matrix_int_calloc(32,32);

  for(int i = 0; i < 6; i++)
    {
      for(int j = 0; j < 6; j++)
	{
	  gsl_matrix_int_set(mask,10+i,10+j,-1);
	}
    }

  for(int i = 0; i < 5; i++)
    {
      for(int j = 0; j < 5; j++)
	{
	  gsl_matrix_int_set(mask,27+i,27+j,-1);
	}
    }

  write_pimgint("./mask.fits",mask);

  gsl_matrix_int_set_all(mask,0);

  for(int i = 0; i < 6; i++)
    {
      for(int j = 0; j < 6; j++)
	{
	  gsl_matrix_int_set(mask,10+i,10+j,-1);
	}
    }

  for(int i = 0; i < 5; i++)
    {
      for(int j = 0; j < 4; j++)
	{
	  gsl_matrix_int_set(mask,14+i,17+j,-1);
	}
    }

  write_imgeint("./mask.fits","mask2",mask);

  return 0;

}




