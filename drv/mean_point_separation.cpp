/***
    Image separation 1.0 
    Julian Merten
    JPL/Caltech 2013
    This tool derives the mean distance of points
    in an input catalogue.
***/

#include <iostream>
#include <cmath>
#include <vector>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <saw/cat_reader.h>
#include <tclap/CmdLine.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Effective lensing depth", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The input catalogue.",true,"./","string",cmd);
  TCLAP::MultiArg<int> columnArg("c","numBINS","The columns of the catalogue containing the position info (counting starts at 0).",true,"int",cmd);
  cmd.parse( argc, argv );

  string infile = inputArg.getValue();
  vector<int> cols = columnArg.getValue();

  CatReader reader1(infile);
  double x,y,distance,x_c,y_c;
  vector<double> sample;

  for(int i = 0; i < reader1.show_rowdim(); i++)
    {
      x = gsl_matrix_get(reader1.show_data(),i,cols[0]);
      y = gsl_matrix_get(reader1.show_data(),i,cols[1]);
      for(int j = 0; j < reader1.show_rowdim(); j++)
	{
	  if(i!=j)
	    {
	      x_c =gsl_matrix_get(reader1.show_data(),j,cols[0]) -x;  
	      y_c =gsl_matrix_get(reader1.show_data(),j,cols[1]) -y;
	      x_c *= x_c;  
	      y_c *= y_c;  
	      distance = sqrt(x_c+y_c);
	      sample.push_back(distance);
	    }
	}
    }
  double mean = gsl_stats_mean(&sample[0],1,sample.size());
  double sd = gsl_stats_sd(&sample[0],1,sample.size());

  cout <<"Average separation: " <<mean <<"+/-" <<sd <<endl;

  return 0;

}


