/***
lens_rescale 1.0: drv file
Author: Julian Merten
JPL/Caltech
2012
This code is public
***/

/**
   This little tool rescales a given FITS map, assumed to be at a given
   redshift or redshift distribution to another unique redshift. This 
   method assumes a standard 0.7/0.3/0.7/-1 cosmology. 
**/

#include <iostream>
#include <string>
#include <exception>
#include <vector>
#include <saw/rw_fits.h>
#include <saw/map_analysis.h>
#include <tclap/CmdLine.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Coordinate transform", ' ', "0.1");
  TCLAP::MultiArg<std::string> inputArg("i","input","Input FITS, one arg if just file, two args if extension",true,"string",cmd);
  TCLAP::MultiArg<std::string> outputArg("o","output","Output FITS, one arg if just file, two args if extension",false,"string",cmd);
  TCLAP::ValueArg<double> sourceArg1("z","redshift","Initial redshift of the map",true,1.5,"double",cmd);
  TCLAP::ValueArg<double> clusterArg("c","cluster","Redshift of the lens",true,1.5,"double",cmd);
  TCLAP::ValueArg<double> sourceArg2("u","unique","Unique redshift the lens is scaled to",true,2.5,"double",cmd);
  TCLAP::MultiArg<std::string> mapArg("m","map","Input redshift FITS, one arg if just file, two args if extension",false,"string",cmd);
  cmd.parse( argc, argv );

  vector<string> input = inputArg.getValue();
  vector<string> output = outputArg.getValue();
  vector<string> map = mapArg.getValue();

  double z_i = sourceArg1.getValue();
  double z_o = sourceArg2.getValue();
  double z_c = clusterArg.getValue();

  int x,y;
  gsl_matrix *convergence_in;
  gsl_matrix *convergence_out;
  gsl_matrix *map_in;
  
  if(input.size() == 1)
    {
      x = read_intheader(input[0],"NAXIS1");
      y = read_intheader(input[0],"NAXIS2");
      convergence_in = gsl_matrix_calloc(y,x);
      convergence_out = gsl_matrix_calloc(y,x);
      read_pimg(input[0],convergence_in);	
    }
  else
    {
      x = read_intheaderext(input[0],input[1],"NAXIS1");
      y = read_intheaderext(input[0],input[1],"NAXIS2");
      convergence_in = gsl_matrix_calloc(y,x);
      convergence_out = gsl_matrix_calloc(y,x);
      read_imge(input[0],input[1],convergence_in);	
    }
  
  if(map.size() == 1)
    {
      map_in = gsl_matrix_calloc(y,x);
      read_pimg(input[0],map_in);	
    }
  else if(map.size() > 1)
    {
      map_in = gsl_matrix_calloc(y,x);
      read_imge(input[0],input[1],map_in);	
    }
  else
    {
      map_in = gsl_matrix_calloc(y,x);
      gsl_matrix_set_all(map_in,z_i);
    }
  
  convergence_out = gsl_matrix_calloc(y,x);
  
  
  astro::cosmology cosmo1(0.3,0.7,-1.0);
  double factor1, factor2, factor3, factor4, scale;
  
  factor1 = cosmo1.angularDist(z_c,z_o);
  factor2 = cosmo1.angularDist(0.0,z_o);
  
  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  factor4 = cosmo1.angularDist(z_c,gsl_matrix_get(map_in,i,j));
	  factor3 = cosmo1.angularDist(0.0,gsl_matrix_get(map_in,i,j));
	  scale = factor1/factor2 * factor3/factor4;
	  gsl_matrix_set(convergence_out,i,j,scale*gsl_matrix_get(convergence_in,i,j));
	}
    }

  if(output.size() == 1)
    {
      write_pimg(output[0],convergence_out);	  	
    }
  else
    {
      write_imge(output[0],output[1],convergence_out);
    }
  
  return 0;
  
}








