/***
    Field Stack 1.0
    Julian Merten
    JPL/Caltech 2012
    jmerten@caltech.edu

    This tool stacks the selected bootstrap realisations and creates
    mean/sd maps for all second order lensing quantities. 
***/

#include <iostream>
#include <vector>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <saw/rw_fits.h>
#include <tclap/CmdLine.h>

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("SL Selection", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The file structure of the iput files. What follows after this structure is basically the bootstrap index.",true,"./","string",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The file structure of the output files. What follows after this structure is basically the bootstrap index.",true,"./","string",cmd);
  TCLAP::ValueArg<int> BSArg("n","numBS","The number of raw bootstrap realisations.",true,1,"int",cmd);
  TCLAP::MultiArg<double> WCSArg("w","WCS","Information about the WCS of the field. Eight arguments, the first two are the reference pixl coordinates. The next two the physical reference coordinates (in deg). The last four are the transformation matrix elements.",false,"double",cmd);

  cmd.parse( argc, argv );

  string input = inputArg.getValue();
  string outputfilename = outputArg.getValue();
  int num = BSArg.getValue();
  vector<double> WCS = WCSArg.getValue();

  if(WCS.size() != 0 && WCS.size() < 8)
    {
      throw invalid_argument("WCS information is too short.");
    }
  if(num < 1)
    {
      throw invalid_argument("Invalid number of bootstraps to analyse.");
    }


  //Getting dimensions 
  string currentfilename, dummyfilename;
  dummyfilename = input + "_BS1.fits";
  int x_dim = read_intheader(dummyfilename,"NAXIS1");
  int y_dim =  read_intheader(dummyfilename,"NAXIS2");
  int dim = num*x_dim*y_dim;


  //Making some memory considerations

  if(dim > 125000000)
    {
      string console;
      cout <<"Warning: You will use more than 1GB of memory for this computation, continue?" <<endl;
      cin >>console;
      if(!(console == "y" || console == "Y" || console == "yes" || console == "Yes"))
	{
	  cout <<"Aborting." <<endl;
	  exit(0);
	}
      if(dim > 1250000000)
	{
	  cout <<"Warning: You will use more than 10GB of memory for this computation, continue?" <<endl;
	  cin >>console;
	  if(!(console == "y" || console == "Y" || console == "yes" || console == "Yes"))
	    {
	      cout <<"Aborting." <<endl;
	      exit(0);
	    }
	}
      if(dim > 12500000000)
	{
	  cout <<"Warning: You will use more than 100GB of memory for this computation, continue?" <<endl;
	  cin >>console;
	  if(!(console == "y" || console == "Y" || console == "yes" || console == "Yes"))
	    {
	      cout <<"Aborting." <<endl;
	      exit(0);
	    }
	}
    }

  //Allocating memory
  double mean;
  double sd;
  gsl_vector *sample = gsl_vector_calloc(num*x_dim*y_dim);
  gsl_matrix *currentmap = gsl_matrix_calloc(y_dim, x_dim);
  gsl_matrix *mean_map = gsl_matrix_calloc(y_dim, x_dim);
  gsl_matrix *sd_map = gsl_matrix_calloc(y_dim, x_dim);
  gsl_matrix_int *mask = gsl_matrix_int_calloc(y_dim, x_dim);
  read_imgeint(dummyfilename,"field_mask",mask);
  double z_s, z_l, smoothing_scale;
  double z_s_current, z_l_current, smoothing_scale_current;

  //Starting main loop for convergence

  int strap_counter = 0;
  cout <<"Reading convergence bootstrap: " <<flush;
  for(int l = 0; l < num; l++)
    {
      cout <<left <<setw(6);
      cout  <<l+1 <<flush;
      CURSORLEFT(6); 
      ostringstream iteration;
      iteration <<l+1;
      currentfilename = input +"_BS"+ iteration.str() + ".fits";
      read_pimg(currentfilename,currentmap);
      z_s = read_doubleheader(currentfilename,"Z_S");
      z_l = read_doubleheader(currentfilename,"Z_L");
      smoothing_scale = read_doubleheader(currentfilename,"S_SCALE");
      if(l != 0)
	{
	  if(z_s != z_s_current || z_l != z_l_current || smoothing_scale != smoothing_scale_current)
	    {
	      throw invalid_argument("The bootstrap sample is not uniform");
	    }
	}
      z_s_current = z_s;
      z_l_current = z_l;
      smoothing_scale_current = smoothing_scale;

      for(int i = 0; i < y_dim; i++)
	{
	  for(int j = 0; j < x_dim; j++)
	    {
	      gsl_vector_set(sample,strap_counter,gsl_matrix_get(currentmap,i,j));
	      strap_counter++;
	    }
	}
    }
  cout <<endl;
  
  size_t stride = x_dim*y_dim;
  cout <<"Processing convergence line: " <<flush;
  for(int i = 0; i < y_dim; i++)
    {
      cout <<left <<setw(6);
      cout  <<i+1 <<flush;
      CURSORLEFT(6);
      for(int j = 0; j < x_dim; j++)
	{
	  
	  mean = gsl_stats_mean (gsl_vector_ptr(sample,i*x_dim+j), stride, num);
	  sd = gsl_stats_sd_m (gsl_vector_ptr(sample,i*x_dim+j), stride, num, mean);
	  gsl_matrix_set(mean_map,i,j,mean); 
	  gsl_matrix_set(sd_map,i,j,sd);
	}
    } 
  
  cout <<endl;
  
  write_pimg(outputfilename,mean_map);
  write_header(outputfilename,"NUM_BS",num,"Number of bootstraps in this map.");
  write_header(outputfilename,"Z_S",z_s,"The source redshift of the map");
  write_header(outputfilename,"Z_L",z_l,"The lens redshift of the map");
  write_header(outputfilename,"S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


  write_imge(outputfilename, "conv_sd", sd_map);
  write_header_ext(outputfilename,"conv_sd","NUM_BS",num,"Number of bootstraps in this map.");
  write_header_ext(outputfilename,"conv_sd","Z_S",z_s,"The source redshift of the map");
  write_header_ext(outputfilename,"conv_sd","Z_L",z_l,"The lens redshift of the map");
  write_header_ext(outputfilename,"conv_sd","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


  gsl_matrix_sub(mean_map,sd_map);
  write_imge(outputfilename, "conv_min", mean_map);
  write_header_ext(outputfilename,"conv_min","NUM_BS",num,"Number of bootstraps in this map.");
  write_header_ext(outputfilename,"conv_min","Z_S",z_s,"The source redshift of the map");
  write_header_ext(outputfilename,"conv_min","Z_L",z_l,"The lens redshift of the map");
  write_header_ext(outputfilename,"conv_min","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


  gsl_matrix_add(mean_map,sd_map);
  gsl_matrix_add(mean_map,sd_map);
  write_imge(outputfilename, "conv_max", mean_map);
  write_header_ext(outputfilename,"conv_max","NUM_BS",num,"Number of bootstraps in this map.");
  write_header_ext(outputfilename,"conv_max","Z_S",z_s,"The source redshift of the map");
  write_header_ext(outputfilename,"conv_max","Z_L",z_l,"The lens redshift of the map");
  write_header_ext(outputfilename,"conv_max","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");
  
    if(WCS.size() != 0)
      {
	add_WCS(outputfilename,WCS,"J2000");
	add_WCS(outputfilename,"conv_sd",WCS,"J2000");
	add_WCS(outputfilename,"conv_min",WCS,"J2000");
	add_WCS(outputfilename,"conv_max",WCS,"J2000");
      }

    //Doing the same trick for shear1

    strap_counter = 0;
    cout <<"Reading shear1 bootstrap: " <<flush;
    for(int l = 0; l < num; l++)
      {
	cout <<left <<setw(6);
	cout  <<l+1 <<flush;
	CURSORLEFT(6); 
	ostringstream iteration;
	iteration <<l+1;
	currentfilename = input +"_BS"+ iteration.str() + ".fits";
	read_imge(currentfilename,"shear1",currentmap);
	z_s = read_doubleheaderext(currentfilename,"shear1","Z_S");
	z_l = read_doubleheaderext(currentfilename,"shear1","Z_L");
	smoothing_scale = read_doubleheaderext(currentfilename,"shear1","S_SCALE");
	if(l != 0)
	  {
	    if(z_s != z_s_current || z_l != z_l_current || smoothing_scale != smoothing_scale_current)
	      {
		throw invalid_argument("The bootstrap sample is not uniform");
	      }
	}
	z_s_current = z_s;
	z_l_current = z_l;
	smoothing_scale_current = smoothing_scale;
	
	for(int i = 0; i < y_dim; i++)
	  {
	    for(int j = 0; j < x_dim; j++)
	      {
		gsl_vector_set(sample,strap_counter,gsl_matrix_get(currentmap,i,j));
		strap_counter++;
	      }
	  }
      }
    cout <<endl;
    
    stride = x_dim*y_dim;
    cout <<"Processing shear1 line: " <<flush;
    for(int i = 0; i < y_dim; i++)
      {
	cout <<left <<setw(6);
	cout  <<i+1 <<flush;
	CURSORLEFT(6);
	for(int j = 0; j < x_dim; j++)
	  {
	    
	    mean = gsl_stats_mean (gsl_vector_ptr(sample,i*x_dim+j), stride, num);
	    sd = gsl_stats_sd_m (gsl_vector_ptr(sample,i*x_dim+j), stride, num, mean);
	    gsl_matrix_set(mean_map,i,j,mean); 
	    gsl_matrix_set(sd_map,i,j,sd);
	  }
      } 
    
    cout <<endl;
    
    write_imge(outputfilename,"shear1",mean_map);
    write_header_ext(outputfilename,"shear1","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"shear1","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"shear1","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"shear1","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    write_imge(outputfilename, "shear1_sd", sd_map);
    write_header_ext(outputfilename,"shear1_sd","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"shear1_sd","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"shear1_sd","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"shear1_sd","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    gsl_matrix_sub(mean_map,sd_map);
    write_imge(outputfilename, "shear1_min", mean_map);
    write_header_ext(outputfilename,"shear1_min","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"shear1_min","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"shear1_min","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"shear1_min","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    gsl_matrix_add(mean_map,sd_map);
    gsl_matrix_add(mean_map,sd_map);
    write_imge(outputfilename, "shear1_max", mean_map);
    write_header_ext(outputfilename,"shear1_max","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"shear1_max","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"shear1_max","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"shear1_max","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    if(WCS.size() != 0)
      {
	add_WCS(outputfilename,"shear1",WCS,"J2000");
	add_WCS(outputfilename,"shear1_sd",WCS,"J2000");
	add_WCS(outputfilename,"shear1_min",WCS,"J2000");
	add_WCS(outputfilename,"shear1_max",WCS,"J2000");
      }

    //And for shear2


    strap_counter = 0;
    cout <<"Reading shear2 bootstrap: " <<flush;
    for(int l = 0; l < num; l++)
      {
	cout <<left <<setw(6);
	cout  <<l+1 <<flush;
	CURSORLEFT(6); 
	ostringstream iteration;
	iteration <<l+1;
	currentfilename = input +"_BS"+ iteration.str() + ".fits";
	read_imge(currentfilename,"shear2",currentmap);
	z_s = read_doubleheaderext(currentfilename,"shear2","Z_S");
	z_l = read_doubleheaderext(currentfilename,"shear2","Z_L");
	smoothing_scale = read_doubleheaderext(currentfilename,"shear2","S_SCALE");
	if(l != 0)
	  {
	    if(z_s != z_s_current || z_l != z_l_current || smoothing_scale != smoothing_scale_current)
	      {
		throw invalid_argument("The bootstrap sample is not uniform");
	      }
	}
	z_s_current = z_s;
	z_l_current = z_l;
	smoothing_scale_current = smoothing_scale;
	
	for(int i = 0; i < y_dim; i++)
	  {
	    for(int j = 0; j < x_dim; j++)
	      {
		gsl_vector_set(sample,strap_counter,gsl_matrix_get(currentmap,i,j));
		strap_counter++;
	      }
	  }
      }
    cout <<endl;
    
    stride = x_dim*y_dim;
    cout <<"Processing shear2 line: " <<flush;
    for(int i = 0; i < y_dim; i++)
      {
	cout <<left <<setw(6);
	cout  <<i+1 <<flush;
	CURSORLEFT(6);
	for(int j = 0; j < x_dim; j++)
	  {
	    
	    mean = gsl_stats_mean (gsl_vector_ptr(sample,i*x_dim+j), stride, num);
	    sd = gsl_stats_sd_m (gsl_vector_ptr(sample,i*x_dim+j), stride, num, mean);
	    gsl_matrix_set(mean_map,i,j,mean); 
	    gsl_matrix_set(sd_map,i,j,sd);
	  }
      } 
    
    cout <<endl;
    
    write_imge(outputfilename,"shear2",mean_map);
    write_header_ext(outputfilename,"shear2","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"shear2","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"shear2","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"shear2","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    write_imge(outputfilename, "shear2_sd", sd_map);
    write_header_ext(outputfilename,"shear2_sd","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"shear2_sd","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"shear2_sd","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"shear2_sd","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    gsl_matrix_sub(mean_map,sd_map);
    write_imge(outputfilename, "shear2_min", mean_map);
    write_header_ext(outputfilename,"shear2_min","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"shear2_min","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"shear2_min","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"shear2_min","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    gsl_matrix_add(mean_map,sd_map);
    gsl_matrix_add(mean_map,sd_map);
    write_imge(outputfilename, "shear2_max", mean_map);
    write_header_ext(outputfilename,"shear2_max","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"shear2_max","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"shear2_max","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"shear2_max","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");

    if(WCS.size() != 0)
      {
	add_WCS(outputfilename,"shear2",WCS,"J2000");
	add_WCS(outputfilename,"shear2_sd",WCS,"J2000");
	add_WCS(outputfilename,"shear2_min",WCS,"J2000");
	add_WCS(outputfilename,"shear2_max",WCS,"J2000");
      }

    //And for jacdet


    strap_counter = 0;
    cout <<"Reading jacdet bootstrap: " <<flush;
    for(int l = 0; l < num; l++)
      {
	cout <<left <<setw(6);
	cout  <<l+1 <<flush;
	CURSORLEFT(6); 
	ostringstream iteration;
	iteration <<l+1;
	currentfilename = input +"_BS"+ iteration.str() + ".fits";
	read_imge(currentfilename,"jacdet",currentmap);
	z_s = read_doubleheaderext(currentfilename,"jacdet","Z_S");
	z_l = read_doubleheaderext(currentfilename,"jacdet","Z_L");
	smoothing_scale = read_doubleheaderext(currentfilename,"jacdet","S_SCALE");
	if(l != 0)
	  {
	    if(z_s != z_s_current || z_l != z_l_current || smoothing_scale != smoothing_scale_current)
	      {
		throw invalid_argument("The bootstrap sample is not uniform");
	      }
	}
	z_s_current = z_s;
	z_l_current = z_l;
	smoothing_scale_current = smoothing_scale;
	
	for(int i = 0; i < y_dim; i++)
	  {
	    for(int j = 0; j < x_dim; j++)
	      {
		gsl_vector_set(sample,strap_counter,gsl_matrix_get(currentmap,i,j));
		strap_counter++;
	      }
	  }
      }
    cout <<endl;
    
    stride = x_dim*y_dim;
    cout <<"Processing jacdet line: " <<flush;
    for(int i = 0; i < y_dim; i++)
      {
	cout <<left <<setw(6);
	cout  <<i+1 <<flush;
	CURSORLEFT(6);
	for(int j = 0; j < x_dim; j++)
	  {
	    
	    mean = gsl_stats_mean (gsl_vector_ptr(sample,i*x_dim+j), stride, num);
	    sd = gsl_stats_sd_m (gsl_vector_ptr(sample,i*x_dim+j), stride, num, mean);
	    gsl_matrix_set(mean_map,i,j,mean); 
	    gsl_matrix_set(sd_map,i,j,sd);
	  }
      } 
    
    cout <<endl;
    
    write_imge(outputfilename,"jacdet",mean_map);
    write_header_ext(outputfilename,"jacdet","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"jacdet","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"jacdet","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"jacdet","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    write_imge(outputfilename, "jacdet_sd", sd_map);
    write_header_ext(outputfilename,"jacdet_sd","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"jacdet_sd","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"jacdet_sd","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"jacdet_sd","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    gsl_matrix_sub(mean_map,sd_map);
    write_imge(outputfilename, "jacdet_min", mean_map);
    write_header_ext(outputfilename,"jacdet_min","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"jacdet_min","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"jacdet_min","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"jacdet_min","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    gsl_matrix_add(mean_map,sd_map);
    gsl_matrix_add(mean_map,sd_map);
    write_imge(outputfilename, "jacdet_max", mean_map);
    write_header_ext(outputfilename,"jacdet_max","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"jacdet_max","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"jacdet_max","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"jacdet_max","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");

    if(WCS.size() != 0)
      {
	add_WCS(outputfilename,"jacdet",WCS,"J2000");
	add_WCS(outputfilename,"jacdet_sd",WCS,"J2000");
	add_WCS(outputfilename,"jacdet_min",WCS,"J2000");
	add_WCS(outputfilename,"jacdet_max",WCS,"J2000");
      }

    //Finally for the magnification


    strap_counter = 0;
    cout <<"Reading magnification bootstrap: " <<flush;
    for(int l = 0; l < num; l++)
      {
	cout <<left <<setw(6);
	cout  <<l+1 <<flush;
	CURSORLEFT(6); 
	ostringstream iteration;
	iteration <<l+1;
	currentfilename = input +"_BS"+ iteration.str() + ".fits";
	read_imge(currentfilename,"magnification",currentmap);
	z_s = read_doubleheaderext(currentfilename,"magnification","Z_S");
	z_l = read_doubleheaderext(currentfilename,"magnification","Z_L");
	smoothing_scale = read_doubleheaderext(currentfilename,"magnification","S_SCALE");
	if(l != 0)
	  {
	    if(z_s != z_s_current || z_l != z_l_current || smoothing_scale != smoothing_scale_current)
	      {
		throw invalid_argument("The bootstrap sample is not uniform");
	      }
	}
	z_s_current = z_s;
	z_l_current = z_l;
	smoothing_scale_current = smoothing_scale;

	for(int i = 0; i < y_dim; i++)
	  {
	    for(int j = 0; j < x_dim; j++)
	      {
		gsl_vector_set(sample,strap_counter,gsl_matrix_get(currentmap,i,j));
		strap_counter++;
	      }
	  }
      }
    cout <<endl;
    
    stride = x_dim*y_dim;
    cout <<"Processing magnification line: " <<flush;
    for(int i = 0; i < y_dim; i++)
      {
	cout <<left <<setw(6);
	cout  <<i+1 <<flush;
	CURSORLEFT(6);
	for(int j = 0; j < x_dim; j++)
	  {
	    
	    mean = gsl_stats_mean (gsl_vector_ptr(sample,i*x_dim+j), stride, num);
	    sd = gsl_stats_sd_m (gsl_vector_ptr(sample,i*x_dim+j), stride, num, mean);
	    gsl_matrix_set(mean_map,i,j,mean); 
	    gsl_matrix_set(sd_map,i,j,sd);
	  }
      } 
    
    cout <<endl;
    
    write_imge(outputfilename,"magnification",mean_map);
    write_header_ext(outputfilename,"magnification","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"magnification","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"magnification","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"magnification","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    write_imge(outputfilename, "magnification_sd", sd_map);
    write_header_ext(outputfilename,"magnification_sd","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"magnification_sd","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"magnification_sd","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"magnification_sd","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    gsl_matrix_sub(mean_map,sd_map);
    write_imge(outputfilename, "magnification_min", mean_map);
    write_header_ext(outputfilename,"magnification_min","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"magnification_min","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"magnification_min","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"magnification_min","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");


    gsl_matrix_add(mean_map,sd_map);
    gsl_matrix_add(mean_map,sd_map);
    write_imge(outputfilename, "magnification_max", mean_map);
    write_header_ext(outputfilename,"magnification_max","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"magnification_max","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"magnification_max","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"magnification_max","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");

    if(WCS.size() != 0)
      {
	add_WCS(outputfilename,"magnification",WCS,"J2000");
	add_WCS(outputfilename,"magnification_sd",WCS,"J2000");
	add_WCS(outputfilename,"magnification_min",WCS,"J2000");
	add_WCS(outputfilename,"magnification_max",WCS,"J2000");
      }

    write_imgeint(outputfilename,"field_mask",mask);
    write_header_ext(outputfilename,"field_mask","NUM_BS",num,"Number of bootstraps in this map.");
    write_header_ext(outputfilename,"field_mask","Z_S",z_s,"The source redshift of the map");
    write_header_ext(outputfilename,"field_mask","Z_L",z_l,"The lens redshift of the map");
    write_header_ext(outputfilename,"field_mask","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");
    if(WCS.size() != 0)
      {
	add_WCS(outputfilename,"field_mask",WCS,"J2000");
      }

  return 0;

}
