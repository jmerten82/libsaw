#include <iostream>
#include <tclap/CmdLine.h>
#include <saw/cat_reader.h>
#include <saw/cat_writer.h>
#include <cmath>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Create new coordinates", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","Input ASCII file",true," ","string",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","Output ASCII file",true," ","string",cmd);
  TCLAP::ValueArg<double> xArg("x","x-pos","The coordinate of the new x centre of the frame",true,0.0,"double",cmd);
  TCLAP::ValueArg<double> yArg("y","y-pos","The coordinate of the new y centre of the frame",true,0.0,"double",cmd);
  TCLAP::ValueArg<double> scaleArg("s","scale","Additional scale factor for the new coordinates",false,0.0,"double",cmd);
  TCLAP::SwitchArg J2000Arg("j","j2000","Toogles the initial coordinate system to be J2000",cmd,false);
  TCLAP::MultiArg<int> columnArg("c","columns","The coordinate columns in the catalogue. Counting starts at 0",true,"int",cmd);
  cmd.parse( argc, argv );



  //Bureaucracy
  string in_file = inputArg.getValue();
  string out_file = outputArg.getValue();
  double x_ref = xArg.getValue();
  double y_ref = yArg.getValue();
  double factor = scaleArg.getValue();
  bool J2000 = J2000Arg.getValue();
  vector<int> cols = columnArg.getValue();

  //Reading input catalogue
  CatReader reader1(in_file);
  CatWriter writer1(&reader1);
  writer1.create_copy();

  //Doing the math
  double x,y;
  for(int i = 0; i < writer1.show_rowdim(); i++)
    {
      x = gsl_matrix_get(writer1.show_data(),i,cols[0]); 
      y = gsl_matrix_get(writer1.show_data(),i,cols[1]);

      x -= x_ref;
      y -= y_ref;
      if(J2000)
	{
	  x *= -1.;
	}
      if(factor != 0.0)
	{
	  x *= factor;
	  y *= factor;
	}
      gsl_matrix_set(writer1.show_data(),i,cols[0],x); 
      gsl_matrix_set(writer1.show_data(),i,cols[1],y);
    }

  writer1.write(out_file); 

  return 0;

}
