#include <iostream>
#include <gsl/gsl_matrix.h>
#include <saw/rw_fits.h>
#include <saw/map_analysis.h>
#include <smoothing.h>
#include <cosmology.h>

using namespace std;

int main()
{

    cosmology cosmo1(0.3,0.7,0.7,-1.0,0.04);

    string infile = "/home/jmerten/science/a2744/final_rec/analysis/smooth_maps/stack_highres_coordJ2000.fits";

    int x_dim = read_intheader(infile,"NAXIS1");
    int y_dim = read_intheader(infile,"NAXIS2");

    gsl_matrix *cmap1 = gsl_matrix_calloc(y_dim,x_dim);
    gsl_matrix *zmap1 = gsl_matrix_calloc(y_dim,x_dim);
    read_pimg(infile,cmap1);
    read_imge(infile,"redshifts",zmap1);
    Convergence_Map map1(cmap1,600.0,zmap1,0.308,35.5,35.5,10,&cosmo1,1.0);

    map1.find_peaks("centre_arcsec","neighbours",-236.0,284.0,138.0,198.0,false,true,"./peakout.dat");

    return 0;

}
