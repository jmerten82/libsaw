/***
add_WCS 1.0: source file
Author: Julian Merten
Inst.: ITA Heidelberg
This code is public.
***/


/**
   This little tool adds a WCS coordinate system to a given FITS file. 
   Can be added to any given image within the file.
**/

#include <iostream>
#include <string>
#include <exception>
#include <saw/rw_fits.h>
#include <saw/util.h>
#include <vector>
#include <tclap/CmdLine.h>

using namespace std;


int main(int argc, char* argv[])
{

/**
   Reading the command line using TCLAP,
   http://tclap.sourceforge.net/
**/

    TCLAP::CmdLine cmd("add_WCS", ' ', "1.0",true);
    TCLAP::ValueArg<std::string> fileArg("i","input","The input FITS file",true," ","string",cmd);

  cmd.parse( argc, argv );

/** 
    Performing the necessary user interaction.
**/

  string filename = fileArg.getValue();
  string extension = "";
  string coord_pix = "";
  string coord_WCS = "";
  string matrix = "";
  string system = "";

  cout <<"#####  add_WCS 1.0  ####" <<endl <<endl;
  cout <<"Would you like to edit primary image or an extension?  (type 'primary' or the extension name)" <<endl;
  cout <<"Extension: " <<flush;
  cin >>extension;
  cout <<"What is the origin in pixel coordinates? (type x-y coordinate separate by a colon)" <<endl;
  cout <<"Pixel centre: " <<flush;
  cin >>coord_pix;
  cout <<"What is the origin in WCS coordinates? (type x-y coordinate separate by a colon)" <<endl;
  cout <<"WCS centre: " <<flush;
  cin >>coord_WCS;
  cout <<"What are the pixel scales? (Type elements of the transformation matrix in the order C1_1, C1_2, C2_1, C2_2, sepearate by colons)" <<endl;
  cout <<"Matrix elements: " <<flush;
  cin >>matrix;
  cout <<"What kind of ccordinate system do you want to apply? (Type 'linear' or 'J2000')" <<endl;
  cout <<"System: " <<flush;
  cin >>system;

  vector<double> c_pix, w_pix, m;

  read_doubles(coord_pix,2,c_pix); 
  read_doubles(coord_WCS,2,w_pix); 
  read_doubles(matrix,4,m);

  if(extension != "primary")
      {

	  write_header_ext(filename,extension,"CRVAL1",w_pix[0]," ");
	  write_header_ext(filename,extension,"CRVAL2",w_pix[1]," ");
	  write_header_ext(filename,extension,"CRPIX1",c_pix[0]," ");
	  write_header_ext(filename,extension,"CRPIX2",c_pix[1]," ");
	  if(system !="J2000")
	      {
		  write_header_ext(filename,extension,"CTYPE1",""," ");
		  write_header_ext(filename,extension,"CTYPE2",""," ");
	      }
	  else
	      {
		  write_header_ext(filename,extension,"CTYPE1","RA---TAN"," ");
		  write_header_ext(filename,extension,"CTYPE2","DEC--TAN"," ");
	      }
	  write_header_ext(filename,extension,"CD1_1",m[0]," ");
	  write_header_ext(filename,extension,"CD1_2",m[1]," ");
	  write_header_ext(filename,extension,"CD2_1",m[2]," ");
	  write_header_ext(filename,extension,"CD2_2",m[3]," ");
      }
  else
      {
	  write_header(filename,"CRVAL1",w_pix[0]," ");
	  write_header(filename,"CRVAL2",w_pix[1]," ");
	  write_header(filename,"CRPIX1",c_pix[0]," ");
	  write_header(filename,"CRPIX2",c_pix[1]," ");
	  if(system !="J2000")
	      {
		  write_header(filename,"CTYPE1",""," ");
		  write_header(filename,"CTYPE2",""," ");
	      }
	  else
	      {
		  write_header(filename,"CTYPE1","RA---TAN"," ");
		  write_header(filename,"CTYPE2","DEC--TAN"," ");
	      }
	  write_header(filename,"CD1_1",m[0]," ");
	  write_header(filename,"CD1_2",m[1]," ");
	  write_header(filename,"CD2_1",m[2]," ");
	  write_header(filename,"CD2_2",m[3]," ");
      }

  return 0;

}






