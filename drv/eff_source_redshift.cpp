/***
    Effective Lensing depth 1.0 
    Julian Merten
    JPL/Caltech 2013
    This tool derives a radial tangential and cross shear 
    profile from an input catalogue and reference centre
***/

#include <iostream>
#include <fstream>
#include <cmath>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_roots.h>
#include <saw/cat_reader.h>
#include <astro/cosmology.h>
#include <tclap/CmdLine.h>

struct function_params
{
  double lens_redshift;
  double target_ratio;
};

double function(double x, void *params)
{
  struct function_params * this_params = (struct function_params *)params;
  double lens_redshift = this_params->lens_redshift;
  double target_ratio = this_params->target_ratio;

  astro::cosmology cosmo1(0.3,0.7,0.7,-1.0,0.04);

  double result = cosmo1.angularDist(0.0,x)/cosmo1.angularDist(lens_redshift,x) - target_ratio;

  return result;
}

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Effective lensing depth", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The input catalogue. Must contain at least one column with redshifts",true,"./","string",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","Output ASCII file summarising all information.",true,"./","string",cmd);
  TCLAP::MultiArg<int> columnArg("c","numBINS","The columns of the catalogue containing the redshift info (counting starts at 0). You need to give value, upper and lower limit column.",true,"int",cmd);
  TCLAP::ValueArg<double> redshiftArg("z","redshift","The redshift of the lens.",true,0.5,"int",cmd);

  cmd.parse( argc, argv );

  string infile = inputArg.getValue();
  string outfile = outputArg.getValue();
  vector<int> z_column = columnArg.getValue();
  double lens_redshift = redshiftArg.getValue();

  //Reading the input catalogue

  CatReader reader1(infile);
  int size = reader1.show_rowdim();

  //Defining cosmology for angular diameter distances
  astro::cosmology cosmo1(0.3,0.7,0.7,-1.0,0.04);

  //Looping through full catalogue and creating the ratio sample 
  vector<double> sample;
  vector<double> sample_low;
  vector<double> sample_high;
  vector<double> weight;

  for(int i = 0; i < size; i++)
    {
      if(gsl_matrix_get(reader1.show_data(),i,z_column[0]) > lens_redshift)
	{
	  sample.push_back(cosmo1.angularDist(0.0,gsl_matrix_get(reader1.show_data(),i,z_column[0]))/cosmo1.angularDist(lens_redshift,gsl_matrix_get(reader1.show_data(),i,z_column[0])));
	}
      if(gsl_matrix_get(reader1.show_data(),i,z_column[1]) > lens_redshift)
	{
	  sample_low.push_back(cosmo1.angularDist(0.0,gsl_matrix_get(reader1.show_data(),i,z_column[1]))/cosmo1.angularDist(lens_redshift,gsl_matrix_get(reader1.show_data(),i,z_column[1])));
	}
      if(gsl_matrix_get(reader1.show_data(),i,z_column[2]) > lens_redshift)
	{
	  sample_high.push_back(cosmo1.angularDist(0.0,gsl_matrix_get(reader1.show_data(),i,z_column[2]))/cosmo1.angularDist(lens_redshift,gsl_matrix_get(reader1.show_data(),i,z_column[2])));
	}
      if(z_column.size() > 3)
	{
	  if(gsl_matrix_get(reader1.show_data(),i,z_column[2]) > lens_redshift)
	    {
	      weight.push_back(gsl_matrix_get(reader1.show_data(),i,z_column[3]));
	    }
	}

    }

  double mean, mean_low, mean_high;

  if(z_column.size() < 4)
    {
      mean = gsl_stats_mean(&sample[0],1,sample.size());
      mean_low = gsl_stats_mean(&sample_low[0],1,sample_low.size());
      mean_high = gsl_stats_mean(&sample_high[0],1,sample_high.size());
    }
  else
    {
      mean = gsl_stats_wmean(&weight[0],1,&sample[0],1,sample.size());
      mean_low = gsl_stats_wmean(&weight[0],1,&sample_low[0],1,sample_low.size());
      mean_high = gsl_stats_wmean(&weight[0],1,&sample_high[0],1,sample_high.size());
    }


  cout <<mean <<"\t" <<mean_low <<"\t" <<mean_high <<endl; 

  //Finding the right source redshift

  gsl_function F;
  F.function = &function;
  function_params params = {lens_redshift,mean};
  F.params = &params;
  int status = GSL_CONTINUE;
  int iter = 0, max_iter = 100;
  double x_low = lens_redshift+0.001;
  double x_high = 5.0;
  double x = 0.;
  const gsl_root_fsolver_type *T;
  gsl_root_fsolver *s;
  T = gsl_root_fsolver_brent;
  s = gsl_root_fsolver_alloc (T);
  gsl_root_fsolver_set (s, &F, x_low, x_high);
  while(status == GSL_CONTINUE && iter < max_iter)
    {
      iter++;
      status = gsl_root_fsolver_iterate (s);
      x = gsl_root_fsolver_root (s);
      x_low = gsl_root_fsolver_x_lower (s);
      x_high = gsl_root_fsolver_x_upper (s);
      status = gsl_root_test_interval (x_low, x_high, 0, 0.001);
    }
  gsl_root_fsolver_free(s);
  
  //running again for upper and lower limit
  
  function_params params_low = {lens_redshift,mean_low};
  F.params = &params_low;
  status = GSL_CONTINUE;
  iter = 0; 
  max_iter = 100;
  x_low = lens_redshift+0.001;
  x_high = 5.0;
  double x_lower = 0.;
  T = gsl_root_fsolver_brent;
  s = gsl_root_fsolver_alloc (T);
  gsl_root_fsolver_set (s, &F, x_low, x_high);
  while(status == GSL_CONTINUE && iter < max_iter)
    {
      iter++;
      status = gsl_root_fsolver_iterate (s);
      x_lower = gsl_root_fsolver_root (s);
      x_low = gsl_root_fsolver_x_lower (s);
      x_high = gsl_root_fsolver_x_upper (s);
      status = gsl_root_test_interval (x_low, x_high, 0, 0.001);
    }
  gsl_root_fsolver_free(s);
  
  function_params params_high = {lens_redshift,mean_high};
  F.params = &params_high;
  status = GSL_CONTINUE;
  iter = 0; 
  max_iter = 100;
  x_low = lens_redshift+0.001;
  x_high = 5.0;
  double x_higher = 0.;
  T = gsl_root_fsolver_brent;
  s = gsl_root_fsolver_alloc (T);
  gsl_root_fsolver_set (s, &F, x_low, x_high);
  while(status == GSL_CONTINUE && iter < max_iter)
    {
      iter++;
      status = gsl_root_fsolver_iterate (s);
      x_higher = gsl_root_fsolver_root (s);
      x_low = gsl_root_fsolver_x_lower (s);
      x_high = gsl_root_fsolver_x_upper (s);
      status = gsl_root_test_interval (x_low, x_high, 0, 0.001);
    }
  gsl_root_fsolver_free(s);

  ofstream output(outfile.c_str());

  output <<"Number of valid galaxies: " <<sample.size() <<endl;
  output <<"Effective lensing redshift: " <<x <<endl;
  output <<"Lower limit: " <<x_lower <<endl;
  output <<"Upper limit: " <<x_higher <<endl;

  output.close();
  

  
  return 0;
  
}









