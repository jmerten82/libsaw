/***
    Alpha_Lens 1.0
    Derive lensing from deflection angle maps. 
    Julian Merten
    JPL/Caltech 
    May 2012
***/

#include <iostream>
#include <vector>
#include <stdexcept>
#include <tclap/CmdLine.h>
#include <gsl/gsl_matrix.h>
#include <saw/findif_fast.h>
#include <saw/rw_fits.h>
#include <astro/cosmology.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Alpha_Lens", ' ', "1.0");
  TCLAP::MultiArg<std::string> inputArg("i","input","The input FITS file. Give one argument if only one FITS image for both components, give two if two.",true,"string",cmd);
  TCLAP::MultiArg<std::string> extArg("e","extension","The FITS extensions of the image. Counts for all filename given above in the same order. Give primary for primary image.",false,"string",cmd);
  TCLAP::MultiArg<double> zArg("z","redshift","At least two arguments: The first is the redshift of the lens, the second is the source redshift of the map. If you give a third argument, the map will be scaled to this source redshift.",true,"double",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output FITS file.",true,"dummy","string",cmd);
  TCLAP::MultiArg<double> wcsArg("w","wcs","Only necessary if you want to have the lens rescaled. Three arguments: The first is the redshift of the map, the second is the redshift the map should be scaled to, the third is the redshift of the lens. If set to one read information from input.",false,"int",cmd);
  TCLAP::SwitchArg shortArg("s","short_WCS","Switch if a WCS in Adi's maps should be directly copied.",cmd);
  TCLAP::SwitchArg awkwardArg("a","awkward","Switch if a WCS in Adi's maps should be directly copied in an even more weird way.",cmd);
  cmd.parse( argc, argv );

  //Setting the stage

  vector<std::string> inputfile = inputArg.getValue();
  vector<std::string> extensions = extArg.getValue();
  vector<double> redshifts = zArg.getValue();
  string outfile = outputArg.getValue();
  vector<double> WCS = wcsArg.getValue();
  bool switchy = shortArg.getValue();
  bool switchyswitch = awkwardArg.getValue();

  //Reading the input maps

  double x1,y1,x2,y2;
  gsl_vector *a1;
  gsl_vector *a2;

  //Filename and FITS arithmetics

  if(WCS.size() > 0 && WCS.size() < 8)
    {
      throw invalid_argument("Unsufficient WCS info given");
    } 


  if(inputfile.size() == 1)
    {
      inputfile.push_back(inputfile[0]);
    }

  if(redshifts.size() < 2)
    {
      throw invalid_argument("Unsufficient redshift arguments.");
    }

  if(extensions.size() == 0) 
    {
      x1 = read_intheader(inputfile[0],"NAXIS1");
      y1 = read_intheader(inputfile[0],"NAXIS2");
      x2 = read_intheader(inputfile[1],"NAXIS1");
      y2 = read_intheader(inputfile[1],"NAXIS2");
      if(switchy)
	{
	  WCS.push_back(read_doubleheader(inputfile[0],"CRVAL1"));
	  WCS.push_back(read_doubleheader(inputfile[0],"CRVAL2"));
	  WCS.push_back(read_doubleheader(inputfile[0],"CRPIX1"));
	  WCS.push_back(read_doubleheader(inputfile[0],"CRPIX2"));
	  if(switchyswitch)
	    {
	      WCS.push_back(read_doubleheader(inputfile[0],"CD1_1"));
	    }
	  else
	    {
	      WCS.push_back(read_doubleheader(inputfile[0],"CDELT1"));
	    }
	  WCS.push_back(0.0);
	  WCS.push_back(0.0);
	  if(switchyswitch)
	    {
	      WCS.push_back(read_doubleheader(inputfile[0],"CD2_2"));
	    }
	  else
	    {
	      WCS.push_back(read_doubleheader(inputfile[0],"CDELT2"));
	    }
	}
    }
  else
    {
      if(extensions.size() < 2)
	{
	  throw invalid_argument("You have to give two extensions.");
	}
      if(extensions[0] == "primary")
	{
	  x1 = read_intheader(inputfile[0],"NAXIS1");
	  y1 = read_intheader(inputfile[0],"NAXIS2");
	  WCS.push_back(read_doubleheader(inputfile[0],"CRVAL1"));
	  WCS.push_back(read_doubleheader(inputfile[0],"CRVAL2"));
	  WCS.push_back(read_doubleheader(inputfile[0],"CRPIX1"));
	  WCS.push_back(read_doubleheader(inputfile[0],"CRPIX2"));
	  if(switchyswitch)
	    {
	      WCS.push_back(read_doubleheader(inputfile[0],"CD1_1"));
	    }
	  else
	    {
	      WCS.push_back(read_doubleheader(inputfile[0],"CDELT1"));
	    }
	  WCS.push_back(0.0);
	  WCS.push_back(0.0);
	  if(switchyswitch)
	    {
	      WCS.push_back(read_doubleheader(inputfile[0],"CD2_2"));
	    }
	  else
	    {
	      WCS.push_back(read_doubleheader(inputfile[0],"CDELT2"));
	    }
	}
      else
	{
	  x1 = read_intheaderext(inputfile[0],extensions[0],"NAXIS1");
	  y1 = read_intheaderext(inputfile[0],extensions[0],"NAXIS2");
	  WCS.push_back(read_doubleheaderext(inputfile[0],extensions[0],"CRVAL1"));
	  WCS.push_back(read_doubleheaderext(inputfile[0],extensions[0],"CRVAL2"));
	  WCS.push_back(read_doubleheaderext(inputfile[0],extensions[0],"CRPIX1"));
	  WCS.push_back(read_doubleheaderext(inputfile[0],extensions[0],"CRPIX2"));
	  if(switchyswitch)
	    {
	      WCS.push_back(read_doubleheaderext(inputfile[0],extensions[0],"CD1_1"));
	    }
	  else
	    {
	      WCS.push_back(read_doubleheaderext(inputfile[0],extensions[0],"CDELT1"));
	    }
	  WCS.push_back(0.0);
	  WCS.push_back(0.0);
	  if(switchyswitch)
	    {
	      WCS.push_back(read_doubleheaderext(inputfile[0],extensions[0],"CD2_2"));
	    }
	  else
	    {
	      WCS.push_back(read_doubleheaderext(inputfile[0],extensions[0],"CDELT2"));
	    }
	}
      if(extensions[1] == "primary")
	{
	  x2 = read_intheader(inputfile[1],"NAXIS1");
	  y2 = read_intheader(inputfile[1],"NAXIS2");
	}
      else
	{
	  x2 = read_intheaderext(inputfile[1],extensions[1],"NAXIS1");
	  y2 = read_intheaderext(inputfile[1],extensions[1],"NAXIS2");
	}
    }

  if(x1 != x2 || y1 != y2)
    {
      throw invalid_argument("Dimensions of input maps do not match.");
    }

  int x = x1;
  int y = y1;

  if(x > 4096 && y > 4096)
    {
      string console;
      cout <<"Warning: Me may run into a memory/runtime problem here. Continue?" <<endl;
      cin >>console;
      if(console != "y")
	{
	  cout <<"Aborting." <<endl;
	  exit(0);
	}
    }

  a1 = gsl_vector_calloc(y*x);
  a2 = gsl_vector_calloc(y*x);

  //Finally reading in

  if(extensions.size() == 0) 
    {
      read_pimg(inputfile[0],a1);
      read_pimg(inputfile[1],a2);
    }
  else
    {
      if(extensions[0] == "primary")
	{
	  read_pimg(inputfile[0],a1);
	}
      else
	{
	  read_imge(inputfile[0],extensions[0],a1);
	  
	}
      if(extensions[1] == "primary")
	{
	  read_pimg(inputfile[1],a2);
	}
      else
	{
	  read_imge(inputfile[1],extensions[1],a2);
	}
    }

  //Checking if the map needs to be rescaled and if so do it

  gsl_vector_scale(a1,1./(double) x);
  gsl_vector_scale(a2,1./(double) y);


  if(redshifts.size() > 2)
    {

      astro::cosmology cosmo1(0.3,0.7,-1.0);
      double scaling = cosmo1.angularDist(redshifts[0],redshifts[2]) / cosmo1.angularDist(0.0,redshifts[2]) * cosmo1.angularDist(0.0,redshifts[1]) / cosmo1.angularDist(redshifts[0],redshifts[1]);
      gsl_vector_scale(a1,scaling);
      gsl_vector_scale(a2,scaling);
    }

  //Writing deflection angles

  write_pimg(outfile,x,y,a1);
  write_imge(outfile,"a2",x,y,a2);


  //Creating the full lens

  gsl_vector *aux1 = gsl_vector_calloc(y*x);
  gsl_vector *aux2 = gsl_vector_calloc(y*x);
  gsl_vector *aux3 = gsl_vector_calloc(y*x);
  gsl_vector *res = gsl_vector_calloc(y*x);
  gsl_vector *jac = gsl_vector_calloc(y*x);



  //Findif setup

  findif_grid_fast grid1(x,y);
  cout <<"Performing finite differences..." <<flush;
  grid1.differentiate(a1,aux1,"x");
  grid1.differentiate(a2,aux2,"y");
  grid1.differentiate(a1,aux3,"y");
  cout <<"done." <<endl;

  cout <<endl;
  cout <<"Calculating convergence..." <<flush;
  for(int i = 0; i < x*y; i++)
    {
      gsl_vector_set(res,i,gsl_vector_get(aux1,i)+gsl_vector_get(aux2,i));
    }
  gsl_vector_scale(res,.5);
  write_imge(outfile,"convergence",x,y,res);
  cout <<"done." <<endl;
  for(int i = 0; i < x*y; i++)
    {
      gsl_vector_set(jac,i,pow(1.0-gsl_vector_get(res,i),2.0));
    }

  cout <<endl;
  cout <<"Calculating shear1..." <<flush;
  for(int i = 0; i < x*y; i++)
    {
      gsl_vector_set(res,i,gsl_vector_get(aux1,i)-gsl_vector_get(aux2,i));
    }
  gsl_vector_scale(res,.5);
  write_imge(outfile,"shear1",x,y,res);
  cout <<"done." <<endl;

  cout <<endl;
  cout <<"Calculating shear2..." <<flush;
  write_imge(outfile,"shear2",x,y,aux3);
  for(int i = 0; i < x*y; i++)
    {
      double value = gsl_vector_get(jac,i);
      value += - (pow(gsl_vector_get(res,i),2.0)+pow(gsl_vector_get(aux3,i),2.0));
      gsl_vector_set(jac,i,value);
      gsl_vector_set(aux1,i,1./value);
    }
  write_imge(outfile,"Jacobian",x,y,jac);
  write_imge(outfile,"Magnification",x,y,aux1);
  cout <<"done." <<endl;

  //Writing WCS

  if(WCS.size() > 0)
    {
      add_WCS(outfile, WCS, "J2000"); 
      add_WCS(outfile,"a2", WCS, "J2000"); 
      add_WCS(outfile,"convergence", WCS, "J2000");
      add_WCS(outfile,"shear1", WCS, "J2000");
      add_WCS(outfile,"shear2", WCS, "J2000");   
      add_WCS(outfile,"Jacobian", WCS, "J2000"); 
      add_WCS(outfile,"Magnification", WCS, "J2000"); 
    }

  //Adding aux info

  if(inputfile.size() == 1)
    {
      write_header(outfile,"File",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"a2","File",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"convergence","File",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"shear1","File",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"shear2","File",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"Jacobian","File",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"Magnification","File",inputfile[0],"File from which map was generated");
    }
  else
    {
      write_header(outfile,"File1",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"a2","File1",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"convergence","File1",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"shear1","File1",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"shear2","File1",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"Jacobian","File1",inputfile[0],"File from which map was generated");
      write_header_ext(outfile,"Magnification","File1",inputfile[0],"File from which map was generated");
      write_header(outfile,"File2",inputfile[1],"File from which map was generated");
      write_header_ext(outfile,"a2","File2",inputfile[1],"File from which map was generated");
      write_header_ext(outfile,"convergence","File2",inputfile[1],"File from which map was generated");
      write_header_ext(outfile,"shear1","File2",inputfile[1],"File from which map was generated");
      write_header_ext(outfile,"shear2","File2",inputfile[1],"File from which map was generated");
      write_header_ext(outfile,"Jacobian","File2",inputfile[1],"File from which map was generated");
      write_header_ext(outfile,"Magnification","File2",inputfile[1],"File from which map was generated");

    }
  if(redshifts.size() > 2)
    {
      write_header(outfile,"LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"a2","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"convergence","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"shear1","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"shear2","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"Jacobian","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"Magnification","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header(outfile,"0_SRC_Z",redshifts[1],"Original source redshift.");
      write_header_ext(outfile,"a2","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"convergence","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"shear1","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"shear2","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"Jacobian","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"Magnification","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header(outfile,"N_SRC_Z",redshifts[2],"Rescaled source redshift.");
      write_header_ext(outfile,"a2","N_SRC_Z",redshifts[2],"Redshift of the lens.");
      write_header_ext(outfile,"convergence","N_SRC_Z",redshifts[2],"Redshift of the lens.");
      write_header_ext(outfile,"shear1","N_SRC_Z",redshifts[2],"Redshift of the lens.");
      write_header_ext(outfile,"shear2","N_SRC_Z",redshifts[2],"Redshift of the lens.");
      write_header_ext(outfile,"Jacobian","N_SRC_Z",redshifts[2],"Redshift of the lens.");
      write_header_ext(outfile,"Magnification","N_SRC_Z",redshifts[2],"Redshift of the lens.");
      
    }
  else
    {
      write_header(outfile,"LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"a2","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"convergence","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"shear1","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"shear2","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"Jacobian","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header_ext(outfile,"Magnification","LENS_Z",redshifts[0],"Redshift of the lens.");
      write_header(outfile,"0_SRC_Z",redshifts[1],"Source redshift.");
      write_header_ext(outfile,"a2","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"convergence","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"shear1","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"shear2","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"Jacobian","0_SRC_Z",redshifts[1],"Redshift of the lens.");
      write_header_ext(outfile,"Magnification","0_SRC_Z",redshifts[1],"Redshift of the lens.");
    }

  return 0;
}


      






