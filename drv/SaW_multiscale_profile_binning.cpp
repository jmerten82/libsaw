/***
    SaWMultiscaleProfile 0.1
    Part of the CLASH pipeline. Create radial profiles from multi-resolution
    maps.
    Julian Merten
    JPL/Caltech 
    August 2013
***/

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <stdexcept>
#include <tclap/CmdLine.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <saw/rw_fits.h>
#include <saw/cat_reader.h>
#include <saw/util.h>

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("SaWMultiscaleProfile", ' ', "0.1");
  TCLAP::ValueArg<std::string> inputArg("i","input","The input map from which a profile should be taken.",true,"./input.fits","string",cmd);
  TCLAP::SwitchArg sawArg("s","saw","Trigger this switch if this is a SaWLens run. Redshift info will be read accordingly.",cmd);
  TCLAP::ValueArg<std::string> binArg("b","bins","An ASCII file containing the bin information. The first two lines are the centre of the profile in pixels, followed by a sequence of three, start, centre and stop for each bin.",true,"./input.fits","string",cmd);
  TCLAP::ValueArg<double> factorArg("f","factor","Add a factor here if you want to introduce a pixel scale. All distances will be multiplied by this and the bins are read in in these units.",false,1.0,"double",cmd);
  TCLAP::ValueArg<double> maskArg("m","mask","A pixel value which will be ignored during the averaging process.",false,0.0,"double",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output ASCII file contain the binned profile.",true,"./output.fits","string",cmd);

  cmd.parse( argc, argv );

  //Parsing and checking the input parameters.

  string infile = inputArg.getValue();
  bool headers = sawArg.getValue();

  if(!exists(infile))
    {
      throw invalid_argument("SaWProfile: Input map does not exist.");
    }

  string binfile = binArg.getValue();

  if(!exists(binfile))
    {
      throw invalid_argument("SaWProfile: Bin definition does not exist.");
    }

  CatReader reader1(binfile);

  if(reader1.show_rowdim() < 2)
    {
      throw invalid_argument("SaWProfile: Bin file does not contain enough information.");
    }

  if((reader1.show_rowdim()-2)%3 != 0)
    {
      throw invalid_argument("SaWProfile: Invalid bin file information.");
    }

  double mult_factor = factorArg.getValue();
  double mask_value = maskArg.getValue();

  string outfile = outputArg.getValue();

  //Parsing the input bin file.

  double x_centre = gsl_matrix_get(reader1.show_data(),0,0);
  double y_centre = gsl_matrix_get(reader1.show_data(),1,0);

  vector<double> bins;

  for(int i = 2; i < reader1.show_rowdim(); i++)
    {
      bins.push_back(gsl_matrix_get(reader1.show_data(),i,0));
    }


  if(bins.size()%3 != 0)
    {
      throw invalid_argument("SaWProfile: Invalid bin defintions.");
    }

  //Opening the map file and creating analysis map

  int x_dim,y_dim;
  x_dim = read_intheader(infile,"NAXIS1");
  y_dim = read_intheader(infile,"NAXIS2");
  gsl_matrix *map = gsl_matrix_calloc(y_dim,x_dim);
  read_pimg(infile,map);
  double map_redshift, lens_redshift;
  if(headers)
    {
      map_redshift = read_doubleheader(infile,"Z_S");
      lens_redshift = read_doubleheader(infile,"Z_L");
    }


  //Creating output file and writing aux information

  ofstream output(outfile.c_str());
  //output <<scientific;
  output <<setprecision(7);
  output <<showpoint;
  if(headers)
    {
      output <<"# Lens redshift: " <<lens_redshift <<endl;
      output <<"# Map redshift: " <<map_redshift <<endl;
    }
  output <<"# Pixel unit conversion factor used: "<<mult_factor <<endl;
  output <<"# COLUMNS: BIN_CENTER \t BIN_START \t BIN_END \t AZIMUTHAL_MEAN_IN_BIN \t AZIMUTHAL_VARIANCE_IN_BIN \t Number of points in bins" <<endl;

  //Writing down value for the central pixel

  if(x_centre >= 0 && x_centre < x_dim && y_centre >= 0 && y_centre < y_dim)
    {
      output <<mult_factor <<"\t" <<0.0 <<"\t" <<mult_factor <<"\t" <<gsl_matrix_get(map,floor(y_centre),floor(x_centre)) <<"\t" <<0.0 <<"\t" <<1 <<endl;
    }
  
  else
    {
      output <<NAN <<"\t" <<NAN <<"\t" <<NAN <<"t" <<NAN <<"\t" <<NAN <<"\t" <<0 <<endl;
    }

  //Going through bins

  for(int i = 0; i < bins.size(); i+=3)
    {
      output <<bins[i+1] <<"\t" <<bins[i] <<"\t" <<bins[i+2] <<flush;
      vector<double> sample;
      double distance;
      for(int l = 0; l < y_dim; l++)
	{
	  for(int k = 0; k < x_dim; k++)
	    {
	      distance = mult_factor*sqrt(pow((double) k-x_centre,2.)+pow((double) l-y_centre,2.));
	      if(distance > bins[i] && distance <= bins[i+2] && gsl_matrix_get(map,l,k) != mask_value && l != y_centre && k != x_centre)
		{
		  sample.push_back(gsl_matrix_get(map,l,k));
		}
	    }
	}

      output <<"\t" <<gsl_stats_mean(&sample[0],1,sample.size()) <<"\t" <<gsl_stats_variance(&sample[0],1,sample.size()) <<"\t" <<sample.size() <<endl;
    }


  output.close();

  return 0;

}
	      



    



		      





