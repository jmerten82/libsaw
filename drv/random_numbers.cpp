#include <iostream>
#include <saw/util.h>
#include <saw/rw_fits.h>
#include <gsl/gsl_matrix.h>

using namespace std;

int main()
{

  int dim = 400;

  gsl_vector *one = gsl_vector_calloc(dim);
  gsl_matrix *two = gsl_matrix_calloc(dim,dim);
  gsl_vector_int *three = gsl_vector_int_calloc(dim);
  gsl_matrix_int *four = gsl_matrix_int_calloc(dim,dim);

  create_random(one, -10.,30.);
  create_random(two,24567.3,24567.6);
  create_random_int(three,12,16,42);
  create_random_int(four,12,16,42);

  write_pimg("./random_numbers.fits",20,20,one);
  write_imge("./random_numbers.fits","d_matrix", two);
  write_imgeint("./random_numbers.fits","i_vector", 20,20,three);
  write_imgeint("./random_numbers.fits","i_matrix", four);

  return 0;

}
  

