/***
    Peak Finder 1.0
    Julian Merten
    University of Oxford 2015
    julian.merten@physics.ox.ac.uk

    This tool finds peaks in an ensemble of convergence maps
    produced by the SaWLens bootstrap routines. 
***/

#include <iostream>
#include <sstream>
#include <cmath>
#include <stdexcept>
#include <gsl/gsl_matrix.h>
#include <tclap/CmdLine.h>
#include <saw/rw_fits.h>
#include <saw/smoothing.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("WL Selection", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The file structure of the input files. What follows after this structure is basically the bootstrap index.",true,"./","string",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output filename for the peak position distribution map.",true,"./","string",cmd);
  TCLAP::ValueArg<int> BSArg("n","numBS","The number of raw bootstrap realisations.",true,1,"int",cmd);
  TCLAP::MultiArg<int> cutArg("c","cut","If this argument is given, it defines a rectangle in which peaks are search, while the rest of the map is discarded. Give x border first, then y border. 4 arguments.",false,"int",cmd);
  TCLAP::ValueArg<double> smoothingArg("s","smoothing","Set a smoothing length here if you want a Gaussian kernel applied to the input maps.",false,0.0,"double",cmd);
  cmd.parse( argc, argv );

  string input_root = inputArg.getValue();
  string outfile = outputArg.getValue();
  int num = BSArg.getValue();
  vector<int> cuts = cutArg.getValue();
  double smoothing = smoothingArg.getValue();
  bool cut;

  if(cuts.size() > 3)
    {
      cut = 1;
    }

  //Standard checks
  if(num < 1)
    {
      throw invalid_argument("Provide a valid number of bootstrap realisations.");
    }
  if(cut)
    {
      if(cuts[0] > cuts[1] || cuts[2] > cuts[3])
	{
	  throw invalid_argument("The provided cutout area is invalid");
	}
    }
  if(smoothing < 0.)
    {
      throw invalid_argument("Provide valid smoothing scale.");
    }

  //Getting map dimensions from the first bootstrap and creating output map

  string current_file = input_root + "_BS1.fits";
  int x_dim = read_intheader(current_file,"NAXIS1");
  int y_dim = read_intheader(current_file,"NAXIS2");

  gsl_matrix_int *output = gsl_matrix_int_calloc(y_dim, x_dim);

  //Looping through the total number of bootstraps

  cout <<"Processing raw realisation: " <<flush;

  if(cuts.size() < 4)
    {
      cuts.resize(4);
      cuts[0] = 0;
      cuts[1] = x_dim-1;
      cuts[2] = 0; 
      cuts[3] = y_dim-1;
    }
  else
    {
      if(cuts[0] < 0 || cuts[0] > x_dim-1)
	{
	  cuts[0] = 0;
	}
      if(cuts[1] < 0 || cuts[1] > x_dim-1)
	{
	  cuts[1] = x_dim-1;
	}
      if(cuts[2] < 0 || cuts[2] > y_dim-1)
	{
	  cuts[2] = 0;
	}
      if(cuts[3] < 0 || cuts[3] > y_dim-1)
	{
	  cuts[3] = y_dim-1;
	}
    }

  int x_dim_cut = cuts[1] - cuts[0] + 1;
  int y_dim_cut = cuts[3] - cuts[2] + 1;


  gsl_matrix *full_map = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix *cut_map = gsl_matrix_calloc(y_dim_cut, x_dim_cut);

  for(int BS = 0; BS < num; BS++)
    {
      cout <<left <<setw(6);
      cout  <<BS+1 <<flush;
      CURSORLEFT(6); 

      //Creating current filename 
      ostringstream index;
      index <<BS+1;


      //Reading map
      current_file = input_root+ "_BS" + index.str() + ".fits";
      read_pimg(current_file,full_map);

      //Smoothing map if necessary
      if(smoothing > 0.)
	{
	  gauss_smooth(full_map, full_map, smoothing);
	}

      //Cutting this map

      for(int i = 0; i < y_dim_cut; i++)
	{
	  for(int j = 0; j < x_dim_cut; j++)
	    {
	      gsl_matrix_set(cut_map,i,j,gsl_matrix_get(full_map,cuts[2]+i,cuts[0]+j));
	    }
	}

      //Finding peak in cut map
      size_t x_max, y_max;
      gsl_matrix_max_index(cut_map,&y_max,&x_max);
      gsl_matrix_int_set(output,cuts[2]+y_max,cuts[0]+x_max,gsl_matrix_int_get(output,cuts[2]+y_max,cuts[0]+x_max)+1);
    }
  cout <<endl;

  write_pimgint(outfile,output);
}
