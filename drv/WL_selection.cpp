/***
    WL_selection 1.0
    Julian Merten
    JPL/Caltech 2012
    jmerten@caltech.edu

    This tool analysis the output of the lowres bootstrapping 
    process of SaWLens. Basically just a selection and rescaling
    of the raw output is done. 
***/

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <vector>
#include <stdexcept>
#include <gsl/gsl_matrix.h>
#include <tclap/CmdLine.h>
#include <saw/rw_fits.h>
#include <saw/interpol.h>
#include <saw/util.h>
#include <astro/cosmology.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("WL Selection", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The file structure of the input files. What follows after this structure is basically the bootstrap index.",true,"./","string",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The file structure of the output files. What follows after this structure is basically the bootstrap index.",true,"./","string",cmd);
  TCLAP::ValueArg<int> BSArg("n","numBS","The number of raw bootstrap realisations.",true,1,"int",cmd);
  TCLAP::ValueArg<std::string> listArg("l","list","A simple list of resolutions, one per column, that should be checked for selection. The last resolution in the list is automatically the target resolution.",true," ","int",cmd);
  TCLAP::MultiArg<double> zArg("z","redshift","Three arguments: The first is the redshift of the map, the second is the redshift the map should be scaled to, the third is the redshift of the lens.",true,"int",cmd);
  TCLAP::ValueArg<std::string> mapArg("m","zmap","A FITS file that contains as primary image a map of the redshifts available in this field. All of the different redshift will the be scaled to a unique one, defined in the z argument.",false,"dummy","string",cmd);
  cmd.parse( argc, argv );

  string input = inputArg.getValue();
  string output = outputArg.getValue();
  int num = BSArg.getValue();
  string listfile = listArg.getValue();
  vector<double> redshift = zArg.getValue();
  string redshiftfile = mapArg.getValue();

  string currentfilename, targetfilename, outputfilename;

  double max = 1.0;
  double min = -1.0;

  astro::cosmology cosmo1(0.27,0.73,0.7,-1.0,0.04);

  //Reading the input list 
  vector<int> BSlist;


  if(!exists(listfile))
    {
      throw invalid_argument("List file does not exist");
    }
  ifstream list_in(listfile.c_str());
  int nofuture;
  while(list_in >>nofuture)
    {
      BSlist.push_back(nofuture);
    }



  string logfile = output + "_info.log";
  ofstream log(logfile.c_str());

  bool sane, write;
  int x_dim, y_dim;
  int x_dim_target, y_dim_target;
  int x_dim_inter, y_dim_inter;
  int good_counter = 0;
  int bad_counter = 0;

  cout <<"Processing raw realisation: " <<flush;

  //Looping through all bootstraps
  for(int i = 0; i < num; i++)
    {
      cout <<left <<setw(6);
      cout  <<i+1 <<flush;
      CURSORLEFT(6); 
      sane = true;
      write = false;  
      log <<i+1 <<"\t" <<flush;
      ostringstream inner_target;
      ostringstream outer_target;
      inner_target <<BSlist[BSlist.size()-1];
      outer_target <<i+1;
      targetfilename = input + "_" + inner_target.str() + "_BS" + outer_target.str() + ".fits" ;
      x_dim_target = read_intheaderext(targetfilename,"field_mask","NAXIS1");
      y_dim_target = read_intheaderext(targetfilename,"field_mask","NAXIS2");
      gsl_matrix_int *target_mask = gsl_matrix_int_calloc(y_dim_target, x_dim_target);
      gsl_matrix *target_conv = gsl_matrix_calloc(y_dim_target, x_dim_target);
      gsl_matrix *target_shear1 = gsl_matrix_calloc(y_dim_target, x_dim_target);
      gsl_matrix *target_shear2 = gsl_matrix_calloc(y_dim_target, x_dim_target);
      gsl_matrix *target_jac = gsl_matrix_calloc(y_dim_target, x_dim_target);
      gsl_matrix *target_mu = gsl_matrix_calloc(y_dim_target, x_dim_target);
      gsl_matrix *target_z = gsl_matrix_calloc(y_dim_target, x_dim_target);
      read_imgeint(targetfilename,"field_mask",target_mask);
      if(redshiftfile =="dummy")
	{
	  gsl_matrix_set_all(target_z,redshift[0]);
	}
      else
	{
	  read_pimg(redshiftfile,target_z);
	}
      //Looping through all resolutions that should be checked
      for(int j = 0; j < BSlist.size(); j++)
	{
	  //Getting the filename
	  ostringstream inner;
	  ostringstream outer;
	  inner <<BSlist[j];
	  outer <<i+1;
	  currentfilename = input + "_" + inner.str() + "_BS" + outer.str() + ".fits" ;	 
	  //Reading the FITS maps
	  x_dim = read_intheaderext(currentfilename,"convergence","NAXIS1");
	  y_dim = read_intheaderext(currentfilename,"convergence","NAXIS2");
	  gsl_matrix *conv = gsl_matrix_calloc(y_dim,x_dim);
	  gsl_matrix *shear1 = gsl_matrix_calloc(y_dim, x_dim);
	  gsl_matrix *shear2 = gsl_matrix_calloc(y_dim, x_dim);
	  read_imge(currentfilename,"convergence",conv);

	  //Checking the map
	  if(gsl_matrix_max(conv) < max && gsl_matrix_min(conv) > min)
	    {
	      if(j == BSlist.size() - 1)
		{
		  log <<"ACCEPTED" <<endl;
		  good_counter++;
		  gsl_matrix_memcpy(target_conv,conv);
		  read_imge(currentfilename,"shear1",shear1);
		  read_imge(currentfilename,"shear2",shear2);
		  x_dim_inter = target_conv->size2;
		  y_dim_inter = target_conv->size1;
		  gsl_matrix_memcpy(target_shear1,shear1);
		  gsl_matrix_memcpy(target_shear2,shear2);
		  write = true;
		}
	    }
	  else
	    {
	      if(j == 0)
		{
		  log <<"REJECTED" <<endl;
		  bad_counter++;
		}
	      else
		{
		  log <<BSlist[j-1] <<endl;
		  ostringstream inner_inter;
		  inner_inter <<BSlist[j-1];
		  string interpolfilename = input + "_" + inner_inter.str() + "_BS" + outer.str() + ".fits" ;	 
		  //Reading the FITS maps
		  x_dim_inter = read_intheader(interpolfilename,"NAXIS1");
		  y_dim_inter = read_intheader(interpolfilename,"NAXIS2");
		  gsl_matrix *conv_inter = gsl_matrix_calloc(y_dim_inter, x_dim_inter);
		  gsl_matrix *shear1_inter = gsl_matrix_calloc(y_dim_inter, x_dim_inter);
		  gsl_matrix *shear2_inter = gsl_matrix_calloc(y_dim_inter, x_dim_inter);
		  gsl_matrix_int *mask_inter = gsl_matrix_int_calloc(y_dim_inter, x_dim_inter);
		  read_imge(interpolfilename,"convergence",conv_inter);
		  read_imge(interpolfilename,"shear1",shear1_inter);
		  read_imge(interpolfilename,"shear2",shear2_inter);
		  read_imgeint(interpolfilename,"field_mask",mask_inter);
		  gsl_matrix_set_all(target_conv,1.0);
		  gsl_matrix_set_all(target_shear1,2.0);
		  gsl_matrix_set_all(target_shear2,3.0);

		  cspline_smooth_morecleverinterpol(conv_inter, mask_inter, target_conv);
		  cspline_smooth_morecleverinterpol(shear1_inter, mask_inter, target_shear1);
		  cspline_smooth_morecleverinterpol(shear2_inter, mask_inter, target_shear2);
		  good_counter++;
		  write = true;
		  gsl_matrix_free(conv_inter);
		  gsl_matrix_free(shear1_inter);
		  gsl_matrix_free(shear2_inter);
		  gsl_matrix_int_free(mask_inter);
		}
	      break;
	    }
	  gsl_matrix_free(conv);
	  gsl_matrix_free(shear1);
	  gsl_matrix_free(shear2);
	}   
      ostringstream counter;
      counter <<good_counter;
      outputfilename = output + "_BS" + counter.str() + ".fits";
      if(write)
	{
	  //Rescaling to target redshift
	    double scaling;
	    double value1, value2, value3, value4;

	    double helper1 = cosmo1.angularDist(redshift[2],redshift[1]) / cosmo1.angularDist(0.0,redshift[1]);

	    for(int l = 0; l < y_dim_target; l++)
	      {
		for(int k = 0; k < x_dim_target; k++)
		  {
		    if(gsl_matrix_int_get(target_mask,l,k) != 1)
		      {
			value1 = gsl_matrix_get(target_conv,l,k);
			value2 = gsl_matrix_get(target_shear1,l,k);
			value3 = gsl_matrix_get(target_shear2,l,k);
			scaling = cosmo1.angularDist(0.0,gsl_matrix_get(target_z,l,k))/cosmo1.angularDist(redshift[2],gsl_matrix_get(target_z,l,k));
			scaling *= helper1;
			value1 *= scaling;
			value2 *= scaling;
			value3 *= scaling;
			value4 = pow((1.0-value1),2.0) - (pow(value2,2.0)+pow(value3,2.0));
			gsl_matrix_set(target_conv,l,k,value1);
			gsl_matrix_set(target_shear1,l,k,value2);
			gsl_matrix_set(target_shear2,l,k,value3);
			gsl_matrix_set(target_jac,l,k,value4);
			gsl_matrix_set(target_mu,l,k,1./value4);
		      }
		    else
		      {
			gsl_matrix_set(target_conv,l,k,0.0);
			gsl_matrix_set(target_shear1,l,k,0.0);
			gsl_matrix_set(target_shear2,l,k,0.0);
			gsl_matrix_set(target_jac,l,k,0.0);
			gsl_matrix_set(target_mu,l,k,0.0);
		      }		    	    
		  }
	      }
	    write_pimg(outputfilename,target_conv);

	    write_header(outputfilename,"Z_S",redshift[1],"The source redshift of the map");
	    write_header(outputfilename,"Z_L",redshift[2],"The lens redshift of the map");
	    write_header(outputfilename,"O_RES_X",x_dim_inter,"The resolution from which was interpolated.");
	    write_header(outputfilename,"O_RES_Y",y_dim_inter,"The resolution from which was interpolated."); 
	    write_header(outputfilename,"S_SCALE",0.0,"Smoothing scale of the map."); 

 
	    write_imge(outputfilename,"shear1",target_shear1);

	    write_header_ext(outputfilename,"shear1","Z_S",redshift[1],"The source redshift of the map");
	    write_header_ext(outputfilename,"shear1","Z_L",redshift[2],"The lens redshift of the map");
	    write_header_ext(outputfilename,"shear1","O_RES_X",x_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"shear1","O_RES_Y",y_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"shear1","S_SCALE",0.0,"Smoothing scale of the map."); 


	    write_imge(outputfilename,"shear2",target_shear2);

	    write_header_ext(outputfilename,"shear2","Z_S",redshift[1],"The source redshift of the map");
	    write_header_ext(outputfilename,"shear2","Z_L",redshift[2],"The lens redshift of the map");
	    write_header_ext(outputfilename,"shear2","O_RES_X",x_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"shear2","O_RES_Y",y_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"shear2","S_SCALE",0.0,"Smoothing scale of the map."); 

	    write_imge(outputfilename,"jacdet",target_jac);

	    write_header_ext(outputfilename,"jacdet","Z_S",redshift[1],"The source redshift of the map");
	    write_header_ext(outputfilename,"jacdet","Z_L",redshift[2],"The lens redshift of the map");
	    write_header_ext(outputfilename,"jacdet","O_RES_X",x_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"jacdet","O_RES_Y",y_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"jacdet","S_SCALE",0.0,"Smoothing scale of the map."); 


	    write_imge(outputfilename,"magnification",target_mu);

	    write_header_ext(outputfilename,"magnification","Z_S",redshift[1],"The source redshift of the map");
	    write_header_ext(outputfilename,"magnification","Z_L",redshift[2],"The lens redshift of the map");
	    write_header_ext(outputfilename,"magnification","O_RES_X",x_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"magnification","O_RES_Y",y_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"magnification","S_SCALE",0.0,"Smoothing scale of the map."); 


	    write_imgeint(outputfilename,"field_mask",target_mask);

	    write_header_ext(outputfilename,"field_mask","Z_S",redshift[1],"The source redshift of the map");
	    write_header_ext(outputfilename,"field_mask","Z_L",redshift[2],"The lens redshift of the map");
	    write_header_ext(outputfilename,"field_mask","O_RES_X",x_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"field_mask","O_RES_Y",y_dim_inter,"The resolution from which was interpolated.");
	    write_header_ext(outputfilename,"field_mask","S_SCALE",0.0,"Smoothing scale of the map."); 
	}
      gsl_matrix_int_free(target_mask);
      gsl_matrix_free(target_conv);
      gsl_matrix_free(target_shear1);
      gsl_matrix_free(target_shear2);
      gsl_matrix_free(target_jac);
      gsl_matrix_free(target_mu);
      gsl_matrix_free(target_z);

    }
  cout <<endl;
  log.close();

  cout <<endl;
  cout <<"Good realisations: " <<good_counter <<endl;
  cout <<"Bad realisations: " <<bad_counter <<endl;
 

  return 0;

}
