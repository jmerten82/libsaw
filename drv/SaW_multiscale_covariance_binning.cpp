/***
    SaWMultiscaleCovriance 0.1
    Part of the CLASH pipeline. Create covriance matrices between profile
    bins from multi-resolution bootstraps.
    Julian Merten
    JPL/Caltech 
    August 2013
***/

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <stdexcept>
#include <tclap/CmdLine.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <saw/cat_reader.h>
#include <saw/rw_fits.h>
#include <saw/util.h>

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("SaWMultiscaleProfile", ' ', "0.1");
  TCLAP::ValueArg<std::string> inputArg("i","input","The input root for the multi-scale bootstraps.",true,"./input.fits","string",cmd);
  TCLAP::ValueArg<std::string> binArg("b","bins","An ASCII file containing the bin information. The first two lines are the centre of the profile in pixels, followed by a sequence of three, start, centre and stop for each bin.",true,"./input.fits","string",cmd);
  TCLAP::ValueArg<double> factorArg("f","factor","Add a factor here if you want to introduce a pixel scale. All distances will be multiplied by this and the bins are read in in these units.",false,1.0,"double",cmd);
  TCLAP::ValueArg<double> maskArg("m","mask","A pixel value which will be ignored during the averaging process.",false,0.0,"double",cmd);
  TCLAP::ValueArg<int> numberArg("n","BS_number","The number of input bootstraps",true,2,"int",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output root which wil become later the base for the profile ASCII and covariance FITS file.  ",true,"./output.fits","string",cmd);

  cmd.parse( argc, argv );

  //Parsing and checking the input parameters.

  string infile_root = inputArg.getValue();

  string binfile = binArg.getValue();

  if(!exists(binfile))
    {
      throw invalid_argument("SaWProfile: Bin definition does not exist.");
    }

  CatReader reader1(binfile);

  if(reader1.show_rowdim() < 2)
    {
      throw invalid_argument("SaWProfile: Bin file does not contain enough information.");
    }

  if((reader1.show_rowdim()-2)%3 != 0)
    {
      throw invalid_argument("SaWProfile: Invalid bin file information.");
    }

  double mult_factor = factorArg.getValue();
  double mask_value = maskArg.getValue();

  string out_root = outputArg.getValue();
  string outprofile = out_root+"_profile.dat";
  string outfile = out_root+"_covariance.fits";

  //Parsing the input bin file.

  double x_centre = gsl_matrix_get(reader1.show_data(),0,0);
  double y_centre = gsl_matrix_get(reader1.show_data(),1,0);

  vector<double> bins;

  for(int i = 2; i < reader1.show_rowdim(); i++)
    {
      bins.push_back(gsl_matrix_get(reader1.show_data(),i,0));
    }


  if(bins.size()%3 != 0)
    {
      throw invalid_argument("SaWProfile: Invalid bin defintions.");
    }

  int numBS = numberArg.getValue();
  int numbins = bins.size() / 3; 

  if(numBS <= 0)
    {
      throw invalid_argument("SaWCov: Invliad bootstrap number.");
    }

  //Creating big data matrix to hold the bin information later on

  gsl_matrix *bin_data = gsl_matrix_calloc(numbins+1,numBS);
  gsl_matrix *bin_variance = gsl_matrix_calloc(numbins+1,numBS);

  //Looping through all bootstrap realisations

  cout <<"Processing realisation: " <<flush;

  for(int n = 0; n < numBS; n++)
    {

      cout <<left <<setw(6);
      cout  <<n+1 <<flush;
      CURSORLEFT(6);

      //Getting righr filename
      ostringstream BS;
      BS << n+1;
      string filename;
      filename = infile_root+"_BS"+BS.str()+".fits";
      //Opening current BS map
      int x_dim,y_dim;
      x_dim = read_intheader(filename,"NAXIS1");
      y_dim = read_intheader(filename,"NAXIS2");
      gsl_matrix *map = gsl_matrix_calloc(y_dim,x_dim);
      read_pimg(filename,map);

      //Getting central pixel value

      gsl_matrix_set(bin_data,0,n,gsl_matrix_get(map,floor(y_centre),floor(x_centre)));
      gsl_matrix_set(bin_variance,0,n,0.0);

      //Going through all bin in this bootstrap

      int counter = 1;
      for(int i = 0; i < bins.size(); i+=3)
	{
	  vector<double> sample;
	  double distance;
	  for(int l = 0; l < y_dim; l++)
	    {
	      for(int k = 0; k < x_dim; k++)
		{
		  distance = mult_factor*sqrt(pow((double) k-x_centre,2.)+pow((double) l-y_centre,2.));
		  if(distance > bins[i] && distance <= bins[i+2] && gsl_matrix_get(map,l,k) != mask_value && l != y_centre && k != x_centre)
		    {
		      sample.push_back(gsl_matrix_get(map,l,k));
		    }
		}	
	    }
	  gsl_matrix_set(bin_data,counter,n,gsl_stats_mean(&sample[0],1,sample.size()));
	  gsl_matrix_set(bin_variance,counter,n,gsl_stats_variance(&sample[0],1,sample.size()));
	  counter++;
	}
    } //BS loop

  cout <<endl;

  //Creating the covariance matrix out of the bin data

  gsl_matrix *cov = gsl_matrix_calloc(numbins+1,numbins+1);

  for(int i = 0; i <cov->size1; i++)
    {
      for(int j = 0; j < cov->size2; j++)
	{
	  gsl_matrix_set(cov,i,j,gsl_stats_covariance(gsl_matrix_ptr(bin_data,i,0),1,gsl_matrix_ptr(bin_data,j,0),1,numBS));
	}
    }

  //Writing the covariance matrix into a file

  write_pimg(outfile,cov);
  write_header(outfile,"ROOT",infile_root,"The root directory of the bootstraps");
  write_header(outfile,"N_BS",numBS,"The number of bootstraps used.");

  //Creating an output ASCII file containing means and azimuthal variance in each bin

  ofstream output(outprofile.c_str());
  //output <<scientific;
  output <<setprecision(7);
  output <<showpoint;
  output <<"# Pixel unit conversion factor used: "<<mult_factor <<endl;
  output <<"# COLUMNS: BIN_CENTER \t BIN_START \t BIN_END \t AZIMUTHAL_MEAN_IN_BIN \t AZIMUTHAL_VARIANCE_IN_BIN  \t DIAGONAL ELEMENT OF COVARIANCE_VARIANCE MATRIX " <<endl;

  output <<mult_factor <<"\t" <<0.0 <<"\t" <<mult_factor <<"\t" <<flush;
  output <<gsl_stats_mean(gsl_matrix_ptr(bin_data,0,0),1,numBS) <<"\t" <<gsl_stats_mean(gsl_matrix_ptr(bin_variance,0,0),1,numBS) <<"\t" <<gsl_matrix_get(cov,0,0) <<endl;

  int counter = 1;
  for(int i = 0; i < bins.size(); i+=3)
    {
      output <<bins[i+1] <<"\t" <<bins[i] <<"\t" <<bins[i+2] <<"\t" <<flush;
      output <<gsl_stats_mean(gsl_matrix_ptr(bin_data,counter,0),1,numBS) <<"\t" <<gsl_stats_mean(gsl_matrix_ptr(bin_variance,counter,0),1,numBS) <<"\t" <<gsl_matrix_get(cov,counter,counter) <<endl;
      counter++;
    }

  output.close();

  return 0;

}
      







