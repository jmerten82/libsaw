#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <stdexcept>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <tclap/CmdLine.h>
#include <saw/rw_fits.h>

/**

   Very simple tool to box-smooth a FITS map. Give FITS file and, if necesssary
   an extension, output FITS (extension) and the size of the smoothing 
   box. Smoothing is done by simple unweighted averaging.

**/


using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Coordinate transform", ' ', "0.1");
  TCLAP::MultiArg<std::string> inputArg("i","input","Input FITS, one arg if just file, two args if extension",false,"string",cmd);
  TCLAP::MultiArg<std::string> outputArg("o","output","Output FITS, one arg if just file, two args if extension",false,"string",cmd);
  TCLAP::ValueArg<int> boxArg("b","box","Smoothing box size",true,0.0,"int",cmd);
  cmd.parse( argc, argv );

  vector<string> input = inputArg.getValue(); 
  vector<string> output = outputArg.getValue();
  int x,y;
  int smooth = boxArg.getValue();
  gsl_matrix *map;
  gsl_matrix *sample = gsl_matrix_calloc(smooth,smooth);
  gsl_matrix *sampleweight = gsl_matrix_calloc(smooth,smooth);
  gsl_matrix *smoothmap;
  int out_x,out_y;


  if(input.size() == 0)
      {
	  throw invalid_argument("No input file given");
      }
  else if(input.size() == 1)
      {
	  x = read_intheader(input[0],"NAXIS1");
	  y = read_intheader(input[0],"NAXIS2");
	  map = gsl_matrix_calloc(y,x);
	  read_pimg(input[0],map);	
      }
  else
      {
	  x = read_intheaderext(input[0],input[1],"NAXIS1");
	  y = read_intheaderext(input[0],input[1],"NAXIS2");
	  map = gsl_matrix_calloc(y,x);
	  read_imge(input[0],input[1],map);	
      }

  if(map->size1 < smooth || map->size2 < smooth)
      {
	  throw invalid_argument("Your map is not big enough for smoothing");
      }

  out_x = ceil((double) x / (double) smooth);
  out_y = ceil((double) y / (double) smooth);

  smoothmap = gsl_matrix_calloc(out_y,out_x);

  for(int i = 0; i < out_y; i++)
      {
	  for(int j = 0; j < out_x; j++)
	      {
		  double value = 0.0;
		  gsl_matrix_set_all(sample,0.0);
		  gsl_matrix_set_all(sampleweight,0.0);
		  for(int l = 0; l < smooth; l++)
		      {
			  for(int k = 0; k < smooth; k++)
			      {
				  if(i*smooth+l < y && j*smooth+k < x)
				      {
					  gsl_matrix_set(sample,l,k,gsl_matrix_get(map,i*smooth+l,j*smooth+k));
					  gsl_matrix_set(sampleweight,l,k,1.0);
				      }
				  else
				      {
					  gsl_matrix_set(sampleweight,l,k,0.0);
				      }
			      }
		      }
		  value = gsl_stats_wmean(gsl_matrix_ptr(sampleweight,0,0),1,gsl_matrix_ptr(sample,0,0),1,sample->size1*sample->size2);
		  gsl_matrix_set(smoothmap,i,j,value);
	      }
      }

  if(output.size() == 0)
      {
	  throw invalid_argument("No output file given");
      }
  else if(input.size() == 1)
      {
	  write_pimg(output[0],smoothmap);	  
      }
  else
      {
	  write_imge(output[0],output[2],smoothmap);	  
      }

  return 0;
}
