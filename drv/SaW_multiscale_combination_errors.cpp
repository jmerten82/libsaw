/***
    SaWCombine Errors 0.1
    Part of the CLASH pipeline. Combine multi-resolution lensing errors maps from SaWLens 
    Julian Merten
    JPL/Caltech 
    August 2013
***/

#include <iostream>
#include <vector>
#include <stdexcept>
#include <tclap/CmdLine.h>
#include <gsl/gsl_matrix.h>
#include <saw/rw_fits.h>
#include <astro/cosmology.h>

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("SaWCombineError", ' ', "0.1");
  TCLAP::MultiArg<std::string> inputArg("i","input","The three input FITS files. The first one is the lowres one, the second one is the medres error map, the third one is the highres map.",true,"string",cmd);
  TCLAP::MultiArg<double> fieldArg("f","field-size","Thw two field sizes in x-dimension for the lowres map (first arg) and the two highres maps (second arg)",true,"double",cmd);
  TCLAP::MultiArg<int> samplingArg("s","over-sampling","The first argument is the oversampling factor for the medres map, the second the factor for highres map.",true,"int",cmd);
  TCLAP::ValueArg<int> cutxArg("x","x-cuts","If you want to set the x insertion point manually, you can do it here.",false,0,"int",cmd);
  TCLAP::ValueArg<int> cutyArg("y","y-cuts","If you want to set the y insertion point manually, you can do it here.",false,0,"int",cmd);
  TCLAP::ValueArg<int> sdArg("n","sd-level","The number of standard deviations you want to show in the map",false,1,"int",cmd);
  TCLAP::MultiArg<double> wcsArg("w","wcs","Offers the possibility to add a WCS system to your final output maps. Expects 4 numbers, the pixel centre and the WCS centre.",false,"int",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output FITS file.",true,"./output.fits","string",cmd);

  cmd.parse( argc, argv );

  //Parsing input arguments

  vector<std::string> files = inputArg.getValue();
  string outfile = outputArg.getValue();
  vector<double> fields = fieldArg.getValue();

  if(fields[0] < fields[1])
    {
      throw invalid_argument("SaWComb: The fields you are trying to combine are incompatible");
    }

  vector<double> WCS = wcsArg.getValue();

  if(WCS.size() < 4 && WCS.size() != 0)
    {
      throw invalid_argument("SaWComb: Invalid WCS information given.");
    }

  int SD = abs(sdArg.getValue());
  if(SD == 0)
    {
      throw invalid_argument("SaWComb: SD factor must not be 0");
    }

  vector<int> cuts;
  cuts.resize(2);
  cuts[0] = cutxArg.getValue();
  cuts[1] = cutyArg.getValue();

  if((cuts[0] == 0 || cuts[1] == 0) && cuts[0] != cuts[1])
    {
      throw invalid_argument("SaWComb: Invalid entry points.");
    }

  //Getting pixel information from the input maps
  int x_lowres = read_intheader(files[0],"NAXIS1");
  int y_lowres = read_intheader(files[0],"NAXIS2");
  int x_medres = read_intheader(files[1],"NAXIS1");
  int y_medres = read_intheader(files[1],"NAXIS2");
  int x_highres = read_intheader(files[2],"NAXIS1");
  int y_highres = read_intheader(files[2],"NAXIS2");

  //Checking input maps for redshift compatibility

  int z_map_low = read_doubleheader(files[0],"Z_S"); 
  int z_map_med = read_doubleheader(files[1],"Z_S"); 
  int z_map_high = read_doubleheader(files[2],"Z_S");
  int z_lens_low = read_doubleheader(files[0],"Z_L"); 
  int z_lens_med = read_doubleheader(files[1],"Z_L"); 
  int z_lens_high = read_doubleheader(files[2],"Z_L");

  if(z_map_low != z_map_med || z_lens_low != z_lens_med)
    {
      throw invalid_argument("SaWComb: Redsifts of input maps incompatible.");
    }
  else if(z_map_high != z_map_med || z_lens_high != z_lens_med)
    {
      throw invalid_argument("SaWComb: Redsifts of input maps incompatible.");
    }

  //Figuring out oversampling

  vector<int> o_sampling = samplingArg.getValue();
  int x_medres_new = o_sampling[0]*x_medres;
  int y_medres_new = o_sampling[0]*y_medres;
  int x_highres_new = o_sampling[1]*x_highres;
  int y_highres_new = o_sampling[1]*y_highres;

  double lowres_res = fields[0]/x_lowres;
  double highres_res = fields[1]/x_highres_new;
  int field_factor = floor(lowres_res / highres_res + 0.5);

  if(x_medres_new != x_highres_new || y_medres_new != y_highres_new)
    {
      throw invalid_argument("SaWComb: Invalid numbers for perfect over-sampling.");
    }

  int x_lowres_new = field_factor*x_lowres;
  int y_lowres_new = field_factor*y_lowres;

  //Reading input maps

  cout <<"reading maps" <<endl;

  gsl_matrix *in_conv_low = gsl_matrix_calloc(y_lowres,x_lowres);
  gsl_matrix *in_conv_med = gsl_matrix_calloc(y_medres,x_medres);
  gsl_matrix *in_conv_high = gsl_matrix_calloc(y_highres,x_highres);
  read_imge(files[0],"conv_sd",in_conv_low);
  read_imge(files[1],"conv_sd",in_conv_med);
  read_imge(files[2],"conv_sd",in_conv_high);

  gsl_matrix *in_shear1_low = gsl_matrix_calloc(y_lowres,x_lowres);
  gsl_matrix *in_shear1_med = gsl_matrix_calloc(y_medres,x_medres);
  gsl_matrix *in_shear1_high = gsl_matrix_calloc(y_highres,x_highres);
  read_imge(files[0],"shear1_sd",in_shear1_low);
  read_imge(files[1],"shear1_sd",in_shear1_med);
  read_imge(files[2],"shear1_sd",in_shear1_high);

  gsl_matrix *in_shear2_low = gsl_matrix_calloc(y_lowres,x_lowres);
  gsl_matrix *in_shear2_med = gsl_matrix_calloc(y_medres,x_medres);
  gsl_matrix *in_shear2_high = gsl_matrix_calloc(y_highres,x_highres);
  read_imge(files[0],"shear2_sd",in_shear2_low);
  read_imge(files[1],"shear2_sd",in_shear2_med);
  read_imge(files[2],"shear2_sd",in_shear2_high);

  gsl_matrix *in_jacdet_low = gsl_matrix_calloc(y_lowres,x_lowres);
  gsl_matrix *in_jacdet_med = gsl_matrix_calloc(y_medres,x_medres);
  gsl_matrix *in_jacdet_high = gsl_matrix_calloc(y_highres,x_highres);
  read_imge(files[0],"jacdet_sd",in_jacdet_low);
  read_imge(files[1],"jacdet_sd",in_jacdet_med);
  read_imge(files[2],"jacdet_sd",in_jacdet_high);

  gsl_matrix *in_magnification_low = gsl_matrix_calloc(y_lowres,x_lowres);
  gsl_matrix *in_magnification_med = gsl_matrix_calloc(y_medres,x_medres);
  gsl_matrix *in_magnification_high = gsl_matrix_calloc(y_highres,x_highres);
  read_imge(files[0],"magnification_sd",in_magnification_low);
  read_imge(files[1],"magnification_sd",in_magnification_med);
  read_imge(files[2],"magnification_sd",in_magnification_high);


  //Oversampling the maps

  gsl_matrix *conv_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
  gsl_matrix *shear1_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
  gsl_matrix *shear2_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
  gsl_matrix *jacdet_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);
  gsl_matrix *magnification_low = gsl_matrix_calloc(y_lowres_new,x_lowres_new);

  for(int i = 0; i < y_lowres; i++)
    {
      for(int j = 0; j < x_lowres; j++)
	{
	  for(int l = 0; l < field_factor; l++)
	    {
	      for(int k = 0; k < field_factor; k++)
		{
		  gsl_matrix_set(conv_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_conv_low,i,j));
		  gsl_matrix_set(shear1_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_shear1_low,i,j));
		  gsl_matrix_set(shear2_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_shear2_low,i,j));
		  gsl_matrix_set(jacdet_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_jacdet_low,i,j));
		  gsl_matrix_set(magnification_low, i*field_factor+l, j*field_factor+k,gsl_matrix_get(in_magnification_low,i,j));
		}
	    }
	}
    }

  gsl_matrix *conv_med = gsl_matrix_calloc(y_medres_new,x_medres_new);
  gsl_matrix *shear1_med = gsl_matrix_calloc(y_medres_new,x_medres_new);
  gsl_matrix *shear2_med = gsl_matrix_calloc(y_medres_new,x_medres_new);
  gsl_matrix *jacdet_med = gsl_matrix_calloc(y_medres_new,x_medres_new);
  gsl_matrix *magnification_med = gsl_matrix_calloc(y_medres_new,x_medres_new);

  for(int i = 0; i < y_medres; i++)
    {
      for(int j = 0; j < x_medres; j++)
	{
	  for(int l = 0; l < o_sampling[0]; l++)
	    {
	      for(int k = 0; k < o_sampling[0]; k++)
		{
		  gsl_matrix_set(conv_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_conv_med,i,j));
		  gsl_matrix_set(shear1_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_shear1_med,i,j));
		  gsl_matrix_set(shear2_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_shear2_med,i,j));
		  gsl_matrix_set(jacdet_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_jacdet_med,i,j));
		  gsl_matrix_set(magnification_med, i*o_sampling[0]+l, j*o_sampling[0]+k,gsl_matrix_get(in_magnification_med,i,j));
		}
	    }
	}
    }

  gsl_matrix *conv_high = gsl_matrix_calloc(y_highres_new,x_highres_new);
  gsl_matrix *shear1_high = gsl_matrix_calloc(y_highres_new,x_highres_new);
  gsl_matrix *shear2_high = gsl_matrix_calloc(y_highres_new,x_highres_new);
  gsl_matrix *jacdet_high = gsl_matrix_calloc(y_highres_new,x_highres_new);
  gsl_matrix *magnification_high = gsl_matrix_calloc(y_highres_new,x_highres_new);

  for(int i = 0; i < y_highres; i++)
    {
      for(int j = 0; j < x_highres; j++)
	{
	  for(int l = 0; l < o_sampling[1]; l++)
	    {
	      for(int k = 0; k < o_sampling[1]; k++)
		{
		  gsl_matrix_set(conv_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_conv_high,i,j));
		  gsl_matrix_set(shear1_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_shear1_high,i,j));
		  gsl_matrix_set(shear2_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_shear2_high,i,j));
		  gsl_matrix_set(jacdet_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_jacdet_high,i,j));
		  gsl_matrix_set(magnification_high, i*o_sampling[1]+l, j*o_sampling[1]+k,gsl_matrix_get(in_magnification_high,i,j));
		}
	    }
	}
    }


  if(cuts[0] == 0)
    {
      cout <<"The final pixel map will be: " <<x_lowres_new <<"x" <<y_lowres_new <<". The linear size ratio between lowres part and med/highres inset is: " <<field_factor <<" and the inset will occupy " <<x_highres_new <<"x" <<y_highres_new <<" pixels. Where would you like to insert the med/highres part in over-sampled pixel coordinates (0 offset)?" <<endl;
      cout <<"x-cut: " <<flush; 
      cin >>cuts[0];
      cout <<"y-cut: " <<flush;
      cin >>cuts[1];
    }

  if(cuts[0] < 0 || cuts[0] >= x_lowres_new || cuts[1] < 0 || cuts[1] >= y_lowres_new)
    {
      throw invalid_argument("SaWComb: Invalid inset locations.");
    }


  double value;

  for(int i = 0; i < y_highres_new; i++)
    {
      for(int j = 0; j < x_highres_new; j++)
	{
	  if(gsl_matrix_get(conv_med,i,j) > gsl_matrix_get(conv_high,i,j))
	    {
	      value = gsl_matrix_get(conv_med,i,j);
	    }
	  else
	    {
	      value = gsl_matrix_get(conv_high,i,j);
	    }
	  if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
	    {
	      value = (3.*gsl_matrix_get(conv_low,cuts[1]+i,cuts[0]+j)+value)/4.;
	    }
	  else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
	    {
	      value = (2.*gsl_matrix_get(conv_low,cuts[1]+i,cuts[0]+j)+value)/3.;
	    }
	  else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
	    {
	      value = (gsl_matrix_get(conv_low,cuts[1]+i,cuts[0]+j)+value)/2.;
	    }
	  gsl_matrix_set(conv_low,cuts[1]+i,cuts[0]+j,value);
	  




	  if(gsl_matrix_get(shear1_med,i,j) > gsl_matrix_get(shear1_high,i,j))
	    {
	      value = gsl_matrix_get(shear1_med,i,j);
	    }
	  else
	    {
	      value = gsl_matrix_get(shear1_high,i,j);
	    }
	  if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
	    {
	      value = (3.*gsl_matrix_get(shear1_low,cuts[1]+i,cuts[0]+j)+value)/4.;
	    }
	  else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
	    {
	      value = (2.*gsl_matrix_get(shear1_low,cuts[1]+i,cuts[0]+j)+value)/3.;
	    }
	  else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
	    {
	      value = (gsl_matrix_get(shear1_low,cuts[1]+i,cuts[0]+j)+value)/2.;
	    }
	  gsl_matrix_set(shear1_low,cuts[1]+i,cuts[0]+j,value);  

	  






	  if(gsl_matrix_get(shear2_med,i,j) > gsl_matrix_get(shear2_high,i,j))
	    {
	      value = gsl_matrix_get(shear2_med,i,j);
	    }
	  else
	    {
	      value = gsl_matrix_get(shear2_high,i,j);
	    }
	  if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
	    {
	      value = (3.*gsl_matrix_get(shear2_low,cuts[1]+i,cuts[0]+j)+value)/4.;
	    }
	  else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
	    {
	      value = (2.*gsl_matrix_get(shear2_low,cuts[1]+i,cuts[0]+j)+value)/3.;
	    }
	  else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
	    {
	      value = (gsl_matrix_get(shear2_low,cuts[1]+i,cuts[0]+j)+value)/2.;
	    }
	  gsl_matrix_set(shear2_low,cuts[1]+i,cuts[0]+j,value); 






	  
	  if(gsl_matrix_get(jacdet_med,i,j) > gsl_matrix_get(jacdet_high,i,j))
	    {
	      value = gsl_matrix_get(jacdet_med,i,j);
	    }
	  else
	    {
	      value = gsl_matrix_get(jacdet_high,i,j);
	    }
	  if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
	    {
	      value = (3.*gsl_matrix_get(jacdet_low,cuts[1]+i,cuts[0]+j)+value)/4.;
	    }
	  else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
	    {
	      value = (2.*gsl_matrix_get(jacdet_low,cuts[1]+i,cuts[0]+j)+value)/3.;
	    }
	  else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
	    {
	      value = (gsl_matrix_get(jacdet_low,cuts[1]+i,cuts[0]+j)+value)/2.;
	    }
	  gsl_matrix_set(jacdet_low,cuts[1]+i,cuts[0]+j,value);  




	  
	  if(gsl_matrix_get(magnification_med,i,j) > gsl_matrix_get(magnification_high,i,j))
	    {
	      value = gsl_matrix_get(magnification_med,i,j);
	    }
	  else
	    {
	      value = gsl_matrix_get(magnification_high,i,j);
	    }
	  if(i < field_factor/4. || (y_highres_new - i) < field_factor/4. || j < field_factor/4. || (x_highres_new - j) < field_factor/4.)
	    {
	      value = (3.*gsl_matrix_get(magnification_low,cuts[1]+i,cuts[0]+j)+value)/4.;
	    }
	  else if(i < field_factor/3. || (y_highres_new - i) < field_factor/3. || j < field_factor/3. || (x_highres_new - j) < field_factor/3.)
	    {
	      value = (2.*gsl_matrix_get(magnification_low,cuts[1]+i,cuts[0]+j)+value)/3.;
	    }

	  else if(i < field_factor/1.5 || (y_highres_new - i) < field_factor/1.5 || j < field_factor/1.5 || (x_highres_new - j) < field_factor/1.5)
	    {
	      value = (gsl_matrix_get(magnification_low,cuts[1]+i,cuts[0]+j)+value)/2.;
	    }
	  gsl_matrix_set(magnification_low,cuts[1]+i,cuts[0]+j,value);   
		
	}
    }

  //Possible scaling

  if(SD != 1)
    {
      gsl_matrix_scale(conv_low,SD);
      gsl_matrix_scale(shear1_low,SD);
      gsl_matrix_scale(shear2_low,SD);
      gsl_matrix_scale(jacdet_low,SD);
      gsl_matrix_scale(magnification_low,SD);
    }


  //Writing out the maps

  write_pimg(outfile,conv_low);
  write_imge(outfile,"shear1",shear1_low);
  write_imge(outfile,"shear2",shear2_low);
  write_imge(outfile,"jacdet",jacdet_low);
  write_imge(outfile,"magnification",magnification_low);

  //Checking for and adding WCS

  if(WCS.size() > 0)
    {
      WCS.resize(8);
      double pixel_size = fields[0]/(x_lowres_new*3600.0);
      WCS[4] = -pixel_size;
      WCS[5] = 0.;
      WCS[6] = 0.;
      WCS[7] = pixel_size;

      add_WCS(outfile,WCS,"J2000");
      add_WCS(outfile,"shear1",WCS,"J2000");
      add_WCS(outfile,"shear2",WCS,"J2000");
      add_WCS(outfile,"jacdet",WCS,"J2000");
      add_WCS(outfile,"magnification",WCS,"J2000");
    }

  return 0;

}



