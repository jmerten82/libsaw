/***
    Redshift_regions 1.0
    Creates ds9 region files from a given ASCII redshift catalogue
    Julian Merten
    JPL/Caltech May 2012
    jmerten@caltech.edu
***/

#include <vector>
#include <stdexcept>
#include <tclap/CmdLine.h>
#include <saw/cat_reader.h>
#include <saw/cat_writer.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Alpha_Lens", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The input ASCII file.",true,"dummy","string",cmd);
  TCLAP::MultiArg<int> posArg("p","position","The two position columns for the objects in the catalogue.",true,"int",cmd);
  TCLAP::MultiArg<int> auxArg("a","aux","The two columns for ID label and redshift information.",true,"int",cmd);
  TCLAP::SwitchArg j2000Arg("j","j2000","Indicates that positions are J2000 WCS.",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The output DS9 region file.",true,"dummy","string",cmd);
  cmd.parse( argc, argv );


  //Setting the stage

  string infile = inputArg.getValue();
  vector<int> pos = posArg.getValue();
  vector<int> aux = auxArg.getValue();
  bool J2000 = j2000Arg.getValue();
  string outfile = outputArg.getValue();

  if(pos.size() < 2)
    {
      throw invalid_argument("Two few columns for source position given.");
    }
  if(aux.size() < 2)
    {
      throw invalid_argument("Two few columns for auxiliary information  given.");
    }


  //Opening the catalogue

  string selection;
  double size;

  if(J2000)
    {
      selection = "J2000";
      size = 1.0;
    }
  else
    {
      selection = "image";
      size = 10;
    }

  CatReader reader1(infile);
  CatWriter writer1(&reader1);
  writer1.write_DS9_redshiftinfo(outfile,pos[0],pos[1],aux[0],aux[1],size,selection);

  return 0;

}
