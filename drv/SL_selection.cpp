/***
    SL_selection 1.0
    Julian Merten
    JPL/Caltech 2012
    jmerten@caltech.edu

    This tool analysis the output of the highres bootstrapping 
    process of SaWLens. Smoothing, selection and rescaling is performed
    on the raw bootstrapping input. 
***/

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <vector>
#include <stdexcept>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_statistics.h>
#include <tclap/CmdLine.h>
#include <saw/rw_fits.h>
#include <saw/smoothing.h>
#include <saw/util.h>
#include <astro/cosmology.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("SL Selection", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","The file structure of the iput files. What follows after this structure is basically the bootstrap index.",true,"./","string",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","The file structure of the output files. What follows after this structure is basically the bootstrap index.",true,"./","string",cmd);
  TCLAP::ValueArg<int> BSArg("n","numBS","The number of raw bootstrap realisations.",true,1,"int",cmd);
  TCLAP::MultiArg<double> zArg("z","redshift","Three arguments: The first is the redshift of the weak lensing regime, the second is the redshift the map should be scaled to, the third is the redshift of the lens.",true,"double",cmd);
  TCLAP::ValueArg<std::string> mapArg("m","zmap","The file structure of the FITS field files, which contain the redshift information on all the individual raw_bootstraps",true,"./","string",cmd);
  TCLAP::ValueArg<double> smoothArg("s","smoothing","The smoothing scale for the maps.",true,1.0,"double",cmd);
  TCLAP::MultiArg<double> boundArg("b","bounds","Allows you to set the upper and lower bound for the converence map selection.",false,"double",cmd);
  cmd.parse( argc, argv );

  string input = inputArg.getValue();
  string outputfile = outputArg.getValue();
  int num = BSArg.getValue();
  vector<double> redshift = zArg.getValue();
  string redshiftfile = mapArg.getValue();
  double smoothing_scale = smoothArg.getValue();
  vector<double> limits = boundArg.getValue();

  string currentfilename, fieldfilename, outputfilename;

  astro::cosmology cosmo1(0.27,0.73,0.7,-1.0,0.04);

  int good_counter = 0;
  int bad_counter = 0;

  //Setting limits
  double min,max;
  if(limits.size() != 0)
    {
      if(limits.size() < 2)
	{
	throw invalid_argument("You must give two limits for selection.");
	}
      else
	{
	  min = limits[0];
	  max = limits[1];
	}
    }
  else
    {
      max = 5.0;
      min = -1.0;
    }

  //Getting reconstruction dimensions and allocating matrices
  string dummy_file = input + "_BS1.fits";
  int x_dim =  read_intheader(dummy_file,"NAXIS1");
  int y_dim =  read_intheader(dummy_file,"NAXIS2");
  gsl_matrix* conv = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix* shear1 = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix* shear2 = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix* conv_s = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix* shear1_s = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix* shear2_s = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix* jacdet_s = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix* mu_s = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix* redshift_map = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix* redshift_map_s = gsl_matrix_calloc(y_dim,x_dim);
  gsl_matrix_int* mask = gsl_matrix_int_calloc(y_dim,x_dim);
  bool sane, write;

  //Main loop over all realisations.
  cout <<"Processing raw realisation: " <<flush;
  for(int i = 0; i < num; i++)
    {
      cout <<left <<setw(6);
      cout  <<i+1 <<flush;
      CURSORLEFT(6); 
      sane = true;
      write = false; 

      //Finding the filenames
      ostringstream index;
      index <<i+1;
      currentfilename = input + "_BS" + index.str() + ".fits";
      fieldfilename = redshiftfile +  "_BS" + index.str() + ".fits";

      //Reading files
      read_pimg(currentfilename,conv);
      read_imge(currentfilename,"shear1",shear1);
      read_imge(currentfilename,"shear2",shear2);
      read_imge(fieldfilename,"redshift_info",redshift_map);
      read_imgeint(fieldfilename,"field_mask",mask);

      for(int i = 0; i < redshift_map->size2; i++)
	{
	  for(int j = 0; j < redshift_map->size1; j++)
	    {
	      if(gsl_matrix_get(redshift_map,i,j) == 0.0)
		{
		  gsl_matrix_set(redshift_map,i,j,redshift[0]);
		}
	    }
	}

      //Smoothing the maps
      gauss_smooth(conv, conv_s, smoothing_scale);
      gauss_smooth(shear1, shear1_s, smoothing_scale);
      gauss_smooth(shear2, shear2_s, smoothing_scale);
      gauss_smooth(redshift_map, redshift_map_s, smoothing_scale);

      //Rescaling the maps the unique redshift and calculating jacdet
      if(sane)
	{
	  double scaling;
	  double value1, value2, value3, value4;
	  double helper1 = cosmo1.angularDist(redshift[2],redshift[1]) / cosmo1.angularDist(0.0,redshift[1]);
	  double helper2 = cosmo1.angularDist(0.0,redshift[0]) / cosmo1.angularDist(redshift[2],redshift[0]);

	  for(int l = 0; l < y_dim; l++)
	    {
	      for(int k = 0; k < x_dim; k++)
		{
		  value1 = gsl_matrix_get(conv_s,l,k);
		  value2 = gsl_matrix_get(shear1_s,l,k);
		  value3 = gsl_matrix_get(shear2_s,l,k);
		  
		  if(gsl_matrix_get(redshift_map_s,l,k) < 0.1)
		    {
		      scaling = helper1*helper2;
		    }
		  else
		    {
		      scaling = cosmo1.angularDist(0.0,gsl_matrix_get(redshift_map_s,l,k))/cosmo1.angularDist(redshift[2],gsl_matrix_get(redshift_map_s,l,k));
		      scaling *= helper1;
		    }
		  value1 *= scaling;
		  value2 *= scaling;
		  value3 *= scaling;
		  value4 = pow((1.0-value1),2.0) - (pow(value2,2.0)+pow(value3,2.0));
		  gsl_matrix_set(conv_s,l,k,value1);
		  gsl_matrix_set(shear1_s,l,k,value2);
		  gsl_matrix_set(shear2_s,l,k,value3);
		  gsl_matrix_set(jacdet_s,l,k,value4);
		  gsl_matrix_set(mu_s,l,k,1./value4);
		}//k
	    }//l
	  //Last litle checl

      //Checking the maps
	  if(gsl_matrix_max(conv_s) >= max || gsl_matrix_min(conv_s) <= min || gsl_matrix_min(jacdet_s) > 0.0)
	    {
	      bad_counter++;
	    }
	  else
	    {
	      good_counter++;
	      write = true;
	    }
	}
      
      //Constructing output filename 
      ostringstream output;
      output <<good_counter;
      outputfilename = outputfile + "_BS" + output.str() + ".fits";

      if(write)
	{
	  write_pimg(outputfilename,conv_s);
	  write_header(outputfilename,"Z_S",redshift[1],"The source redshift of the map");
	  write_header(outputfilename,"Z_L",redshift[2],"The lens redshift of the map");
	  write_header(outputfilename,"S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");
	  write_header(outputfilename,"O_RES_X",x_dim,"The resolution from which was interpolated.");
	  write_header(outputfilename,"O_RES_Y",y_dim,"The resolution from which was interpolated."); 


	  write_imge(outputfilename,"shear1", shear1_s);
	  write_header_ext(outputfilename,"shear1","Z_S",redshift[1],"The source redshift of the map");
	  write_header_ext(outputfilename,"shear1","Z_L",redshift[2],"The lens redshift of the map");
	  write_header_ext(outputfilename,"shear1","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");
	  write_header_ext(outputfilename,"shear1","O_RES_X",x_dim,"The resolution from which was interpolated.");
	  write_header_ext(outputfilename,"shear1","O_RES_Y",y_dim,"The resolution from which was interpolated.");


	  write_imge(outputfilename,"shear2", shear2_s);
	  write_header_ext(outputfilename,"shear2","Z_S",redshift[1],"The source redshift of the map");
	  write_header_ext(outputfilename,"shear2","Z_L",redshift[2],"The lens redshift of the map");
	  write_header_ext(outputfilename,"shear2","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");
	  write_header_ext(outputfilename,"shear2","O_RES_X",x_dim,"The resolution from which was interpolated.");
	  write_header_ext(outputfilename,"shear2","O_RES_Y",y_dim,"The resolution from which was interpolated.");


	  write_imge(outputfilename,"jacdet", jacdet_s);
	  write_header_ext(outputfilename,"jacdet","Z_S",redshift[1],"The source redshift of the map");
	  write_header_ext(outputfilename,"jacdet","Z_L",redshift[2],"The lens redshift of the map");
	  write_header_ext(outputfilename,"jacdet","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");
	  write_header_ext(outputfilename,"jacdet","O_RES_X",x_dim,"The resolution from which was interpolated.");
	  write_header_ext(outputfilename,"jacdet","O_RES_Y",y_dim,"The resolution from which was interpolated.");


	  write_imge(outputfilename,"magnification", mu_s);
	  write_header_ext(outputfilename,"magnification","Z_S",redshift[1],"The source redshift of the map");
	  write_header_ext(outputfilename,"magnification","Z_L",redshift[2],"The lens redshift of the map");
	  write_header_ext(outputfilename,"magnification","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");
	  write_header_ext(outputfilename,"magnification","O_RES_X",x_dim,"The resolution from which was interpolated.");
	  write_header_ext(outputfilename,"magnification","O_RES_Y",y_dim,"The resolution from which was interpolated.");


	  write_imgeint(outputfilename,"field_mask", mask);
	  write_header_ext(outputfilename,"field_mask","Z_S",redshift[1],"The source redshift of the map");
	  write_header_ext(outputfilename,"field_mask","Z_L",redshift[2],"The lens redshift of the map");
	  write_header_ext(outputfilename,"field_mask","S_SCALE",smoothing_scale,"The sigma value of the smoothing kernel.");
	  write_header_ext(outputfilename,"field_mask","O_RES_X",x_dim,"The resolution from which was interpolated.");
	  write_header_ext(outputfilename,"field_mask","O_RES_Y",y_dim,"The resolution from which was interpolated.");
	}
    }//main loop
 
  cout <<endl;
  cout <<"Good realisations: " <<good_counter <<endl;
  cout <<"Bad realisations: " <<bad_counter <<endl;

  return 0;

}
