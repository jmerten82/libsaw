#include <iostream>
#include <saw/cat_reader.h>
#include <string>
#include <vector>
#include <gsl/gsl_statistics.h>
#include <tclap/CmdLine.h>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Critline distance", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","initial","The input catalogue",true,"input.cat","string",cmd);
  cmd.parse( argc, argv );

  string file = inputArg.getValue();

  CatReader reader1(file);

  vector<double> sample;

  for(int i = 0; i < reader1.show_rowdim(); i++)
    {
      double distance;
      distance = pow(gsl_matrix_get(reader1.show_data(),i,0),2.);
      distance += pow(gsl_matrix_get(reader1.show_data(),i,1),2.);
      distance = sqrt(distance);
      sample.push_back(distance);
    }

  double mean = gsl_stats_mean(&sample[0],1,sample.size());
  double sd = gsl_stats_sd(&sample[0],1,sample.size());

  cout <<mean <<"+-" <<sd <<endl;

}



