#include <iostream>
#include <tclap/CmdLine.h>
#include <saw/cat_reader.h>
#include <saw/cat_writer.h>
#include <cmath>

using namespace std;

int main(int argc, char* argv[])
{

  TCLAP::CmdLine cmd("Rotating shears", ' ', "1.0");
  TCLAP::ValueArg<std::string> inputArg("i","input","Input ASCII file",true," ","string",cmd);
  TCLAP::ValueArg<std::string> outputArg("o","output","Output ASCII file",true," ","string",cmd);
  TCLAP::ValueArg<double> angleArg("r","angle","The angle by which the field should be rotated",true,0.0,"double",cmd);
  TCLAP::MultiArg<int> columnArg("c","columns","The shear columns in the catalogue. Counting starts at 0",true,"int",cmd);
  cmd.parse( argc, argv );


  //Input argument bureaucracy 
  string in_file = inputArg.getValue();
  string out_file = outputArg.getValue();
  double phi = angleArg.getValue();
  vector<int> cols = columnArg.getValue();


  //Handling the angles, remember shear is spin 2
  double Pi = acos(-1.);
  phi *= 1./180.;
  phi *= 2.*Pi;
  double cosfactor = cos(phi);
  double sinfactor = sin(phi);

  //Reading the catalogue
  CatReader reader1(in_file);
  CatWriter writer1(&reader1);
  double inshear1, inshear2, outshear1, outshear2;

  //Looping through entries and doing the math

  writer1.create_copy();

  for(int i = 0; i < writer1.show_rowdim(); i++)
    {
      inshear1 = gsl_matrix_get(writer1.show_data(),i,cols[0]);
      inshear2 = gsl_matrix_get(writer1.show_data(),i,cols[1]);

       outshear1 = cosfactor*inshear1 + sinfactor*inshear2;
       outshear2 = -inshear1*sinfactor + cosfactor*inshear2;

       gsl_matrix_set(writer1.show_data(),i,cols[0],outshear1);
       gsl_matrix_set(writer1.show_data(),i,cols[1],outshear2);
    }
  writer1.write(out_file);

  return 0;

}
