/***
lensmodel.cpp: source file of the libsaw library
Author: Julian Merten
Institution: ITA Heidelberg
Mail: jmerten@ita.uni-heidelberg.de
***/

#include <saw/lensmodel.h>

using namespace std;

LensModel::LensModel(FinDifGrid *input)
{

  lensgrid = input;
  coordinates = false;
  J2000 = false;
  //This is added to extend the class later by the possibility
  //to allocate only parts of the lens depending on the differtiation
  //order.
  allocationtable.resize(4);
  allocationtable[0] = true;
  allocationtable[1] = true;
  allocationtable[2] = true;
  allocationtable[3] = true;
  int size = lensgrid->show_int("numpix");
  pot = gsl_vector_calloc(size);
  a1 = gsl_vector_calloc(size);
  a2 = gsl_vector_calloc(size);
  c = gsl_vector_calloc(size);
  s1 = gsl_vector_calloc(size);
  s2 = gsl_vector_calloc(size);
  g1 = gsl_vector_calloc(size);
  g2 = gsl_vector_calloc(size);
  jacdet = gsl_vector_calloc(size);
  mu = gsl_vector_calloc(size);
  F1 = gsl_vector_calloc(size);
  F2 = gsl_vector_calloc(size);
  G1 = gsl_vector_calloc(size);
  G2 = gsl_vector_calloc(size);
}

LensModel::LensModel(FinDifGrid *input, gsl_vector *potin)
{

  lensgrid = input;
  coordinates = false;
  J2000 = false;

  int size = lensgrid->show_int("numpix");

  if(potin->size != size)
    {
      throw invalid_argument("Invalid number of pixels in the input potential");
    }

  allocationtable.resize(4);
  allocationtable[0] = true;
  allocationtable[1] = true;
  allocationtable[2] = true;
  allocationtable[3] = true;

  pot = gsl_vector_calloc(size);
  gsl_vector_memcpy(pot,potin);
  a1 = gsl_vector_calloc(size);
  a2 = gsl_vector_calloc(size);
  c = gsl_vector_calloc(size);
  s1 = gsl_vector_calloc(size);
  s2 = gsl_vector_calloc(size);
  g1 = gsl_vector_calloc(size);
  g2 = gsl_vector_calloc(size);
  jacdet = gsl_vector_calloc(size);
  mu = gsl_vector_calloc(size);
  F1 = gsl_vector_calloc(size);
  F2 = gsl_vector_calloc(size);
  G1 = gsl_vector_calloc(size);
  G2 = gsl_vector_calloc(size);

  lensgrid->mult_vec("a1",pot,a1);
  lensgrid->mult_vec("a2",pot,a2);
  lensgrid->mult_vec("c",pot,c);
  lensgrid->mult_vec("s1",pot,s1);
  lensgrid->mult_vec("s2",pot,s2);

  double aux_value = 0.0;

  for(int i = 0; i < size; i++)
    {
      gsl_vector_set(g1,i,gsl_vector_get(s1,i) / (1.0-gsl_vector_get(c,i)));
      gsl_vector_set(g2,i,gsl_vector_get(s2,i) / (1.0-gsl_vector_get(c,i)));
      aux_value = pow((1.0-gsl_vector_get(c,i)),2.0)-(pow(gsl_vector_get(s1,i),2.0))+pow(gsl_vector_get(s2,i),2.0);
      gsl_vector_set(jacdet,i,aux_value);
      gsl_vector_set(mu,i,1.0/aux_value);
    }
  aux_value = 0.0;

  lensgrid->mult_vec("f1",pot,F1);
  lensgrid->mult_vec("f2",pot,F2);
  lensgrid->mult_vec("g1",pot,G1);
  lensgrid->mult_vec("g2",pot,G2);

}

LensModel::LensModel(FinDifGrid *inputgrid, vector<double> originin, vector<double> scalein, bool J2000flag)
{

  if(originin.size() != 4)
    {
      throw invalid_argument("The origin vector is invalid");
    }
  if(scalein.size() != 4 && scalein.size() != 2 && scalein.size() != 1)
    {
      throw invalid_argument("The scale vector is invalid");
    }
  lensgrid = inputgrid;
  origin.resize(4);
  scale.resize(4);
  origin = originin;
  J2000 = J2000flag;
  if(scalein.size() == 4)
    {
      scale = scalein;
    }
  else if(scalein.size() == 2)
    {
      scale[0] = scalein[0];
      scale[1] = 0.0;
      scale[2] = 0.0;
      scale[3] = scalein[1];
    }
  else
    {
      if(J2000)
	{
	  scale[0] = -scalein[0];
	  scale[1] = 0.0;
	  scale[2] = 0.0;
	  scale[3] = scalein[0];
	}
      else
	{
	  scale[0] = scalein[0];
	  scale[1] = 0.0;
	  scale[2] = 0.0;
	  scale[3] = scalein[0];
	}

    }

  coordinates = true;

  allocationtable.resize(4);
  allocationtable[0] = true;
  allocationtable[1] = true;
  allocationtable[2] = true;
  allocationtable[3] = true;

  int size = lensgrid->show_int("numpix");
  pot = gsl_vector_calloc(size);
  a1 = gsl_vector_calloc(size);
  a2 = gsl_vector_calloc(size);
  c = gsl_vector_calloc(size);
  s1 = gsl_vector_calloc(size);
  s2 = gsl_vector_calloc(size);
  g1 = gsl_vector_calloc(size);
  g2 = gsl_vector_calloc(size);
  jacdet = gsl_vector_calloc(size);
  mu = gsl_vector_calloc(size);
  F1 = gsl_vector_calloc(size);
  F2 = gsl_vector_calloc(size);
  G1 = gsl_vector_calloc(size);
  G2 = gsl_vector_calloc(size);
}

LensModel::LensModel(FinDifGrid *input, gsl_vector *potin, vector<double> originin, vector<double> scalein, bool J2000flag)
{

  lensgrid = input;
  coordinates = true;
  J2000 = J2000flag;

  int size = lensgrid->show_int("numpix");

  if(potin->size != size)
    {
      throw invalid_argument("Invalid number of pixels in the input potential");
    }
  if(originin.size() != 4)
    {
      throw invalid_argument("The origin vector is invalid");
    }
  if(scalein.size() != 4 && scalein.size() != 2 && scalein.size() != 1)
    {
      throw invalid_argument("The scale vector is invalid");
    }
  origin.resize(4);
  scale.resize(4);
  origin = originin;
  J2000 = J2000flag;
  if(scalein.size() == 4)
    {
      scale = scalein;
    }
  else if(scalein.size() == 2)
    {
      scale[0] = scalein[0];
      scale[1] = 0.0;
      scale[2] = 0.0;
      scale[3] = scalein[1];
    }
  else
    {
      if(J2000)
	{
	  scale[0] = -scalein[0];
	  scale[1] = 0.0;
	  scale[2] = 0.0;
	  scale[3] = scalein[0];
	}
      else
	{
	  scale[0] = scalein[0];
	  scale[1] = 0.0;
	  scale[2] = 0.0;
	  scale[3] = scalein[0];
	}

    }
 
  allocationtable.resize(4);
  allocationtable[0] = true;
  allocationtable[1] = true;
  allocationtable[2] = true;
  allocationtable[3] = true;

  pot = gsl_vector_calloc(size);
  gsl_vector_memcpy(pot,potin);
  a1 = gsl_vector_calloc(size);
  a2 = gsl_vector_calloc(size);
  c = gsl_vector_calloc(size);
  s1 = gsl_vector_calloc(size);
  s2 = gsl_vector_calloc(size);
  g1 = gsl_vector_calloc(size);
  g2 = gsl_vector_calloc(size);
  jacdet = gsl_vector_calloc(size);
  mu = gsl_vector_calloc(size);
  F1 = gsl_vector_calloc(size);
  F2 = gsl_vector_calloc(size);
  G1 = gsl_vector_calloc(size);
  G2 = gsl_vector_calloc(size);

  lensgrid->mult_vec("a1",pot,a1);
  lensgrid->mult_vec("a2",pot,a2);
  lensgrid->mult_vec("c",pot,c);
  lensgrid->mult_vec("s1",pot,s1);
  lensgrid->mult_vec("s2",pot,s2);

  double aux_value = 0.0;

  for(int i = 0; i < size; i++)
    {
      gsl_vector_set(g1,i,gsl_vector_get(s1,i) / (1.0-gsl_vector_get(c,i)));
      gsl_vector_set(g2,i,gsl_vector_get(s2,i) / (1.0-gsl_vector_get(c,i)));
      aux_value = pow((1.0-gsl_vector_get(c,i)),2.0)-(pow(gsl_vector_get(s1,i),2.0))+pow(gsl_vector_get(s2,i),2.0);
      gsl_vector_set(jacdet,i,aux_value);
      gsl_vector_set(mu,i,1.0/aux_value);
    }
  aux_value = 0.0;

  lensgrid->mult_vec("f1",pot,F1);
  lensgrid->mult_vec("f2",pot,F2);
  lensgrid->mult_vec("g1",pot,G1);
  lensgrid->mult_vec("g2",pot,G2);

}

LensModel::~LensModel()
{
  if(allocationtable[0])
    {
      gsl_vector_free(pot);
    }
  if(allocationtable[1])
    {
      gsl_vector_free(a1);
      gsl_vector_free(a2);
    }
  if(allocationtable[2])
    {
      gsl_vector_free(s1);
      gsl_vector_free(s2);
      gsl_vector_free(c);
      gsl_vector_free(g1);
      gsl_vector_free(g2);
      gsl_vector_free(jacdet);
      gsl_vector_free(mu);
    }
  if(allocationtable[3])
    {
      gsl_vector_free(F1);
      gsl_vector_free(F2);
      gsl_vector_free(G1);
      gsl_vector_free(G2);
    }
}



int LensModel::show_int(string selection)
{

  if(selection == "x_dim")
    {
      return lensgrid->show_int("x_dim");
    }
  else if(selection == "y_dim")
    {
      return lensgrid->show_int("y_dim");
    }
  else if(selection == "numpix")
    {
      return lensgrid->show_int("numpix");
    }
  else
    {
      throw invalid_argument("Invalid selection for Lens show_int");
    }
}

double LensModel::show_double(string selection)
{
  if(!coordinates)
    {
      throw invalid_argument("This Lens field has no coordinates");
    }
  if(selection == "pixel_origin_x")
    {
      return origin[0];
    }
  else if(selection == "pixel_origin_y")
    {
      return origin[1];
    }
  else if(selection == "physical_origin_x")
    {
      return origin[2];
    }
  else if(selection == "physical_origin_y")
    {
      return origin[3];
    }
  else
    {
      throw invalid_argument("Invalid selection for Lens show_double");
    }
}

void LensModel::build_lens_pot()
{

  if(gsl_vector_isnull(pot))
    {
      cout <<"Warning: Potential of lens looks flat and zero" <<endl;
    }

  lensgrid->mult_vec("a1",pot,a1);
  lensgrid->mult_vec("a2",pot,a2);
  lensgrid->mult_vec("c",pot,c);
  lensgrid->mult_vec("s1",pot,s1);
  lensgrid->mult_vec("s2",pot,s2);

  int size = lensgrid->show_int("numpix");

  int aux_value = 0.0;

  for(int i = 0; i < size; i++)
    {
      gsl_vector_set(g1,i,gsl_vector_get(s1,i) / (1.0-gsl_vector_get(c,i)));
      gsl_vector_set(g2,i,gsl_vector_get(s2,i) / (1.0-gsl_vector_get(c,i)));
      aux_value = pow((1.0-gsl_vector_get(c,i)),2.0)-(pow(gsl_vector_get(s1,i),2.0))+pow(gsl_vector_get(s2,i),2.0);
      gsl_vector_set(jacdet,i,aux_value);
      gsl_vector_set(mu,i,1.0/aux_value);
    }
  aux_value = 0.0;

  lensgrid->mult_vec("f1",pot,F1);
  lensgrid->mult_vec("f2",pot,F2);
  lensgrid->mult_vec("g1",pot,G1);
  lensgrid->mult_vec("g2",pot,G2);
}

void LensModel::build_lens_nopot()
{

  if(gsl_vector_isnull(s1) || gsl_vector_isnull(s2) || gsl_vector_isnull(c))
    {
      cout <<"Warning: One or more of s1, s2 and seem appear to be flat and zero" <<endl;
    }

  int size = lensgrid->show_int("numpix");
  int aux_value = 0.0;

  for(int i = 0; i < size; i++)
    {
      gsl_vector_set(g1,i,gsl_vector_get(s1,i) / (1.0-gsl_vector_get(c,i)));
      gsl_vector_set(g2,i,gsl_vector_get(s2,i) / (1.0-gsl_vector_get(c,i)));
      aux_value = pow((1.0-gsl_vector_get(c,i)),2.0)-(pow(gsl_vector_get(s1,i),2.0))+pow(gsl_vector_get(s2,i),2.0);
      gsl_vector_set(jacdet,i,aux_value);
      gsl_vector_set(mu,i,1.0/aux_value);
    }
}

void LensModel::rescale(gsl_matrix *zmap, double lens_z, double target_z)
{
  if(zmap->size1 != lensgrid->show_int("y_dim") || zmap->size2 != lensgrid->show_int("x_dim"))
    {
      throw invalid_argument("Dimensions of input redshift map invalid");
    }

  int size = lensgrid->show_int("numpix");
  gsl_vector *lens_zmap = gsl_vector_calloc(size);
  lensgrid->convert(zmap,lens_zmap);

  astro::cosmology cosmo1(0.3,0.7,-1.0);
  double helper1 = cosmo1.angularDist(lens_z,target_z) / cosmo1.angularDist(0.0,target_z);


  gsl_vector *scalevector = gsl_vector_calloc(size);

  for(int i = 0; i < size; i++)
    {
      gsl_vector_set(scalevector,i, cosmo1.angularDist(0.0,gsl_vector_get(lens_zmap,i))/cosmo1.angularDist(lens_z,gsl_vector_get(lens_zmap,i)));
    }
  gsl_vector_scale(scalevector,helper1);

  gsl_vector_mul(a1,scalevector);
  gsl_vector_mul(a2,scalevector);
  gsl_vector_mul(c,scalevector);
  gsl_vector_mul(s1,scalevector);
  gsl_vector_mul(s2,scalevector);
  gsl_vector_mul(F1,scalevector);
  gsl_vector_mul(F2,scalevector);
  gsl_vector_mul(G1,scalevector);
  gsl_vector_mul(G2,scalevector);

  build_lens_nopot();
}

gsl_vector* LensModel::data(string selection)
{

  if(selection == "pot")
    {
      return pot;
    }
  else if(selection == "a1")
    {
      return a1;
    }
  else if(selection == "a2")
    {
      return a2;
    }
  else if(selection == "c")
    {
      return c;
    }
  else if(selection == "s1")
    {
      return s1;
    }
  else if(selection == "s2")
    {
      return s2;
    }
  else if(selection == "g1")
    {
      return g1;
    }
  else if(selection == "g2")
    {
      return g2;
    }
  else if(selection == "jacdet")
    {
      return jacdet;
    }
  else if(selection == "mu")
    {
      return mu;
    }
  else if(selection == "F1")
    {
      return F1;
    }
  else if(selection == "F2")
    {
      return F2;
    }
  else if(selection == "G1")
    {
      return G1;
    }
  else if(selection == "G2")
    {
      return G2;
    }
  else
    {
      throw invalid_argument("Invalid selection for data access to lens");
    }
}

void LensModel::read_vector(string selection, gsl_vector *input)
{
  if(input->size != lensgrid->show_int("numpix"))
    {
      throw invalid_argument("Input vector has wrong dimensions for lens read");
    }

  if(selection == "pot")
    {
      gsl_vector_memcpy(pot,input);
    }
  else if(selection == "a1")
    {
      gsl_vector_memcpy(a1,input);
    }
  else if(selection == "a2")
    {
      gsl_vector_memcpy(a2,input);
    }
  else if(selection == "c")
    {
      gsl_vector_memcpy(c,input);
    }
  else if(selection == "s1")
    {
      gsl_vector_memcpy(s1,input);
    }
  else if(selection == "s2")
    {
      gsl_vector_memcpy(s2,input);
    }
  else if(selection == "g1")
    {
      gsl_vector_memcpy(g1,input);
    }
  else if(selection == "g2")
    {
      gsl_vector_memcpy(g2,input);
    }
  else if(selection == "jacdet")
    {
      gsl_vector_memcpy(jacdet,input);
    }
  else if(selection == "mu")
    {
      gsl_vector_memcpy(mu,input);
    }
  else if(selection == "F1")
    {
      gsl_vector_memcpy(F1,input);
    }
  else if(selection == "F2")
    {
      gsl_vector_memcpy(F2,input);
    }
  else if(selection == "G1")
    {
      gsl_vector_memcpy(G1,input);
    }
  else if(selection == "G2")
    {
      gsl_vector_memcpy(G2,input);
    }
  else
    {
      throw invalid_argument("Invalid selection for lens read");
    }
}

void LensModel::read_map(string selection, gsl_matrix *input)
{

  if(input->size1 != lensgrid->show_int("y_dim") || input->size2 != lensgrid->show_int("x_dim"))
    {
      throw invalid_argument("Wrong dimensions of input map for lens read_map");
    }

  if(selection == "pot")
    {
      lensgrid->convert(input,pot);
    }
  else if(selection == "a1")
    {
      lensgrid->convert(input,a1);
    }
  else if(selection == "a2")
    {
      lensgrid->convert(input,a2);
    }
  else if(selection == "c")
    {
      lensgrid->convert(input,c);
    }
  else if(selection == "s1")
    {
      lensgrid->convert(input,s1);
    }
  else if(selection == "s2")
    {
      lensgrid->convert(input,s2);
    }
  else if(selection == "g1")
    {
      lensgrid->convert(input,g1);
    }
  else if(selection == "g2")
    {
      lensgrid->convert(input,g2);
    }
  else if(selection == "jacdet")
    {
      lensgrid->convert(input,jacdet);
    }
  else if(selection == "mu")
    {
      lensgrid->convert(input,mu);
    }
  else if(selection == "F1")
    {
      lensgrid->convert(input,F1);
    }
  else if(selection == "F2")
    {
      lensgrid->convert(input,F2);
    }
  else if(selection == "G1")
    {
      lensgrid->convert(input,G1);
    }
  else if(selection == "G2")
    {
      lensgrid->convert(input,G2);
    }
  else
    {
      throw invalid_argument("Invalid selection for lens read_map");
    }
}

void LensModel::write_vector(string selection, gsl_vector *output)
{

  if(output->size != lensgrid->show_int("numpix"))
    {
      throw invalid_argument("Wrong dimensions of output vector for lens write");
    }
  if(selection == "pot")
    {
      gsl_vector_memcpy(output,pot);
    }
  else if(selection == "a1")
    {
      gsl_vector_memcpy(output,a1);
    }
  else if(selection == "a2")
    {
      gsl_vector_memcpy(output,a2);
    }
   else if(selection == "c")
    {
      gsl_vector_memcpy(output,c);
    }
  else if(selection == "g1")
    {
      gsl_vector_memcpy(output,g1);
    }
  else if(selection == "g2")
    {
      gsl_vector_memcpy(output,g2);
    }
  else if(selection == "jacdet")
    {
      gsl_vector_memcpy(output,jacdet);
    }
  else if(selection == "mu")
    {
      gsl_vector_memcpy(output,mu);
    }
  else if(selection == "F1")
    {
      gsl_vector_memcpy(output,F1);
    }
  else if(selection == "F2")
    {
      gsl_vector_memcpy(output,F2);
    }
  else if(selection == "G1")
    {
      gsl_vector_memcpy(output,G1);
    }
  else if(selection == "G2")
    {
      gsl_vector_memcpy(output,G2);
    }
  else
    {
      throw invalid_argument("Invalid selection for lens write vector");
    }
}

void LensModel::write_map(string selection, gsl_matrix *output)
{

  if(output->size1 != lensgrid->show_int("y_dim") || output->size2 != lensgrid->show_int("x_dim"))
    {
      throw invalid_argument("Dimensions of output matrix invalid for lens write");
    }

  if(selection == "pot")
    {
      lensgrid->convert(pot,output);
    }
  else if(selection == "a1")
    {
      lensgrid->convert(a1,output);
    }
  else if(selection == "a2")
    {
      lensgrid->convert(a2,output);
    }
  else if(selection == "c")
    {
      lensgrid->convert(c,output);
    }
  else if(selection == "s1")
    {
      lensgrid->convert(s1,output);
    }
  else if(selection == "s2")
    {
      lensgrid->convert(s2,output);
    }
  else if(selection == "g1")
    {
      lensgrid->convert(g1,output);
    }
  else if(selection == "g2")
    {
      lensgrid->convert(g2,output);
    }
  else if(selection == "jacdet")
    {
      lensgrid->convert(jacdet,output);
    }
  else if(selection == "mu")
    {
      lensgrid->convert(mu,output);
    }
  else if(selection == "F1")
    {
      lensgrid->convert(F1,output);
    }
  else if(selection == "F2")
    {
      lensgrid->convert(F2,output);
    }
  else if(selection == "G1")
    {
      lensgrid->convert(G1,output);
    }
  else if(selection == "G2")
    {
      lensgrid->convert(G2,output);
    }
  else
    {
      throw invalid_argument("Invalid selection for lens write map");
    }

}

void LensModel::write_fits(string filename, int verboselevel)
{
  int x = lensgrid->show_int("x_dim");
  int y = lensgrid->show_int("y_dim");

  gsl_matrix *map = gsl_matrix_calloc(y,x);

  lensgrid->convert(pot,map);
  write_pimg(filename,map);
  
  if(coordinates)
    {
      write_header(filename,"CRVAL1",origin[2]," ");
      write_header(filename,"CRVAL2",origin[3]," ");
      write_header(filename,"CRPIX1",origin[0]+1.0," ");
      write_header(filename,"CRPIX2",origin[1]+1.0," ");
      if(J2000)
	{
	  write_header(filename,"CTYPE1","RA---TAN "," ");
	  write_header(filename,"CTYPE2","DEC--TAN"," ");
	}
      else
	{
      write_header(filename,"CTYPE1"," "," ");
      write_header(filename,"CTYPE2"," "," ");
	}
      write_header(filename,"CD1_1",scale[0]," ");
      write_header(filename,"CD1_2",scale[1]," ");
      write_header(filename,"CD2_1",scale[2]," ");
      write_header(filename,"CD2_2",scale[3]," ");
    }
  
  if(verboselevel > 0)
    {
      lensgrid->convert(a1,map);
      write_imge(filename,"a1",map);
      lensgrid->convert(a2,map);
      write_imge(filename,"a2",map);
      if(coordinates)
	{
	  write_header_ext(filename,"a1","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"a1","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"a1","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"a1","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"a1","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"a1","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"a1","CTYPE1"," "," ");
	      write_header_ext(filename,"a1","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"a1","CD1_1",scale[0]," ");
	  write_header_ext(filename,"a1","CD1_2",scale[1]," ");
	  write_header_ext(filename,"a1","CD2_1",scale[2]," ");
	  write_header_ext(filename,"a1","CD2_2",scale[3]," ");

	  write_header_ext(filename,"a2","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"a2","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"a2","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"a2","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"a2","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"a2","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"a2","CTYPE1"," "," ");
	      write_header_ext(filename,"a2","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"a2","CD1_1",scale[0]," ");
	  write_header_ext(filename,"a2","CD1_2",scale[1]," ");
	  write_header_ext(filename,"a2","CD2_1",scale[2]," ");
	  write_header_ext(filename,"a2","CD2_2",scale[3]," ");
	}
    }
  if(verboselevel > 1)
    {
      lensgrid->convert(c,map);
      write_imge(filename,"c",map);
      lensgrid->convert(s1,map);
      write_imge(filename,"s1",map);
      lensgrid->convert(s2,map);
      write_imge(filename,"s2",map);
      lensgrid->convert(g1,map);
      write_imge(filename,"g1",map);
      lensgrid->convert(g2,map);
      write_imge(filename,"g2",map);
      lensgrid->convert(jacdet,map);
      write_imge(filename,"jacdet",map);
      lensgrid->convert(mu,map);
      write_imge(filename,"mu",map);

      if(coordinates)
	{
	  write_header_ext(filename,"c","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"c","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"c","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"c","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"c","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"c","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"c","CTYPE1"," "," ");
	      write_header_ext(filename,"c","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"c","CD1_1",scale[0]," ");
	  write_header_ext(filename,"c","CD1_2",scale[1]," ");
	  write_header_ext(filename,"c","CD2_1",scale[2]," ");
	  write_header_ext(filename,"c","CD2_2",scale[3]," ");

	  write_header_ext(filename,"s1","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"s1","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"s1","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"s1","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"s1","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"s1","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"s1","CTYPE1"," "," ");
	      write_header_ext(filename,"s1","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"s1","CD1_1",scale[0]," ");
	  write_header_ext(filename,"s1","CD1_2",scale[1]," ");
	  write_header_ext(filename,"s1","CD2_1",scale[2]," ");
	  write_header_ext(filename,"s1","CD2_2",scale[3]," ");

	  write_header_ext(filename,"s2","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"s2","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"s2","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"s2","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"s2","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"s2","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"s2","CTYPE1"," "," ");
	      write_header_ext(filename,"s2","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"s2","CD1_1",scale[0]," ");
	  write_header_ext(filename,"s2","CD1_2",scale[1]," ");
	  write_header_ext(filename,"s2","CD2_1",scale[2]," ");
	  write_header_ext(filename,"s2","CD2_2",scale[3]," ");

	  write_header_ext(filename,"g1","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"g1","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"g1","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"g1","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"g1","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"g1","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"g1","CTYPE1"," "," ");
	      write_header_ext(filename,"g1","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"g1","CD1_1",scale[0]," ");
	  write_header_ext(filename,"g1","CD1_2",scale[1]," ");
	  write_header_ext(filename,"g1","CD2_1",scale[2]," ");
	  write_header_ext(filename,"g1","CD2_2",scale[3]," ");

	  write_header_ext(filename,"g2","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"g2","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"g2","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"g2","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"g2","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"g2","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"g2","CTYPE1"," "," ");
	      write_header_ext(filename,"g2","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"g2","CD1_1",scale[0]," ");
	  write_header_ext(filename,"g2","CD1_2",scale[1]," ");
	  write_header_ext(filename,"g2","CD2_1",scale[2]," ");
	  write_header_ext(filename,"g2","CD2_2",scale[3]," ");

	  write_header_ext(filename,"jacdet","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"jacdet","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"jacdet","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"jacdet","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"jacdet","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"jacdet","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"jacdet","CTYPE1"," "," ");
	      write_header_ext(filename,"jacdet","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"jacdet","CD1_1",scale[0]," ");
	  write_header_ext(filename,"jacdet","CD1_2",scale[1]," ");
	  write_header_ext(filename,"jacdet","CD2_1",scale[2]," ");
	  write_header_ext(filename,"jacdet","CD2_2",scale[3]," ");

	  write_header_ext(filename,"mu","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"mu","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"mu","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"mu","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"mu","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"mu","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"mu","CTYPE1"," "," ");
	      write_header_ext(filename,"mu","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"mu","CD1_1",scale[0]," ");
	  write_header_ext(filename,"mu","CD1_2",scale[1]," ");
	  write_header_ext(filename,"mu","CD2_1",scale[2]," ");
	  write_header_ext(filename,"mu","CD2_2",scale[3]," ");
	}

    }
  if(verboselevel > 2)
    {
      lensgrid->convert(F1,map);
      write_imge(filename,"F1",map);
      lensgrid->convert(F2,map);
      write_imge(filename,"F2",map);
      lensgrid->convert(G1,map);
      write_imge(filename,"G1",map);
      lensgrid->convert(G2,map);
      write_imge(filename,"G2",map);
    

      if(coordinates)
	{
	  write_header_ext(filename,"F1","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"F1","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"F1","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"F1","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"F1","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"F1","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"F1","CTYPE1"," "," ");
	      write_header_ext(filename,"F1","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"F1","CD1_1",scale[0]," ");
	  write_header_ext(filename,"F1","CD1_2",scale[1]," ");
	  write_header_ext(filename,"F1","CD2_1",scale[2]," ");
	  write_header_ext(filename,"F1","CD2_2",scale[3]," ");

	  write_header_ext(filename,"F2","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"F2","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"F2","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"F2","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"F2","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"F2","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"F2","CTYPE1"," "," ");
	      write_header_ext(filename,"F2","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"F2","CD1_1",scale[0]," ");
	  write_header_ext(filename,"F2","CD1_2",scale[1]," ");
	  write_header_ext(filename,"F2","CD2_1",scale[2]," ");
	  write_header_ext(filename,"F2","CD2_2",scale[3]," ");

	  write_header_ext(filename,"G1","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"G1","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"G1","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"G1","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"G1","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"G1","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"G1","CTYPE1"," "," ");
	      write_header_ext(filename,"G1","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"G1","CD1_1",scale[0]," ");
	  write_header_ext(filename,"G1","CD1_2",scale[1]," ");
	  write_header_ext(filename,"G1","CD2_1",scale[2]," ");
	  write_header_ext(filename,"G1","CD2_2",scale[3]," ");

	  write_header_ext(filename,"G2","CRVAL1",origin[2]," ");
	  write_header_ext(filename,"G2","CRVAL2",origin[3]," ");
	  write_header_ext(filename,"G2","CRPIX1",origin[0]+1.0," ");
	  write_header_ext(filename,"G2","CRPIX2",origin[1]+1.0," ");
	  if(J2000)
	    {
	      write_header_ext(filename,"G2","CTYPE1","RA---TAN "," ");
	      write_header_ext(filename,"G2","CTYPE2","DEC--TAN"," ");
	    }
	  else
	    {
	      write_header_ext(filename,"G2","CTYPE1"," "," ");
	      write_header_ext(filename,"G2","CTYPE2"," "," ");
	    }
	  write_header_ext(filename,"G2","CD1_1",scale[0]," ");
	  write_header_ext(filename,"G2","CD1_2",scale[1]," ");
	  write_header_ext(filename,"G2","CD2_1",scale[2]," ");
	  write_header_ext(filename,"G2","CD2_2",scale[3]," ");
	}
    }
  
}





   







  













