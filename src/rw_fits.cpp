/***
Source file included in the SaWLens reconstruction package.
Developed by Julian Merten (2010)
at ITA Heidelberg and OA Bologna
Not to be used without permission
of the author.
jmerten@ita.uni-heidelberg.de
***/

#include <saw/rw_fits.h>

using namespace CCfits;

void write_pimg(string filename, gsl_matrix *data)
{

  long naxis = 2;
  int x_dim = data->size2;
  int y_dim = data->size1;
  long naxes[2] = {x_dim, y_dim};
  int dim = x_dim * y_dim;
  std::auto_ptr<FITS> img(0);
  filename = "!"+filename;
  img.reset(new FITS(filename,DOUBLE_IMG,naxis,naxes));

  std::valarray<double> array(dim); 
  for (int i = 0; i < y_dim; i++)
    {
      for(int j = 0; j < x_dim; j++)
	{
	 array[i*x_dim + j] = gsl_matrix_get(data, i, j);
	}
    }
  

  img->pHDU().write(1, dim, array);
}



void write_pimgint(string filename, gsl_matrix_int *data)
{
  //same as above with integer values

  int x_dim = data->size2;
  int y_dim = data->size1;
  long naxis = 2;
  long naxes[2] = {x_dim, y_dim};
  int dim = x_dim * y_dim;
  filename = "!"+filename;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,32,naxis,naxes));

  std::valarray<int> array(dim); 
  for (int i = 0; i < y_dim; i++)
    {
      for(int j = 0; j < x_dim; j++)
	{
	  array[i*x_dim + j] = gsl_matrix_int_get(data, i, j);
	}
    }
  
  img->pHDU().write(1, dim, array);
}
void write_pimgfloat(string filename, gsl_matrix_float *data)
{
  //same as above with integer values

  int x_dim = data->size2;
  int y_dim = data->size1;
  long naxis = 2;
  long naxes[2] = {x_dim, y_dim};
  int dim = x_dim * y_dim;
  filename = "!"+filename;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,FLOAT_IMG,naxis,naxes));

  std::valarray<float> array(dim); 
  for (int i = 0; i < y_dim; i++)
    {
      for(int j = 0; j < x_dim; j++)
	{
	  array[i*x_dim + j] = gsl_matrix_float_get(data, i, j);
	}
    }
  
  img->pHDU().write(1, dim, array);
}

void write_pimg(string filename, int x_dim, int y_dim, gsl_vector *data)
{

  //see above

  if(x_dim*y_dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }

  long naxis = 2;
  long naxes[2] = {x_dim, y_dim};
  int dim = x_dim * y_dim;
  filename = "!"+filename;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,DOUBLE_IMG,naxis,naxes));


  std::valarray<double> array(dim); 
  for (int i = 0; i < dim; i++)
    {
      array[i] = gsl_vector_get(data,i);
    }
  
  
  img->pHDU().write(1, dim, array);
}

void write_pimg(string filename, int x_dim, int y_dim, std::valarray<double> data)
{

  //see above

    if(x_dim*y_dim != data.size())
    {
      throw invalid_argument("FITS dims must match");
    }

  long naxis = 2;
  long naxes[2] = {x_dim, y_dim};
  int dim = x_dim * y_dim;
  filename = "!"+filename;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,DOUBLE_IMG,naxis,naxes));
  
  
  img->pHDU().write(1, dim, data);
}


void write_pimgint(string filename, int x_dim, int y_dim, gsl_vector_int *data)
{
  if(x_dim*y_dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }

  long naxis = 2;
  long naxes[2] = {x_dim, y_dim};
  int dim = x_dim * y_dim;
  filename = "!"+filename;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,32,naxis,naxes));

  std::valarray<int> array(dim); 
  for (int i = 0; i < dim; i++)
    {
      array[i] = gsl_vector_int_get(data,i);
    }
  
    
  img->pHDU().write(1, dim, array);
}

void write_pimgint(string filename, int x_dim, int y_dim, std::valarray<int> data)
{
    if(x_dim*y_dim != data.size())
    {
      throw invalid_argument("FITS dims must match");
    }

  long naxis = 2;
  long naxes[2] = {x_dim, y_dim};
  int dim = x_dim * y_dim;
  filename = "!"+filename;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,32,naxis,naxes));

  
    
  img->pHDU().write(1, dim, data);
}

void write_pimgfloat(string filename, int x_dim, int y_dim, gsl_vector_float *data)
{
  if(x_dim*y_dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }

  long naxis = 2;
  long naxes[2] = {x_dim, y_dim};
  int dim = x_dim * y_dim;
  filename = "!"+filename;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,FLOAT_IMG,naxis,naxes));

  std::valarray<float> array(dim); 
  for (int i = 0; i < dim; i++)
    {
      array[i] = gsl_vector_float_get(data,i);
    }
  
    
  img->pHDU().write(1, dim, array);
}

void write_pimgint(string filename, int x_dim, int y_dim, std::valarray<float> data)
{
    if(x_dim*y_dim != data.size())
    {
      throw invalid_argument("FITS dims must match");
    }

  long naxis = 2;
  long naxes[2] = {x_dim, y_dim};
  int dim = x_dim * y_dim;
  filename = "!"+filename;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,FLOAT_IMG,naxis,naxes));

  
    
  img->pHDU().write(1, dim, data);
}

void write_imge(string filename, string extname, gsl_matrix *data)
{

  int x_dim = data->size2;
  int y_dim = data->size1; 
  int dim = x_dim * y_dim;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));


  std::valarray<double> array(dim); 
  for (int i = 0; i < y_dim; i++)
    {
      for(int j = 0; j < x_dim; j++)
	{
	  array[i*x_dim + j] = gsl_matrix_get(data, i, j);
	}
    }
  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExt = img->addImage(extname, DOUBLE_IMG, extAx);
  imageExt->write(1, dim, array);
}

void write_imgeint(string filename, string extname, gsl_matrix_int *data)
{
  int x_dim = data->size2;
  int y_dim = data->size1;
  int dim = x_dim * y_dim;
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));
  
  std::valarray<int> array(dim); 
  for (int i = 0; i < y_dim; i++)
    {
      for(int j = 0; j < x_dim; j++)
	{
	  array[i*x_dim + j] = gsl_matrix_int_get(data, i, j);
	}
    }
  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExt = img->addImage(extname, 32, extAx);
  imageExt->write(1, dim, array);
}
void write_imgefloat(string filename, string extname, gsl_matrix_float *data)
{
  int x_dim = data->size2;
  int y_dim = data->size1;
  int dim = x_dim * y_dim;
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));
  
  std::valarray<float> array(dim); 
  for (int i = 0; i < y_dim; i++)
    {
      for(int j = 0; j < x_dim; j++)
	{
	  array[i*x_dim + j] = gsl_matrix_float_get(data, i, j);
	}
    }
  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExt = img->addImage(extname, FLOAT_IMG, extAx);
  imageExt->write(1, dim, array);
}

void write_imge(string filename, string extname, int x_dim, int y_dim, gsl_vector *data)
{
  if(x_dim*y_dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }

  int dim = x_dim * y_dim;
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));
  
  std::valarray<double> array(dim); 
  for (int i = 0; i < dim; i++)
    {
      array[i] = gsl_vector_get(data,i);
    }
  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExt = img->addImage(extname, DOUBLE_IMG, extAx);
  imageExt->write(1, dim, array);
}

void write_imge(string filename, string extname, int x_dim, int y_dim, std::valarray<double> data)
{
    if(x_dim*y_dim != data.size())
    {
      throw invalid_argument("FITS dims must match");
    }

  int dim = x_dim * y_dim;
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));

  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExt = img->addImage(extname, DOUBLE_IMG, extAx);
  imageExt->write(1, dim, data);
}

void write_imgeint(string filename, string extname, int x_dim, int y_dim, gsl_vector_int *data)
{
  if(x_dim*y_dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }

  int dim = x_dim * y_dim;
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));
  
  std::valarray<int> array(dim); 
  for (int i = 0; i < dim; i++)
    {
      array[i] = gsl_vector_int_get(data,i);
    }
  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExt = img->addImage(extname, 32, extAx);
  imageExt->write(1, dim, array);
}

void write_imgeint(string filename, string extname, int x_dim, int y_dim, std::valarray<int> data)
{
    if(x_dim*y_dim != data.size())
    {
      throw invalid_argument("FITS dims must match");
    }

  int dim = x_dim * y_dim;
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));

  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExt = img->addImage(extname, 32, extAx);
  imageExt->write(1, dim, data);
}


void write_imgefloat(string filename, string extname, int x_dim, int y_dim, gsl_vector_float *data)
{
  if(x_dim*y_dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }

  int dim = x_dim * y_dim;
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));
  
  std::valarray<float> array(dim); 
  for (int i = 0; i < dim; i++)
    {
      array[i] = gsl_vector_float_get(data,i);
    }
  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExt = img->addImage(extname, FLOAT_IMG, extAx);
  imageExt->write(1, dim, array);
}

void write_imgefloat(string filename, string extname, int x_dim, int y_dim, std::valarray<float> data)
{
    if(x_dim*y_dim != data.size())
    {
      throw invalid_argument("FITS dims must match");
    }

  int dim = x_dim * y_dim;
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));

  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExt = img->addImage(extname, FLOAT_IMG, extAx);
  imageExt->write(1, dim, data);
}

void read_pimg(string filename, gsl_matrix *data)
{
  //reads the double pHDU of a fits file

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();

  
  std::valarray<double> contents;
  
  
  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));

  if(ax2 != data->size1 || ax1 != data->size2)
    {
      throw invalid_argument("FITS dims must match");
    }


  for (int i = 0; i < ax2; i++)
    {
      for(int j = 0; j < ax1; j++)
	{
	  gsl_matrix_set(data, i, j, contents[i*ax1 + j]);
	}
    }
  
}

void read_pimgint(string filename, gsl_matrix_int *data)
{

  //same in integer

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();

 
  std::valarray<int> contents;


  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));

  if(ax2 != data->size1 || ax1 != data->size2)
    {
      throw invalid_argument("FITS dims must match");
    }

  
  
  for (int i = 0; i < ax2; i++)
    {
      for(int j = 0; j < ax1; j++)
	{
	  gsl_matrix_int_set(data, i, j, contents[i*ax1 + j]);
	}
    }
  
}
void read_pimgfloat(string filename, gsl_matrix_float *data)
{

  //same in integer

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();

 
  std::valarray<float> contents;


  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));

  if(ax2 != data->size1 || ax1 != data->size2)
    {
      throw invalid_argument("FITS dims must match");
    }

  
  
  for (int i = 0; i < ax2; i++)
    {
      for(int j = 0; j < ax1; j++)
	{
	  gsl_matrix_float_set(data, i, j, contents[i*ax1 + j]);
	}
    }
  
}

void read_pimg(string filename, gsl_vector *data)
{
  //see above

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();
  
  
  std::valarray<double> contents;
  

  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));
  int dim = ax1 * ax2;


  if(dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }


  for (int i = 0; i < dim; i++)
    {
      gsl_vector_set(data, i, contents[i]);
    }

}

void read_pimgint(string filename, gsl_vector_int *data)
{

  //see above


  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();

 
  std::valarray<int> contents;


  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));
  int dim = ax1 * ax2;
  

  if(dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }

  for (int i = 0; i < dim; i++)
    {
      gsl_vector_int_set(data, i, contents[i]);
    }

}
void read_pimgfloat(string filename, gsl_vector_float *data)
{

  //see above


  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();

 
  std::valarray<float> contents;


  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));
  int dim = ax1 * ax2;
  

  if(dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }

  for (int i = 0; i < dim; i++)
    {
      gsl_vector_float_set(data, i, contents[i]);
    }

}

void read_imge(string filename, string extname, gsl_matrix *data)
{

  //reads a double image extension of a fits file

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);
  std::valarray<double> contents;
  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));

  if(ax2 != data->size1 || ax1 != data->size2)
    {
      throw invalid_argument("FITS dims must match");
    }

  for (int i = 0; i < ax2; i++)
    {
      for(int j = 0; j < ax1; j++)
	{
	  gsl_matrix_set(data, i, j, contents[i*ax1 + j]);
	}
    }
}

void read_imgeint(string filename, string extname, gsl_matrix_int *data)
{

  //same in integer

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);
  std::valarray<int> contents;
  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));

  if(ax2 != data->size1 || ax1 != data->size2)
    {
      throw invalid_argument("FITS dims must match");
    }

  for (int i = 0; i < ax2; i++)
    {
      for(int j = 0; j < ax1; j++)
	{
	  gsl_matrix_int_set(data, i, j, contents[i*ax1 + j]);
	}
    }
}
void read_imgefloat(string filename, string extname, gsl_matrix_float *data)
{

  //same in integer

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);
  std::valarray<float> contents;
  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));

  if(ax2 != data->size1 || ax1 != data->size2)
    {
      throw invalid_argument("FITS dims must match");
    }

  for (int i = 0; i < ax2; i++)
    {
      for(int j = 0; j < ax1; j++)
	{
	  gsl_matrix_float_set(data, i, j, contents[i*ax1 + j]);
	}
    }
}

void read_imge(string filename, string extname, gsl_vector *data)
{

  //see above
  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);
  std::valarray<double> contents;
  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));
  int dim = ax1 * ax2;

  if(dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }

  for (int i = 0; i < dim; i++)
    {
      gsl_vector_set(data,i, contents[i]);
    }
}

void read_imgeint(string filename, string extname, gsl_vector_int *data)
{

  //see above

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);
  std::valarray<int> contents;
  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));
  int dim = ax1 * ax2;

  if(dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }


  for (int i = 0; i < dim; i++)
    {
      gsl_vector_int_set(data,i, contents[i]);
    }
}
void read_imgefloat(string filename, string extname, gsl_vector_float *data)
{

  //see above

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);
  std::valarray<float> contents;
  img.readAllKeys();
  img.read(contents);
  int ax1(img.axis(0));
  int ax2(img.axis(1));
  int dim = ax1 * ax2;

  if(dim != data->size)
    {
      throw invalid_argument("FITS dims must match");
    }


  for (int i = 0; i < dim; i++)
    {
      gsl_vector_float_set(data,i, contents[i]);
    }
}

void write_header(string filename,string Key, double value, string description)
{

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));

  img->pHDU().addKey(Key,value,description);

}


void write_header(string filename,string Key, int value, string description)
{

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));

  img->pHDU().addKey(Key,value,description);

}
void write_header(string filename,string Key, float value, string description)
{

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));

  img->pHDU().addKey(Key,value,description);

}

void write_header(string filename,string Key, string value, string description)
{

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));

  img->pHDU().addKey(Key,value,description);

}

void write_header_ext(string filename,string extensionname, string Key, double value, string description)
{
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write,extensionname));
  img->currentExtension().addKey(Key,value,description);
}


void write_header_ext(string filename, string extensionname, string Key, int value, string description)
{
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write,extensionname));
  img->currentExtension().addKey(Key,value,description);

}
void write_header_ext(string filename,string extensionname, string Key, float value, string description)
{
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write,extensionname));
  img->currentExtension().addKey(Key,value,description);

}

void write_header_ext(string filename,string extensionname, string Key, string value, string description)
{
  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write,extensionname));
  img->currentExtension().addKey(Key,value,description);

}

double read_doubleheader(string filename, string name)
{
  double read;

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();

  img.readKey(name,read);

  return read;
}

double read_doubleheaderext(string filename, string extname, string name)
{
  double read;

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);

  img.readKey(name,read);

  return read;
}

int read_intheader(string filename, string name)
{
  int read;

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();

  img.readKey(name,read);

  return read;
}

int read_intheaderext(string filename, string extname, string name)
{
  int read;

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);
  img.readKey(name,read);

  return read;
}
int read_intheaderext(string filename, string name)
{
  int read;

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(1);
  img.readKey(name,read);

  return read;
}

float read_floatheader(string filename, string name)
{
  int read;

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();

  img.readKey(name,read);

  return read;
}

float read_floatheaderext(string filename, string extname, string name)
{
  int read;

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);
  img.readKey(name,read);

  return read;
}

string read_stringheader(string filename, string name)
{
  string read;

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  PHDU& img = pInfile->pHDU();

  img.readKey(name,read);

  return read;
}

string read_stringheaderext(string filename, string extname, string name)
{
  string read;

  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& img = pInfile->extension(extname);
  img.readKey(name,read);

  return read;
}

void check_table_dims(string filename, string extension, long *rows, int *cols)
{
  fitsfile *table_file;
  int status = 0;
  char *hossa = new char[extension.size()];
  strcpy(hossa,extension.c_str());


  if(!exists(filename))
    {
      throw invalid_argument("FITS table read: File does not exist.");
    }

  if(fits_open_file(&table_file,filename.c_str(),READONLY,&status))
    {
      throw invalid_argument("FITS table read: Cannot read file");
    }
  
  if(fits_movnam_hdu(table_file,BINARY_TBL,hossa,0,&status))
    {
      throw invalid_argument("FITS table read: Cannot open extension");
    }

  fits_get_num_rows(table_file,rows,&status);
  fits_get_num_cols(table_file,cols,&status);
  fits_close_file(table_file,&status);
}

void read_float_tablecolumn(string filename, string extension, int i_col, gsl_vector_float *output)
{

  fitsfile *table_file;
  int status = 0;
  long rows;
  int nulls;
  char *hossa = new char[extension.size()];
  strcpy(hossa,extension.c_str());


  if(!exists(filename))
    {
      throw invalid_argument("FITS table read: File does not exist.");
    }

  if(fits_open_file(&table_file,filename.c_str(),READONLY,&status))
    {
      throw invalid_argument("FITS table read: Cannot read file");
    }
  
  if(fits_movnam_hdu(table_file,BINARY_TBL,hossa,0,&status))
    {
      throw invalid_argument("FITS table read: Cannot open extension");
    }
  if(fits_get_num_rows(table_file,&rows,&status))
    {
      throw invalid_argument("FITS table read: Cannot check row dims");
    }

  if(output->size != rows)
    {
      throw invalid_argument("FITS table read: Wrong output vector dimension");
    }

  if(fits_read_col(table_file, TFLOAT,i_col,1,1,rows,NULL,gsl_vector_float_ptr(output,0),&nulls,&status))
    {
      throw invalid_argument("FITS table read: Cannot read table");
    }

}

double read_double_table_header(string filename, string extension, string keyword)
{

  fitsfile *table_file;
  int status = 0;
  long rows;
  int nulls;
  char *hossa = new char[extension.size()];
  strcpy(hossa,extension.c_str());
  char *hossa2 = new char[keyword.size()];
  strcpy(hossa2,keyword.c_str());

  double value;
  char dummy[256];

  if(!exists(filename))
    {
      throw invalid_argument("FITS table read: File does not exist.");
    }

  if(fits_open_file(&table_file,filename.c_str(),READONLY,&status))
    {
      throw invalid_argument("FITS table read: Cannot read file");
    }
  
  if(fits_movnam_hdu(table_file,BINARY_TBL,hossa,0,&status))
    {
      throw invalid_argument("FITS table read: Cannot open extension");
    }

  if(fits_read_key(table_file, TDOUBLE, hossa2,&value, dummy,&status))
    {
      throw invalid_argument("FITS table read: Cannot read keyword");
    }
  return value;
}

int read_int_table_header(string filename, string extension, string keyword)
{

  fitsfile *table_file;
  int status = 0;
  long rows;
  int nulls;
  char *hossa = new char[extension.size()];
  strcpy(hossa,extension.c_str());
  char *hossa2 = new char[keyword.size()];
  strcpy(hossa2,keyword.c_str());

  int value;
  char dummy[256];

  if(!exists(filename))
    {
      throw invalid_argument("FITS table read: File does not exist.");
    }

  if(fits_open_file(&table_file,filename.c_str(),READONLY,&status))
    {
      throw invalid_argument("FITS table read: Cannot read file");
    }
  
  if(fits_movnam_hdu(table_file,BINARY_TBL,hossa,0,&status))
    {
      throw invalid_argument("FITS table read: Cannot open extension");
    }

  if(fits_read_key(table_file, TINT, hossa2,&value, dummy,&status))
    {
      throw invalid_argument("FITS table read: Cannot read keyword");
    }
  return value;
}

void add_WCS(string filename, vector<double> WCS, string selection)
{

  if(!(WCS.size() == 6 || WCS.size() == 8))
    {
      throw invalid_argument("RW_FITS: WCS input of invalid size.");
    }

  vector<double> system;

  if(WCS.size() == 8)
    {
      system = WCS;
    }
  else
    {
      for(int i = 0; i < 5; i++)
	{
	  system.push_back(WCS[i]);
	}
      system.push_back(0.0);
      system.push_back(0.0);
      system.push_back(WCS[5]);
    }


  if(selection =="J2000")
    { 
      write_header(filename,"CRVAL1",system[0]," ");
      write_header(filename,"CRVAL2",system[1]," ");
      write_header(filename,"CRPIX1",system[2]," ");
      write_header(filename,"CRPIX2",system[3]," ");
      write_header(filename,"CTYPE1","RA---TAN "," ");
      write_header(filename,"CTYPE2","DEC--TAN"," ");
      write_header(filename,"CD1_1",system[4]," ");
      write_header(filename,"CD1_2",system[5]," ");
      write_header(filename,"CD2_1",system[6]," ");
      write_header(filename,"CD2_2",system[7]," ");
    }
  else if(selection == "linear")
    {
      write_header(filename,"CRVAL1",system[0]," ");
      write_header(filename,"CRVAL2",system[1]," ");
      write_header(filename,"CRPIX1",system[2]," ");
      write_header(filename,"CRPIX2",system[3]," ");
      write_header(filename,"CTYPE1"," "," ");
      write_header(filename,"CTYPE2"," "," ");
      write_header(filename,"CD1_1",system[4]," ");
      write_header(filename,"CD1_2",system[5]," ");
      write_header(filename,"CD2_1",system[6]," ");
      write_header(filename,"CD2_2",system[7]," ");
    }
  else
    {
      throw invalid_argument("RW_FITS: Invalid selection for writing WCS");
    }
}

void add_WCS(string filename, string extension, vector<double> WCS, string selection)
{

  if(!(WCS.size() == 6 || WCS.size() == 8))
    {
      throw invalid_argument("RW_FITS: WCS input of invalid size.");
    }

  vector<double> system;

  if(WCS.size() == 8)
    {
      system = WCS;
    }
  else
    {
      for(int i = 0; i < 5; i++)
	{
	  system.push_back(WCS[i]);
	}
      system.push_back(0.0);
      system.push_back(0.0);
      system.push_back(WCS[5]);
    }


  if(selection =="J2000")
    { 
      write_header_ext(filename,extension,"CRVAL1",system[0]," ");
      write_header_ext(filename,extension,"CRVAL2",system[1]," ");
      write_header_ext(filename,extension,"CRPIX1",system[2]," ");
      write_header_ext(filename,extension,"CRPIX2",system[3]," ");
      write_header_ext(filename,extension,"CTYPE1","RA---TAN "," ");
      write_header_ext(filename,extension,"CTYPE2","DEC--TAN"," ");
      write_header_ext(filename,extension,"CD1_1",system[4]," ");
      write_header_ext(filename,extension,"CD1_2",system[5]," ");
      write_header_ext(filename,extension,"CD2_1",system[6]," ");
      write_header_ext(filename,extension,"CD2_2",system[7]," ");
    }
  else if(selection == "linear")
    {
      write_header_ext(filename,extension,"CRVAL1",system[0]," ");
      write_header_ext(filename,extension,"CRVAL2",system[1]," ");
      write_header_ext(filename,extension,"CRPIX1",system[2]," ");
      write_header_ext(filename,extension,"CRPIX2",system[3]," ");
      write_header_ext(filename,extension,"CTYPE1"," "," ");
      write_header_ext(filename,extension,"CTYPE2"," "," ");
      write_header_ext(filename,extension,"CD1_1",system[4]," ");
      write_header_ext(filename,extension,"CD1_2",system[5]," ");
      write_header_ext(filename,extension,"CD2_1",system[6]," ");
      write_header_ext(filename,extension,"CD2_2",system[7]," ");
    }
  else
    {
      throw invalid_argument("RW_FITS: Invalid selection for writing WCS");
    }
}

void write_3D_datacube(string filename, int x, int y, int z, gsl_vector *data)
{

  if(x*y*z > data->size)
    {
      throw invalid_argument("Cube dimension too large for data vector");
    }
  fitsfile *fitsptr;
  int status = 0;

  string file = "!"+filename;

  long ax[3] = {x,y,z};

  if(fits_create_file(&fitsptr, file.c_str(), &status))
    {
      throw invalid_argument("RW_FITS: Cannot create 3D cube file");
    }
  if(fits_movabs_hdu(fitsptr,1,NULL,&status))
    {
      throw invalid_argument("RW_FITS: Cannot switch to primary HDU");
    }
  if(fits_create_img(fitsptr,DOUBLE_IMG, 3, ax, &status))
    {
      throw invalid_argument("RW_FITS: Cannot create 3D FITS image");
    }

  if(fits_write_3d_dbl(fitsptr,0, (long) x, (long) y, (long) x, (long) y, (long) z, gsl_vector_ptr(data,0), &status))
    {
      throw invalid_argument("RW_FITS: Cannot write 3D data into cube");
    }
  fits_close_file(fitsptr,&status);
}

void write_rgb(string filename, gsl_matrix *r, gsl_matrix *g, gsl_matrix *b)
{

  if(r->size1 != g->size1 || r->size1 != b->size1 || r->size2 != g->size2 || r->size2 != b->size2)
    {
      throw invalid_argument("RW_FITS: RGB matrix dims do not match");
    }

  int x_dim = r->size2;
  int y_dim = r->size1; 
  int dim = x_dim * y_dim;

  std::auto_ptr<FITS> img(0);
  img.reset(new FITS(filename,Write));


  std::valarray<double> array(dim); 
  for (int i = 0; i < y_dim; i++)
    {
      for(int j = 0; j < x_dim; j++)
	{
	  array[i*x_dim + j] = gsl_matrix_get(r, i, j);
	}
    }
  
  std::vector<long> extAx(2, 0);
  extAx.front() = x_dim;
  extAx.back() = y_dim;
  ExtHDU* imageExtR = img->addImage("red", DOUBLE_IMG, extAx);
  imageExtR->write(1, dim, array);


  for (int i = 0; i < y_dim; i++)
    {
      for(int j = 0; j < x_dim; j++)
	{
	  array[i*x_dim + j] = gsl_matrix_get(g, i, j);
	}
    }
  ExtHDU* imageExtG = img->addImage("green", DOUBLE_IMG, extAx);
  imageExtG->write(1, dim, array);

  for (int i = 0; i < y_dim; i++)
    {
      for(int j = 0; j < x_dim; j++)
	{
	  array[i*x_dim + j] = gsl_matrix_get(b, i, j);
	}
    }
  ExtHDU* imageExtB = img->addImage("blue", DOUBLE_IMG, extAx);
  imageExtB->write(1, dim, array);
}

void read_rgb(string filename, gsl_matrix *r, gsl_matrix *g, gsl_matrix *b)
{

  if(r->size1 != g->size1 || r->size1 != b->size1 || r->size2 != g->size2 || r->size2 != b->size2)
    {
      throw invalid_argument("RW_FITS: RGB matrix dims do not match");
    }
  
  std::auto_ptr<FITS> pInfile(new FITS(filename, Read, true));
  ExtHDU& imgR = pInfile->extension(1);
  ExtHDU& imgG = pInfile->extension(2);
  ExtHDU& imgB = pInfile->extension(3);
  std::valarray<double> contents;
  imgR.readAllKeys();
  imgR.read(contents);
  int ax1(imgR.axis(0));
  int ax2(imgR.axis(1));

  if(ax2 != r->size1 || ax1 != r->size2)
    {
      throw invalid_argument("FITS dims must match");
    }

  for (int i = 0; i < ax2; i++)
    {
      for(int j = 0; j < ax1; j++)
	{
	  gsl_matrix_set(r, i, j, contents[i*ax1 + j]);
	}
    }

  imgG.readAllKeys();
  imgG.read(contents);
  ax1 = imgG.axis(0);
  ax2 = imgG.axis(1);
  if(ax2 != g->size1 || ax1 != g->size2)
    {
      throw invalid_argument("FITS dims must match");
    }

  for (int i = 0; i < ax2; i++)
    {
      for(int j = 0; j < ax1; j++)
	{
	  gsl_matrix_set(g, i, j, contents[i*ax1 + j]);
	}
    }

  imgB.readAllKeys();
  imgB.read(contents);
  ax1 = imgB.axis(0);
  ax2 = imgB.axis(1);
  if(ax2 != b->size1 || ax1 != b->size2)
    {
      throw invalid_argument("FITS dims must match");
    }

  for (int i = 0; i < ax2; i++)
    {
      for(int j = 0; j < ax1; j++)
	{
	  gsl_matrix_set(b, i, j, contents[i*ax1 + j]);
	}
    }


}





 











