/***
    Coordinate grid class, moderating between a pixelised grid 
    representation and underlying physical coordinates. Allows for
    masking within DS9.

    Julian Merten
    JPL / Caltech
    jmerten@caltech.edu
    Dec. 2011

***/

#include <saw/coordinate_grid.h>

coordinate_grid::coordinate_grid(int x, int y)
{

  if(x <= 0 || y <= 0)
    {
      throw invalid_argument("COORD_GRID: Initial dimension must be > 0");
    }

  pix_dim.resize(2);
  phys_dim.resize(2);
  pix_origin.resize(2);
  phys_origin.resize(2);
  M.resize(4);

  pix_dim[0] = x;
  pix_dim[1] = y;
  phys_dim[0] = (double) x;
  phys_dim[1] = (double) y;

  J2000 = false;
  masked = false;

  pix_origin[0] = 0.;
  pix_origin[1] = 0.;
  phys_origin[0] = 0.;
  phys_origin[1] = 0.;
  M[0] = 1.;
  M[1] = 0.;
  M[2] = 0.;
  M[3] = 1.;

}

coordinate_grid::coordinate_grid(int x, int y, double pix_orig_x, double pix_orig_y, double phys_orig_x, double phys_orig_y, double m00, double m01, double m10, double m11, bool J2000in)
{

  if(x <= 0 || y <= 0)
    {
      throw invalid_argument("COORD_GRID: Initial dimension must be > 0");
    }

  if(pix_orig_x < 0 || pix_orig_x > x || pix_orig_y < 0 || pix_orig_y > y)
    {
      throw invalid_argument("COORD_GRID: Pixel origin invalid.");
    }

  if(m00 > 0. && J2000)
    {
      cout <<"Warning: Are you sure about your J2000 system. M[0] is positive." <<endl;
    }

  pix_dim.resize(2);
  phys_dim.resize(2);
  pix_origin.resize(2);
  phys_origin.resize(2);
  M.resize(4);

  pix_dim[0] = x;
  pix_dim[1] = y;
  phys_dim[0] = (double) x * abs(m00);
  phys_dim[1] = (double) y * abs(m11);

  J2000 = J2000in;
  masked = false;

  pix_origin[0] = pix_orig_x;
  pix_origin[1] = pix_orig_y;
  phys_origin[0] = phys_orig_x;
  phys_origin[1] = phys_orig_y;
  M[0] = m00;
  M[1] = m01;
  M[2] = m10;
  M[3] = m11;

}

coordinate_grid::~coordinate_grid()
{

  pix_dim.clear();
  phys_dim.clear();
  pix_origin.clear();
  phys_origin.clear();
  M.clear();
  rect_masks.clear();
  circ_masks.clear();
  annuli_masks.clear();
}

void coordinate_grid::add_WCS(double pix_orig_x, double pix_orig_y, double phys_orig_x, double phys_orig_y,double m00, double m01, double m10, double m11, bool J2000in)
{

  if(pix_orig_x < 0 || pix_orig_x > pix_dim[0] || pix_orig_y < 0 || pix_orig_y > pix_dim[1])
    {
      throw invalid_argument("COORD_GRID: Pixel origin invalid.");
    }

  phys_dim[0] = (double) pix_dim[0] * abs(m00);
  phys_dim[1] = (double) pix_dim[1] * abs(m11);
  J2000 = J2000in;
  pix_origin[0] = pix_orig_x;
  pix_origin[1] = pix_orig_y;
  phys_origin[0] = phys_orig_x;
  phys_origin[1] = phys_orig_y;
  M[0] = m00;
  M[1] = m01;
  M[2] = m10;
  M[3] = m11;
}

void coordinate_grid::reset_WCS()
{

  phys_dim[0] = (double) pix_dim[0];
  phys_dim[1] = (double) pix_dim[1];
  J2000 = false;
  pix_origin[0] = 0.;
  pix_origin[1] = 0.;
  phys_origin[0] = 0.;
  phys_origin[1] = 0.;
  M[0] = 1.;
  M[1] = 0.;
  M[2] = 0.;
  M[3] = 1.;
}

void coordinate_grid::add_masks(string ds9_file)
{
  if(!exists(ds9_file))
    {
      throw invalid_argument("COORD_GRID: DS9 region file does not exist");
    }

  ifstream input(ds9_file.c_str());
  if(!input.is_open())
    {
      throw invalid_argument("COORD: DS9 region file corrupted.");
    }

  string line;

  double my_Pi = acos(-1.);

  while(getline(input,line))
    {
      if(line.find("text={rect_mask",0) != string::npos)
	{
	  vector<double> nofuture;
	  double value;
	  read_doubles(line,5,nofuture);
	  if(nofuture.size() != 5)
	    {
	      throw invalid_argument("Wrong rectangular mask definition in DS9 region");
	    }

	  if(J2000)
	    {
	      rect_masks.push_back(nofuture[0]);
	      rect_masks.push_back(nofuture[1]);
	      
	      value = nofuture[2]/3600.;
	      rect_masks.push_back(value);
	      
	      value = nofuture[3]/3600.;
	      rect_masks.push_back(value);

	      value = nofuture[4] / 180. *my_Pi;
	      rect_masks.push_back(value);

	    }
	  else
	    {
	      rect_masks.push_back(nofuture[0]);
	      rect_masks.push_back(nofuture[1]);
	      rect_masks.push_back(nofuture[2]);
	      rect_masks.push_back(nofuture[3]);
	      value = nofuture[4] / 180. *my_Pi;
	      rect_masks.push_back(value);

	    }
	}

      if(line.find("text={circ_mask",0) != string::npos)
	{
	  vector<double> nofuture;
	  double value;
	  read_doubles(line,3,nofuture);
	  if(nofuture.size() != 3)
	    {
	      throw invalid_argument("Wrong circular mask definition in DS9 region");
	    }
	  circ_masks.push_back(nofuture[0]);
	  circ_masks.push_back(nofuture[1]);
	  if(J2000)
	    {
	      circ_masks.push_back(nofuture[2]/3600.);
	    }
	  else
	    {
	      circ_masks.push_back(nofuture[3]);
	    }

	}

      if(line.find("text={annuli_mask",0) != string::npos)
	{
	  vector<double> nofuture;
	  double value;
	  read_doubles(line,4,nofuture);
	  if(nofuture.size() != 4)
	    {
	      throw invalid_argument("Wrong annuli mask definition in DS9 region");
	    }
	  annuli_masks.push_back(nofuture[0]);
	  annuli_masks.push_back(nofuture[1]);
	  if(J2000)
	    {
	      annuli_masks.push_back(nofuture[2]/3600.);
	      annuli_masks.push_back(nofuture[3]/3600.);
	    }
	  else
	    {
	      annuli_masks.push_back(nofuture[2]);
	      annuli_masks.push_back(nofuture[3]);
	    }
	}

    }

  //Checking mask sanity
  if((int) (rect_masks.size() / 5) != (float) ((float) rect_masks.size() / 5.))
    {
      throw invalid_argument("COORD_GRID: Invalid rect mask length");
    }
  if((int) (annuli_masks.size() / 4) != (float) ((float) annuli_masks.size() / 4.))
    {
      throw invalid_argument("COORD_GRID: Invalid annuli mask length");
    }
  if((int) (circ_masks.size() / 3) != (float) ((float) circ_masks.size() / 3.))
    {
      throw invalid_argument("COORD_GRID: Invalid circ mask length");
    }

}

int coordinate_grid::return_pixel_data(string selection)
{
  
  if(selection == "x_dim")
    {
      return pix_dim[0];
    }
  else if(selection == "y_dim")
    {
      return pix_dim[1];
    }
  else if(selection == "field_size")
    {
      return pix_dim[0] * pix_dim[1];
    }
  else
    {
      throw invalid_argument("COORD_GRID: Invalid selection for return_pixel_data");
    }
} 

int coordinate_grid::return_phys_data(string selection)
{
if(selection == "x_dim")
    {
      return phys_dim[0];
    }
  else if(selection == "y_dim")
    {
      return phys_dim[1];
    }
  else if(selection == "field_size")
    {
      return phys_dim[0] * phys_dim[1];
    }
  else
    {
      throw invalid_argument("COORD_GRID: Invalid selection for return_phys_data");
    }

}

vector<double> coordinate_grid::return_WCS()
{
  vector<double> output;
  output.resize(8);
  output[0] = pix_origin[0];
  output[1] = pix_origin[1];
  output[2] = phys_origin[0];
  output[3] = phys_origin[1];
  output[4] = M[0];
  output[5] = M[1];
  output[6] = M[2];
  output[7] = M[3];

  return output;
}

double coordinate_grid::return_pix_coord(double phys_coord_x, double phys_coord_y, string selection)
{
  double x, y;

  x = M[3]*phys_coord_x - M[1]*phys_coord_y +M[1]*phys_origin[1] - M[3]*phys_origin[0];
  x *= 1./(M[0]*M[3] - M[1]*M[2]);
  x -= pix_origin[0];

  y = M[2]*phys_coord_x - M[0]*phys_coord_y +M[0]*phys_origin[1] - M[2]*phys_origin[0];
  y *= 1./(M[1]*M[2] - M[0]*M[3]);
  y -= pix_origin[1];

  if(selection == "x")
    {
      return x;
    }
  else if(selection == "y")
    {
      return y;
    }
  else
    {
      throw invalid_argument("COORD_GRID: Invalid selection for return_pix_coord");
    }
}

double coordinate_grid::return_phys_coord(double pix_coord_x, double pix_coord_y, string selection)
{
  double x,y;

  x = M[0]*(pix_coord_x-pix_origin[0]) + M[1]*(pix_coord_y-pix_origin[1]) + phys_origin[0];

  y = M[2]*(pix_coord_x-pix_origin[0]) + M[3]*(pix_coord_y-pix_origin[1]) + phys_origin[1];

if(selection == "x")
    {
      return x;
    }
  else if(selection == "y")
    {
      return y;
    }
  else
    {
      throw invalid_argument("COORD_GRID: Invalid selection for return_phys_coord");
    }
}

bool coordinate_grid::is_pix_masked(double pix_coord_x, double pix_coord_y)
{

  double my_Pi = acos(-1);
  double angle;
  double x, y,shift_x, shift_y, rot_x, rot_y;
  int masks;
  bool gotcha = false;
  x = return_phys_coord(pix_coord_x,pix_coord_y,"x");
  y = return_phys_coord(pix_coord_x,pix_coord_y,"y");



  
  //Running through rect masks
  masks = rect_masks.size() / 4;

  for(int i = 0; i < masks; i++)
    {
      int pos = i*5;

      //Rotating the pixel into the coordinate system of the mask. 

      angle = 2.*my_Pi - rect_masks[pos+4];
      shift_x = x - rect_masks[pos];
      if(J2000)
	{
	  shift_x *= -1;
	}
      shift_y = y - rect_masks[pos+1];
      rot_x = shift_x*cos(angle) - shift_y*sin(angle);
      rot_y = shift_x*sin(angle) + shift_y*cos(angle);

      if(abs(rot_x) < rect_masks[pos+2]/2. && abs(rot_y) < rect_masks[pos+3]/2.)
	{
	  gotcha = true;
	  break;
	}
	
    }

  //Running through circular masks
  if(!gotcha)
    {
      double distance;
      masks = circ_masks.size() / 3;
      for(int i = 0; i < masks; i++)
	{
	  int pos = i*3;
	  distance = pow(x-circ_masks[pos],2.)+pow(y-circ_masks[pos+1],2.);
	  if(distance <= pow(circ_masks[pos+2],2.))
	    {
	      gotcha = true;
	      break;
	    }
	}
      if(!gotcha)
	{
	  masks = annuli_masks.size() / 4;
	  for(int i = 0; i < masks; i++)
	    {
	      int pos = i*4;
	      distance = pow(x-annuli_masks[pos],2.)+pow(y-annuli_masks[pos+1],2.);
	      if(distance > pow(annuli_masks[pos+2],2.) && distance < pow(annuli_masks[pos+3],2.))
		{
		  gotcha = true;
		  break;
		}
	    }
	}
    }
  return gotcha;
	
}

bool coordinate_grid::is_phys_masked(double phys_coord_x, double phys_coord_y)
{
  double x = phys_coord_x; 
  double y = phys_coord_y; 
  int masks;
  bool gotcha = false;

  
  //Running through rect masks
  masks = rect_masks.size() / 4;

  for(int i = 0; i < masks; i++)
    {
      int pos = i*4;
      if(x > rect_masks[pos] && x <= rect_masks[pos+1] && y > rect_masks[pos+2] && y <= rect_masks[pos+3])
	gotcha = true;
      break;
    } 

  //Running through circular masks
  if(!gotcha)
    {
      double distance;
      masks = circ_masks.size() / 3;
      for(int i = 0; i < masks; i++)
	{
	  int pos = i*3;
	  distance = pow(x-circ_masks[pos],2.)+pow(y-circ_masks[pos+1],2.);
	  if(distance <= pow(circ_masks[pos+2],2.))
	    {
	      gotcha = true;
	      break;
	    }
	}
      if(!gotcha)
	{
	  masks = annuli_masks.size() / 4;
	  for(int i = 0; i < masks; i++)
	    {
	      int pos = i*4;
	      distance = pow(x-annuli_masks[pos],2.)+pow(y-annuli_masks[pos+1],2.);
	      if(distance > pow(annuli_masks[pos+2],2.) && distance < pow(annuli_masks[pos+3],2.))
		{
		  gotcha = true;
		  break;
		}
	    }
	}
    }
  return gotcha;

}

void coordinate_grid::visualise(gsl_matrix_int *output)
{
  if(output->size2 != pix_dim[0] || output->size1 != pix_dim[1])
    {
      throw invalid_argument("COORD_GRID: Matrix dimensions are wrong for visuliase");
    }

  gsl_matrix_int_set_all(output,0);

  for(int i = 0; i < pix_dim[1]; i++)
    {
      for(int j = 0; j < pix_dim[0]; j++)
	{
	  if(is_pix_masked((float) j+.5, (float) i+.5))
	    {
	      gsl_matrix_int_set(output,i,j,1);
	    }
	}
    }
}

void coordinate_grid::visualise(string filename)
{


  gsl_matrix_int *output = gsl_matrix_int_calloc(pix_dim[1],pix_dim[1]);
  visualise(output);
  write_pimgint(filename,output);

  write_header(filename,"CRVAL1",phys_origin[0]," ");
  write_header(filename,"CRVAL2",phys_origin[1]," ");
  write_header(filename,"CRPIX1",pix_origin[0]+0.5," ");
  write_header(filename,"CRPIX2",pix_origin[1]+0.5," ");
  if(!J2000)
    {
      write_header(filename,"CTYPE1",""," ");
      write_header(filename,"CTYPE2",""," ");
    }
  else
    {
      write_header(filename,"CTYPE1","RA---TAN"," ");
      write_header(filename,"CTYPE2","DEC--TAN"," ");
	      }
  write_header(filename,"CD1_1",M[0]," ");
  write_header(filename,"CD1_2",M[1]," ");
  write_header(filename,"CD2_1",M[2]," ");
  write_header(filename,"CD2_2",M[3]," ");
  
}








