#include <saw/smoothing.h>

using namespace std;

void gauss_smooth(gsl_matrix* inmap, gsl_matrix* outmap, double sigma)
{

  int x_dim = inmap->size2;
  int y_dim = inmap->size1;
  if(x_dim != outmap->size2 || y_dim != outmap->size1)
    {
      throw invalid_argument("matrix dimensions do not match");
    }

  if(sigma < 0.0)
    {
      throw invalid_argument("Sigma must be positive");
    }


  gsl_vector *kernelx = gsl_vector_calloc(x_dim);
  gsl_vector *kernely = gsl_vector_calloc(y_dim);

  gsl_matrix *nofuture = gsl_matrix_calloc(y_dim,x_dim);

  //Performing horizontal smoothing 

  for(int x = 0; x < x_dim; x++)
    {

      //Creating kernel, this step could be sign.
      // fastened up by just shifting a given kernel.

      double value = 0.0;
      double sum = 0.0;
      for(int l = 0; l < kernelx->size; l++)
	{
	    value = exp(-pow((double) l-x,2.0)/(2.0*pow(sigma,2.0)));
	    gsl_vector_set(kernelx,l,value);
	    sum += value;
	}
      gsl_vector_scale(kernelx,1.0/sum);



      for(int y = 0; y < y_dim; y++)
	{

	    value = 0.0;
	    sum = 0.0;

	    //   cout <<x <<"\t" <<y <<"\t" <<"hor"<<endl;

	  for(int l = 0; l < kernelx->size; l++)
	    {
	      value = gsl_matrix_get(inmap,y,l)*gsl_vector_get(kernelx,l);
	      sum += value;
	    }
	  gsl_matrix_set(nofuture,y,x,sum);
	}
    }


  //Performing vertical smoothing 

  for(int y = 0; y < y_dim; y++)
    {

      //Creating kernel, this step could be sign.
      // fastened up by just shifting a given kernel.

      double value = 0.0;
      double sum = 0.0;
      for(int l = 0; l < kernely->size; l++)
	{
	    value = exp(-pow((double) l-y,2.0)/(2.0*pow(sigma,2.0)));
	  gsl_vector_set(kernely,l,value);
	  sum += value;
	}

      gsl_vector_scale(kernely,1.0/sum);
      //cout <<sum <<endl;




      for(int x = 0; x < x_dim; x++)
	{
	    value = 0.0;
	    sum = 0.0;
//	    cout <<x <<"\t" <<y <<"\t" <<"ver"<<endl;

	  for(int l = 0; l < kernely->size; l++)
	    {
		value = gsl_matrix_get(nofuture,l,x)*gsl_vector_get(kernely,l);
	      sum += value;
	    }
	  gsl_matrix_set(outmap,y,x,sum);
	}
    }

}


