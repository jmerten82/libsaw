/***
findif.cpp: source file of the libsaw library
Author: Julian Merten
Institution: ITA Heidelberg
Mail: jmerten@ita.uni-heidelberg.de
***/

#include <saw/findif.h>

using namespace std;

FinDifGrid::FinDifGrid(int x_dim_given, int y_dim_given, double size_given, int order_given)
{

    allocationtable.resize(8);
    for(int i = 0; i < allocationtable.size(); i++)
	{
	    allocationtable[i] = false;
	}


    if(x_dim_given < 1 || y_dim_given < 1)
	{
	    throw invalid_argument("Findif grid dimension must pe positive integer");
	} 

    x = x_dim_given;
    y = y_dim_given;
    numpix = x*y;
    order = order_given;
    size = size_given;

    typemap = gsl_matrix_int_calloc(y, x);

    if(order < 1 || order > 4)
	{
	    throw invalid_argument("Invalid order for a fin dif grid");
	}

    allocationtable[1] = true;
    a1 = gsl_matrix_calloc(numpix,3);
    a2 = gsl_matrix_calloc(numpix,3);
    a1pos = gsl_matrix_int_calloc(numpix,3);
    a2pos = gsl_matrix_int_calloc(numpix,3);
    if(order > 1)
	{
	    allocationtable[2] = true;
	    //gsl_vector_bool_set(allocationtable,2, true);
	    c = gsl_matrix_calloc(numpix,9);
	    cpos = gsl_matrix_int_calloc(numpix,9);
	    s1 = gsl_matrix_calloc(numpix,5);
	    s2 = gsl_matrix_calloc(numpix,7);
	    s1pos = gsl_matrix_int_calloc(numpix,5);
	    s2pos = gsl_matrix_int_calloc(numpix,7);
	}
    if(order > 2)
	{
	    allocationtable[3] = true;
	    //gsl_vector_bool_set(allocationtable,3, true);
	    f1 = gsl_matrix_calloc(numpix,8);
	    f2 = gsl_matrix_calloc(numpix,8);
	    f1pos = gsl_matrix_int_calloc(numpix,8);
	    f2pos = gsl_matrix_int_calloc(numpix,8);
	    g1 = gsl_matrix_calloc(numpix,8);
	    g2 = gsl_matrix_calloc(numpix,8);
	    g1pos = gsl_matrix_int_calloc(numpix,8);
	    g2pos = gsl_matrix_int_calloc(numpix,8);
	}

    //Type definitions depending on the grid order

    if(order < 3)
      {
	gsl_matrix_int_set(typemap,0,0,1);
	gsl_matrix_int_set(typemap,0,x-1,2);
	gsl_matrix_int_set(typemap,y-1,0,3);
	gsl_matrix_int_set(typemap,y-1,x-1,4);
	
	for(int i = 1; i < x-1; i++)
	  {
	    gsl_matrix_int_set(typemap,0,i,5); 
	    gsl_matrix_int_set(typemap,y-1,i,6);
	  }
	
	for(int i = 1; i < y-1; i++)
	  {
	    gsl_matrix_int_set(typemap,i,0,7); 
	    gsl_matrix_int_set(typemap,i,x-1,8);
	  }
      }
    else
      {
	for(int i = 0; i < 2; i++)
	  {
	    for(int j = 0; j < 2; j++)
	      {
		gsl_matrix_int_set(typemap,0+i,0+j,1);
	      }
	  }
	for(int i = 0; i < 2; i++)
	  {
	    for(int j = 0; j < 2; j++)
	      {
		gsl_matrix_int_set(typemap,0+i,x-1-j,2);
	      }
	  }
	for(int i = 0; i < 2; i++)
	  {
	    for(int j = 0; j < 2; j++)
	      {
		gsl_matrix_int_set(typemap,y-1-i,0+j,3);
	      }
	  }
	for(int i = 0; i < 2; i++)
	  {
	    for(int j = 0; j < 2; j++)
	      {
		gsl_matrix_int_set(typemap,y-1-i,x-1-j,4);
	      }
	  }
	
	for(int i = 2; i < x-2; i++)
	  {
	    gsl_matrix_int_set(typemap,0,i,5);
	    gsl_matrix_int_set(typemap,1,i,5);  
	    gsl_matrix_int_set(typemap,y-1,i,6);
	    gsl_matrix_int_set(typemap,y-2,i,6);
	  }
	
	for(int i = 2; i < y-2; i++)
	  {
	    gsl_matrix_int_set(typemap,i,0,7);
	    gsl_matrix_int_set(typemap,i,1,7);  
	    gsl_matrix_int_set(typemap,i,x-1,8);
	    gsl_matrix_int_set(typemap,i,x-2,8);
	  }
      }


    double h, factor1, factor2, factor3;
    h =(double) x / size;
    factor1 = pow(h,1.0);
    factor2 = pow(h,2.0);
    factor3 = pow(h,3.0);
    int counter = 0;

    for(int i = 0; i < y; i++)
	{
	    for(int j = 0; j < x; j++)
		{

//Setting a1

		    if(gsl_matrix_int_get(typemap,i,j) == 1 || gsl_matrix_int_get(typemap,i,j) == 7 || gsl_matrix_int_get(typemap,i,j) == 3)
			{
			    gsl_matrix_set(a1,counter,0,-3./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,0,counter);
			    gsl_matrix_set(a1,counter,1,2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,1,counter+1);
			    gsl_matrix_set(a1,counter,2,-1./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,2,counter+2);
			}
		    if(gsl_matrix_int_get(typemap,i,j) == 2 || gsl_matrix_int_get(typemap,i,j) == 8 || gsl_matrix_int_get(typemap,i,j) == 4)
			{
			    gsl_matrix_set(a1,counter,0,1./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,0,counter-2);
			    gsl_matrix_set(a1,counter,1,-2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,1,counter-1);
			    gsl_matrix_set(a1,counter,2,3./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,2,counter);
			}
		    if(gsl_matrix_int_get(typemap,i,j) == 0 || gsl_matrix_int_get(typemap,i,j) == 5 || gsl_matrix_int_get(typemap,i,j) == 6)
			{
			    gsl_matrix_set(a1,counter,0,-1./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,0,counter-1);
			    gsl_matrix_set(a1,counter,1,1./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,1,counter+1);
			}


// Setting a2
		    if(gsl_matrix_int_get(typemap,i,j) == 1 || gsl_matrix_int_get(typemap,i,j) == 5 || gsl_matrix_int_get(typemap,i,j) == 2)
			{
			    gsl_matrix_set(a2,counter,0,-3./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,0,counter);
			    gsl_matrix_set(a2,counter,1,2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,1,counter+x);
			    gsl_matrix_set(a2,counter,2,-1./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,2,counter+2*x);
			}
		    if(gsl_matrix_int_get(typemap,i,j) == 3 || gsl_matrix_int_get(typemap,i,j) == 6 || gsl_matrix_int_get(typemap,i,j) == 4)
			{
			    gsl_matrix_set(a2,counter,0,1./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,0,counter-2*x);
			    gsl_matrix_set(a2,counter,1,-2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,1,counter-x);
			    gsl_matrix_set(a2,counter,2,3./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,2,counter);
			}
		    if(gsl_matrix_int_get(typemap,i,j) == 0 || gsl_matrix_int_get(typemap,i,j) == 7 || gsl_matrix_int_get(typemap,i,j) == 8)
			{
			    gsl_matrix_set(a2,counter,0,-1./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,0,counter-x);
			    gsl_matrix_set(a2,counter,1,1./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,1,counter+x);
			}
// Setting 2nd order

		    if(order > 1)
			{

			    if(gsl_matrix_int_get(typemap,i,j) == 1)
				{
//setting s1
				    gsl_matrix_set(s1,counter,0,-factor2);
				    gsl_matrix_int_set(s1pos,counter,0,counter+1);
				    gsl_matrix_set(s1,counter,1,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,counter+2);
				    gsl_matrix_set(s1,counter,2,factor2);
				    gsl_matrix_int_set(s1pos,counter,2,counter+x);	
				    gsl_matrix_set(s1,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,counter+2*x);

//setting s2

				    gsl_matrix_set(s2,counter,0,factor2);
				    gsl_matrix_int_set(s2pos,counter,0,counter);
				    gsl_matrix_set(s2,counter,1,-factor2);
				    gsl_matrix_int_set(s2pos,counter,1,counter+1);
				    gsl_matrix_set(s2,counter,2,-factor2);
				    gsl_matrix_int_set(s2pos,counter,2,counter+x);	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,counter+x+1);


//setting c

				    gsl_matrix_set(c,counter,0,factor2);
				    gsl_matrix_int_set(cpos,counter,0,counter);
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,counter+1);
				    gsl_matrix_set(c,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,counter+2);	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,counter+x);
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,counter+2*x);
				}
			    if(gsl_matrix_int_get(typemap,i,j) == 2)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,counter-2);
				    gsl_matrix_set(s1,counter,1,-factor2);
				    gsl_matrix_int_set(s1pos,counter,1,counter-1);
				    gsl_matrix_set(s1,counter,2,factor2);
				    gsl_matrix_int_set(s1pos,counter,2,counter+x);	
				    gsl_matrix_set(s1,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,counter+2*x);

//setting s2

				    gsl_matrix_set(s2,counter,0,factor2);
				    gsl_matrix_int_set(s2pos,counter,0,counter-1);
				    gsl_matrix_set(s2,counter,1,-factor2);
				    gsl_matrix_int_set(s2pos,counter,1,counter);
				    gsl_matrix_set(s2,counter,2,-factor2);
				    gsl_matrix_int_set(s2pos,counter,2,counter+x-1);	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,counter+x);

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,counter-2);
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,counter-1);
				    gsl_matrix_set(c,counter,2,factor2);
				    gsl_matrix_int_set(cpos,counter,2,counter);	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,counter+x);
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,counter+2*x);
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 3)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,counter-2*x);
				    gsl_matrix_set(s1,counter,1,factor2);
				    gsl_matrix_int_set(s1pos,counter,1,counter-1*x);
				    gsl_matrix_set(s1,counter,2,-factor2);
				    gsl_matrix_int_set(s1pos,counter,2,counter+1);	
				    gsl_matrix_set(s1,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,counter+2);

//setting s2

				    gsl_matrix_set(s2,counter,0,factor2);
				    gsl_matrix_int_set(s2pos,counter,0,counter-x);
				    gsl_matrix_set(s2,counter,1,-factor2);
				    gsl_matrix_int_set(s2pos,counter,1,counter-x+1);
				    gsl_matrix_set(s2,counter,2,-factor2);
				    gsl_matrix_int_set(s2pos,counter,2,counter);	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,counter+1);

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,counter-2*x);
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,counter-x);
				    gsl_matrix_set(c,counter,2,factor2);
				    gsl_matrix_int_set(cpos,counter,2,counter);	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,counter+1);
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,counter+2);

				}

			    if(gsl_matrix_int_get(typemap,i,j) == 4)
				{
//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,counter-2*x);
				    gsl_matrix_set(s1,counter,1,factor2);
				    gsl_matrix_int_set(s1pos,counter,1,counter-1*x);
				    gsl_matrix_set(s1,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,2,counter-2);	
				    gsl_matrix_set(s1,counter,3,-factor2);
				    gsl_matrix_int_set(s1pos,counter,3,counter-1);

//setting s2

				    gsl_matrix_set(s2,counter,0,factor2);
				    gsl_matrix_int_set(s2pos,counter,0,counter-x-1);
				    gsl_matrix_set(s2,counter,1,-factor2);
				    gsl_matrix_int_set(s2pos,counter,1,counter-x);
				    gsl_matrix_set(s2,counter,2,-factor2);
				    gsl_matrix_int_set(s2pos,counter,2,counter-1);	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,counter);

//setting c 

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,counter-2*x);
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,counter-x);
				    gsl_matrix_set(c,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,counter-2);	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,counter-1);
				    gsl_matrix_set(c,counter,4,factor2);
				    gsl_matrix_int_set(cpos,counter,4,counter);
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 5)
				{
//setting s1
				    gsl_matrix_set(s1,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,counter-1);
				    gsl_matrix_set(s1,counter,1,-3./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,counter);
				    gsl_matrix_set(s1,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,2,counter+1);	
				    gsl_matrix_set(s1,counter,3,factor2);
				    gsl_matrix_int_set(s1pos,counter,3,counter+x);
				    gsl_matrix_set(s1,counter,4,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,4,counter+2*x);
//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,counter-1);
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,counter+1);
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,counter+x-1);	
				    gsl_matrix_set(s2,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,3,counter+x+1);

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,counter-1);
				    gsl_matrix_set(c,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,1,counter);
				    gsl_matrix_set(c,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,counter+1);	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,counter+x);
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,counter+2*x);
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 6)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,counter-2*x);
				    gsl_matrix_set(s1,counter,1,factor2);
				    gsl_matrix_int_set(s1pos,counter,1,counter-x);
				    gsl_matrix_set(s1,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,2,counter-1);	
				    gsl_matrix_set(s1,counter,3,-3./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,counter);
				    gsl_matrix_set(s1,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,4,counter+1);

//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,counter-x-1);
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,counter-x+1);
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,counter-1);	
				    gsl_matrix_set(s2,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,3,counter+1);

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,counter-2*x);
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,counter-x);
				    gsl_matrix_set(c,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,counter-1);	
				    gsl_matrix_set(c,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,3,counter);
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,counter+1);
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 7)
				{
//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,counter-x);
				    gsl_matrix_set(s1,counter,1,3./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,counter);
				    gsl_matrix_set(s1,counter,2,-factor2);
				    gsl_matrix_int_set(s1pos,counter,2,counter+1);	
				    gsl_matrix_set(s1,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,counter+2);
				    gsl_matrix_set(s1,counter,4,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,4,counter+x);

//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,counter-x);
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,counter-x+1);
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,counter+x);	
				    gsl_matrix_set(s2,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,3,counter+x+1);

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,counter-x);
				    gsl_matrix_set(c,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,1,counter);
				    gsl_matrix_set(c,counter,2,-factor2);
				    gsl_matrix_int_set(cpos,counter,2,counter+1);	
				    gsl_matrix_set(c,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,3,counter+2);
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,counter+x);
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 8)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,counter-x);
				    gsl_matrix_set(s1,counter,1,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,counter-2);
				    gsl_matrix_set(s1,counter,2,-factor2);
				    gsl_matrix_int_set(s1pos,counter,2,counter-1);	
				    gsl_matrix_set(s1,counter,3,3./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,counter);
				    gsl_matrix_set(s1,counter,4,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,4,counter+x);

//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,counter-x-1);
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,counter-x);
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,counter+x-1);	
				    gsl_matrix_set(s2,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,3,counter+x);

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,counter-x);
				    gsl_matrix_set(c,counter,1,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,1,counter-2);
				    gsl_matrix_set(c,counter,2,-factor2);
				    gsl_matrix_int_set(cpos,counter,2,counter-1);	
				    gsl_matrix_set(c,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,3,counter);
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,counter+x);
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 0)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,counter-x);
				    gsl_matrix_set(s1,counter,1,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,counter-1);
				    gsl_matrix_set(s1,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,2,counter+1);	
				    gsl_matrix_set(s1,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,counter+x);

//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,counter-x-1);
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,counter-x);
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,counter-1);	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,counter);
				    gsl_matrix_set(s2,counter,4,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,4,counter+1);
				    gsl_matrix_set(s2,counter,5,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,5,counter+x);
				    gsl_matrix_set(s2,counter,6,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,6,counter+x+1);

//setting c

				    gsl_matrix_set(c,counter,0,1./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,counter-x-1);
				    gsl_matrix_set(c,counter,1,-1./6.*factor2);
				    gsl_matrix_int_set(cpos,counter,1,counter-x);
				    gsl_matrix_set(c,counter,2,1./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,counter-x+1);	
				    gsl_matrix_set(c,counter,3,-1./6.*factor2);
				    gsl_matrix_int_set(cpos,counter,3,counter-1);
				    gsl_matrix_set(c,counter,4,-2./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,counter);
				    gsl_matrix_set(c,counter,5,-1./6.*factor2);
				    gsl_matrix_int_set(cpos,counter,5,counter+1);
				    gsl_matrix_set(c,counter,6,1./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,6,counter+x-1);
				    gsl_matrix_set(c,counter,7,-1./6.*factor2);
				    gsl_matrix_int_set(cpos,counter,7,counter+x);
				    gsl_matrix_set(c,counter,8,1./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,8,counter+x+1);
				}
				    
			}


		    if(order > 2)
			{

			    if(gsl_matrix_int_get(typemap,i,j) == 1)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-factor3);
				    gsl_matrix_int_set(f1pos,counter,0,counter);
				    gsl_matrix_set(f1,counter,1,2.0*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,counter+1);
				    gsl_matrix_set(f1,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,counter+2);
				    gsl_matrix_set(f1,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,counter+3);
				    gsl_matrix_set(f1,counter,4,factor3);
				    gsl_matrix_int_set(f1pos,counter,4,counter+x);
				    gsl_matrix_set(f1,counter,5,-factor3);
				    gsl_matrix_int_set(f1pos,counter,5,counter+x+1);
				    gsl_matrix_set(f1,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,counter+2*x);
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,counter+2*x+1);
//setting g1
				    gsl_matrix_set(g1,counter,0,factor3);
				    gsl_matrix_int_set(g1pos,counter,0,counter);
				    gsl_matrix_set(g1,counter,1,0.);
				    gsl_matrix_int_set(g1pos,counter,1,counter+1);
				    gsl_matrix_set(g1,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,counter+2);
				    gsl_matrix_set(g1,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,counter+3);
				    gsl_matrix_set(g1,counter,4,-3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,counter+x);
				    gsl_matrix_set(g1,counter,5,3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,counter+x+1);
				    gsl_matrix_set(g1,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,counter+2*x);
				    gsl_matrix_set(g1,counter,7,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,counter+2*x+1);
//setting f2
				    gsl_matrix_set(f2,counter,0,-factor3);
				    gsl_matrix_int_set(f2pos,counter,0,counter);
				    gsl_matrix_set(f2,counter,1,factor3);
				    gsl_matrix_int_set(f2pos,counter,1,counter+1);
				    gsl_matrix_set(f2,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,counter+2);
				    gsl_matrix_set(f2,counter,3,2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,counter+x);
				    gsl_matrix_set(f2,counter,4,-factor3);
				    gsl_matrix_int_set(f2pos,counter,4,counter+x+1);
				    gsl_matrix_set(f2,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,counter+x+2);
				    gsl_matrix_set(f2,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,counter+2*x);
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,counter+3*x);
//setting g2
				    gsl_matrix_set(g2,counter,0,-factor3);
				    gsl_matrix_int_set(g2pos,counter,0,counter);
				    gsl_matrix_set(g2,counter,1,3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,counter+1);
				    gsl_matrix_set(g2,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,counter+2);
				    gsl_matrix_set(g2,counter,3,0.);
				    gsl_matrix_int_set(g2pos,counter,3,counter+x);
				    gsl_matrix_set(g2,counter,4,-3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,counter+x+1);
				    gsl_matrix_set(g2,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,counter+x+2);
				    gsl_matrix_set(g2,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,counter+2*x);
				    gsl_matrix_set(g2,counter,7,-1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,counter+3*x);


				}

			    if(gsl_matrix_int_get(typemap,i,j) == 2)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,counter-3);
				    gsl_matrix_set(f1,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,counter-2);
				    gsl_matrix_set(f1,counter,2,-2.0*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,counter-1);
				    gsl_matrix_set(f1,counter,3,factor3);
				    gsl_matrix_int_set(f1pos,counter,3,counter);
				    gsl_matrix_set(f1,counter,4,factor3);
				    gsl_matrix_int_set(f1pos,counter,4,counter+x-1);
				    gsl_matrix_set(f1,counter,5,-factor3);
				    gsl_matrix_int_set(f1pos,counter,5,counter+x);
				    gsl_matrix_set(f1,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,counter+2*x-1);
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,counter+2*x);
//setting g1
				    gsl_matrix_set(g1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,counter-3);
				    gsl_matrix_set(g1,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,counter-2);
				    gsl_matrix_set(g1,counter,2,0.);
				    gsl_matrix_int_set(g1pos,counter,2,counter-1);
				    gsl_matrix_set(g1,counter,3,-factor3);
				    gsl_matrix_int_set(g1pos,counter,3,counter);
				    gsl_matrix_set(g1,counter,4,-3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,counter+x-1);
				    gsl_matrix_set(g1,counter,5,3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,counter+x);
				    gsl_matrix_set(g1,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,counter+2*x-1);
				    gsl_matrix_set(g1,counter,7,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,counter+2*x);

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,counter-2);
				    gsl_matrix_set(f2,counter,1,factor3);
				    gsl_matrix_int_set(f2pos,counter,1,counter-1);
				    gsl_matrix_set(f2,counter,2,-factor3);
				    gsl_matrix_int_set(f2pos,counter,2,counter);
				    gsl_matrix_set(f2,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,counter+x-2);
				    gsl_matrix_set(f2,counter,4,-factor3);
				    gsl_matrix_int_set(f2pos,counter,4,counter+x-1);
				    gsl_matrix_set(f2,counter,5,2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,counter+x);
				    gsl_matrix_set(f2,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,counter+2*x);
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,counter+3*x);

//setting g2
				    gsl_matrix_set(g2,counter,0,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,counter-2);
				    gsl_matrix_set(g2,counter,1,3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,counter-1);
				    gsl_matrix_set(g2,counter,2,-factor3);
				    gsl_matrix_int_set(g2pos,counter,2,counter);
				    gsl_matrix_set(g2,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,counter+x-2);
				    gsl_matrix_set(g2,counter,4,-3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,counter+x-1);
				    gsl_matrix_set(g2,counter,5,0.);
				    gsl_matrix_int_set(g2pos,counter,5,counter+x);
				    gsl_matrix_set(g2,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,counter+2*x);
				    gsl_matrix_set(g2,counter,7,-1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,counter+3*x);



				}

			    if(gsl_matrix_int_get(typemap,i,j) == 3)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,counter-2*x);
				    gsl_matrix_set(f1,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,counter-2*x+1);
				    gsl_matrix_set(f1,counter,2,factor3);
				    gsl_matrix_int_set(f1pos,counter,2,counter-x);
				    gsl_matrix_set(f1,counter,3,-factor3);
				    gsl_matrix_int_set(f1pos,counter,3,counter-x+1);
				    gsl_matrix_set(f1,counter,4,-factor3);
				    gsl_matrix_int_set(f1pos,counter,4,counter);
				    gsl_matrix_set(f1,counter,5,2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,counter+1);
				    gsl_matrix_set(f1,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,counter+2);
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,counter+3);
//setting g1
				    gsl_matrix_set(g1,counter,0,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,counter-2*x);
				    gsl_matrix_set(g1,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,counter-2*x+1);
				    gsl_matrix_set(g1,counter,2,-3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,counter-x);
				    gsl_matrix_set(g1,counter,3,3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,counter-x+1);
				    gsl_matrix_set(g1,counter,4,factor3);
				    gsl_matrix_int_set(g1pos,counter,4,counter);
				    gsl_matrix_set(g1,counter,5,0.);
				    gsl_matrix_int_set(g1pos,counter,5,counter+1);
				    gsl_matrix_set(g1,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,counter+2);
				    gsl_matrix_set(g1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,counter+3);

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,counter-3*x);
				    gsl_matrix_set(f2,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,counter-2*x);
				    gsl_matrix_set(f2,counter,2,-2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,counter-x);
				    gsl_matrix_set(f2,counter,3,factor3);
				    gsl_matrix_int_set(f2pos,counter,3,counter-x+1);
				    gsl_matrix_set(f2,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,counter-x+2);
				    gsl_matrix_set(f2,counter,5,factor3);
				    gsl_matrix_int_set(f2pos,counter,5,counter);
				    gsl_matrix_set(f2,counter,6,-factor3);
				    gsl_matrix_int_set(f2pos,counter,6,counter+1);
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,counter+2);

//setting g2
				    gsl_matrix_set(g2,counter,0,1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,counter-3*x);
				    gsl_matrix_set(g2,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,counter-2*x);
				    gsl_matrix_set(g2,counter,2,0.);
				    gsl_matrix_int_set(g2pos,counter,2,counter-x);
				    gsl_matrix_set(g2,counter,3,3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,counter-x+1);
				    gsl_matrix_set(g2,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,counter-x+2);
				    gsl_matrix_set(g2,counter,5,factor3);
				    gsl_matrix_int_set(g2pos,counter,5,counter);
				    gsl_matrix_set(g2,counter,6,-3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,counter+1);
				    gsl_matrix_set(g2,counter,7,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,counter+2);



				}

			    if(gsl_matrix_int_get(typemap,i,j) == 4)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,counter-2*x-1);
				    gsl_matrix_set(f1,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,counter-2*x);
				    gsl_matrix_set(f1,counter,2,factor3);
				    gsl_matrix_int_set(f1pos,counter,2,counter-x-1);
				    gsl_matrix_set(f1,counter,3,-factor3);
				    gsl_matrix_int_set(f1pos,counter,3,counter-x);
				    gsl_matrix_set(f1,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,counter-3);
				    gsl_matrix_set(f1,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,counter-2);
				    gsl_matrix_set(f1,counter,6,-2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,counter-1);
				    gsl_matrix_set(f1,counter,7,factor3);
				    gsl_matrix_int_set(f1pos,counter,7,counter);
//setting g1
				    gsl_matrix_set(g1,counter,0,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,counter-2*x-1);
				    gsl_matrix_set(g1,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,counter-2*x);
				    gsl_matrix_set(g1,counter,2,-3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,counter-x-1);
				    gsl_matrix_set(g1,counter,3,3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,counter-x);
				    gsl_matrix_set(g1,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,counter-3);
				    gsl_matrix_set(g1,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,counter-2);
				    gsl_matrix_set(g1,counter,6,0.);
				    gsl_matrix_int_set(g1pos,counter,6,counter-1);
				    gsl_matrix_set(g1,counter,7,-factor3);
				    gsl_matrix_int_set(g1pos,counter,7,counter);

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,counter-3*x);
				    gsl_matrix_set(f2,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,counter-2*x);
				    gsl_matrix_set(f2,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,counter-x-2);
				    gsl_matrix_set(f2,counter,3,factor3);
				    gsl_matrix_int_set(f2pos,counter,3,counter-x-1);
				    gsl_matrix_set(f2,counter,4,-2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,counter-x);
				    gsl_matrix_set(f2,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,counter-2);
				    gsl_matrix_set(f2,counter,6,-factor3);
				    gsl_matrix_int_set(f2pos,counter,6,counter-1);
				    gsl_matrix_set(f2,counter,7,factor3);
				    gsl_matrix_int_set(f2pos,counter,7,counter);

//setting g2
				    gsl_matrix_set(g2,counter,0,1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,counter-3*x);
				    gsl_matrix_set(g2,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,counter-2*x);
				    gsl_matrix_set(g2,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,counter-x-2);
				    gsl_matrix_set(g2,counter,3,3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,counter-x-1);
				    gsl_matrix_set(g2,counter,4,0.);
				    gsl_matrix_int_set(g2pos,counter,4,counter-x);
				    gsl_matrix_set(g2,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,counter-2);
				    gsl_matrix_set(g2,counter,6,-3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,counter-1);
				    gsl_matrix_set(g2,counter,7,factor3);
				    gsl_matrix_int_set(g2pos,counter,7,counter);



				}

			    if(gsl_matrix_int_get(typemap,i,j) == 5)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,counter-2);
				    gsl_matrix_set(f1,counter,1,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,counter-1);
				    gsl_matrix_set(f1,counter,2,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,counter+1);
				    gsl_matrix_set(f1,counter,3,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,counter+2);
				    gsl_matrix_set(f1,counter,4,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,counter+x-1);
				    gsl_matrix_set(f1,counter,5,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,counter+x+1);
				    gsl_matrix_set(f1,counter,6,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,counter+2*x-1);
				    gsl_matrix_set(f1,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,counter+2*x+1);
//setting g1
				    gsl_matrix_set(g1,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,counter-2);
				    gsl_matrix_set(g1,counter,1,5./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,counter-1);
				    gsl_matrix_set(g1,counter,2,-5./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,counter+1);
				    gsl_matrix_set(g1,counter,3,1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,counter+2);
				    gsl_matrix_set(g1,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,counter+x-1);
				    gsl_matrix_set(g1,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,counter+x+1);
				    gsl_matrix_set(g1,counter,6,3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,counter+2*x-1);
				    gsl_matrix_set(g1,counter,7,-3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,counter+2*x+1);

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,counter-1);
				    gsl_matrix_set(f2,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,counter);
				    gsl_matrix_set(f2,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,counter+1);
				    gsl_matrix_set(f2,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,counter+x-1);
				    gsl_matrix_set(f2,counter,4,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,counter+x);
				    gsl_matrix_set(f2,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,counter+x+1);
				    gsl_matrix_set(f2,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,counter+2*x);
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,counter+3*x);

//setting g2
				    gsl_matrix_set(g2,counter,0,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,counter-1);
				    gsl_matrix_set(g2,counter,1,7./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,counter);
				    gsl_matrix_set(g2,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,counter+1);
				    gsl_matrix_set(g2,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,counter+x-1);
				    gsl_matrix_set(g2,counter,4,-9./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,counter+x);
				    gsl_matrix_set(g2,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,counter+x+1);
				    gsl_matrix_set(g2,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,counter+2*x);
				    gsl_matrix_set(g2,counter,7,-1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,counter+3*x);



				}

			    if(gsl_matrix_int_get(typemap,i,j) == 6)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,counter-2*x-1);
				    gsl_matrix_set(f1,counter,1,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,counter-2*x+1);
				    gsl_matrix_set(f1,counter,2,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,counter-x-1);
				    gsl_matrix_set(f1,counter,3,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,counter-x+1);
				    gsl_matrix_set(f1,counter,4,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,counter-2);
				    gsl_matrix_set(f1,counter,5,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,counter-1);
				    gsl_matrix_set(f1,counter,6,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,counter+1);
				    gsl_matrix_set(f1,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,counter+2);
//setting g1
				    gsl_matrix_set(g1,counter,0,3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,counter-2*x-1);
				    gsl_matrix_set(g1,counter,1,-3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,counter-2*x+1);
				    gsl_matrix_set(g1,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,counter-x-1);
				    gsl_matrix_set(g1,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,counter-x+1);
				    gsl_matrix_set(g1,counter,4,-1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,counter-2);
				    gsl_matrix_set(g1,counter,5,5./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,counter-1);
				    gsl_matrix_set(g1,counter,6,-5./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,counter+1);
				    gsl_matrix_set(g1,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,counter+2);

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,counter-3*x);
				    gsl_matrix_set(f2,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,counter-2*x);
				    gsl_matrix_set(f2,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,counter-x-1);
				    gsl_matrix_set(f2,counter,3,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,counter-x);
				    gsl_matrix_set(f2,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,counter-x+1);
				    gsl_matrix_set(f2,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,counter-1);
				    gsl_matrix_set(f2,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,counter);
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,counter+1);

//setting g2
				    gsl_matrix_set(g2,counter,0,1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,counter-3*x);
				    gsl_matrix_set(g2,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,counter-2*x);
				    gsl_matrix_set(g2,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,counter-x-1);
				    gsl_matrix_set(g2,counter,3,9./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,counter-x);
				    gsl_matrix_set(g2,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,counter-x+1);
				    gsl_matrix_set(g2,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,counter-1);
				    gsl_matrix_set(g2,counter,6,-7./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,counter);
				    gsl_matrix_set(g2,counter,7,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,counter+1);


				}

			    if(gsl_matrix_int_get(typemap,i,j) == 7)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,counter-x);
				    gsl_matrix_set(f1,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,counter-x+1);
				    gsl_matrix_set(f1,counter,2,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,counter);
				    gsl_matrix_set(f1,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,counter+1);
				    gsl_matrix_set(f1,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,counter+2);
				    gsl_matrix_set(f1,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,counter+3);
				    gsl_matrix_set(f1,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,counter+x);
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,counter+x+1);
//setting g1
				    gsl_matrix_set(g1,counter,0,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,counter-x);
				    gsl_matrix_set(g1,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,counter-x+1);
				    gsl_matrix_set(g1,counter,2,-7./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,counter);
				    gsl_matrix_set(g1,counter,3,9./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,counter+1);
				    gsl_matrix_set(g1,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,counter+2);
				    gsl_matrix_set(g1,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,counter+3);
				    gsl_matrix_set(g1,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,counter+x);
				    gsl_matrix_set(g1,counter,7,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,counter+x+1);

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,counter-2*x);
				    gsl_matrix_set(f2,counter,1,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,counter-x);
				    gsl_matrix_set(f2,counter,2,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,counter-x+1);
				    gsl_matrix_set(f2,counter,3,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,counter-x+2);
				    gsl_matrix_set(f2,counter,4,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,counter+x);
				    gsl_matrix_set(f2,counter,5,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,counter+x+1);
				    gsl_matrix_set(f2,counter,6,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,counter+x+2);
				    gsl_matrix_set(f2,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,counter+2*x);

//setting g2
				    gsl_matrix_set(g2,counter,0,1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,counter-2*x);
				    gsl_matrix_set(g2,counter,1,-5./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,counter-x);
				    gsl_matrix_set(g2,counter,2,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,counter-x+1);
				    gsl_matrix_set(g2,counter,3,-3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,counter-x+2);
				    gsl_matrix_set(g2,counter,4,5./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,counter+x);
				    gsl_matrix_set(g2,counter,5,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,counter+x+1);
				    gsl_matrix_set(g2,counter,6,3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,counter+x+2);
				    gsl_matrix_set(g2,counter,7,-1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,counter+2*x);


				}

			    if(gsl_matrix_int_get(typemap,i,j) == 8)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,counter-x-1);
				    gsl_matrix_set(f1,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,counter-x);
				    gsl_matrix_set(f1,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,counter-3);
				    gsl_matrix_set(f1,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,counter-2);
				    gsl_matrix_set(f1,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,counter-1);
				    gsl_matrix_set(f1,counter,5,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,counter);
				    gsl_matrix_set(f1,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,counter+x-1);
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,counter+x);
//setting g1
				    gsl_matrix_set(g1,counter,0,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,counter-x-1);
				    gsl_matrix_set(g1,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,counter-x);
				    gsl_matrix_set(g1,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,counter-3);
				    gsl_matrix_set(g1,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,counter-2);
				    gsl_matrix_set(g1,counter,4,-9./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,counter-1);
				    gsl_matrix_set(g1,counter,5,7./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,counter);
				    gsl_matrix_set(g1,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,counter+x-1);
				    gsl_matrix_set(g1,counter,7,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,counter+x);

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,counter-2*x);
				    gsl_matrix_set(f2,counter,1,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,counter-x-2);
				    gsl_matrix_set(f2,counter,2,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,counter-x-1);
				    gsl_matrix_set(f2,counter,3,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,counter-x);
				    gsl_matrix_set(f2,counter,4,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,counter+x-2);
				    gsl_matrix_set(f2,counter,5,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,counter+x-1);
				    gsl_matrix_set(f2,counter,6,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,counter+x);
				    gsl_matrix_set(f2,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,counter+2*x);

//setting g2
				    gsl_matrix_set(g2,counter,0,1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,counter-2*x);
				    gsl_matrix_set(g2,counter,1,-3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,counter-x-2);
				    gsl_matrix_set(g2,counter,2,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,counter-x-1);
				    gsl_matrix_set(g2,counter,3,-5./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,counter-x);
				    gsl_matrix_set(g2,counter,4,3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,counter+x-2);
				    gsl_matrix_set(g2,counter,5,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,counter+x-1);
				    gsl_matrix_set(g2,counter,6,5./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,counter+x);
				    gsl_matrix_set(g2,counter,7,-1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,counter+2*x);


				}

			    if(gsl_matrix_int_get(typemap,i,j) == 0)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,counter-x-1);
				    gsl_matrix_set(f1,counter,1,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,counter-x+1);
				    gsl_matrix_set(f1,counter,2,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,counter-2);
				    gsl_matrix_set(f1,counter,3,factor3);
				    gsl_matrix_int_set(f1pos,counter,3,counter-1);
				    gsl_matrix_set(f1,counter,4,-factor3);
				    gsl_matrix_int_set(f1pos,counter,4,counter+1);
				    gsl_matrix_set(f1,counter,5,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,counter+2);
				    gsl_matrix_set(f1,counter,6,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,counter+x-1);
				    gsl_matrix_set(f1,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,counter+x+1);
//setting g1
				    gsl_matrix_set(g1,counter,0,3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,counter-x-1);
				    gsl_matrix_set(g1,counter,1,-3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,counter-x+1);
				    gsl_matrix_set(g1,counter,2,-1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,counter-2);
				    gsl_matrix_set(g1,counter,3,-factor3);
				    gsl_matrix_int_set(g1pos,counter,3,counter-1);
				    gsl_matrix_set(g1,counter,4,factor3);
				    gsl_matrix_int_set(g1pos,counter,4,counter+1);
				    gsl_matrix_set(g1,counter,5,1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,counter+2);
				    gsl_matrix_set(g1,counter,6,3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,counter+x-1);
				    gsl_matrix_set(g1,counter,7,-3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,counter+x+1);

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,counter-2*x);
				    gsl_matrix_set(f2,counter,1,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,counter-x-1);
				    gsl_matrix_set(f2,counter,2,factor3);
				    gsl_matrix_int_set(f2pos,counter,2,counter-x);
				    gsl_matrix_set(f2,counter,3,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,counter-x+1);
				    gsl_matrix_set(f2,counter,4,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,counter+x-1);
				    gsl_matrix_set(f2,counter,5,-factor3);
				    gsl_matrix_int_set(f2pos,counter,5,counter+x);
				    gsl_matrix_set(f2,counter,6,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,counter+x+1);
				    gsl_matrix_set(f2,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,counter+2*x);

//setting g2
				    gsl_matrix_set(g2,counter,0,1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,counter-2*x);
				    gsl_matrix_set(g2,counter,1,-3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,counter-x-1);
				    gsl_matrix_set(g2,counter,2,factor3);
				    gsl_matrix_int_set(g2pos,counter,2,counter-x);
				    gsl_matrix_set(g2,counter,3,-3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,counter-x+1);
				    gsl_matrix_set(g2,counter,4,3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,counter+x-1);
				    gsl_matrix_set(g2,counter,5,-factor3);
				    gsl_matrix_int_set(g2pos,counter,5,counter+x);
				    gsl_matrix_set(g2,counter,6,3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,counter+x+1);
				    gsl_matrix_set(g2,counter,7,-1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,counter+2*x);


				}
			}


			    

		    counter++;
		}
	}

}

FinDifGrid::FinDifGrid(gsl_matrix_int *mask, double size_given, int order_given)
{
    if(order_given < 1 || order_given > 4)
	{
	    throw invalid_argument("Invalid order for a fin dif grid");
	}

    allocationtable.resize(8);
    for(int i = 0; i < allocationtable.size(); i++)
	{
	  allocationtable[i] = false;
	}

    //Basic quantities, main allocation can just start when we have the 
    //number of pixels in the grid.

    bool badgrid = true;
    int badpixels;
    x = mask->size2;
    y = mask->size1;
    size = size_given;
    order = order_given;
    bool notfoundyet;
    int run_counter;
    typemap = gsl_matrix_int_calloc(y,x);

    //Copying the mask, for starters to the typemap...
    gsl_matrix_int_memcpy(typemap,mask);

    //The case of order == 3, where we need to fit larger stamps for 
    // each operator.


    if(order == 3)
      {
	run_counter = 0;
	while(badgrid && run_counter < 5)
	  {
	    badpixels = 0;
	    numpix = 0;
	    for(int i = 0; i < y; i++)
	      {
		for(int j = 0; j < x; j++)
		  {
		    notfoundyet = true;
		    if(gsl_matrix_int_get(typemap,i,j) != -1)
		      {
			if(notfoundyet && i+2 < y && i-2 >= 0 && j+2 < x && j-2 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i+2,j) != -1 && gsl_matrix_int_get(typemap,i+1,j) !=-1 && gsl_matrix_int_get(typemap,i-1,j) !=-1 &&gsl_matrix_int_get(typemap,i-2,j) != -1 && gsl_matrix_int_get(typemap,i,j-2) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i,j+2) != -1 && gsl_matrix_int_get(typemap,i+1,j-1) != -1 && gsl_matrix_int_get(typemap,i+1,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j-1) != -1 && gsl_matrix_int_get(typemap,i-1,j+1) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,0);
				notfoundyet = false;
				numpix++;
			      } 
			  }
			if(notfoundyet && i+2 < y && i-2 >= 0 && j-3 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i+2,j) != -1 && gsl_matrix_int_get(typemap,i+1,j) !=-1 && gsl_matrix_int_get(typemap,i-1,j) !=-1 &&gsl_matrix_int_get(typemap,i-2,j) != -1 && gsl_matrix_int_get(typemap,i,j-3) != -1 && gsl_matrix_int_get(typemap,i,j-2) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 && gsl_matrix_int_get(typemap,i+1,j-2) != -1 && gsl_matrix_int_get(typemap,i+1,j-1) != -1 && gsl_matrix_int_get(typemap,i-1,j-2) != -1 && gsl_matrix_int_get(typemap,i-1,j-1) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,8);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && i+2 < y && i-2 >= 0 && j+3 < x)
			  {
			    if(gsl_matrix_int_get(typemap,i+2,j) != -1 && gsl_matrix_int_get(typemap,i+1,j) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1 &&gsl_matrix_int_get(typemap,i-2,j) != -1 && gsl_matrix_int_get(typemap,i,j+3) != -1 && gsl_matrix_int_get(typemap,i,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i+1,j+2) != -1 && gsl_matrix_int_get(typemap,i+1,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j+2) != -1 && gsl_matrix_int_get(typemap,i-1,j+1) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,7);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && j+2 < x && j-2 >= 0 && i-3 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 &&gsl_matrix_int_get(typemap,i,j-2) != -1 && gsl_matrix_int_get(typemap,i-3,j) != -1 && gsl_matrix_int_get(typemap,i-2,j) != -1 && gsl_matrix_int_get(typemap,i-2,j-1) != -1 && gsl_matrix_int_get(typemap,i-2,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j-1) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,6);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && j+2 < x && j-2 >= 0 && i+3 < y)
			  {		    
			    if(gsl_matrix_int_get(typemap,i,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 &&gsl_matrix_int_get(typemap,i,j-2) != -1 && gsl_matrix_int_get(typemap,i+3,j) != -1 && gsl_matrix_int_get(typemap,i+2,j) != -1 && gsl_matrix_int_get(typemap,i+2,j-1) != -1 && gsl_matrix_int_get(typemap,i+2,j+1) != -1 && gsl_matrix_int_get(typemap,i+1,j+1) != -1 && gsl_matrix_int_get(typemap,i+1,j-1) != -1 && gsl_matrix_int_get(typemap,i+1,j)!= -1)
			      {
				gsl_matrix_int_set(typemap,i,j,5);
				notfoundyet = false;
				numpix++;
			      }		    
			  }
			if(notfoundyet && j-3 >= 0 && i-3 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i-3,j) != -1 && gsl_matrix_int_get(typemap,i-2,j) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1 &&gsl_matrix_int_get(typemap,i-2,j-1) != -1 && gsl_matrix_int_get(typemap,i-1,j-1) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 && gsl_matrix_int_get(typemap,i-2,j-2) != -1 && gsl_matrix_int_get(typemap,i-1,j-2) != -1 && gsl_matrix_int_get(typemap,i,j-2) != -1 && gsl_matrix_int_get(typemap,i,j-3) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,4);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && j+3 < x && i-3 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i-3,j) != -1 && gsl_matrix_int_get(typemap,i-2,j) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1 &&gsl_matrix_int_get(typemap,i-2,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j+1) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i-2,j+2) != -1 && gsl_matrix_int_get(typemap,i-1,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+3) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,3);
				notfoundyet = false;
				numpix++;
			      }
			  }
			
			if(notfoundyet && j-3 >= 0 && i+3 < y)
			  {
			    if(gsl_matrix_int_get(typemap,i+3,j) != -1 && gsl_matrix_int_get(typemap,i+2,j) != -1 && gsl_matrix_int_get(typemap,i+1,j) != -1 &&gsl_matrix_int_get(typemap,i+2,j-1) != -1 && gsl_matrix_int_get(typemap,i+1,j-1) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 && gsl_matrix_int_get(typemap,i+2,j-2) != -1 && gsl_matrix_int_get(typemap,i+1,j-2) != -1 && gsl_matrix_int_get(typemap,i,j-2) != -1 && gsl_matrix_int_get(typemap,i,j-3) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,2);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && j+3 < x && i+3 < y)
			  {
			    if(gsl_matrix_int_get(typemap,i+3,j) != -1 && gsl_matrix_int_get(typemap,i+2,j) != -1 && gsl_matrix_int_get(typemap,i+1,j) != -1 &&gsl_matrix_int_get(typemap,i+2,j+1) != -1 && gsl_matrix_int_get(typemap,i+1,j+1) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i+2,j+2) != -1 && gsl_matrix_int_get(typemap,i+1,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+3) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,1);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet)
			  {
			    badpixels++;
			    gsl_matrix_int_set(typemap,i,j,-1);
			  }
		      }
		    
		  }
	      }
	    if(badpixels == 0)
	      {
		badgrid = false;
	      }
	    else
	      {
		run_counter++;
	      }
	  }
      }
    //The case of grid of order <3, here we need to fits smaller findif
    // stamps.
    else
      {
	run_counter = 0;
	while(badgrid && run_counter < 5)
	  {
	    badpixels = 0;
	    numpix = 0;
	    for(int i = 0; i < y; i++)
	      {
		for(int j = 0; j < x; j++)
		  {
		    notfoundyet = true;
		    if(gsl_matrix_int_get(typemap,i,j) != -1)
		      {
			if(notfoundyet && i+1 < y && i-1 >= 0 && j+1 < x && j-1 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i+1,j) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i+1,j-1) != -1 && gsl_matrix_int_get(typemap,i+1,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j-1) != -1 && gsl_matrix_int_get(typemap,i-1,j+1) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,0);
				notfoundyet = false;
				numpix++;
			      } 
			  } 
			if(notfoundyet && i+1 < y && i-1 >= 0 && j-2 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i+1,j) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1 && gsl_matrix_int_get(typemap,i,j-2) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 && gsl_matrix_int_get(typemap,i+1,j-1) != -1 && gsl_matrix_int_get(typemap,i-1,j-1)!= -1)
			      {
				gsl_matrix_int_set(typemap,i,j,8);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && i+1 < y && i-1 >= 0 && j+2 < x)
			  {
			    if(gsl_matrix_int_get(typemap,i+1,j) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1 && gsl_matrix_int_get(typemap,i,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i+1,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j+1)!= -1)
			      {
				gsl_matrix_int_set(typemap,i,j,7);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && j+1 < x && j-1 >= 0 && i-2 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 && gsl_matrix_int_get(typemap,i-2,j) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1 && gsl_matrix_int_get(typemap,i-1,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j-1)!= -1)
			      {
				gsl_matrix_int_set(typemap,i,j,6);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && j+1 < x && j-1 >= 0 && i+2 < y)
			  {
			    if(gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 && gsl_matrix_int_get(typemap,i+2,j) != -1 && gsl_matrix_int_get(typemap,i+1,j) != -1 && gsl_matrix_int_get(typemap,i+1,j+1) != -1 && gsl_matrix_int_get(typemap,i+1,j-1)!= -1)
			      {
				gsl_matrix_int_set(typemap,i,j,5);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && j-2 >= 0 && i-2 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i,j-2) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1 && gsl_matrix_int_get(typemap,i-1,j-1) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1 && gsl_matrix_int_get(typemap,i-2,j) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,4);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && j+2 < y && i-2 >= 0)
			  {
			    if(gsl_matrix_int_get(typemap,i,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j+1) != -1 && gsl_matrix_int_get(typemap,i-1,j) != -1 && gsl_matrix_int_get(typemap,i-2,j) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,3);
				notfoundyet = false;
				numpix++;
			      }
			  }
			
			if(notfoundyet && j-2 >= 0 && i+2 < y)
			  {
			    if(gsl_matrix_int_get(typemap,i+2,j) != -1 && gsl_matrix_int_get(typemap,i+1,j) != -1 && gsl_matrix_int_get(typemap,i+1,j-1) != -1 && gsl_matrix_int_get(typemap,i,j-2) != -1 && gsl_matrix_int_get(typemap,i,j-1) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,2);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet && j+2 < x && i+2 < y)
			  {
			    if(gsl_matrix_int_get(typemap,i+2,j) != -1 && gsl_matrix_int_get(typemap,i+1,j) != -1 && gsl_matrix_int_get(typemap,i+1,j+1) != -1 && gsl_matrix_int_get(typemap,i,j+2) != -1 && gsl_matrix_int_get(typemap,i,j+1) != -1)
			      {
				gsl_matrix_int_set(typemap,i,j,1);
				notfoundyet = false;
				numpix++;
			      }
			  }
			if(notfoundyet)
			  {
			    badpixels++;
			    gsl_matrix_int_set(typemap,i,j,-1);
			  }
		      }
		  }
	      }
	    if(badpixels == 0)
	      {
		badgrid = false;
	      }
	    else
	      {
		run_counter++;
	      }
	  }	
      }

    if(run_counter >= 5)
      {
	throw runtime_error("Grid masking too complicated");
      }
    //Type definition finished

    //Creating the indexmap

    allocationtable[0] = true;
    indexmap = gsl_matrix_int_calloc(y,x);
    gsl_matrix_int_set_all(indexmap,-1);

    int counter = 0;

    for(int i = 0; i < y; i++)
      {
	for(int j = 0; j < x; j++)
	  {
	    if(gsl_matrix_int_get(typemap,i,j) != -1)
	      {
		gsl_matrix_int_set(indexmap,i,j,counter);
		counter++;
	      }
	  }
      }

    //Allocating necessary coefficient and position vector before they
    //are needed.

    allocationtable[1] = true;
    a1 = gsl_matrix_calloc(numpix,3);
    a2 = gsl_matrix_calloc(numpix,3);
    a1pos = gsl_matrix_int_calloc(numpix,3);
    a2pos = gsl_matrix_int_calloc(numpix,3);
    if(order > 1)
	{
	    allocationtable[2] = true;
	    //gsl_vector_bool_set(allocationtable,2, true);
	    c = gsl_matrix_calloc(numpix,9);
	    cpos = gsl_matrix_int_calloc(numpix,9);
	    s1 = gsl_matrix_calloc(numpix,5);
	    s2 = gsl_matrix_calloc(numpix,7);
	    s1pos = gsl_matrix_int_calloc(numpix,5);
	    s2pos = gsl_matrix_int_calloc(numpix,7);
	}
    if(order > 2)
	{
	    allocationtable[3] = true;
	    //gsl_vector_bool_set(allocationtable,3, true);
	    f1 = gsl_matrix_calloc(numpix,8);
	    f2 = gsl_matrix_calloc(numpix,8);
	    f1pos = gsl_matrix_int_calloc(numpix,8);
	    f2pos = gsl_matrix_int_calloc(numpix,8);
	    g1 = gsl_matrix_calloc(numpix,8);
	    g2 = gsl_matrix_calloc(numpix,8);
	    g1pos = gsl_matrix_int_calloc(numpix,8);
	    g2pos = gsl_matrix_int_calloc(numpix,8);
	}


    double h, factor1, factor2, factor3;
    h =(double) x / size;
    factor1 = pow(h,1.0);
    factor2 = pow(h,2.0);
    factor3 = pow(h,3.0);
    counter = 0;
    for(int i = 0; i < y; i++)
	{
	    for(int j = 0; j < x; j++)
		{

		  //cout <<i <<"\t" <<j <<endl;
		  //cout <<gsl_matrix_int_get(typemap,i,j) <<endl;
//Setting a1
		    if(gsl_matrix_int_get(typemap,i,j) == 1 || gsl_matrix_int_get(typemap,i,j) == 7 || gsl_matrix_int_get(typemap,i,j) == 3)
			{

			    gsl_matrix_set(a1,counter,0,-3./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,0,gsl_matrix_int_get(indexmap,i,j));
			    gsl_matrix_set(a1,counter,1,2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,1,gsl_matrix_int_get(indexmap,i,j+1));
			    gsl_matrix_set(a1,counter,2,-1./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,2,gsl_matrix_int_get(indexmap,i,j+2));

			}
		    if(gsl_matrix_int_get(typemap,i,j) == 2 || gsl_matrix_int_get(typemap,i,j) == 8 || gsl_matrix_int_get(typemap,i,j) == 4)
			{
			    gsl_matrix_set(a1,counter,0,1./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,0,gsl_matrix_int_get(indexmap,i,j-2));
			    gsl_matrix_set(a1,counter,1,-2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,1,gsl_matrix_int_get(indexmap,i,j-1));
			    gsl_matrix_set(a1,counter,2,3./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,2,gsl_matrix_int_get(indexmap,i,j));
			}
		    if(gsl_matrix_int_get(typemap,i,j) == 0 || gsl_matrix_int_get(typemap,i,j) == 5 || gsl_matrix_int_get(typemap,i,j) == 6)
			{
			    gsl_matrix_set(a1,counter,0,-1./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,0,gsl_matrix_int_get(indexmap,i,j-1));
			    gsl_matrix_set(a1,counter,1,1./2.*factor1);
			    gsl_matrix_int_set(a1pos,counter,1,gsl_matrix_int_get(indexmap,i,j+1));
			}

// Setting a2
		    if(gsl_matrix_int_get(typemap,i,j) == 1 || gsl_matrix_int_get(typemap,i,j) == 5 || gsl_matrix_int_get(typemap,i,j) == 2)
			{
			    gsl_matrix_set(a2,counter,0,-3./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,0,gsl_matrix_int_get(indexmap,i,j));
			    gsl_matrix_set(a2,counter,1,2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,1,gsl_matrix_int_get(indexmap,i+1,j));
			    gsl_matrix_set(a2,counter,2,-1./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,2,gsl_matrix_int_get(indexmap,i+2,j));
			}
		    if(gsl_matrix_int_get(typemap,i,j) == 3 || gsl_matrix_int_get(typemap,i,j) == 6 || gsl_matrix_int_get(typemap,i,j) == 4)
			{
			    gsl_matrix_set(a2,counter,0,1./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
			    gsl_matrix_set(a2,counter,1,-2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
			    gsl_matrix_set(a2,counter,2,3./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,2,gsl_matrix_int_get(indexmap,i,j));
			}
		    if(gsl_matrix_int_get(typemap,i,j) == 0 || gsl_matrix_int_get(typemap,i,j) == 7 || gsl_matrix_int_get(typemap,i,j) == 8)
			{
			    gsl_matrix_set(a2,counter,0,-1./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
			    gsl_matrix_set(a2,counter,1,1./2.*factor1);
			    gsl_matrix_int_set(a2pos,counter,1,gsl_matrix_int_get(indexmap,i+1,j));
			}
// Setting 2nd order
		    if(order > 1)
			{

			    if(gsl_matrix_int_get(typemap,i,j) == 1)
				{
//setting s1
				    gsl_matrix_set(s1,counter,0,-factor2);
				    gsl_matrix_int_set(s1pos,counter,0,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(s1,counter,1,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(s1,counter,2,factor2);
				    gsl_matrix_int_set(s1pos,counter,2,gsl_matrix_int_get(indexmap,i+1,j));	
				    gsl_matrix_set(s1,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,gsl_matrix_int_get(indexmap,i+2,j));

//setting s2

				    gsl_matrix_set(s2,counter,0,factor2);
				    gsl_matrix_int_set(s2pos,counter,0,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(s2,counter,1,-factor2);
				    gsl_matrix_int_set(s2pos,counter,1,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(s2,counter,2,-factor2);
				    gsl_matrix_int_set(s2pos,counter,2,gsl_matrix_int_get(indexmap,i+1,j));	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j+1));


//setting c

				    gsl_matrix_set(c,counter,0,factor2);
				    gsl_matrix_int_set(cpos,counter,0,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(c,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,gsl_matrix_int_get(indexmap,i,j+2));	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,gsl_matrix_int_get(indexmap,i+2,j));
				}
			    if(gsl_matrix_int_get(typemap,i,j) == 2)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(s1,counter,1,-factor2);
				    gsl_matrix_int_set(s1pos,counter,1,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(s1,counter,2,factor2);
				    gsl_matrix_int_set(s1pos,counter,2,gsl_matrix_int_get(indexmap,i+1,j));	
				    gsl_matrix_set(s1,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,gsl_matrix_int_get(indexmap,i+2,j));

//setting s2

				    gsl_matrix_set(s2,counter,0,factor2);
				    gsl_matrix_int_set(s2pos,counter,0,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(s2,counter,1,-factor2);
				    gsl_matrix_int_set(s2pos,counter,1,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(s2,counter,2,-factor2);
				    gsl_matrix_int_set(s2pos,counter,2,gsl_matrix_int_get(indexmap,i+1,j-1));	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j));

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(c,counter,2,factor2);
				    gsl_matrix_int_set(cpos,counter,2,gsl_matrix_int_get(indexmap,i,j));	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,gsl_matrix_int_get(indexmap,i+2,j));
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 3)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(s1,counter,1,factor2);
				    gsl_matrix_int_set(s1pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s1,counter,2,-factor2);
				    gsl_matrix_int_set(s1pos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));	
				    gsl_matrix_set(s1,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,gsl_matrix_int_get(indexmap,i,j+2));

//setting s2

				    gsl_matrix_set(s2,counter,0,factor2);
				    gsl_matrix_int_set(s2pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s2,counter,1,-factor2);
				    gsl_matrix_int_set(s2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(s2,counter,2,-factor2);
				    gsl_matrix_int_set(s2pos,counter,2,gsl_matrix_int_get(indexmap,i,j));	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,gsl_matrix_int_get(indexmap,i,j+1));

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(c,counter,2,factor2);
				    gsl_matrix_int_set(cpos,counter,2,gsl_matrix_int_get(indexmap,i,j));	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,gsl_matrix_int_get(indexmap,i,j+2));

				}

			    if(gsl_matrix_int_get(typemap,i,j) == 4)
				{
//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(s1,counter,1,factor2);
				    gsl_matrix_int_set(s1pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s1,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,2,gsl_matrix_int_get(indexmap,i,j-2));	
				    gsl_matrix_set(s1,counter,3,-factor2);
				    gsl_matrix_int_set(s1pos,counter,3,gsl_matrix_int_get(indexmap,i,j-1));

//setting s2

				    gsl_matrix_set(s2,counter,0,factor2);
				    gsl_matrix_int_set(s2pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(s2,counter,1,-factor2);
				    gsl_matrix_int_set(s2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s2,counter,2,-factor2);
				    gsl_matrix_int_set(s2pos,counter,2,gsl_matrix_int_get(indexmap,i,j-1));	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,gsl_matrix_int_get(indexmap,i,j));

//setting c 

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(c,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,gsl_matrix_int_get(indexmap,i,j-2));	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(c,counter,4,factor2);
				    gsl_matrix_int_set(cpos,counter,4,gsl_matrix_int_get(indexmap,i,j));
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 5)
				{
//setting s1
				    gsl_matrix_set(s1,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(s1,counter,1,-3./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(s1,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));	
				    gsl_matrix_set(s1,counter,3,factor2);
				    gsl_matrix_int_set(s1pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(s1,counter,4,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,4,gsl_matrix_int_get(indexmap,i+2,j));
//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,gsl_matrix_int_get(indexmap,i+1,j-1));	
				    gsl_matrix_set(s2,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j+1));

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(c,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,1,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(c,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));	
				    gsl_matrix_set(c,counter,3,-factor2);
				    gsl_matrix_int_set(cpos,counter,3,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,gsl_matrix_int_get(indexmap,i+2,j));
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 6)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(s1,counter,1,factor2);
				    gsl_matrix_int_set(s1pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s1,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,2,gsl_matrix_int_get(indexmap,i,j-1));	
				    gsl_matrix_set(s1,counter,3,-3./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(s1,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,4,gsl_matrix_int_get(indexmap,i,j+1));

//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,gsl_matrix_int_get(indexmap,i,j-1));	
				    gsl_matrix_set(s2,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,3,gsl_matrix_int_get(indexmap,i,j+1));

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(c,counter,1,-factor2);
				    gsl_matrix_int_set(cpos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(c,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,gsl_matrix_int_get(indexmap,i,j-1));	
				    gsl_matrix_set(c,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,3,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,gsl_matrix_int_get(indexmap,i,j+1));
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 7)
				{
//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s1,counter,1,3./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(s1,counter,2,-factor2);
				    gsl_matrix_int_set(s1pos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));	
				    gsl_matrix_set(s1,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(s1,counter,4,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));

//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,gsl_matrix_int_get(indexmap,i+1,j));	
				    gsl_matrix_set(s2,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j+1));

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(c,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,1,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(c,counter,2,-factor2);
				    gsl_matrix_int_set(cpos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));	
				    gsl_matrix_set(c,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,3,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 8)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s1,counter,1,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(s1,counter,2,-factor2);
				    gsl_matrix_int_set(s1pos,counter,2,gsl_matrix_int_get(indexmap,i,j-1));	
				    gsl_matrix_set(s1,counter,3,3./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(s1,counter,4,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));

//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,gsl_matrix_int_get(indexmap,i+1,j-1));	
				    gsl_matrix_set(s2,counter,3,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j));

//setting c

				    gsl_matrix_set(c,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(c,counter,1,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,1,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(c,counter,2,-factor2);
				    gsl_matrix_int_set(cpos,counter,2,gsl_matrix_int_get(indexmap,i,j-1));	
				    gsl_matrix_set(c,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,3,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(c,counter,4,1./2.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));
				}

			    if(gsl_matrix_int_get(typemap,i,j) == 0)
				{

//setting s1
				    gsl_matrix_set(s1,counter,0,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s1,counter,1,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,1,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(s1,counter,2,1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));	
				    gsl_matrix_set(s1,counter,3,-1./2.*factor2);
				    gsl_matrix_int_set(s1pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j));

//setting s2

				    gsl_matrix_set(s2,counter,0,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(s2,counter,1,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(s2,counter,2,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,2,gsl_matrix_int_get(indexmap,i,j-1));	
				    gsl_matrix_set(s2,counter,3,factor2);
				    gsl_matrix_int_set(s2pos,counter,3,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(s2,counter,4,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,4,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(s2,counter,5,-1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(s2,counter,6,1./2.*factor2);
				    gsl_matrix_int_set(s2pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j+1));

//setting c

				    gsl_matrix_set(c,counter,0,1./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,0,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(c,counter,1,-1./6.*factor2);
				    gsl_matrix_int_set(cpos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(c,counter,2,1./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,2,gsl_matrix_int_get(indexmap,i-1,j+1));	
				    gsl_matrix_set(c,counter,3,-1./6.*factor2);
				    gsl_matrix_int_set(cpos,counter,3,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(c,counter,4,-2./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,4,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(c,counter,5,-1./6.*factor2);
				    gsl_matrix_int_set(cpos,counter,5,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(c,counter,6,1./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,6,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(c,counter,7,-1./6.*factor2);
				    gsl_matrix_int_set(cpos,counter,7,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(c,counter,8,1./3.*factor2);
				    gsl_matrix_int_set(cpos,counter,8,gsl_matrix_int_get(indexmap,i+1,j+1));
				}
				    
			}


		    if(order > 2)
			{

			    if(gsl_matrix_int_get(typemap,i,j) == 1)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-factor3);
				    gsl_matrix_int_set(f1pos,counter,0,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f1,counter,1,2.0*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(f1,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(f1,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,gsl_matrix_int_get(indexmap,i,j+3));
				    gsl_matrix_set(f1,counter,4,factor3);
				    gsl_matrix_int_set(f1pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(f1,counter,5,-factor3);
				    gsl_matrix_int_set(f1pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(f1,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j));
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j+1));
//setting g1
				    gsl_matrix_set(g1,counter,0,factor3);
				    gsl_matrix_int_set(g1pos,counter,0,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g1,counter,1,0.);
				    gsl_matrix_int_set(g1pos,counter,1,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(g1,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(g1,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,gsl_matrix_int_get(indexmap,i,j+3));
				    gsl_matrix_set(g1,counter,4,-3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(g1,counter,5,3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(g1,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j));
				    gsl_matrix_set(g1,counter,7,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j+1));
//setting f2
				    gsl_matrix_set(f2,counter,0,-factor3);
				    gsl_matrix_int_set(f2pos,counter,0,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f2,counter,1,factor3);
				    gsl_matrix_int_set(f2pos,counter,1,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(f2,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(f2,counter,3,2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(f2,counter,4,-factor3);
				    gsl_matrix_int_set(f2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(f2,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+2));
				    gsl_matrix_set(f2,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j));
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,gsl_matrix_int_get(indexmap,i+3,j));
//setting g2
				    gsl_matrix_set(g2,counter,0,-factor3);
				    gsl_matrix_int_set(g2pos,counter,0,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g2,counter,1,3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(g2,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(g2,counter,3,0.);
				    gsl_matrix_int_set(g2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(g2,counter,4,-3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(g2,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+2));
				    gsl_matrix_set(g2,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j));
				    gsl_matrix_set(g2,counter,7,-1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,gsl_matrix_int_get(indexmap,i+3,j));


				}

			    if(gsl_matrix_int_get(typemap,i,j) == 2)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,gsl_matrix_int_get(indexmap,i,j-3));
				    gsl_matrix_set(f1,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(f1,counter,2,-2.0*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f1,counter,3,factor3);
				    gsl_matrix_int_set(f1pos,counter,3,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f1,counter,4,factor3);
				    gsl_matrix_int_set(f1pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(f1,counter,5,-factor3);
				    gsl_matrix_int_set(f1pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(f1,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j-1));
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j));
//setting g1
				    gsl_matrix_set(g1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,gsl_matrix_int_get(indexmap,i,j-3));
				    gsl_matrix_set(g1,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(g1,counter,2,0.);
				    gsl_matrix_int_set(g1pos,counter,2,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g1,counter,3,-factor3);
				    gsl_matrix_int_set(g1pos,counter,3,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g1,counter,4,-3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(g1,counter,5,3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(g1,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j-1));
				    gsl_matrix_set(g1,counter,7,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j));

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(f2,counter,1,factor3);
				    gsl_matrix_int_set(f2pos,counter,1,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f2,counter,2,-factor3);
				    gsl_matrix_int_set(f2pos,counter,2,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f2,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j-2));
				    gsl_matrix_set(f2,counter,4,-factor3);
				    gsl_matrix_int_set(f2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(f2,counter,5,2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(f2,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j));
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,gsl_matrix_int_get(indexmap,i+3,j));

//setting g2
				    gsl_matrix_set(g2,counter,0,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(g2,counter,1,3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g2,counter,2,-factor3);
				    gsl_matrix_int_set(g2pos,counter,2,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g2,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j-2));
				    gsl_matrix_set(g2,counter,4,-3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(g2,counter,5,0.);
				    gsl_matrix_int_set(g2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(g2,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j));
				    gsl_matrix_set(g2,counter,7,-1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,gsl_matrix_int_get(indexmap,i+3,j));



				}

			    if(gsl_matrix_int_get(typemap,i,j) == 3)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(f1,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j+1));
				    gsl_matrix_set(f1,counter,2,factor3);
				    gsl_matrix_int_set(f1pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f1,counter,3,-factor3);
				    gsl_matrix_int_set(f1pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(f1,counter,4,-factor3);
				    gsl_matrix_int_set(f1pos,counter,4,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f1,counter,5,2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(f1,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,gsl_matrix_int_get(indexmap,i,j+3));
//setting g1
				    gsl_matrix_set(g1,counter,0,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(g1,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j+1));
				    gsl_matrix_set(g1,counter,2,-3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g1,counter,3,3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(g1,counter,4,factor3);
				    gsl_matrix_int_set(g1pos,counter,4,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g1,counter,5,0.);
				    gsl_matrix_int_set(g1pos,counter,5,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(g1,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(g1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,gsl_matrix_int_get(indexmap,i,j+3));

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,gsl_matrix_int_get(indexmap,i-3,j));
				    gsl_matrix_set(f2,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(f2,counter,2,-2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f2,counter,3,factor3);
				    gsl_matrix_int_set(f2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(f2,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,gsl_matrix_int_get(indexmap,i-1,j+2));
				    gsl_matrix_set(f2,counter,5,factor3);
				    gsl_matrix_int_set(f2pos,counter,5,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f2,counter,6,-factor3);
				    gsl_matrix_int_set(f2pos,counter,6,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,gsl_matrix_int_get(indexmap,i,j+2));

//setting g2
				    gsl_matrix_set(g2,counter,0,1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,gsl_matrix_int_get(indexmap,i-3,j));
				    gsl_matrix_set(g2,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(g2,counter,2,0.);
				    gsl_matrix_int_set(g2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g2,counter,3,3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(g2,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,gsl_matrix_int_get(indexmap,i-1,j+2));
				    gsl_matrix_set(g2,counter,5,factor3);
				    gsl_matrix_int_set(g2pos,counter,5,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g2,counter,6,-3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(g2,counter,7,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,gsl_matrix_int_get(indexmap,i,j+2));



				}

			    if(gsl_matrix_int_get(typemap,i,j) == 4)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j-1));
				    gsl_matrix_set(f1,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(f1,counter,2,factor3);
				    gsl_matrix_int_set(f1pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(f1,counter,3,-factor3);
				    gsl_matrix_int_set(f1pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f1,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,gsl_matrix_int_get(indexmap,i,j-3));
				    gsl_matrix_set(f1,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(f1,counter,6,-2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f1,counter,7,factor3);
				    gsl_matrix_int_set(f1pos,counter,7,gsl_matrix_int_get(indexmap,i,j));
//setting g1
				    gsl_matrix_set(g1,counter,0,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j-1));
				    gsl_matrix_set(g1,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(g1,counter,2,-3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(g1,counter,3,3.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g1,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,gsl_matrix_int_get(indexmap,i,j-3));
				    gsl_matrix_set(g1,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(g1,counter,6,0.);
				    gsl_matrix_int_set(g1pos,counter,6,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g1,counter,7,-factor3);
				    gsl_matrix_int_set(g1pos,counter,7,gsl_matrix_int_get(indexmap,i,j));

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,gsl_matrix_int_get(indexmap,i-3,j));
				    gsl_matrix_set(f2,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(f2,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-2));
				    gsl_matrix_set(f2,counter,3,factor3);
				    gsl_matrix_int_set(f2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(f2,counter,4,-2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f2,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(f2,counter,6,-factor3);
				    gsl_matrix_int_set(f2pos,counter,6,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f2,counter,7,factor3);
				    gsl_matrix_int_set(f2pos,counter,7,gsl_matrix_int_get(indexmap,i,j));

//setting g2
				    gsl_matrix_set(g2,counter,0,1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,gsl_matrix_int_get(indexmap,i-3,j));
				    gsl_matrix_set(g2,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(g2,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-2));
				    gsl_matrix_set(g2,counter,3,3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(g2,counter,4,0.);
				    gsl_matrix_int_set(g2pos,counter,4,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g2,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(g2,counter,6,-3.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g2,counter,7,factor3);
				    gsl_matrix_int_set(g2pos,counter,7,gsl_matrix_int_get(indexmap,i,j));



				}

			    if(gsl_matrix_int_get(typemap,i,j) == 5)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(f1,counter,1,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f1,counter,2,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(f1,counter,3,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(f1,counter,4,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(f1,counter,5,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(f1,counter,6,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j-1));
				    gsl_matrix_set(f1,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j+1));
//setting g1
				    gsl_matrix_set(g1,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(g1,counter,1,5./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g1,counter,2,-5./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(g1,counter,3,1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(g1,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(g1,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(g1,counter,6,3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j-1));
				    gsl_matrix_set(g1,counter,7,-3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j+1));

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f2,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f2,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(f2,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(f2,counter,4,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(f2,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(f2,counter,6,-3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j));
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,gsl_matrix_int_get(indexmap,i+3,j));

//setting g2
				    gsl_matrix_set(g2,counter,0,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g2,counter,1,7./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g2,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(g2,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(g2,counter,4,-9./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(g2,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(g2,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,gsl_matrix_int_get(indexmap,i+2,j));
				    gsl_matrix_set(g2,counter,7,-1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,gsl_matrix_int_get(indexmap,i+3,j));



				}

			    if(gsl_matrix_int_get(typemap,i,j) == 6)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j-1));
				    gsl_matrix_set(f1,counter,1,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j+1));
				    gsl_matrix_set(f1,counter,2,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(f1,counter,3,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(f1,counter,4,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(f1,counter,5,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f1,counter,6,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(f1,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,gsl_matrix_int_get(indexmap,i,j+2));
//setting g1
				    gsl_matrix_set(g1,counter,0,3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j-1));
				    gsl_matrix_set(g1,counter,1,-3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j+1));
				    gsl_matrix_set(g1,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(g1,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(g1,counter,4,-1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(g1,counter,5,5./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g1,counter,6,-5./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(g1,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,gsl_matrix_int_get(indexmap,i,j+2));

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,gsl_matrix_int_get(indexmap,i-3,j));
				    gsl_matrix_set(f2,counter,1,3./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(f2,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(f2,counter,3,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f2,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(f2,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f2,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f2,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,gsl_matrix_int_get(indexmap,i,j+1));

//setting g2
				    gsl_matrix_set(g2,counter,0,1./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,gsl_matrix_int_get(indexmap,i-3,j));
				    gsl_matrix_set(g2,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(g2,counter,2,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(g2,counter,3,9./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g2,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(g2,counter,5,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g2,counter,6,-7./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g2,counter,7,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,gsl_matrix_int_get(indexmap,i,j+1));


				}

			    if(gsl_matrix_int_get(typemap,i,j) == 7)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f1,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(f1,counter,2,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f1,counter,3,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(f1,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(f1,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,gsl_matrix_int_get(indexmap,i,j+3));
				    gsl_matrix_set(f1,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,gsl_matrix_int_get(indexmap,i+1,j+1));
//setting g1
				    gsl_matrix_set(g1,counter,0,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g1,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(g1,counter,2,-7./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g1,counter,3,9./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(g1,counter,4,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(g1,counter,5,1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,gsl_matrix_int_get(indexmap,i,j+3));
				    gsl_matrix_set(g1,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(g1,counter,7,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,gsl_matrix_int_get(indexmap,i+1,j+1));

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(f2,counter,1,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f2,counter,2,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(f2,counter,3,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+2));
				    gsl_matrix_set(f2,counter,4,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(f2,counter,5,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(f2,counter,6,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j+2));
				    gsl_matrix_set(f2,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j));

//setting g2
				    gsl_matrix_set(g2,counter,0,1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(g2,counter,1,-5./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g2,counter,2,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(g2,counter,3,-3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+2));
				    gsl_matrix_set(g2,counter,4,5./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(g2,counter,5,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(g2,counter,6,3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j+2));
				    gsl_matrix_set(g2,counter,7,-1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j));


				}

			    if(gsl_matrix_int_get(typemap,i,j) == 8)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(f1,counter,1,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f1,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,gsl_matrix_int_get(indexmap,i,j-3));
				    gsl_matrix_set(f1,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,3,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(f1,counter,4,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,4,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f1,counter,5,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(f1,counter,6,-1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(f1,counter,7,1./2.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,gsl_matrix_int_get(indexmap,i+1,j));
//setting g1
				    gsl_matrix_set(g1,counter,0,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(g1,counter,1,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g1,counter,2,-1./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,gsl_matrix_int_get(indexmap,i,j-3));
				    gsl_matrix_set(g1,counter,3,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,3,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(g1,counter,4,-9./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,4,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g1,counter,5,7./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,gsl_matrix_int_get(indexmap,i,j));
				    gsl_matrix_set(g1,counter,6,3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(g1,counter,7,-3./2.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,gsl_matrix_int_get(indexmap,i+1,j));

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(f2,counter,1,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j-2));
				    gsl_matrix_set(f2,counter,2,1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(f2,counter,3,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f2,counter,4,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-2));
				    gsl_matrix_set(f2,counter,5,-1./2.*factor3);
				    gsl_matrix_int_set(f2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(f2,counter,6,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(f2,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j));

//setting g2
				    gsl_matrix_set(g2,counter,0,1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(g2,counter,1,-3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j-2));
				    gsl_matrix_set(g2,counter,2,3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(g2,counter,3,-5./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g2,counter,4,3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-2));
				    gsl_matrix_set(g2,counter,5,-3./2.*factor3);
				    gsl_matrix_int_set(g2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(g2,counter,6,5./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(g2,counter,7,-1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j));


				}

			    if(gsl_matrix_int_get(typemap,i,j) == 0)
				{
//setting f1
				    gsl_matrix_set(f1,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(f1,counter,1,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(f1,counter,2,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,2,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(f1,counter,3,factor3);
				    gsl_matrix_int_set(f1pos,counter,3,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(f1,counter,4,-factor3);
				    gsl_matrix_int_set(f1pos,counter,4,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(f1,counter,5,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,5,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(f1,counter,6,-1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(f1,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f1pos,counter,7,gsl_matrix_int_get(indexmap,i+1,j+1));
//setting g1
				    gsl_matrix_set(g1,counter,0,3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,0,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(g1,counter,1,-3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(g1,counter,2,-1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,2,gsl_matrix_int_get(indexmap,i,j-2));
				    gsl_matrix_set(g1,counter,3,-factor3);
				    gsl_matrix_int_set(g1pos,counter,3,gsl_matrix_int_get(indexmap,i,j-1));
				    gsl_matrix_set(g1,counter,4,factor3);
				    gsl_matrix_int_set(g1pos,counter,4,gsl_matrix_int_get(indexmap,i,j+1));
				    gsl_matrix_set(g1,counter,5,1./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,5,gsl_matrix_int_get(indexmap,i,j+2));
				    gsl_matrix_set(g1,counter,6,3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(g1,counter,7,-3./4.*factor3);
				    gsl_matrix_int_set(g1pos,counter,7,gsl_matrix_int_get(indexmap,i+1,j+1));

//setting f2
				    gsl_matrix_set(f2,counter,0,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(f2,counter,1,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(f2,counter,2,factor3);
				    gsl_matrix_int_set(f2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(f2,counter,3,-1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(f2,counter,4,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(f2,counter,5,-factor3);
				    gsl_matrix_int_set(f2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(f2,counter,6,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(f2,counter,7,1./4.*factor3);
				    gsl_matrix_int_set(f2pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j));

//setting g2
				    gsl_matrix_set(g2,counter,0,1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,0,gsl_matrix_int_get(indexmap,i-2,j));
				    gsl_matrix_set(g2,counter,1,-3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,1,gsl_matrix_int_get(indexmap,i-1,j-1));
				    gsl_matrix_set(g2,counter,2,factor3);
				    gsl_matrix_int_set(g2pos,counter,2,gsl_matrix_int_get(indexmap,i-1,j));
				    gsl_matrix_set(g2,counter,3,-3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,3,gsl_matrix_int_get(indexmap,i-1,j+1));
				    gsl_matrix_set(g2,counter,4,3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,4,gsl_matrix_int_get(indexmap,i+1,j-1));
				    gsl_matrix_set(g2,counter,5,-factor3);
				    gsl_matrix_int_set(g2pos,counter,5,gsl_matrix_int_get(indexmap,i+1,j));
				    gsl_matrix_set(g2,counter,6,3./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,6,gsl_matrix_int_get(indexmap,i+1,j+1));
				    gsl_matrix_set(g2,counter,7,-1./4.*factor3);
				    gsl_matrix_int_set(g2pos,counter,7,gsl_matrix_int_get(indexmap,i+2,j));


				}
			}


			    
		    if(gsl_matrix_int_get(typemap,i,j) != -1)
		      {
			counter++;
		      }
		}
	}




}


FinDifGrid::~FinDifGrid()
{

    gsl_matrix_int_free(typemap);

    if(allocationtable[0])
	{
	    gsl_matrix_int_free(indexmap);
	}

    if(allocationtable[1])
	{
	    gsl_matrix_free(a1);
	    gsl_matrix_free(a2);
	    gsl_matrix_int_free(a1pos);
	    gsl_matrix_int_free(a2pos);
	}
    if(allocationtable[2])
	{
	    gsl_matrix_free(s1);
	    gsl_matrix_free(s2);
	    gsl_matrix_free(c);
	    gsl_matrix_int_free(s1pos);
	    gsl_matrix_int_free(s2pos);
	    gsl_matrix_int_free(cpos);
	}

    if(allocationtable[3])
	{
	    gsl_matrix_free(f1);
	    gsl_matrix_free(f2);
	    gsl_matrix_free(g1);
	    gsl_matrix_free(g2);
	    gsl_matrix_int_free(f1pos);
	    gsl_matrix_int_free(f2pos);
	    gsl_matrix_int_free(g1pos);
	    gsl_matrix_int_free(g2pos);
	}

    if(allocationtable[4])
	{
	    gsl_matrix_free(inv_a1);
	    gsl_matrix_free(inv_a2);
	    gsl_matrix_int_free(inv_a1pos);
	    gsl_matrix_int_free(inv_a2pos);
	}
   if(allocationtable[5])
	{
	    gsl_matrix_free(inv_s1);
	    gsl_matrix_free(inv_s2);
	    gsl_matrix_free(inv_c);
	    gsl_matrix_int_free(inv_s1pos);
	    gsl_matrix_int_free(inv_s2pos);
	    gsl_matrix_int_free(inv_cpos);
	}

   if(allocationtable[6])
       {
	    gsl_matrix_free(inv_f1);
	    gsl_matrix_free(inv_f2);
	    gsl_matrix_free(inv_g1);
	    gsl_matrix_free(inv_g2);
	    gsl_matrix_int_free(inv_f1pos);
	    gsl_matrix_int_free(inv_f2pos);
	    gsl_matrix_int_free(inv_g1pos);
	    gsl_matrix_int_free(inv_g2pos);
	}
   if(allocationtable[7])
       {
	   gsl_matrix_int_free(mask);
       }
}

int FinDifGrid::show_int(string selection)
{

    if(selection == "x_dim")
	{
	    return x;
	}
    else if(selection == "y_dim")
	{
	    return y;
	}
    else if(selection == "numpix")
	{
	    return numpix;
	}
    else if(selection == "findif order")
	{
	    return order;
	}
    else
	{
	    throw invalid_argument("Invalid selection for findif showint");
	}
}

double FinDifGrid::show_double(string selection)
{

    if(selection == "size")
	{
	    return size;
	}
    else
	{
	    throw invalid_argument("Invalid selection for findif showdouble");
	}
}


gsl_matrix_int* FinDifGrid::show_data(string selection)
{

    if(selection == "typemap")
	{
	    return typemap;
	}
    else if(selection == "indexmap")
	{
	    if(allocationtable[0])
		{
		    return indexmap;
		}
	    else
		{
		    allocationtable[0] = true;
		    //  gsl_vector_bool_set(allocationtable,0,true);
		    indexmap = gsl_matrix_int_calloc(y,x);
		    int counter = 0;
		    for(int i = 0; i < y; i++)
			{
			    for(int j = 0; j < x; j++)
				{
				    gsl_matrix_int_set(indexmap,i,j,counter);
				    counter++;
				}
			}
		    return indexmap;
		}
	}
    else if(selection == "mask")
	{
	    if(allocationtable[7])
		{
		    return mask;
		}
	    else
		{
		    allocationtable[7] = true;
		    //gsl_vector_bool_set(allocationtable,7,true);
		    mask = gsl_matrix_int_calloc(y,x);
		    for(int i = 0; i < y; i++)
			{
			    for(int j = 0; j < x; j++)
				{
				    if(gsl_matrix_int_get(typemap,i,j) < 0)
					{
					    gsl_matrix_int_set(mask,i,j,1);
					}
				}
			}
		    return mask;
		}
	}
    else
	{
	    throw invalid_argument("Invalid selection for findif grid show data");
	}
}

gsl_matrix* FinDifGrid::coeffs(string selection)
{
    if(selection == "a1")
	{
	    return a1;
	}
    else if(selection == "a2")
	{
	    return a2;
	}
    else if(selection == "s1")
	{
	    if(order > 1)
		{
		    return s1;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "s2")
	{
	    if(order > 1)
		{
		    return s2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "c")
	{
	    if(order > 1)
		{
		    return c;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "f1")
	{
	    if(order > 2)
		{
		    return f1;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "f2")
	{
	    if(order > 2)
		{
		    return f2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "g1")
	{
	    if(order > 2)
		{
		    return g1;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "g2")
	{
	    if(order > 2)
		{
		    return g2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_a1")
	{
	    if(allocationtable[4])
		{
		    return inv_a1;
		}
	    else
		{
		    throw invalid_argument("Grid has not been inverted.");
		}
	}
    else if(selection == "inv_a2")
	{
	    if(allocationtable[4])
		{
		    return inv_a2;
		}
	    else
		{
		    throw invalid_argument("Grid has not been inverted.");
		}
	}
    else if(selection == "inv_s1")
	{
	    if(order > 1)
		{
		    if(allocationtable[5])
			{
			    return inv_s1;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_s2")
	{
	    if(order > 1)
		{
		    if(allocationtable[5])
			{
			    return inv_s2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_c")
	{
	    if(order > 1)
		{
		    if(allocationtable[5])
			{
			    return inv_c;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_f1")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return inv_f1;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_f2")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return inv_f2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_g1")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return inv_g1;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_g2")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return inv_g2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
}

gsl_matrix_int* FinDifGrid::positions(string selection)
{
    if(selection == "a1")
	{
	    return a1pos;
	}
    else if(selection == "a2")
	{
	    return a2pos;
	}
    else if(selection == "s1")
	{
	    if(order > 1)
		{
		    return s1pos;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "s2")
	{
	    if(order > 1)
		{
		    return s2pos;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "c")
	{
	    if(order > 1)
		{
		    return cpos;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "f1")
	{
	    if(order > 2)
		{
		    return f1pos;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "f2")
	{
	    if(order > 2)
		{
		    return f2pos;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "g1")
	{
	    if(order > 2)
		{
		    return g1pos;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "g2")
	{
	    if(order > 2)
		{
		    return g2pos;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_a1")
	{
	    if(allocationtable[4])
		{
		    return inv_a1pos;
		}
	    else
		{
		    throw invalid_argument("Grid has not been inverted.");
		}
	}
    else if(selection == "inv_a2")
	{
	    if(allocationtable[4])
		{
		    return inv_a2pos;
		}
	    else
		{
		    throw invalid_argument("Grid has not been inverted.");
		}
	}
    else if(selection == "inv_s1")
	{
	    if(order > 1)
		{
		    if(allocationtable[5])
			{
			    return inv_s1pos;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_s2")
	{
	    if(order > 1)
		{
		    if(allocationtable[5])
			{
			    return inv_s2pos;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_c")
	{
	    if(order > 1)
		{
		    if(allocationtable[5])
			{
			    return inv_cpos;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_f1")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return inv_f1pos;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_f2")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return inv_f2pos;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_g1")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return inv_g1pos;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_g2")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return inv_g2pos;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
}

int FinDifGrid::findif_length(string selection)
{
    if(selection == "a1")
	{
	    return (int) a1->size2;
	}
    else if(selection == "a2")
	{
	    return (int) a2->size2;
	}
    else if(selection == "s1")
	{
	    if(order > 1)
		{
		    return (int) s1->size2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "s2")
	{
	    if(order > 1)
		{
		    return (int) s2->size2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "c")
	{
	    if(order > 1)
		{
		    return (int) c->size2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "f1")
	{
	    if(order > 2)
		{
		    return f1->size2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "f2")
	{
	    if(order > 2)
		{
		    return (int) f2->size2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "g1")
	{
	    if(order > 2)
		{
		    return (int) g1->size2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "g2")
	{
	    if(order > 2)
		{
		    return (int) g2->size2;
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_a1")
	{
	    if(allocationtable[4])
		{
		    return (int) inv_a1->size2;
		}
	    else
		{
		    throw invalid_argument("Grid has not been inverted.");
		}
	}
    else if(selection == "inv_a2")
	{
	    if(allocationtable[4])
		{
		    return (int) inv_a2->size2;
		}
	    else
		{
		    throw invalid_argument("Grid has not been inverted.");
		}
	}
    else if(selection == "inv_s1")
	{
	    if(order > 1)
		{
		    if(allocationtable[5])
			{
			    return (int) inv_s1->size2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_s2")
	{
	    if(order > 1)
		{
		    if(allocationtable[5])
			{
			    return (int) inv_s2->size2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_c")
	{
	    if(order > 1)
		{
		    if(allocationtable[5])
			{
			    return (int) inv_c->size2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_f1")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return (int) inv_f1->size2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_f2")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return (int) inv_f2->size2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_g1")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return (int) inv_g1->size2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
    else if(selection == "inv_g2")
	{
	    if(order > 2)
		{
		    if(allocationtable[6])
			{
			    return (int) inv_g2->size2;
			}
		    else
			{
			    throw invalid_argument("Grid has not been inverted.");
			}
		}
	    else
		{
		    throw invalid_argument("Grid order was not chosen high enough");
		}
	}
}

void FinDifGrid::findif_matrix(string selection, gsl_matrix *out)
{

    if(out->size1 != numpix || out->size2 != numpix)
	{
	    throw invalid_argument("Dimensions of output matrix invalid for findif copy");
	}

    if(selection == "a1")
	{
	    for(int i = 0; i < numpix; i++)
		{
		    for(int j = 0; j < a1->size2; j++)
			{
			    gsl_matrix_set(out,i,gsl_matrix_int_get(a1pos,i,j),gsl_matrix_get(a1,i,j));
			}
		}
	}
    else if(selection == "a2")
	{
	    for(int i = 0; i < numpix; i++)
		{
		    for(int j = 0; j < a2->size2; j++)
			{
			    gsl_matrix_set(out,i,gsl_matrix_int_get(a2pos,i,j),gsl_matrix_get(a2,i,j));
			}
		}
	}
    else if(selection == "s1")
	{
	    if(order > 1)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    for(int j = 0; j < s1->size2; j++)
				{
				    gsl_matrix_set(out,i,gsl_matrix_int_get(s1pos,i,j),gsl_matrix_get(s1,i,j));
				}
			}
		}
	    else
		{
		    throw invalid_argument("Findif order too low for output of this findif matrix.");
		}
	}
    else if(selection == "s2")
	{
	    if(order > 1)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    for(int j = 0; j < s2->size2; j++)
				{
				    gsl_matrix_set(out,i,gsl_matrix_int_get(s2pos,i,j),gsl_matrix_get(s2,i,j));
				}
			}
		}
	    else
		{
		    throw invalid_argument("Findif order too low for output of this findif matrix.");
		}
	}
    else if(selection == "c")
	{
	    if(order > 1)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    for(int j = 0; j < c->size2; j++)
				{
				    gsl_matrix_set(out,i,gsl_matrix_int_get(cpos,i,j),gsl_matrix_get(c,i,j));
				}
			}
		}
	    else
		{
		    throw invalid_argument("Findif order too low for output of this findif matrix.");
		}
	}
    
    else if(selection == "f1")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    for(int j = 0; j < f1->size2; j++)
				{
				    gsl_matrix_set(out,i,gsl_matrix_int_get(f1pos,i,j),gsl_matrix_get(f1,i,j));
				}
			}
		}
	    else
		{
		    throw invalid_argument("Findif order too low for output of this findif matrix.");
		}
	}
    else if(selection == "f2")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    for(int j = 0; j < f2->size2; j++)
				{
				    gsl_matrix_set(out,i,gsl_matrix_int_get(f2pos,i,j),gsl_matrix_get(f2,i,j));
				}
			}
		}
	    else
		{
		    throw invalid_argument("Findif order too low for output of this findif matrix.");
		}
	}
    else if(selection == "g1")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    for(int j = 0; j < g1->size2; j++)
				{
				    gsl_matrix_set(out,i,gsl_matrix_int_get(g1pos,i,j),gsl_matrix_get(g1,i,j));
				}
			}
		}
	    else
		{
		    throw invalid_argument("Findif order too low for output of this findif matrix.");
		}
	}
    else if(selection == "g2")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    for(int j = 0; j < g2->size2; j++)
				{
				    gsl_matrix_set(out,i,gsl_matrix_int_get(g2pos,i,j),gsl_matrix_get(g2,i,j));
				}
			}
		}
	    else
		{
		    throw invalid_argument("Findif order too low for output of this findif matrix.");
		}
	}
    else
	{
	    throw invalid_argument("Invalid argument for findif matrix.");
	}
}


void FinDifGrid::invert()
{

    gsl_matrix *nofuture = gsl_matrix_calloc(numpix,numpix);

//Setting a1
    findif_matrix("a1",nofuture);
    int max = 0;
    int alltimemax = 0;

    for(int i = 0; i < numpix; i++)
	{
	    max = 0;
	    for(int j = 0; j < numpix; j++)
		{
		    if(gsl_matrix_get(nofuture,j,i) != 0.0)
			{
			    max++;
			}
		}
	    if(max > alltimemax)
		{
		    alltimemax = max;
		}
	}
    inv_a1 = gsl_matrix_calloc(numpix,alltimemax);
    inv_a1pos = gsl_matrix_int_calloc(numpix,alltimemax);
    allocationtable[4] = true;
    //gsl_vector_bool_set(allocationtable,4,true);
    alltimemax = 0;
    for(int i = 0; i < numpix; i++)
	{
	    max = 0;
	    for(int j = 0; j < numpix; j++)
		{
		    if(gsl_matrix_get(nofuture,j,i) != 0.0)
			{
			    gsl_matrix_set(inv_a1,i,max,gsl_matrix_get(nofuture,j,i));
			    gsl_matrix_int_set(inv_a1pos,i,max,j);
			    max++;
			}
		}
	}

//Setting a2
    findif_matrix("a2",nofuture);

    for(int i = 0; i < numpix; i++)
	{
	    max = 0;
	    for(int j = 0; j < numpix; j++)
		{
		    if(gsl_matrix_get(nofuture,j,i) != 0.0)
			{
			    max++;
			}
		}
	    if(max > alltimemax)
		{
		    alltimemax = max;
		}
	}
    inv_a2 = gsl_matrix_calloc(numpix,alltimemax);
    inv_a2pos = gsl_matrix_int_calloc(numpix,alltimemax);
    allocationtable[4] = true;
    //gsl_vector_bool_set(allocationtable,4,true);
    alltimemax = 0;
    for(int i = 0; i < numpix; i++)
	{
	    max = 0;
	    for(int j = 0; j < numpix; j++)
		{
		    if(gsl_matrix_get(nofuture,j,i) != 0.0)
			{
			    gsl_matrix_set(inv_a2,i,max,gsl_matrix_get(nofuture,j,i));
			    gsl_matrix_int_set(inv_a2pos,i,max,j);
			    max++;
			}
		}
	}

    if(order > 1)
	{

//Setting s1

	    findif_matrix("s1",nofuture);
	    
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    max++;
				}
			}
		    if(max > alltimemax)
			{
			    alltimemax = max;
			}
		}
	    inv_s1 = gsl_matrix_calloc(numpix,alltimemax);
	    inv_s1pos = gsl_matrix_int_calloc(numpix,alltimemax);
	    allocationtable[5] = true;
	    //gsl_vector_bool_set(allocationtable,5,true);
	    alltimemax = 0;
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    gsl_matrix_set(inv_s1,i,max,gsl_matrix_get(nofuture,j,i));
				    gsl_matrix_int_set(inv_s1pos,i,max,j);
				    max++;
				}
			}
		}

//Setting s2

	    findif_matrix("s2",nofuture);
	    
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    max++;
				}
			}
		    if(max > alltimemax)
			{
			    alltimemax = max;
			}
		}
	    inv_s2 = gsl_matrix_calloc(numpix,alltimemax);
	    inv_s2pos = gsl_matrix_int_calloc(numpix,alltimemax);
	    allocationtable[5] = true;
	    //gsl_vector_bool_set(allocationtable,5,true);
	    alltimemax = 0;
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    gsl_matrix_set(inv_s2,i,max,gsl_matrix_get(nofuture,j,i));
				    gsl_matrix_int_set(inv_s2pos,i,max,j);
				    max++;
				}
			}
		}

//Setting c

	    findif_matrix("c",nofuture);
	    
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    max++;
				}
			}
		    if(max > alltimemax)
			{
			    alltimemax = max;
			}
		}
	    inv_c = gsl_matrix_calloc(numpix,alltimemax);
	    inv_cpos = gsl_matrix_int_calloc(numpix,alltimemax);
	    allocationtable[5] = true;
	    //gsl_vector_bool_set(allocationtable,5,true);
	    alltimemax = 0;
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    gsl_matrix_set(inv_c,i,max,gsl_matrix_get(nofuture,j,i));
				    gsl_matrix_int_set(inv_cpos,i,max,j);
				    max++;
				}
			}
		}

	}

    if(order > 2)
	{

//Setting f1

	    findif_matrix("f1",nofuture);
	    
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    max++;
				}
			}
		    if(max > alltimemax)
			{
			    alltimemax = max;
			}
		}
	    inv_f1 = gsl_matrix_calloc(numpix,alltimemax);
	    inv_f1pos = gsl_matrix_int_calloc(numpix,alltimemax);
	    allocationtable[6] = true;
	    //gsl_vector_bool_set(allocationtable,6,true);
	    alltimemax = 0;
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    gsl_matrix_set(inv_f1,i,max,gsl_matrix_get(nofuture,j,i));
				    gsl_matrix_int_set(inv_f1pos,i,max,j);
				    max++;
				}
			}
		}

//Setting f2

	    findif_matrix("f2",nofuture);
	    
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    max++;
				}
			}
		    if(max > alltimemax)
			{
			    alltimemax = max;
			}
		}
	    inv_f2 = gsl_matrix_calloc(numpix,alltimemax);
	    inv_f2pos = gsl_matrix_int_calloc(numpix,alltimemax);
	    allocationtable[6] = true;
	    //gsl_vector_bool_set(allocationtable,6,true);
	    alltimemax = 0;
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    gsl_matrix_set(inv_f2,i,max,gsl_matrix_get(nofuture,j,i));
				    gsl_matrix_int_set(inv_f2pos,i,max,j);
				    max++;
				}
			}
		}

//Setting g1

	    findif_matrix("g1",nofuture);
	    
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    max++;
				}
			}
		    if(max > alltimemax)
			{
			    alltimemax = max;
			}
		}
	    inv_g1 = gsl_matrix_calloc(numpix,alltimemax);
	    inv_g1pos = gsl_matrix_int_calloc(numpix,alltimemax);
	    allocationtable[6] = true;
	    //gsl_vector_bool_set(allocationtable,6,true);
	    alltimemax = 0;
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    gsl_matrix_set(inv_g1,i,max,gsl_matrix_get(nofuture,j,i));
				    gsl_matrix_int_set(inv_g1pos,i,max,j);
				    max++;
				}
			}
		}

//Setting g2

	    findif_matrix("g2",nofuture);
	    
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    max++;
				}
			}
		    if(max > alltimemax)
			{
			    alltimemax = max;
			}
		}
	    inv_g2 = gsl_matrix_calloc(numpix,alltimemax);
	    inv_g2pos = gsl_matrix_int_calloc(numpix,alltimemax);
	    allocationtable[6] = true;
	    //gsl_vector_bool_set(allocationtable,6,true);
	    alltimemax = 0;
	    for(int i = 0; i < numpix; i++)
		{
		    max = 0;
		    for(int j = 0; j < numpix; j++)
			{
			    if(gsl_matrix_get(nofuture,j,i) != 0.0)
				{
				    gsl_matrix_set(inv_g2,i,max,gsl_matrix_get(nofuture,j,i));
				    gsl_matrix_int_set(inv_g2pos,i,max,j);
				    max++;
				}
			}
		}

	}
    gsl_matrix_free(nofuture);
}

void FinDifGrid::mult_vec(string selection, gsl_vector *in, gsl_vector *out)
{
    double sum;

    if(in->size != numpix || out->size != numpix)
	{
	    throw invalid_argument("Invalid vector sizes for findif multvec");
	}

    if(selection == "a1")
	{
	    for(int i = 0; i < numpix; i++)
		{
		    sum = 0.;
		    for(int j = 0; j < a1->size2; j++)
			{
			    sum += gsl_matrix_get(a1,i,j)*gsl_vector_get(in,gsl_matrix_int_get(a1pos,i,j));
			}
		    gsl_vector_set(out,i,sum);
		}
	}
    else if(selection == "a2")
	{
	    for(int i = 0; i < numpix; i++)
		{
		    sum = 0.;
		    for(int j = 0; j < a2->size2; j++)
			{
			    sum += gsl_matrix_get(a2,i,j)*gsl_vector_get(in,gsl_matrix_int_get(a2pos,i,j));
			}
		    gsl_vector_set(out,i,sum);
		}
	}

    else if(selection == "s1")
	{
	    if(order > 1)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < s1->size2; j++)
				{
				    sum += gsl_matrix_get(s1,i,j)*gsl_vector_get(in,gsl_matrix_int_get(s1pos,i,j));
				}
			    gsl_vector_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "s2")
	{
	    if(order > 1)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < s2->size2; j++)
				{
				    sum += gsl_matrix_get(s2,i,j)*gsl_vector_get(in,gsl_matrix_int_get(s2pos,i,j));
				}
			    gsl_vector_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "c")
	{
	    if(order > 1)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < c->size2; j++)
				{
				    sum += gsl_matrix_get(c,i,j)*gsl_vector_get(in,gsl_matrix_int_get(cpos,i,j));
				}
			    gsl_vector_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "f1")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{			    sum = 0.;
			    for(int j = 0; j < f1->size2; j++)
				{
				    sum += gsl_matrix_get(f1,i,j)*gsl_vector_get(in,gsl_matrix_int_get(f1pos,i,j));
				}
			    gsl_vector_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "f2")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < f2->size2; j++)
				{
				    sum += gsl_matrix_get(f2,i,j)*gsl_vector_get(in,gsl_matrix_int_get(f2pos,i,j));
				}
			    gsl_vector_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "g1")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < g1->size2; j++)
				{
				    sum += gsl_matrix_get(g1,i,j)*gsl_vector_get(in,gsl_matrix_int_get(g1pos,i,j));
				}
			    gsl_vector_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "g2")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < g2->size2; j++)
				{
				    sum += gsl_matrix_get(g2,i,j)*gsl_vector_get(in,gsl_matrix_int_get(g2pos,i,j));
				}
			    gsl_vector_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else
	{
	    throw invalid_argument("Invalid selection for multvec of findif");
	}
}

void FinDifGrid::mult_vec(string selection, gsl_vector_float *in, gsl_vector_float *out)
{
    float sum;

    if(in->size != numpix || out->size != numpix)
	{
	    throw invalid_argument("Invalid vector sizes for findif multvec");
	}

    if(selection == "a1")
	{
	    for(int i = 0; i < numpix; i++)
		{
		    sum = 0.;
		    for(int j = 0; j < a1->size2; j++)
			{
			  sum +=(float) gsl_matrix_get(a1,i,j)*gsl_vector_float_get(in,gsl_matrix_int_get(a1pos,i,j));
			}
		    gsl_vector_float_set(out,i,sum);
		}
	}
    else if(selection == "a2")
	{
	    for(int i = 0; i < numpix; i++)
		{
		    sum = 0.;
		    for(int j = 0; j < a2->size2; j++)
			{
			  sum += (float) gsl_matrix_get(a2,i,j)*gsl_vector_float_get(in,gsl_matrix_int_get(a2pos,i,j));
			}
		    gsl_vector_float_set(out,i,sum);
		}
	}

    else if(selection == "s1")
	{
	    if(order > 1)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < s1->size2; j++)
				{
				  sum += (float) gsl_matrix_get(s1,i,j)*gsl_vector_float_get(in,gsl_matrix_int_get(s1pos,i,j));
				}
			    gsl_vector_float_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "s2")
	{
	    if(order > 1)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < s2->size2; j++)
				{
				  sum += (float) gsl_matrix_get(s2,i,j)*gsl_vector_float_get(in,gsl_matrix_int_get(s2pos,i,j));
				}
			    gsl_vector_float_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "c")
	{
	    if(order > 1)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < c->size2; j++)
				{
				  sum += (float) gsl_matrix_get(c,i,j)*gsl_vector_float_get(in,gsl_matrix_int_get(cpos,i,j));
				}
			    gsl_vector_float_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "f1")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{			    sum = 0.;
			    for(int j = 0; j < f1->size2; j++)
				{
				  sum += (float) gsl_matrix_get(f1,i,j)*gsl_vector_float_get(in,gsl_matrix_int_get(f1pos,i,j));
				}
			    gsl_vector_float_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "f2")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < f2->size2; j++)
				{
				  sum += (float) gsl_matrix_get(f2,i,j)*gsl_vector_float_get(in,gsl_matrix_int_get(f2pos,i,j));
				}
			    gsl_vector_float_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "g1")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < g1->size2; j++)
				{
				  sum += (float) gsl_matrix_get(g1,i,j)*gsl_vector_float_get(in,gsl_matrix_int_get(g1pos,i,j));
				}
			    gsl_vector_float_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else if(selection == "g2")
	{
	    if(order > 2)
		{
		    for(int i = 0; i < numpix; i++)
			{
			    sum = 0.;
			    for(int j = 0; j < g2->size2; j++)
				{
				  sum += (float) gsl_matrix_get(g2,i,j)*gsl_vector_float_get(in,gsl_matrix_int_get(g2pos,i,j));
				}
			    gsl_vector_float_set(out,i,sum);
			}
		}

	    else
		{
		    throw invalid_argument("Findif order not high enough to perform this multvec");
		}
	}
    else
	{
	    throw invalid_argument("Invalid selection for multvec of findif");
	}
}

void FinDifGrid::convert(gsl_matrix *in, gsl_vector *out)
{

  if(in->size1 != y || in->size2 !=x)
    {
      throw invalid_argument("matrix dimensions do not match for convert");
    }
  if(out->size != numpix)
    {
      throw invalid_argument("vector dimensions do not match for convert");
    }

  int counter = 0;
  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  if(gsl_matrix_int_get(typemap,i,j) != -1)
	    {
	      gsl_vector_set(out,counter,gsl_matrix_get(in,i,j));
	      counter++;
	    }
	}
    }
}

void FinDifGrid::convert(gsl_vector *in, gsl_matrix *out)
{

  if(out->size1 != y || out->size2 !=x)
    {
      throw invalid_argument("matrix dimensions do not match for convert");
    }
  if(in->size != numpix)
    {
      throw invalid_argument("vector dimensions do not match for convert");
    }

  int counter = 0;
  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  if(gsl_matrix_int_get(typemap,i,j) != -1)
	    {
	      gsl_matrix_set(out,i,j,gsl_vector_get(in,counter));
	      counter++;
	    }
	  else
	    {
	      gsl_matrix_set(out,i,j,-1.0);
	    }
	}
    }
}

void FinDifGrid::convert(gsl_matrix_int *in, gsl_vector_int *out)
{

  if(in->size1 != y || in->size2 !=x)
    {
      throw invalid_argument("matrix dimensions do not match for convert");
    }
  if(out->size != numpix)
    {
      throw invalid_argument("vector dimensions do not match for convert");
    }

  int counter = 0;
  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  if(gsl_matrix_int_get(typemap,i,j) != -1)
	    {
	      gsl_vector_int_set(out,counter,gsl_matrix_int_get(in,i,j));
	      counter++;
	    }
	}
    }
}

void FinDifGrid::convert(gsl_vector_int *in, gsl_matrix_int *out)
{

  if(out->size1 != y || out->size2 !=x)
    {
      throw invalid_argument("matrix dimensions do not match for convert");
    }
  if(in->size != numpix)
    {
      throw invalid_argument("vector dimensions do not match for convert");
    }

  int counter = 0;
  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  if(gsl_matrix_int_get(typemap,i,j) != -1)
	    {
	      gsl_matrix_int_set(out,i,j,gsl_vector_int_get(in,counter));
	      counter++;
	    }
	  else
	    {
	      gsl_matrix_int_set(out,i,j,-1.0);
	    }
	}
    }
}
   












