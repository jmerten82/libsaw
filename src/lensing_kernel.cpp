/***
    This class represents a typical 2D lensing kernel, describing the
    relation between convergence and shear in Fourier space. 

    Julian Merten
    JPL/Caltech
    jmerten@caltech.edu
    Dec. 2011

***/

#include <saw/lensing_kernel.h>



using namespace std;

lensing_kernel::lensing_kernel(int x, int y)
{


  //Allocating quantities
  real_base_grid = gsl_matrix_calloc(y,x);
  imaginary_base_grid = gsl_matrix_calloc(y,x);

  //Building the kernel in the naive way on the grid.
  for(int i = 0; i < y; i++)
    {
      double ky;
      if(i < y/2)
	{
	  ky = i;
	}
      else
	{
	  ky = i -y;
	} 
      for(int j = 0; j < x; j++)
	{
	  double kx;
	  if(j < x/2)
	    {
	      kx = j;
	    }
	  else
	    {
	      kx = j -x;
	    }
	  double k;
	  if(i == 0 && j == 0)
	    {
	      k = 1.;
	    }
	  else
	    {
	      k = kx*kx + ky*ky;
	    }
	  double factor1 = (kx*kx-ky*ky)/k;
	  double factor2 = -2.*kx*ky/k;
	  gsl_matrix_set(real_base_grid,i,j,factor1);
	  gsl_matrix_set(imaginary_base_grid,i,j,factor2);
	}
    }
}

lensing_kernel::~lensing_kernel()
{

  gsl_matrix_free(real_base_grid);
  gsl_matrix_free(imaginary_base_grid);

}

int lensing_kernel::show_x()
{
  return real_base_grid->size2;
}

int lensing_kernel::show_y()
{
  return real_base_grid->size1;
}


void lensing_kernel::convolve(gsl_matrix *in_re, gsl_matrix *in_im, gsl_matrix *out_re, gsl_matrix *out_im, bool inversion)
{


  fftw_complex *full_map;
  fftw_plan pmap, pinv;

  int x = real_base_grid->size2;
  int y = real_base_grid->size1;

  if(in_re -> size1 != out_re -> size1 || in_re -> size2 != out_re -> size2 || in_im->size1 != in_re->size1 || in_im->size2 != in_re->size2 ||out_im->size1 != out_re->size1 || out_im->size2 != out_re->size2)
    {
      throw invalid_argument("LENSING_KERNEL: In and out dims do not match");
    }

  if(in_re -> size1 != y || in_re -> size2 != x)
    {
      throw invalid_argument("LENSING_KERNEL: Input map does not match the lensing kernel.");
    }

  //Performing Fourier transform of the input.

  full_map = (fftw_complex*) fftw_malloc((x*y*sizeof(fftw_complex)));
  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  full_map[i*x+j][0] = gsl_matrix_get(in_re,i,j);
	  full_map[i*x+j][1] = gsl_matrix_get(in_im,i,j);
	}
    }


  cout <<"forward FT map..." <<flush;
  pmap = fftw_plan_dft_2d(y,x,full_map,full_map, FFTW_FORWARD,FFTW_ESTIMATE);
  fftw_execute(pmap);
  fftw_destroy_plan(pmap);
  cout <<"done." <<endl;

  
  //Multiplying with the kernel in Fourier space
  
  for(int i = 0; i < y; i++)
    { 
      for(int j = 0; j < x; j++)
	{ 
	  double map_re = full_map[i*x+j][0];
	  double map_im = full_map[i*x+j][1];
	  double kernel_re = gsl_matrix_get(real_base_grid,i,j);
	  double kernel_im = gsl_matrix_get(imaginary_base_grid,i,j);
	  full_map[i*x+j][0] = map_re*kernel_re - map_im*kernel_im;
	  if(inversion)
	    {
	      full_map[i*x+j][1] = map_im*kernel_re - map_re*kernel_im;
	    }
	  else
	    {
	      full_map[i*x+j][1] = map_im*kernel_re + map_re*kernel_im;
	    }
	}
    }

  //Transforming back to obtain the shear
  
  cout <<"Backward FT..."<<flush;
  pinv = fftw_plan_dft_2d(y,x,full_map,full_map,FFTW_BACKWARD,FFTW_ESTIMATE);
  fftw_execute(pinv);
  fftw_destroy_plan(pinv);
  cout <<"done." <<endl;

  //This is to deal with the insane FFTW frequency ordering!

  int counter1 = 0;
  int counter2 = 0;

  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  gsl_matrix_set(out_re,i,j,full_map[i*x+j][0]);
	  gsl_matrix_set(out_im,i,j,full_map[i*x+j][1]);
	}
    }
    
  // NOT SURE ABOUT THIS NORMALISATION!



  gsl_matrix_scale(out_re,1./(x*y));
  gsl_matrix_scale(out_im,1./(x*y));
}

void lensing_kernel::visualise(string filename, bool real_space)
{
  int x = real_base_grid->size2;
  int y = real_base_grid->size1;
  

  write_pimg(filename, real_base_grid);
  write_imge(filename, "IMG", imaginary_base_grid);

  if(real_space)
    {
      gsl_matrix *out_re = gsl_matrix_calloc(y,x);
      gsl_matrix *out_im = gsl_matrix_calloc(y,x);

      fftw_complex *full_map;
      fftw_plan pinv;

      full_map = (fftw_complex*) fftw_malloc((x*y*sizeof(fftw_complex)));
      for(int i = 0; i < y; i++)
	{
	  for(int j = 0; j < x; j++)
	    {
	      full_map[i*x+j][0] = gsl_matrix_get(real_base_grid,i,j);
	      full_map[i*x+j][1] = gsl_matrix_get(imaginary_base_grid,i,j);
	    }
	}
      cout <<"Backward FT map..." <<flush;
      pinv = fftw_plan_dft_2d(y,x,full_map,full_map, FFTW_BACKWARD,FFTW_ESTIMATE);

      fftw_execute(pinv);
      fftw_destroy_plan(pinv);
      cout <<"done." <<endl;
      
      for(int i = 0; i < y; i++)
	{
	  for(int j = 0; j < x; j++)
	    {
	  gsl_matrix_set(out_re,i,j,full_map[i*x+j][0]);
	  gsl_matrix_set(out_im,i,j,full_map[i*x+j][1]);
	    }
	}
      write_imge(filename,"Kernel_RE",out_re);
      write_imge(filename,"Kernel_IM",out_im);
    }
}










