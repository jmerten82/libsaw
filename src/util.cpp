//FINAL MPI
//util.h
//some little utility routines
//some of them are very important



#include <saw/util.h>


using namespace std;


//useful time operations and cursor controls

#define START(a) start = time(NULL); cout << a << "..." << flush;
#define STOP() cout << "finished (" << difftime(time(NULL),start) << " seconds)" <<endl;
#define CLS (cout <<"\033[2J")
#define LOCATE(z,s) (cout <<"\033["<<z <<';' <<s <<'H')
#define CURSORLEFT(n) (cout <<"\033[" <<n <<'D')
#define CURSORRIGHT(n) (cout <<"\033[" <<n <<'C')
#define CURSORUP(n) (cout <<"\033[" <<n <<'A')
#define CURSORDOWN(n) (cout <<"\033[" <<n <<'B')


void matvec(gsl_matrix *input, gsl_vector *output)
{
  int counter = 0;
  for(int i = 0; i < input->size1; i++)
    {
      for(int j = 0; j < input->size2; j++)
	{
	  gsl_vector_set(output,counter,gsl_matrix_get(input,i,j));
	  counter++;
	}
    }
}
void matvec(gsl_matrix_int *input, int x_dim, int y_dim, gsl_vector_int *output)
{
  int counter = 0;
  for(int i = 0; i < input->size1; i++)
    {
      for(int j = 0; j < input->size2; j++)
	{
	  gsl_vector_int_set(output,counter, gsl_matrix_int_get(input,i,j));
	  counter++;
	}
    }
}

void vecmat(gsl_vector *input, gsl_matrix *output)
{

  int counter = 0;
  for(int i = 0; i < output->size1; i++)
    {
      for(int j = 0; j < output->size2; j++)
	{
	  gsl_matrix_set(output, i,j, gsl_vector_get(input,counter));
	  counter++;
	}
    }
}

void vecmat(gsl_vector_int *input, gsl_matrix_int *output)
{

  int counter;
  for(int i = 0; i < output->size2; i++)
    {
      for(int j = 0; j < output->size1; j++)
	{
	  gsl_matrix_int_set(output, i,j, gsl_vector_int_get(input, counter));
	  counter++;
	}
    }
}


double change(gsl_vector *one, gsl_vector *two)
{
  gsl_vector_sub(one,two);
  for(int i = 0; i < one->size; i++)
    {
      gsl_vector_set(one, i, abs(gsl_vector_get(one,i)));
    }
  return gsl_vector_max(one);
}

double change(gsl_matrix *one, gsl_matrix *two)
{
  //same for gsl-matrices
  gsl_matrix_sub(one,two);
  for(int i = 0; i < one->size1; i++)
    {
      for(int j = 0; j < one->size2; j++)
	{
	  gsl_matrix_set(one,i,j,abs(gsl_matrix_get(one,i,j)));
	}
    }
  return gsl_matrix_max(one);
}

void cut(gsl_vector *invector,int x_dim,int y_dim, int x1, int x2, int y1, int y2, gsl_vector *outvector)

     //does a selected cutout of a given gsl-vector
{
  int x = x2 - x1;
  int y = y2 - y1;

  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  gsl_vector_set(outvector, i*x+j,gsl_vector_get(invector, (y1+i)*x_dim+(x1+j)));
	}
    }
}

void cut(gsl_matrix *inmatrix,int x_dim,int y_dim, int x1, int x2, int y1, int y2, gsl_matrix *outmatrix)
{

  //same for matrix
  int x = x2 - x1;
  int y = y2 - y1;

  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  gsl_matrix_set(outmatrix, i,j,gsl_matrix_get(inmatrix, y1+i,x1+j));
	}
    }
}

void cut(gsl_vector_int *invector,int x_dim,int y_dim, int x1, int x2, int y1, int y2, gsl_vector_int *outvector)

     //does a selected cutout of a given gsl-vector
{
  int x = x2 - x1;
  int y = y2 - y1;

  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  gsl_vector_int_set(outvector, i*x+j,gsl_vector_int_get(invector, (y1+i)*x_dim+(x1+j)));
	}
    }
}

void cut(gsl_matrix_int *inmatrix,int x_dim,int y_dim, int x1, int x2, int y1, int y2, gsl_matrix_int *outmatrix)
{

  //same for matrix
  int x = x2 - x1;
  int y = y2 - y1;

  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  gsl_matrix_int_set(outmatrix, i,j,gsl_matrix_int_get(inmatrix, y1+i,x1+j));
	}
    }
}

void radialprofile(gsl_matrix *input,int midx,int midy,const char *outfile )
{

  //does a radial profile of a given input matrix wrt a given center
  //and writes it into ascii-file
  int x_dim = input->size2;
  int y_dim = input->size1;
  int dim = x_dim*y_dim;
  gsl_matrix *workingmatrix = gsl_matrix_calloc(dim,2);
  gsl_matrix *result = gsl_matrix_calloc(dim,2);

  for(int i = 0; i < y_dim; i++)
    {
      for(int j = 0;j < x_dim; j++)
	{
	  gsl_matrix_set(workingmatrix,i*x_dim+j,1,(i-midy)*(i-midy)+(j-midx)*(j-midx));
	  gsl_matrix_set(workingmatrix,i*x_dim+j,0,gsl_matrix_get(input,i,j));
	}
    }

  for(int i = 0;i < dim; i++)
    {
      double value = 0.0;
      int counter = 0;

      for(int j = 0;j < dim; j++)
	{
	  if(gsl_matrix_get(workingmatrix,j,1)==gsl_matrix_get(workingmatrix,i,1))
	    {
	      value = value + gsl_matrix_get(workingmatrix,j,0);
	      counter++;
	    }
	}
      value = value/counter;
      gsl_matrix_set(result,i,0,gsl_matrix_get(workingmatrix,i,1));
      gsl_matrix_set(result,i,1,value);
    }  
  ofstream output(outfile);
  for(int i = 0; i < dim; i++)
    {
      output <<sqrt(gsl_matrix_get(result,i,0)) <<"\t" <<gsl_matrix_get(result,i,1) <<endl;
    }
}

void zeronorm(gsl_matrix *convinput)
{
  //normalises a given conv-map to 0 wrt to faintest point applying
  //mass-sheet-degenerancie-trafo 
  double minimum = gsl_matrix_min(convinput);
  double lambda = 1.0/(1.0-minimum);
  gsl_matrix_scale(convinput,lambda);
  gsl_matrix_add_constant(convinput,1.0-lambda);
}
void zeronorm(gsl_vector *convinput)
{
  double minimum = gsl_vector_min(convinput);
  double lambda = 1.0/(1.0-minimum);
  gsl_vector_scale(convinput,lambda);
  gsl_vector_add_constant(convinput,1.0-lambda);
}

void merge_maps(gsl_matrix *coarsemap, gsl_matrix *finemap,int x1pos, int x2pos, int y1pos, int y2pos, gsl_matrix *outmap)
{

  //merges a high and a low resolution gsl map, you need to give both
  //maps and their resolutions and also the position where the smaller
  //map fits into to big one.
  //of course the cutposition should match the size of the fine map

  int coarsex = coarsemap->size2;
  int coarsey = coarsemap->size1;
  int finex = outmap->size2;
  int finey = outmap->size1;

  double xratio = (double) coarsex/finex;
  double yratio = (double) coarsey/finey;
  int coarseposx;
  int coarseposy;


  for(int i = 0; i < finey; i++)
    {
      for(int j = 0; j < finex; j++)
	{
	  coarseposy = (int) floor(i*yratio);
	  coarseposx = (int) floor(j*xratio);
	  if((i >= y1pos && i < y2pos) && (j >= x1pos && j < x2pos))
	    {
	      gsl_matrix_set(outmap,i,j,gsl_matrix_get(finemap,i-y1pos,j-x1pos));
	    }
	  else
	    {
	      gsl_matrix_set(outmap,i,j,gsl_matrix_get(coarsemap,coarseposy,coarseposx));
	    }
	}
    }
}

double cosmicweight(double lensredshift, double sourceredshift)
{

  double cosmicweight;
  astro::cosmology cosmo1(0.3,0.7,-1.0);
  cosmicweight = (cosmo1.angularDist(lensredshift,sourceredshift)*cosmo1.angularDist(0,100000.0))/(cosmo1.angularDist(0,sourceredshift)*cosmo1.angularDist(lensredshift,100000.0));

  return cosmicweight;
}

void ReadMsystemInfo(string input, gsl_matrix *output)
{

  ifstream input1(input.c_str());
  string line;
  double value1;
  int syscounter = -1;
  int index = 0;

  while(input1)
    {
      getline(input1,line);
      
      if(line.size() > 1)
	{
	  if(line.at(0) == '#')
	    {
	      syscounter++;
	      index = 0;
	    }
	  else
	    {
	      istringstream read1(line);
	      //istringstream read2(line.substr(line.find_first_of(',')+1,line.size()));
	      read1 >> value1;
	      //read2 >> value2;
	      gsl_matrix_set(output,index,syscounter,value1);
	      index++;
	      //gsl_matrix_set(output,index,syscounter,value2);
	      //index++;
	    }
	}
    }
  input1.close();
}

void WriteMsystemInfo(gsl_matrix *input,string output)
{
  ofstream output1(output.c_str());

  for(int i = 0 ; i < input->size2; i++)
    {

      output1 <<"#" <<i+1 <<endl;

      for(int j = 0; j < input->size1; j++)
	{
	  output1 <<showpoint <<gsl_matrix_get(input,j,i) <<endl;
	}
    }

  output1.close();

}

void read_doubles(string input, int number, vector<double> &output)
{
    string::size_type index1;

    for(int i = 0; i < number; i++)
	{
	    istringstream helper;
	    string subline;
	    double value;
	    index1 = input.find_first_of("1234567890.-",0);
	    if(index1 != string::npos)
		{
		    input.erase(0,index1);
		    subline = input.substr(0,input.find_first_not_of("1234567890.-",0));
		    helper.str(subline);
		    if(subline.size() < 1)
			{
			    throw invalid_argument("Invalid double in string");
			}
		    input.erase(0,subline.size());
		    subline.clear();
		    helper >>value;
		    output.push_back(value);
		}
	    else
		{
		    throw invalid_argument("Not enough doubles in string");
		}
	}
}

void read_ints(string input, int number, vector<int> &output)
{
    string::size_type index1;

    for(int i = 0; i < number; i++)
	{
	    istringstream helper;
	    string subline;
	    int value;
	    index1 = input.find_first_of("1234567890-",0);
	    if(index1 != string::npos)
		{
		    input.erase(0,index1);
		    subline = input.substr(0,input.find_first_not_of("1234567890-",0));
		    helper.str(subline);
		    if(subline.size() < 1)
			{
			    throw invalid_argument("Invalid double in string");
			}
		    input.erase(0,subline.size());
		    subline.clear();
		    helper >>value;
		    output.push_back(value);
		}
	    else
		{
		    throw invalid_argument("Not enough doubles in string");
		}
	}
}

void read_doubles(string input, vector<double> &output)
{
    string::size_type index1 = 0;

    for(int i = 0; index1 != string::npos; i++)
	{
	    istringstream helper;
	    string subline;
	    double value;
	    index1 = input.find_first_of("1234567890.+-e",0);

	    input.erase(0,index1);
	    subline = input.substr(0,input.find_first_not_of("1234567890.+-e",0));
	    helper.str(subline);
	    if(subline.size() < 1)
		{
		    throw invalid_argument("Invalid double in string");
		}
	    input.erase(0,subline.size());
	    subline.clear();
	    helper >>value;
	    output.push_back(value);
	    index1 = input.find_first_of("1234567890.+-e",0);		
	}
}

void read_ints(string input, vector<int> &output)
{
    string::size_type index1 = 0;

    for(int i = 0; index1 != string::npos; i++)
	{
	    istringstream helper;
	    string subline;
	    int value;
	    index1 = input.find_first_of("1234567890-",0);

	    input.erase(0,index1);
	    subline = input.substr(0,input.find_first_not_of("1234567890-",0));
	    helper.str(subline);
	    if(subline.size() < 1)
		{
		    throw invalid_argument("Invalid integer in string");
		}
	    input.erase(0,subline.size());
	    subline.clear();
	    helper >>value;
	    output.push_back(value);
	    index1 = input.find_first_of("1234567890-",0);		
	}
}

double read_double(string input)
{
    string::size_type index1;
    istringstream helper;
    string subline;
    double value;
    index1 = input.find_first_of("1234567890.+-e",0);
    
    input.erase(0,index1);
    subline = input.substr(0,input.find_first_not_of("1234567890.+-e",0));
    helper.str(subline);
    if(subline.size() < 1)
	{
	    throw invalid_argument("Double in string invalid");
	}
    subline.clear();
    helper >>value;

    return value;
}

int read_int(string input)
{
    string::size_type index1;
    istringstream helper;
    string subline;
    int value;
    index1 = input.find_first_of("1234567890-",0);
    
    input.erase(0,index1);
    subline = input.substr(0,input.find_first_not_of("1234567890-",0));
    helper.str(subline);
    if(subline.size() < 1)
	{
	    throw invalid_argument("Integer in string invalid");
	}
    subline.clear();
    helper >>value;

    return value;
}



string read_word(string &input, string symbol)
{

    string::size_type index1;
    string subline;

    index1 = input.find(symbol,0);
    if(index1 == string::npos)
	{
	    throw invalid_argument("Marker symbol not found in string");
	}
    input.erase(0,index1+symbol.size());
    subline = input.substr(0,input.find(symbol,0));
    input.erase(0,subline.size());
    
    return subline;
}

bool read_word(string &input, string &output, string symbol)
{

    string::size_type index1;
    string subline;
    bool found = true;

    index1 = input.find(symbol,0);
    if(index1 == string::npos)
	{
	  found = false;;
	}
    else
      {
	input.erase(0,index1+symbol.size());
	output = input.substr(0,input.find(symbol,0));
	input.erase(0,subline.size());
      }
    
    return found;
}

bool read_mind(string input)
{

    if((input.find("YES",0) != string::npos || input.find("yes",0) != string::npos || input.find("Jawohl",0) != string::npos || input.find("Yes",0) != string::npos || input.find("TRUE",0) != string::npos || input.find("true",0) != string::npos) && input.find("NO",0) == string::npos && input.find("no",0) == string::npos && input.find("FALSE",0) == string::npos && input.find("false",0) == string::npos && input.find("No",0) == string::npos && input.find("Nicht doch",0) == string::npos)
	{

	    return true;
	}
    else if(input.find_first_of("yYtT",0) < input.find_first_of("nNfF",0))
	{
	    return true;
	}
    else
	{
	    return false;
	}
}

void read_sequence(string input, vector<int> &output)
{

    string subline;
    output.clear();
    int value1, value2;

    while(input.size() != 0)
	{
	    subline = input.substr(0,input.find_first_of(","));
	    if(input.find(",",0) != string::npos)
		{
		    input.erase(0,input.find_first_of(",")+1);
		}
	    else
		{
		    input.clear();
		}


	    if(subline.find("-",0) != string::npos)
		{
		    istringstream helper1;
		    istringstream helper2;

		    helper1.str(subline.substr(0,subline.find_first_of("-",0)));
		    helper2.str(subline.substr(subline.find_first_of("-")+1));
		    helper1 >>value1;
		    helper2 >>value2;


		    for(int i = value1; i <= value2; i++)
			{
			    output.push_back(i);
			}  
		}
	    else
		{
		    istringstream helper1;
		    helper1.str(subline);
		    helper1 >>value1;
		    output.push_back(value1);
		}
	}
}


void create_random(gsl_vector* output, double range_lower, double range_upper)
{

  if(range_lower > range_upper)
    {
      throw invalid_argument("RANDOM: Lower range must be smaller then upper");
    } 

  if(output->size < 1)
    {
      throw invalid_argument("RANDOM: Output vector invalid");
    }

  int dim = output->size;

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  time_t seed;
  time(&seed);
  gsl_rng_set(r,seed); 

  for(int i = 0; i < dim; i++)
    {
      gsl_vector_set(output,i,gsl_rng_uniform(r)*(range_upper - range_lower) + range_lower);
    }
}

void create_random(gsl_vector* output, double range_lower, double range_upper, int seed)
{

  if(range_lower > range_upper)
    {
      throw invalid_argument("RANDOM: Lower range must be smaller then upper");
    } 

  if(output->size < 1)
    {
      throw invalid_argument("RANDOM: Output vector invalid");
    }

  int dim = output->size;

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  gsl_rng_set(r,seed); 

  for(int i = 0; i < dim; i++)
    {
      gsl_vector_set(output,i,gsl_rng_uniform(r)*(range_upper - range_lower) + range_lower);
    }
}

void create_random(gsl_matrix* output, double range_lower, double range_upper)
{

  if(range_lower > range_upper)
    {
      throw invalid_argument("RANDOM: Lower range must be smaller then upper");
    } 

  if(output->size1 < 1 || output->size2 < 1)
    {
      throw invalid_argument("RANDOM: Output matrix invalid");
    }

  int dim1 = output->size1;
  int dim2 = output->size2;

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  time_t seed;
  time(&seed);
  gsl_rng_set(r,seed); 

  for(int i = 0; i < dim1; i++)
    {
      for(int j = 0; j < dim2; j++)
	{
	  gsl_matrix_set(output,i,j,gsl_rng_uniform(r)*(range_upper - range_lower) + range_lower);
	}
    }
}

void create_random(gsl_matrix* output, double range_lower, double range_upper, int seed)
{

  if(range_lower > range_upper)
    {
      throw invalid_argument("RANDOM: Lower range must be smaller then upper");
    } 

  if(output->size1 < 1 || output->size2 < 1)
    {
      throw invalid_argument("RANDOM: Output matrix invalid");
    }

  int dim1 = output->size1;
  int dim2 = output->size2;

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  gsl_rng_set(r,seed); 

  for(int i = 0; i < dim1; i++)
    {
      for(int j = 0; j < dim2; j++)
	{
	  gsl_matrix_set(output,i,j,gsl_rng_uniform(r)*(range_upper - range_lower) + range_lower);
	}
    }
}

void create_random_int(gsl_vector_int* output, int range_lower, int range_upper)
{

  if(range_lower > range_upper)
    {
      throw invalid_argument("RANDOM: Lower range must be smaller then upper");
    } 

  if(output->size < 1)
    {
      throw invalid_argument("RANDOM: Output vector invalid");
    }

  int dim = output->size;

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  time_t seed;
  time(&seed);
  gsl_rng_set(r,seed); 

  for(int i = 0; i < dim; i++)
    {
      gsl_vector_int_set(output,i,floor(gsl_rng_uniform(r)*((double)range_upper - (double)range_lower) + (double)range_lower));
    }
}

void create_random_int(gsl_vector_int* output, int range_lower, int range_upper, int seed)
{

  if(range_lower > range_upper)
    {
      throw invalid_argument("RANDOM: Lower range must be smaller then upper");
    } 

  if(output->size < 1)
    {
      throw invalid_argument("RANDOM: Output vector invalid");
    }

  int dim = output->size;

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  gsl_rng_set(r,seed); 

  for(int i = 0; i < dim; i++)
    {
      gsl_vector_int_set(output,i,floor(gsl_rng_uniform(r)*((double)range_upper - (double)range_lower) + (double)range_lower));
    }
}

void create_random_int(gsl_matrix_int* output, int range_lower, int range_upper)
{

  if(range_lower > range_upper)
    {
      throw invalid_argument("RANDOM: Lower range must be smaller then upper");
    } 

  if(output->size1 < 1 || output->size2 < 1)
    {
      throw invalid_argument("RANDOM: Output matrix invalid");
    }

  int dim1 = output->size1;
  int dim2 = output->size2;

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  time_t seed;
  time(&seed);
  gsl_rng_set(r,seed); 

  for(int i = 0; i < dim1; i++)
    {
      for(int j = 0; j < dim2; j++)
	{
	  gsl_matrix_int_set(output,i,j,floor(gsl_rng_uniform(r)*((double)range_upper - (double)range_lower) + (double)range_lower));
	}
    }
}

void create_random_int(gsl_matrix_int* output, int range_lower, int range_upper, int seed)
{

  if(range_lower > range_upper)
    {
      throw invalid_argument("RANDOM: Lower range must be smaller then upper");
    } 

  if(output->size1 < 1 || output->size2 < 1)
    {
      throw invalid_argument("RANDOM: Output matrix invalid");
    }

  int dim1 = output->size1;
  int dim2 = output->size2;

  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  gsl_rng_set(r,seed); 

  for(int i = 0; i < dim1; i++)
    {
      for(int j = 0; j < dim2; j++)
	{
	  gsl_matrix_int_set(output,i,j,floor(gsl_rng_uniform(r)*((double)range_upper - (double)range_lower) + (double)range_lower));
	}
    }
}

void add_mask(gsl_matrix_int *output, int value, double x_lower, double x_upper, double y_lower, double y_upper)
{

  int x = output->size2;
  int y = output->size1;
  if(x_lower > x_upper || y_lower > y_upper)
    {
      throw invalid_argument("MASK: invalid range definition");
    }

  if(x_lower < 0 || x_upper >= x || y_lower < 0 || y_upper >= y)
    {
      throw invalid_argument("MASK: invalid range definition");
    }

    for(int l = 0; l < y; l++)
      {
	for(int k = 0; k < x; k++)
	  {
	    if(l >= floor(y_lower+0.5) && l <= floor(y_upper+0.5) && k >= floor(x_lower+0.5) && k <= floor(x_upper+0.5))
	      {	
		gsl_matrix_int_set(output,l,k,value);
	      }
	  }
      }
}

void add_mask(gsl_matrix_int *output, int value, double x_lower, double x_upper, double y_lower, double y_upper, double angle)
{

  int x = output->size2;
  int y = output->size1;
  if(x_lower > x_upper || y_lower > y_upper)
    {
      throw invalid_argument("MASK: invalid range definition");
    }

  if(x_lower < 0 || x_upper >= x || y_lower < 0 || y_upper >= y)
    {
      throw invalid_argument("MASK: invalid range definition");
    }

  int l_prime, k_prime;
  int x_centre, y_centre;

  angle = angle/180.;
  angle *= Pi;


  x_centre = x_upper - x_lower;
  x_centre = x_upper - .5*x_centre;
  y_centre = y_upper - y_lower;
  y_centre = y_upper - .5*y_centre;

  for(int l = 0; l < y; l++)
    {
      for(int k = 0; k < x; k++)
	{
	  if(l >= floor(y_lower+.5) && l <= floor(y_upper+.5) && k >= floor(x_lower+.5) && k <= floor(x_upper+.5))
	    {
	      
	      l_prime = floor((double) (k - x_centre) * sin(angle) + (double) (l-y_centre) * cos(angle)+.5) + y_centre;
	      k_prime = floor((double) (k - x_centre) * cos(angle) - (double) (l - y_centre) * sin(angle)+.5) + x_centre;
	      if(l_prime > 0 && l_prime < y && k_prime > 0 && k_prime < x)
		{
		  gsl_matrix_int_set(output,l_prime,k_prime,value);
		}
	    }
	}
    }

  int counter = 0;
  
  for(int i = 0; i < y; i++)
    {
      for(int j = 0; j < x; j++)
	{
	  counter = 0;
	  if(gsl_matrix_int_get(output,i,j) != value)
	    {
	      for(int l = -1; l <= 1; l++)
		{
		  for(int k = -1; k <= 1; k++)
		    {
		      if(i+l >= 0 && i+l < y && j+k >= 0 && j+k < x)
			{
			  if(gsl_matrix_int_get(output,i+l,j+k) == value)
			    {
			      counter++;
			    }
			}
		    }
		}
	      if(counter >= 6)
		{
		  gsl_matrix_int_set(output,i,j,value);
		}
	    }
	}
    }
  
}

void add_mask(gsl_matrix_int *output, int value, double x_origin, double y_origin, double radius)
{

  int x = output->size2;
  int y = output->size1;

  double distance;
  
  if(radius < 0. || x_origin > x ||  x_origin < 0. || y_origin > y || y_origin < 0.)
	{
	  throw invalid_argument("MASKS: invalid circular mask selection");
	}


  for(int l = 0; l < y; l++)
    {
      for(int k = 0; k < x; k++)
	{
	  distance = pow(x_origin - (double) k +.5,2.0) + pow(y_origin - (double) l +.5,2.0);
	  distance = sqrt(distance);  
	  if(distance < radius)
	    {
	      gsl_matrix_int_set(output,l,k,value);
	    }
	}
    }
}

int exists(string filename)
{
    FILE *file;
    if (file = fopen(filename.c_str(), "r"))
    {
        fclose(file);
        return 1;
    }
    return 0;
}

void show_xy(int x_dim, int y_dim, int index, int *x, int *y)
{

  if(index > x_dim*y_dim-1)
    {
      throw invalid_argument("SHOW_XY: index too large");
    }

  *y = (int) (index/x_dim);
  *x = index - *y*x_dim; 

} 





			       
   






