/***
Source file included in the SaWLens reconstruction package.
Developed by Julian Merten (2010)
at ITA Heidelberg and OA Bologna
Not to be used without permission
of the author.
jmerten@ita.uni-heidelberg.de
***/

#include <saw/map_analysis.h>


Convergence_Map::Convergence_Map(gsl_matrix *inmap, double givenfieldsize, gsl_matrix *zmap, double mapredshift, double centrex, double centrey, int oversampling, astro::cosmology *cosmogiven, double standard_redshift)
{


//Chekcing inputparameters

    if((inmap->size1 != zmap->size1) || (inmap->size2 != zmap->size2))
	{
	    throw invalid_argument("C map and zmap size must match.");
	}

    if(oversampling < 1)
	{
	    throw invalid_argument("Sampling factor must be larger than zero");
	}

    if(centrex < 0 || centrex > inmap->size2 || centrey < 0 || centrey > inmap->size1)
	{
	    throw invalid_argument("Central coordinate outof bounds");
	}
    if(givenfieldsize <= 0)
	{
	    throw invalid_argument("Field size must be bigger than zero");
	}

// Defining core quantities
    density = true;
    scaled = false;
    masked = false;
    centre_x_o = centrex;
    centre_y_o = centrey;
    normal_x = inmap->size2;
    normal_y = inmap->size1;
    map_redshift = mapredshift;
    sampling_factor = oversampling;
    fieldsize = givenfieldsize;
    over_x = normal_x*sampling_factor;
    over_y = normal_y*sampling_factor;

    orig_convergence = gsl_matrix_calloc(normal_y,normal_x);
    redshifts = gsl_matrix_calloc(normal_y,normal_x);
    overs_convergence = gsl_matrix_calloc(over_y, over_x);
    overs_redshifts = gsl_matrix_calloc(over_y, over_x);
    density_map = gsl_matrix_calloc(over_y, over_x);

    cosmo = cosmogiven;

    gsl_matrix_memcpy(orig_convergence,inmap);
    gsl_matrix_memcpy(redshifts,zmap);



//Deriving quantities

    double frac_x = centrex / (normal_x - 1);
    double frac_y = centrey / (normal_y - 1);
    centre_x = (over_x - 1) *frac_x; 
    centre_y = (over_y - 1) *frac_y;  

    pixel_size = fieldsize / (double) over_x;
    pixel_size_o = fieldsize / (double) normal_x;
    physical_pixel_size = pixel_size/(3600.0*180.0) *PI* cosmo->angularDist(0.0,map_redshift)*cH;
    physical_pixel_size_o = pixel_size_o/(3600.0*180.0) *PI* cosmo->angularDist(0.0,map_redshift)*cH;


    for(int i = 0; i < normal_y; i++)
	{
	    for(int j = 0; j < normal_x; j++)
		{
		    for(int l = 0; l < sampling_factor; l++)
			{
			    for(int k = 0; k < sampling_factor; k++)
				{
				    gsl_matrix_set(overs_convergence,i*sampling_factor+l, j*sampling_factor+k, gsl_matrix_get(orig_convergence,i,j));
				    gsl_matrix_set(overs_redshifts,i*sampling_factor+l, j*sampling_factor+k, gsl_matrix_get(redshifts,i,j));
				}
			}
		}
	}


//Creating a density map


//Definint prefactor such that we obtain units of Mpc and M_sol
    const double properG = NewtonG * pow(MPCtoCM,-3.0) * MSOLtoG;
    const double properC2 = pow(speed_of_light / MPCtoCM,2.0);
    const double constfactor = properC2/(4.0*PI*properG);

    const double Dd = cosmo->angularDist(0.0,map_redshift)*cH;
    double Ds, Dds,redshift, value, standard_Ds, standard_Dds;

    standard_Ds = cosmo->angularDist(0.0,standard_redshift)*cH;
    standard_Dds = cosmo->angularDist(map_redshift,standard_redshift)*cH;



//This will return the physical density in M_sol * h /Mpc^2

    for(int i = 0; i < over_y; i++)
	{
	    for(int j = 0; j < over_x; j++)
		{
		    redshift = gsl_matrix_get(overs_redshifts,i,j);
		    if(abs(redshift - standard_redshift) > 0.01)
			{
			    Ds = cosmo->angularDist(0.0,redshift)*cH;
			    Dds = cosmo->angularDist(map_redshift,redshift)*cH;
			}

		    else
			{
			    Ds = standard_Ds;
			    Dds = standard_Dds;
			}

		    value = constfactor*Ds/(Dd*Dds);
		    gsl_matrix_set(density_map,i,j,gsl_matrix_get(overs_convergence,i,j)*value);
		}
	}
}

Convergence_Map::Convergence_Map(gsl_matrix *inmap, gsl_matrix_int *mask, double givenfieldsize, gsl_matrix *zmap, double mapredshift, double centrex, double centrey, int oversampling, astro::cosmology *cosmogiven, double standard_redshift)
{

//Checkng inputparameters

    if((inmap->size1 != zmap->size1) || (inmap->size2 != zmap->size2))
	{
	    throw invalid_argument("C map and zmap size must match.");
	}
    if((inmap->size1 != mask->size1) || (inmap->size2 != mask->size2))
	{
	    throw invalid_argument("C map and mask size must match.");
	}

    if(oversampling < 1)
	{
	    throw invalid_argument("Sampling factor must be larger than zero");
	}

    if(centrex < 0 || centrex > inmap->size2 || centrey < 0 || centrey > inmap->size1)
	{
	    throw invalid_argument("Central coordinate outof bounds");
	}
    if(givenfieldsize <= 0)
	{
	    throw invalid_argument("Field size must be bigger than zero");
	}

// Defining core quantities
    density = true;
    scaled = false;
    masked = true;
    centre_x_o = centrex;
    centre_y_o = centrey;
    normal_x = inmap->size2;
    normal_y = inmap->size1;
    map_redshift = mapredshift;
    sampling_factor = oversampling;
    fieldsize = givenfieldsize;
    over_x = normal_x*sampling_factor;
    over_y = normal_y*sampling_factor;

    orig_convergence = gsl_matrix_calloc(normal_y,normal_x);
    redshifts = gsl_matrix_calloc(normal_y,normal_x);
    overs_convergence = gsl_matrix_calloc(over_y, over_x);
    overs_redshifts = gsl_matrix_calloc(over_y, over_x);
    overs_mask = gsl_matrix_int_calloc(over_y, over_x);
    density_map = gsl_matrix_calloc(over_y, over_x);

    cosmo = cosmogiven;

    gsl_matrix_memcpy(orig_convergence,inmap);
    gsl_matrix_memcpy(redshifts,zmap);



//Deriving quantities

    double frac_x = centrex / (normal_x - 1);
    double frac_y = centrey / (normal_y - 1);
    centre_x = (over_x - 1) *frac_x; 
    centre_y = (over_y - 1) *frac_y;  

    pixel_size = fieldsize / (double) over_x;
    pixel_size_o = fieldsize / (double) normal_x;
    physical_pixel_size = pixel_size/(3600.0*180.0) *PI* cosmo->angularDist(0.0,map_redshift)*cH;
    physical_pixel_size_o = pixel_size_o/(3600.0*180.0) *PI* cosmo->angularDist(0.0,map_redshift)*cH;


    for(int i = 0; i < normal_y; i++)
	{
	    for(int j = 0; j < normal_x; j++)
		{
		    for(int l = 0; l < sampling_factor; l++)
			{
			    for(int k = 0; k < sampling_factor; k++)
				{
				    gsl_matrix_set(overs_convergence,i*sampling_factor+l, j*sampling_factor+k, gsl_matrix_get(orig_convergence,i,j));
				    gsl_matrix_set(overs_redshifts,i*sampling_factor+l, j*sampling_factor+k, gsl_matrix_get(redshifts,i,j));
				    gsl_matrix_int_set(overs_mask,i*sampling_factor+l, j*sampling_factor+k, gsl_matrix_int_get(mask,i,j));
				}
			}
		}
	}


//Creating a density map


//Definint prefactor such that we obtain units of Mpc and M_sol
    const double properG = NewtonG * pow(MPCtoCM,-3.0) * MSOLtoG;
    const double properC2 = pow(speed_of_light / MPCtoCM,2.0);
    const double constfactor = properC2/(4.0*PI*properG);

    const double Dd = cosmo->angularDist(0.0,map_redshift)*cH;
    double Ds, Dds,redshift, value, standard_Ds, standard_Dds;

    standard_Ds = cosmo->angularDist(0.0,standard_redshift)*cH;
    standard_Dds = cosmo->angularDist(map_redshift,standard_redshift)*cH;



//This will return the physical density in M_sol * h /Mpc^2

    for(int i = 0; i < over_y; i++)
	{
	    for(int j = 0; j < over_x; j++)
		{
		  if(gsl_matrix_int_get(overs_mask,i,j) !=0)
		    {
		      redshift = gsl_matrix_get(overs_redshifts,i,j);
		      if(abs(redshift - standard_redshift) > 0.01)
			{
			  Ds = cosmo->angularDist(0.0,redshift)*cH;
			  Dds = cosmo->angularDist(map_redshift,redshift)*cH;
			}
		      
		      else
			{
			  Ds = standard_Ds;
			  Dds = standard_Dds;
			}
		      
		      value = constfactor*Ds/(Dd*Dds);
		      gsl_matrix_set(density_map,i,j,gsl_matrix_get(overs_convergence,i,j)*value);
		    }
		  else
		    {
		      gsl_matrix_set(density_map,i,j,0.);
		    }
		}
	}

}

Convergence_Map::Convergence_Map(gsl_matrix *inmap, double givenfieldsize, double mapredshift, double centrex, double centrey, int oversampling, astro::cosmology *cosmogiven)
{

    if(oversampling < 1)
	{
	    throw invalid_argument("Sampling factor must be larger than zero");
	}

    if(centrex < 0 || centrex > inmap->size2 || centrey < 0 || centrey > inmap->size1)
	{
	    throw invalid_argument("Central coordinate outof bounds");
	}
    if(givenfieldsize <= 0)
	{
	    throw invalid_argument("Field size must be bigger than zero");
	}


// Defining core quantities
    density = false;
    scaled = false;
    masked = false;
    normal_x = inmap->size2;
    normal_y = inmap->size1;
    centre_x_o = centrex;
    centre_y_o = centrey;
    map_redshift = mapredshift;
    sampling_factor = oversampling;
    fieldsize = givenfieldsize;
    over_x = normal_x*sampling_factor;
    over_y = normal_y*sampling_factor;

    orig_convergence = gsl_matrix_calloc(normal_y,normal_x);
    overs_convergence = gsl_matrix_calloc(over_y, over_x);
    gsl_matrix_memcpy(orig_convergence,inmap);
    cosmo = cosmogiven; 


//Deriving quantities

    double frac_x = centrex / (normal_x - 1);
    double frac_y = centrey / (normal_y - 1);
    centre_x = (over_x - 1) *frac_x; 
    centre_y = (over_y - 1) *frac_y;

    pixel_size = fieldsize / (double) over_x;
    pixel_size_o = fieldsize / (double) normal_x;
    physical_pixel_size = pixel_size/(3600.0*180.0) *PI* cosmo->angularDist(0.0,map_redshift)*cH;
    physical_pixel_size_o = pixel_size_o/(3600.0*180.0) *PI* cosmo->angularDist(0.0,map_redshift)*cH;

//Oversampling the maps

    for(int i = 0; i < normal_y; i++)
	{
	    for(int j = 0; j < normal_x; j++)
		{
		    for(int l = 0; l < sampling_factor; l++)
			{
			    for(int k = 0; k < sampling_factor; k++)
				{
				    gsl_matrix_set(overs_convergence,i*sampling_factor+l, j*sampling_factor+k, gsl_matrix_get(orig_convergence,i,j));
				}
			}
		}
	}
}

Convergence_Map::Convergence_Map(gsl_matrix *inmap, gsl_matrix_int *mask,double givenfieldsize,double mapredshift, double centrex, double centrey, int oversampling, astro::cosmology *cosmogiven)
{

    if((inmap->size1 != mask->size1) || (inmap->size2 != mask->size2))
      {
	throw invalid_argument("C map and mask size must match.");
      }
    if(oversampling < 1)
	{
	    throw invalid_argument("Sampling factor must be larger than zero");
	}

    if(centrex < 0 || centrex > inmap->size2 || centrey < 0 || centrey > inmap->size1)
	{
	    throw invalid_argument("Central coordinate outof bounds");
	}
    if(givenfieldsize <= 0)
	{
	    throw invalid_argument("Field size must be bigger than zero");
	}


// Defining core quantities
    density = false;
    scaled = false;
    masked = true;
    normal_x = inmap->size2;
    normal_y = inmap->size1;
    centre_x_o = centrex;
    centre_y_o = centrey;
    map_redshift = mapredshift;
    sampling_factor = oversampling;
    fieldsize = givenfieldsize;
    over_x = normal_x*sampling_factor;
    over_y = normal_y*sampling_factor;

    orig_convergence = gsl_matrix_calloc(normal_y,normal_x);
    overs_convergence = gsl_matrix_calloc(over_y, over_x);
    overs_mask = gsl_matrix_int_calloc(over_y, over_x);
    gsl_matrix_memcpy(orig_convergence,inmap);
    cosmo = cosmogiven; 


//Deriving quantities

    double frac_x = centrex / (normal_x - 1);
    double frac_y = centrey / (normal_y - 1);
    centre_x = (over_x - 1) *frac_x; 
    centre_y = (over_y - 1) *frac_y;

    pixel_size = fieldsize / (double) over_x;
    pixel_size_o = fieldsize / (double) normal_x;
    physical_pixel_size = pixel_size/(3600.0*180.0) *PI* cosmo->angularDist(0.0,map_redshift)*cH;
    physical_pixel_size_o = pixel_size_o/(3600.0*180.0) *PI* cosmo->angularDist(0.0,map_redshift)*cH;

//Oversampling the maps

    for(int i = 0; i < normal_y; i++)
	{
	    for(int j = 0; j < normal_x; j++)
		{
		    for(int l = 0; l < sampling_factor; l++)
			{
			    for(int k = 0; k < sampling_factor; k++)
				{
				    gsl_matrix_set(overs_convergence,i*sampling_factor+l, j*sampling_factor+k, gsl_matrix_get(orig_convergence,i,j));
				    gsl_matrix_int_set(overs_mask,i*sampling_factor+l, j*sampling_factor+k, gsl_matrix_int_get(mask,i,j));
				}
			}
		}
	}

}

Convergence_Map::~Convergence_Map()
{

    gsl_matrix_free(orig_convergence);
    gsl_matrix_free(overs_convergence);

    if(density)
	{
	    gsl_matrix_free(redshifts);
	    gsl_matrix_free(overs_redshifts);
	    gsl_matrix_free(density_map);
	}
    if(masked)
      {
	gsl_matrix_int_free(overs_mask);
      }
}

double Convergence_Map::x_c(string selection, int j)
{
    double value;
    if(j < 0 || j >= overs_convergence->size2)
	{
	    throw invalid_argument("Pixel coordinates out of bounds");
	}

    if(selection == "left_arcsec")
	{
	    return ((double) j - 0.5 - centre_x)*pixel_size;
	}
    else if(selection == "centre_arcsec")
	{
	    return ((double) j - centre_x)*pixel_size;
	}
    else if(selection == "right_arcsec")
	{
	    return ((double) j + 0.5 - centre_x)*pixel_size;
	}
    else if(selection == "left_mpc")
	{
	    return ((double) j - 0.5 - centre_x)*physical_pixel_size;
	}
    else if(selection == "centre_mpc")
	{
	    return ((double) j  - centre_x)*physical_pixel_size;
	}
   else if(selection == "right_mpc")
	{
	    return ((double) j + 0.5 - centre_x)*physical_pixel_size;
	}
   else
       {
	   throw invalid_argument("Invalid selection for x_c");
       }
}

double Convergence_Map::x_c_o(string selection, int j)
{
    double value;
    if(j < 0 || j >= overs_convergence->size2)
	{
	    throw invalid_argument("Pixel coordinates out of bounds");
	}

    if(selection == "left_arcsec")
	{
	    return ((double) j - 0.5 - centre_x_o)*pixel_size_o;
	}
    else if(selection == "centre_arcsec")
	{
	    return ((double) j - centre_x_o)*pixel_size_o;
	}
    else if(selection == "right_arcsec")
	{
	    return ((double) j + 0.5 - centre_x_o)*pixel_size_o;
	}
    else if(selection == "left_mpc")
	{
	    return ((double) j - 0.5 - centre_x_o)*physical_pixel_size_o;
	}
    else if(selection == "centre_mpc")
	{
	    return ((double) j  - centre_x_o)*physical_pixel_size_o;
	}
   else if(selection == "right_mpc")
	{
	    return ((double) j + 0.5 - centre_x_o)*physical_pixel_size_o;
	}
   else
       {
	   throw invalid_argument("Invalid selection for x_c_o");
       }
}

double Convergence_Map::y_c(string selection, int i)
{
    if(i < 0 || i >= overs_convergence->size1)
	{
	    throw invalid_argument("Pixel coordinates out of bounds");
	}

    if(selection == "bottom_arcsec")
	{
	    return ((double) i - 0.5 - centre_y)*pixel_size;
	}
    else if(selection == "centre_arcsec")
	{
	    return ((double) i - centre_y)*pixel_size;
	}
    else if(selection == "top_arcsec")
	{
	    return ((double) i + 0.5 - centre_y)*pixel_size;
	}
    else if(selection == "bottom_mpc")
	{
	    return ((double) i - 0.5 - centre_y)*physical_pixel_size;
	}
    else if(selection == "centre_mpc")
	{
	    return ((double) i  - centre_y)*physical_pixel_size;
	}
   else if(selection == "top_mpc")
	{
	    return ((double) i + 0.5 - centre_y)*physical_pixel_size;
	}
   else
       {
	   throw invalid_argument("Invalid selection for y_c");
       }
}
double Convergence_Map::y_c_o(string selection, int j)
{
    double value;
    if(j < 0 || j >= overs_convergence->size1)
	{
	    throw invalid_argument("Pixel coordinates out of bounds");
	}

    if(selection == "bottom_arcsec")
	{
	    return ((double) j - 0.5 - centre_y_o)*pixel_size_o;
	}
    else if(selection == "centre_arcsec")
	{
	    return ((double) j - centre_y_o)*pixel_size_o;
	}
    else if(selection == "top_arcsec")
	{
	    return ((double) j + 0.5 - centre_y_o)*pixel_size_o;
	}
    else if(selection == "bottom_mpc")
	{
	    return ((double) j - 0.5 - centre_y_o)*physical_pixel_size_o;
	}
    else if(selection == "centre_mpc")
	{
	    return ((double) j  - centre_y_o)*physical_pixel_size_o;
	}
   else if(selection == "top_mpc")
	{
	    return ((double) j + 0.5 - centre_y_o)*physical_pixel_size_o;
	}
   else
       {
	   throw invalid_argument("Invalid selection for y_c_o");
       }
}

int Convergence_Map::show_xpixel(string selection, double x_c)
{


    if(selection == "arcsec")
	{
	    double pos_j = x_c/pixel_size +centre_x;
	    return  round(pos_j);
	}
    else if(selection == "mpc")
	{
	    double pos_j = x_c/physical_pixel_size +centre_x;
	    return round(pos_j);
	}
    else
	{
	    throw invalid_argument("Invalid selection for showpixel");
	}

}
int Convergence_Map::show_xpixel_o(string selection, double x_c)
{


    if(selection == "arcsec")
	{
	    double pos_j = x_c/pixel_size_o +centre_x_o;
	    return  round(pos_j);
	}
    else if(selection == "mpc")
	{
	    double pos_j = x_c/physical_pixel_size_o +centre_x_o;
	    return round(pos_j);
	}
    else
	{
	    throw invalid_argument("Invalid selection for showpixel_o");
	}

}

int Convergence_Map::show_ypixel(string selection, double y_c)
{

    if(selection == "arcsec")
	{
	    double pos_i = y_c/pixel_size +centre_y;
	    return round(pos_i);
	}

    else if(selection == "mpc")
	{
	    double pos_i = y_c/physical_pixel_size +centre_y;
	    return  round(pos_i);
	}
    else
	{
	    throw invalid_argument("Invalid selection for showpixel");
	}
	
}
int Convergence_Map::show_ypixel_o(string selection, double y_c)
{

    if(selection == "arcsec")
	{
	    double pos_i = y_c/pixel_size_o +centre_y_o;
	    return round(pos_i);
	}

    else if(selection == "mpc")
	{
	    double pos_i = y_c/physical_pixel_size_o +centre_y_o;
	    return  round(pos_i);
	}
    else
	{
	    throw invalid_argument("Invalid selection for showpixel_o");
	}
	
}


gsl_matrix* Convergence_Map::data(string selection)
{

    if(selection == "orig_convergence")
	{
	    return orig_convergence;
	}
    else if(selection == "overs_convergence")
	{
	    return overs_convergence;
	}
    else if(selection == "redshifts")
	{
	    if(density)
		{
		    return redshifts;
		}
	    else
		{
		    throw invalid_argument("No redshift constraints available");
		}
	}
    else if(selection == "overs_redshifts")
	{
	    if(density)
		{
		    return overs_redshifts;
		}
	    else
		{
		    throw invalid_argument("No redshift constraints available");
		}
	}

    else if(selection == "density")
	{
	    if(density)
		{
		    return density_map;
		}
	    else
		{
		    throw invalid_argument("No density constraints available");
		}
	}

    else
	{
	    throw invalid_argument("Invalid selection for C-map data.");
	}
}

gsl_matrix_int* Convergence_Map::mask()
{
  if(masked)
    {
      return overs_mask;
    }
  else
    {
      throw invalid_argument("MAP_ANALYSIS: Field is not masked.");
    }
} 

double Convergence_Map::double_data(string selection)
{

    if(selection == "physical_pixel_size")
	{
	    return physical_pixel_size;
	}
    else if(selection == "pixel_size")
	{
	    return pixel_size;
	}
    else if(selection == "physical_pixel_size_o")
	{
	    return physical_pixel_size;
	}
    else if(selection == "pixel_size_o")
	{
	    return pixel_size;
	}
    else if(selection == "fieldsize")
	{
	    return fieldsize;
	}
    else if(selection == "map_redshift")
	{
	    return map_redshift;
	}
    else
	{
	    throw invalid_argument("Invalid selection for showdata in C-Map");
	}
}

int Convergence_Map::int_data(string selection)
{

    if(selection == "normal_x")
	{
	    return normal_x;
	}
    else if(selection == "normal_y")
	{
	    return normal_y;
	}
    else if(selection == "over_x")
	{
	    return over_x;
	}
    else if(selection == "over_y")
	{
	    return over_y;
	}
    else if(selection == "sampling_factor")
	{
	    return sampling_factor;
	}
    else if(selection == "centre_x")
	{
	    return centre_x;
	}
    else if(selection == "centre_y")
	{
	    return centre_y;
	}
    else if(selection == "centre_x_o")
	{
	    return centre_x_o;
	}
    else if(selection == "centre_y_o")
	{
	    return centre_y_o;
	}
    else
	{
	    throw invalid_argument("Invalid selection for showdata in C-Map");
	}
}

bool Convergence_Map::bool_data(string selection)
{
    if(selection == "density")
	{
	    return density;
	}
    else if(selection == "scaled")
	{
	    return scaled;
	}
    else
	{
	    throw invalid_argument("Invalid argument for C-map bool_data");
	}
}

void Convergence_Map::set_bool(string selection, bool value)
{
    if(selection == "density")
	{
	    density = value;
	}
    else if(selection == "scaled")
	{
	    scaled = value;
	}
    else
	{
	    throw invalid_argument("Invalid argument for C-map bool_data");
	}
}




void Convergence_Map::masssheet_normalise(string selection, double value)
{

    double lambda;
    double reference;

    if(selection == "min")
	{
	  if(!masked)
	    {
	      reference = gsl_matrix_min(overs_convergence);
	    }
	  else
	    {
	      gsl_matrix *aux = gsl_matrix_calloc(overs_convergence->size1, overs_convergence->size2);
	      for(int i = 0; i < aux->size1; i++)
		{
		  for(int j = 0; j < aux->size2; j++)
		    {
		      if(gsl_matrix_int_get(overs_mask,i,j) != 0)
			{
			  gsl_matrix_set(aux,i,j,gsl_matrix_get(overs_convergence,i,j));
			}
		      else
			{
			  gsl_matrix_set(aux,i,j,1e10);
			}
		    }
		}
	      reference = gsl_matrix_min(aux);
	      gsl_matrix_free(aux);
	    }
	    lambda = (1.0-value) / (1.0-reference);
	    
	    gsl_matrix_scale(overs_convergence,lambda);
	    gsl_matrix_add_constant(overs_convergence,(1.0-lambda));


	}

    else if(selection == "max")
	{
	  if(!masked)
	    {
	      reference = gsl_matrix_min(overs_convergence);
	    }
	  else
	    {
	      gsl_matrix *aux = gsl_matrix_calloc(overs_convergence->size1, overs_convergence->size2);
	      for(int i = 0; i < aux->size1; i++)
		{
		  for(int j = 0; j < aux->size2; j++)
		    {
		      if(gsl_matrix_int_get(overs_mask,i,j) != 0)
			{
			  gsl_matrix_set(aux,i,j,gsl_matrix_get(overs_convergence,i,j));
			}
		      else
			{
			  gsl_matrix_set(aux,i,j,-1e10);
			}
		    }
		}
	      reference = gsl_matrix_max(aux);
	      gsl_matrix_free(aux);
	    }
	    reference = gsl_matrix_max(overs_convergence);
	    lambda = (1.0-value) / (1.0-reference);
	    
	    gsl_matrix_scale(overs_convergence,lambda);
	    gsl_matrix_add_constant(overs_convergence,(1.0-lambda));

	}

    else if(selection == "border")
	{
	    int ref;

	    if(overs_convergence->size1 > overs_convergence->size2)
		{
		    ref = overs_convergence->size1;
		}
	    else
		{
		    ref = overs_convergence->size2;
		}
	    int border_line = ceil((double) ref * value);

	    vector<double> borderensemble;
	    vector<double> borderweight;

	    for(int i = 0; i < over_y; i++)
		{
		    for(int j = 0; j < over_x; j++)
			{
			    if(i < border_line || i >= (over_y - border_line) || j < border_line || j >= (over_x - border_line))
				
				{
				    borderensemble.push_back(gsl_matrix_get(overs_convergence,i,j));
				    if(gsl_matrix_int_get(overs_mask,i,j) != 0)
				      {
					borderweight.push_back(1.0);
				      }
				    else
				      {
					borderweight.push_back(0.0);
				      }
				}
			}
		}
		
	    cout <<borderensemble.size() <<endl;

	    reference = gsl_stats_wmean(&borderweight[0],1,&borderensemble[0],1,borderensemble.size());
	    lambda = 1.0 / (1.0-reference);
	    
	    gsl_matrix_scale(overs_convergence,lambda);
	    gsl_matrix_add_constant(overs_convergence,(1.0-lambda));
	    
	}
		

    else
	{
	    throw invalid_argument("Invalid selection for masssheet normalise.");
	}

}


void Convergence_Map::scaling(double redshift_S_old, double redshift_S_new, double redshift_D)
{
    scaled = true;
    double factor1, factor2, factor3, factor4, scale;

    factor1 = cosmo->angularDist(redshift_D,redshift_S_new);
    factor2 = cosmo->angularDist(0.0,redshift_S_new);
    factor4 = cosmo->angularDist(redshift_D,redshift_S_old);
    factor3 = cosmo->angularDist(0.0,redshift_S_old);
    scale = factor1/factor2*factor3/factor4;

    gsl_matrix_scale(overs_convergence,scale);


}

void Convergence_Map::convergence_profile(string selection, double x, double y, double radius, int sampling, bool discard,  string filename)
{

    double r;
    double step = radius / (double) sampling;

    double x_pos;
    double y_pos;
    int x_pix;
    int y_pix;

    gsl_matrix *result = gsl_matrix_calloc(sampling,2);
    double temp_result;


    for(int n = 0; n < sampling; n++)
	{
	    r = n*step;
	    if(n == 0)
		{
		    gsl_matrix_set(result,n,0,0.0);
		    if(selection == "arcsec")
			{
			    x_pix = show_xpixel("arcsec",x);
			    y_pix = show_ypixel("arcsec",y);
			}
		    else
			{
			    x_pix = show_xpixel("mpc",x);
			    y_pix = show_ypixel("mpc",y);
			}
		    if(!masked)
		      {
			temp_result = gsl_matrix_get(overs_convergence,y_pix, x_pix);
		      }
		    else
		      {
			if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) != 0)
			  {
			    temp_result = gsl_matrix_get(overs_convergence,y_pix, x_pix);
			  }
			else
			  {
			    temp_result = 0.;
			  }
		      }
		}
	    else
		{
		    int circ_samples;
		    double circ_step;
//Check adequate angle sampling...
		    if(selection == "arcsec")
			{
			    circ_step = pixel_size*1.4142 / r;

			}
		    else
			{
			    circ_step = physical_pixel_size*1.4142 / r;
			}
		
		    circ_samples = ceil(2*PI/circ_step);
		    vector<double> sample;
		    vector<double> weight;
		    for(int m = 0; m < circ_samples; m++)
			{
			    x_pos = x + r*cos(m*circ_step);
			    y_pos = y + r*sin(m*circ_step);
			    if(selection == "arcsec")
				{
				    x_pix = show_xpixel("arcsec",x_pos);
				    y_pix = show_ypixel("arcsec",y_pos);
				}
			    else
				{
				    x_pix = show_xpixel("mpc",x_pos);
				    y_pix = show_ypixel("mpc",y_pos);
				}
			    if(discard)
			      {
				if(gsl_matrix_get(overs_convergence,y_pix,x_pix) > 0.0)
				  {
				    sample.push_back(gsl_matrix_get(overs_convergence,y_pix,x_pix));
				    if(masked)
				      {
					if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
					  {
					    weight.push_back(0.0);
					  }
					else
					  {
					    weight.push_back(1.);
					  }
				      }
				    else
				      {
					weight.push_back(1.);
				      }
				  }
			      }
			    else
			      {
				sample.push_back(gsl_matrix_get(overs_convergence,y_pix,x_pix));
				if(masked)
				  {
				    if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
				      {
					weight.push_back(0.0);
				      }
				    else
				      {
					weight.push_back(1.);
				      }
				  }
				else
				  {
				    weight.push_back(1.);
				  }
			      }
			
			}
		    temp_result = gsl_stats_wmean(&weight[0],1,&sample[0],1,sample.size());
		}
	    gsl_matrix_set(result,n,0,r);
	    gsl_matrix_set(result,n,1,temp_result);
	}

    ofstream output(filename.c_str());
    for(int i = 0; i < result->size1; i++)
	{
	    output <<gsl_matrix_get(result,i,0) <<"\t" <<gsl_matrix_get(result,i,1) <<endl;
	}
    output.close();
}

double Convergence_Map::convergence(string selection, double x, double y, double radius, bool discard)
{

    double r = radius;

    double x_pos;
    double y_pos;
    int x_pix;
    int y_pix;

    double temp_result;



    if(r == 0.0)
	{
	    if(selection == "arcsec")
		{
		    x_pix = show_xpixel("arcsec",x);
		    y_pix = show_ypixel("arcsec",y);
		}
	    else
		{
		    x_pix = show_xpixel("mpc",x);
		    y_pix = show_ypixel("mpc",y);
		}
	    if(masked)
	      {
		if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
		  {
		    temp_result = 0.;
		  }
		  else
		    {
		      temp_result = gsl_matrix_get(overs_convergence,y_pix, x_pix);
		    }
	      }
	    else
	      {
		temp_result = gsl_matrix_get(overs_convergence,y_pix, x_pix);
	      }
	}

    else
	{
	    int circ_samples;
	    double circ_step;
//Check adequate angle sampling...
	    if(selection == "arcsec")
		{
		    circ_step = pixel_size*1.4142 / r;
		    
		}
	    else
		{
		    circ_step = physical_pixel_size*1.4142 / r;
		}
	    
	    circ_samples = ceil(2*PI/circ_step);
	    vector<double> sample;
	    vector<double> weight;
	    for(int m = 0; m < circ_samples; m++)
	      {
		x_pos = x + r*cos(m*circ_step);
		y_pos = y + r*sin(m*circ_step);
		if(selection == "arcsec")
		  {
		    x_pix = show_xpixel("arcsec",x_pos);
		    y_pix = show_ypixel("arcsec",y_pos);
		  }
		else
		  {
		    x_pix = show_xpixel("mpc",x_pos);
		    y_pix = show_ypixel("mpc",y_pos);
		  }
		if(discard)
		  {
		    if(gsl_matrix_get(overs_convergence,y_pix,x_pix) > 0.0)
		      {
			sample.push_back(gsl_matrix_get(overs_convergence,y_pix,x_pix));
			if(masked)
			  {
			    if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
			      {
				weight.push_back(0.);
			      }
			    else
			      {
				weight.push_back(1.);
			      }
			  }
			else
			  {
			    weight.push_back(1.);
			  }
			
		      }
		  }
		else
		  {
		    sample.push_back(gsl_matrix_get(overs_convergence,y_pix,x_pix));
		    if(masked)
		      {
			if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
			  {
			    weight.push_back(0.);
			  }
			else
			  {
			    weight.push_back(1.);
			  }
		      }
		    else
		      {
			weight.push_back(1.);
		      }
		    
		  }
	      }
	    temp_result = gsl_stats_wmean(&weight[0],1,&sample[0],1,sample.size());
	}
    
    return temp_result;
}

	

void Convergence_Map::density_profile(string selection, double x, double y, double radius, int sampling, bool discard,  string filename)
{

    double r;
    double step = radius / (double) sampling;

    double x_pos;
    double y_pos;
    int x_pix;
    int y_pix;

    gsl_matrix *result = gsl_matrix_calloc(sampling,2);
    double temp_result;


    for(int n = 0; n < sampling; n++)
	{
	    r = n*step;
	    if(n == 0)
		{
		    gsl_matrix_set(result,n,0,0.0);
		    if(selection == "arcsec")
			{
			    x_pix = show_xpixel("arcsec",x);
			    y_pix = show_ypixel("arcsec",y);
			}
		    else
			{
			    x_pix = show_xpixel("mpc",x);
			    y_pix = show_ypixel("mpc",y);
			}
		    if(!masked)
		      {
			temp_result = gsl_matrix_get(density_map,y_pix, x_pix);
		      }
		    else
		      {
			if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) != 0)
			  {
			    temp_result = gsl_matrix_get(density_map,y_pix, x_pix);
			  }
			else
			  {
			    temp_result = 0.;
			  }
		      }
		}
	    else
		{
		    int circ_samples;
		    double circ_step;
//Check adequate angle sampling...
		    if(selection == "arcsec")
			{
			    circ_step = pixel_size*1.4142 / r;

			}
		    else
			{
			    circ_step = physical_pixel_size*1.4142 / r;
			}
		
		    circ_samples = ceil(2*PI/circ_step);
		    vector<double> sample;
		    vector<double> weight;
		    for(int m = 0; m < circ_samples; m++)
			{
			    x_pos = x + r*cos(m*circ_step);
			    y_pos = y + r*sin(m*circ_step);
			    if(selection == "arcsec")
				{
				    x_pix = show_xpixel("arcsec",x_pos);
				    y_pix = show_ypixel("arcsec",y_pos);
				}
			    else
				{
				    x_pix = show_xpixel("mpc",x_pos);
				    y_pix = show_ypixel("mpc",y_pos);
				}
			    if(discard)
			      {
				if(gsl_matrix_get(density_map,y_pix,x_pix) > 0.0)
				  {
				    sample.push_back(gsl_matrix_get(density_map,y_pix,x_pix));
				    if(masked)
				      {
					if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
					  {
					    weight.push_back(0.0);
					  }
					else
					  {
					    weight.push_back(1.);
					  }
				      }
				    else
				      {
					weight.push_back(1.);
				      }
				  }
			      }
			    else
			      {
				sample.push_back(gsl_matrix_get(density_map,y_pix,x_pix));
				if(masked)
				  {
				    if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
				      {
					weight.push_back(0.0);
				      }
				    else
				      {
					weight.push_back(1.);
				      }
				  }
				else
				  {
				    weight.push_back(1.);
				  }
			      }
			
			}
		    temp_result = gsl_stats_wmean(&weight[0],1,&sample[0],1,sample.size());
		}
	    gsl_matrix_set(result,n,0,r);
	    gsl_matrix_set(result,n,1,temp_result);
	}

    ofstream output(filename.c_str());
    for(int i = 0; i < result->size1; i++)
	{
	    output <<gsl_matrix_get(result,i,0) <<"\t" <<gsl_matrix_get(result,i,1) <<endl;
	}
    output.close();

}

double Convergence_Map::density_shell(string selection, double x, double y, bool discard, double radius)
{

    double r = radius;

    double x_pos;
    double y_pos;
    int x_pix;
    int y_pix;

    double temp_result;



    if(r == 0.0)
	{
	    if(selection == "arcsec")
		{
		    x_pix = show_xpixel("arcsec",x);
		    y_pix = show_ypixel("arcsec",y);
		}
	    else
		{
		    x_pix = show_xpixel("mpc",x);
		    y_pix = show_ypixel("mpc",y);
		}
	    if(masked)
	      {
		if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
		  {
		    temp_result = 0.;
		  }
		  else
		    {
		      temp_result = gsl_matrix_get(density_map,y_pix, x_pix);
		    }
	      }
	    else
	      {
		temp_result = gsl_matrix_get(density_map,y_pix, x_pix);
	      }
	}

    else
      {
	int circ_samples;
	double circ_step;
	//Check adequate angle sampling...
	    if(selection == "arcsec")
	      {
		circ_step = pixel_size*1.4142 / r;
		
	      }
	    else
	      {
		circ_step = physical_pixel_size*1.4142 / r;
	      }
	    
	    circ_samples = ceil(2*PI/circ_step);
	    vector<double> sample;
	    vector<double> weight;
	    for(int m = 0; m < circ_samples; m++)
	      {
		x_pos = x + r*cos(m*circ_step);
		y_pos = y + r*sin(m*circ_step);
		if(selection == "arcsec")
		  {
		    x_pix = show_xpixel("arcsec",x_pos);
		    y_pix = show_ypixel("arcsec",y_pos);
		  }
		else
		  {
		    x_pix = show_xpixel("mpc",x_pos);
		    y_pix = show_ypixel("mpc",y_pos);
		  }
		if(discard)
		  {
		    if(gsl_matrix_get(density_map,y_pix,x_pix) > 0.0)
		      {
			sample.push_back(gsl_matrix_get(density_map,y_pix,x_pix));
			if(masked)
			  {
			    if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
			      {
				weight.push_back(0.);
			      }
			    else
			      {
				weight.push_back(1.);
			      }
			  }
			else
			  {
			    weight.push_back(1.);
			  }
			
		      }
		  }
		else
		  {
		    sample.push_back(gsl_matrix_get(density_map,y_pix,x_pix));
		    if(masked)
		      {
			    if(gsl_matrix_int_get(overs_mask,y_pix,x_pix) == 0)
			      {
				weight.push_back(0.);
			      }
			    else
			      {
				weight.push_back(1.);
			      }
		      }
		    else
		      {
			weight.push_back(1.);
		      }
		    
		  }
	      }
	    
	    temp_result = gsl_stats_wmean(&weight[0],1,&sample[0],1,sample.size());
      }
    
    return temp_result;
}

double Convergence_Map::mass(string selection, double x, double y, double radius, bool discard)
{

    double x_pos, y_pos;
    double distance, value;
    value = 0.0;

    for(int i = 0; i < over_y; i++)
	{
	    for(int j = 0; j < over_x; j++)
		{
		    if(selection == "arcsec")
			{
			    x_pos = x_c("centre_arcsec",j);
			    y_pos = y_c("centre_arcsec",i);
			}
		    else if(selection == "mpc")
			{
			    x_pos = x_c("centre_mpc",j);
			    y_pos = y_c("centre_mpc",i);
			}
		    else
			{
			    throw invalid_argument("Invalid selection for mass/radius");
			}
		    distance = sqrt(pow(x_pos-x,2.0) + pow(y_pos-y,2.0));

		    if(distance <= radius)
			{
			  if(discard)
			    {
			      if(gsl_matrix_get(density_map,i,j) > 0.)
				{
				  if(masked)
				    {
				      if(gsl_matrix_int_get(overs_mask,i,j) != 0)
					{
					  value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
					}
				    }
				  else
				    {
				      value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
				    }
				  
				}
			    }
			  else
			    {
			      if(masked)
				{
				  if(gsl_matrix_int_get(overs_mask,i,j) != 0)
				    {
				      value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
				    }
				}
			      else
				{
				  value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
				}
			    }
			}
		}
	}
    return value;
}

double Convergence_Map::mass(string selection, double x, double y, double x_extension, double y_extension, bool discard)
{

    double x_pos, y_pos;
    double value, x_window1, x_window2, y_window1, y_window2;
    value = 0.0;

    x_window1 = x - x_extension/2.0;
    x_window2 = x + x_extension/2.0;
    y_window1 = x - y_extension/2.0;
    y_window2 = x + y_extension/2.0;

    for(int i = 0; i < over_y; i++)
	{
	    for(int j = 0; j < over_x; j++)
		{
		    if(selection == "arcsec")
			{
			    x_pos = x_c("centre_arcsec",j);
			    y_pos = y_c("centre_arcsec",i);
			}
		    else if(selection == "mpc")
			{
			    x_pos = x_c("centre_mpc",j);
			    y_pos = y_c("centre_mpc",i);
			}
		    else
			{
			    throw invalid_argument("Invalid selection for mass/radius");
			}
		    if(x_pos >= x_window1 && x_pos <= x_window2 && x_pos >= y_window1 && y_pos <= y_window2)
			{
			  if(discard)
			    {
			      if(gsl_matrix_get(density_map,i,j) > 0.)
				{
				  if(masked)
				    {
				      if(gsl_matrix_int_get(overs_mask,i,j) != 0)
					{
					  value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
					}
				    }
				  else
				    {
				      value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
				    }
				  
				}
			    }
			  else
			    {
			      if(masked)
				{
				  if(gsl_matrix_int_get(overs_mask,i,j) != 0)
				    {
				      value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
				    }
				}
			      else
				{
				  value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
				}
			    }
			}
		}
	}
    return value;
}

double Convergence_Map::annulus_mass(string selection, double x, double y, double inner_radius, double outer_radius, bool discard)
{

    double value1, value2;
    if(inner_radius > outer_radius)
	{
	    throw invalid_argument("Outer radius must be larger than inner radius");
	}

    if(discard)
      {
	value1 = mass(selection,x,y,outer_radius,true);
	value2 = mass(selection,x,y,inner_radius,true);
      }
    else
      {
	value1 = mass(selection,x,y,outer_radius,false);
	value2 = mass(selection,x,y,inner_radius,false);
      }
    return (value1 - value2);
}

double Convergence_Map::mass()
{

    if(!density)
	{
	    throw invalid_argument("No mass can be calculated without redshifts");
	}

    double value = 0.0;

    for(int i = 0; i < over_y; i++)
	{
	    for(int j = 0; j < over_x; j++)
		{
		  if(masked)
		    {
		      if(gsl_matrix_int_get(overs_mask,i,j) != 0)
			{
			  value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
			}
		    }
		  else
		    {
		      value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
		    }
		}
	}
    
    return value;
} 

double Convergence_Map::surface_mass(string selection, double x, double y, double radius, string selection2, bool discard)
{

    double x_pos, y_pos;
    double distance, value;
    value = 0.0;

    int counter = 0;

    for(int i = 0; i < over_y; i++)
	{
	    for(int j = 0; j < over_x; j++)
		{
		    if(selection == "arcsec")
			{
			    x_pos = x_c("centre_arcsec",j);
			    y_pos = y_c("centre_arcsec",i);
			}
		    else if(selection == "mpc")
			{
			    x_pos = x_c("centre_mpc",j);
			    y_pos = y_c("centre_mpc",i);
			}
		    else
			{
			    throw invalid_argument("Invalid selection for mass/radius");
			}
		    distance = sqrt(pow(x_pos-x,2.0) + pow(y_pos-y,2.0));

		    if(distance <= radius)
			{
			  if(discard)
			    {
			      if(gsl_matrix_get(density_map,i,j) > 0.)
				{
				  if(masked)
				    {
				      if(gsl_matrix_int_get(overs_mask,i,j) != 0)
					{
					  value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
					  counter++;
					}
				    }
				  else
				    {
				      value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
				      counter++;
				    }
				}
			    }
			  else
			    {
			      if(masked)
				{
				  if(gsl_matrix_int_get(overs_mask,i,j) != 0)
				    {
				      
				      value += gsl_matrix_get(density_map,i,j)*pow(physical_pixel_size,2.0);
				      counter++;
				    }
				}
			      else
				{
				  value += gsl_matrix_get(density_map,i,j);
				  counter++;
				}
			    }
			}
		}
	}
    if(selection2 == "cosmo2")
	{
	    return value/counter;
	}

    else if(selection2 == "cgm")
	{
	    return value/counter * 2.0890821665866856e-16;
	}
    else
	{
	    throw invalid_argument("Invalid unit selecion for surface_mass");
	}
}

void Convergence_Map::find_peaks(string selection, string method, bool maxpeak, bool pixel_coord, string outfile)
{

    ofstream out(outfile.c_str(), ios_base::app);
    out <<showpoint;
    out <<fixed;

    if(method == "maximum")
	{
	    size_t x,y;
	    double pos_x, pos_y;
	    gsl_matrix_max_index(orig_convergence,&y,&x);

	    if(selection == "left_arcsec")
		{
		    pos_x = x_c_o("left_arcsec",x);
		    pos_y = y_c_o("bottom_arcsec",y);
		}
	    else if(selection == "right_arcsec")
		{
		    pos_x = x_c_o("right_arcsec",x);
		    pos_y = y_c_o("top_arcsec",y);
		}
	    else if(selection == "centre_arcsec")
		{
		    pos_x = x_c_o("centre_arcsec",x);
		    pos_y = y_c_o("centre_arcsec",y);
		}
	    else if(selection == "left_mpc")
		{
		    pos_x = x_c_o("left_mpc",x);
		    pos_y = y_c_o("bottom_mpc",y);
		}
	    else if(selection == "right_mpc")
		{
		    pos_x = x_c_o("right_mpc",x);
		    pos_y = y_c_o("top_mpc",y);
		}
	    else if(selection == "centre_mpc")
		{
		    pos_x = x_c_o("centre_mpc",x);
		    pos_y = y_c_o("cente_mpc",y);
		}
	    else
		{
		    throw invalid_argument("Invalid selection for find peaks");
		}
	    out <<pos_x <<"\t" <<pos_y <<flush;
	    if(pixel_coord)
		{
		    out <<"\t" <<x <<"\t" <<y <<flush;
		}
	    out <<endl;
	}

    else if(method == "neighbours")
	{
	    bool peak = true;
	    double reference;
	    double pos_x, pos_y;
	    double highest_peak = gsl_matrix_min(orig_convergence);
	    double peak_x, peak_y;
	    int peak_pixel_x, peak_pixel_y;

	    for(int i = 0; i < orig_convergence->size1; i++)
		{

		    for(int j = 0; j < orig_convergence->size2; j++)
			{
			    peak = true;
			    reference = gsl_matrix_get(orig_convergence,i,j);
			    for(int l = -1; l < 2; l++)
				{
				    for(int k = -1; k < 2; k++)
					{
					    if((i+l) > -1 && (i+l) < orig_convergence->size1 && (j+k) > -1 && (j+k) < orig_convergence->size2)
						{
						    if(reference < gsl_matrix_get(orig_convergence,i+l,j+k))
							{
							    peak = false;  
							}
						}
					}
				}
			    if(peak)
				{
				    if(selection == "left_arcsec")
					{
					    pos_x = x_c_o("left_arcsec",j);
					    pos_y = y_c_o("bottom_arcsec",i);
					}
				    else if(selection == "right_arcsec")
					{
					    pos_x = x_c_o("right_arcsec",j);
					    pos_y = y_c_o("top_arcsec",i);
					}
				    else if(selection == "centre_arcsec")
					{
					    pos_x = x_c_o("centre_arcsec",j);
					    pos_y = y_c_o("centre_arcsec",i);
					}
				    else if(selection == "left_mpc")
					{
					    pos_x = x_c_o("left_mpc",j);
					    pos_y = y_c_o("bottom_mpc",i);
					}
				    else if(selection == "right_mpc")
					{
					    pos_x = x_c_o("right_mpc",j);
					    pos_y = y_c_o("top_mpc",i);
					}
				    else if(selection == "centre_mpc")
					{
					    pos_x = x_c_o("centre_mpc",j);
					    pos_y = y_c_o("centre_mpc",i);
					}
				    else
					{
					    throw invalid_argument("Invalid selection for find_peaks");
					}
				    if(!maxpeak)
					{
					    out <<pos_x <<"\t" <<pos_y <<flush;
					    if(pixel_coord)
						{
						    out <<"\t" <<j <<"\t" <<i <<flush;
						}
					    out <<endl;
					}
				    else
					{
					    if(reference > highest_peak)
						{
						    highest_peak = reference;
						    peak_x = pos_x;
						    peak_y = pos_y;
						    peak_pixel_x = j;
						    peak_pixel_y = i;
						}
					}

				}

			}
		}
	    if(maxpeak)
		{
		    out <<peak_x <<"\t" <<peak_y <<flush;
		    if(pixel_coord)
			{
			    out <<"\t" <<peak_pixel_x <<"\t" <<peak_pixel_y <<flush;
			}
		    out <<endl;
		}

	}

    else
	{
	    throw invalid_argument("Invalid method selection for find_peaks");
	}

}

void Convergence_Map::find_peaks(string selection, string method, int xmin, int xmax, int ymin, int ymax, bool maxpeak, bool pixel_coord, string outfile)
{

    ofstream out(outfile.c_str(),ios_base::app);
    out <<showpoint;
    out <<fixed;

    if(method == "maximum")
	{
	    size_t x,y;
	    double pos_x, pos_y;
	    gsl_matrix_max_index(orig_convergence,&y,&x);

	    if(selection == "left_arcsec")
		{
		    pos_x = x_c_o("left_arcsec",x);
		    pos_y = y_c_o("bottom_arcsec",y);
		}
	    else if(selection == "right_arcsec")
		{
		    pos_x = x_c_o("right_arcsec",x);
		    pos_y = y_c_o("top_arcsec",y);
		}
	    else if(selection == "centre_arcsec")
		{
		    pos_x = x_c_o("centre_arcsec",x);
		    pos_y = y_c_o("centre_arcsec",y);
		}
	    else if(selection == "left_mpc")
		{
		    pos_x = x_c_o("left_mpc",x);
		    pos_y = y_c_o("bottom_mpc",y);
		}
	    else if(selection == "right_mpc")
		{
		    pos_x = x_c_o("right_mpc",x);
		    pos_y = y_c_o("top_mpc",y);
		}
	    else if(selection == "centre_mpc")
		{
		    pos_x = x_c_o("centre_mpc",x);
		    pos_y = y_c_o("centre_mpc",y);
		}
	    else
		{
		    throw invalid_argument("Invalid selection for find peaks");
		}
	    if(pos_x >= xmin && pos_x <= xmax && pos_y >= ymin && pos_y <= ymax)
		{
		    out <<pos_x <<"\t" <<pos_y <<flush;

		    if(pixel_coord)
			{
			    out <<"\t" <<x <<"\t" <<y <<flush;
			}
		    out <<endl;
		}
	}

    else if(method == "neighbours")
	{
	    bool peak = true;
	    double reference;
	    double pos_x, pos_y;
	    double highest_peak = gsl_matrix_min(orig_convergence);
	    double peak_x, peak_y;
	    int peak_pixel_x, peak_pixel_y;
	    bool foundone = false;
	    bool infield = true;

	    for(int i = 0; i < orig_convergence->size1; i++)
		{		    
		    for(int j = 0; j < orig_convergence->size2; j++)
			{
			    peak = true;
			    infield = true;

			    if(selection == "left_arcsec" && (x_c_o("left_arcsec",j) > xmax || x_c_o("left_arcsec",j) < xmin || y_c_o("bottom_arcsec",i) > ymax || y_c_o("bottom_arcsec",i) < ymin))
				{
				    infield = false;
				}
			    if(selection == "right_arcsec" && (x_c_o("right_arcsec",j) > xmax || x_c_o("right_arcsec",j) < xmin || y_c_o("top_arcsec",i) > ymax || y_c_o("top_arcsec",i) < ymin))
				{
				    infield = false;
				}
			    if(selection == "centre_arcsec" && (x_c_o("centre_arcsec",j) > xmax || x_c_o("centre_arcsec",j) < xmin || y_c_o("centre_arcsec",i) > ymax || y_c_o("centre_arcsec",i) < ymin))
				{
				    infield = false;
				}
			    if(selection == "left_mpc" && (x_c_o("left_mpc",j) > xmax || x_c_o("left_mpc",j) < xmin || y_c_o("bottom_mpc",i) > ymax || y_c_o("bottom_mpc",i) < ymin))
				{
				    infield = false;
				}
			    if(selection == "right_mpc" && (x_c_o("right_mpc",j) > xmax || x_c_o("right_mpc",j) < xmin || y_c_o("top_mpc",i) > ymax || y_c_o("top_mpc",i) < ymin))
				{
				    infield = false;
				}
			    if(selection == "centre_mpc" && (x_c_o("centre_mpc",j) > xmax || x_c_o("centre_mpc",j) < xmin || y_c_o("centre_mpc",i) > ymax || y_c_o("centre_mpc",i) < ymin))
				{
				    infield = false;
				}
			    if(infield)
				{
				    reference = gsl_matrix_get(orig_convergence,i,j);
				    for(int l = -1; l < 2; l++)
					{
					    for(int k = -1; k < 2; k++)
						{
						    if(i+l > -1 && i+l < orig_convergence->size1 && j+k > -1 && j+k < orig_convergence->size2)
							{
							    if(reference < gsl_matrix_get(orig_convergence,i+l,j+k))
								{
								    peak = false;
								}
							}
						}
					}
				    if(peak)
					{
					    foundone = true;
					    if(selection == "left_arcsec")
						{
						    pos_x = x_c_o("left_arcsec",j);
						    pos_y = y_c_o("bottom_arcsec",i);
						}
					    else if(selection == "right_arcsec")
						{
						    pos_x = x_c_o("right_arcsec",j);
						    pos_y = y_c_o("top_arcsec",i);
						}
					    else if(selection == "centre_arcsec")
						{
						    pos_x = x_c_o("centre_arcsec",j);
						    pos_y = y_c_o("centre_arcsec",i);
						}
					    else if(selection == "left_mpc")
						{
						    pos_x = x_c_o("left_mpc",j);
						    pos_y = y_c_o("bottom_mpc",i);
						}
					    else if(selection == "right_mpc")
						{
						    pos_x = x_c_o("right_mpc",j);
						    pos_y = y_c_o("top_mpc",i);
						}
					    else if(selection == "centre_mpc")
						{
						    pos_x = x_c_o("centre_mpc",j);
						    pos_y = y_c_o("centre_mpc",i);
						}
					    else
						{
						    throw invalid_argument("Invalid selection for find_peaks");
						}
					    if(!maxpeak)
						{
						    out <<pos_x <<"\t" <<pos_y <<flush;
						    if(pixel_coord)
							{
							    out <<"\t" <<j <<"\t" <<i <<flush;
							}
						    out <<endl;
						}
					    else
						{
						    if(reference > highest_peak)
							{
							    highest_peak = reference;
							    peak_x = pos_x;
							    peak_y = pos_y;
							    peak_pixel_x = j;
							    peak_pixel_y = i;
							}
						}
					    
					}

				}
			}
		}
	    if(maxpeak && foundone)
		{
		    out <<peak_x <<"\t" <<peak_y <<flush;
		    if(pixel_coord)
			{
			    out <<"\t" <<peak_pixel_x <<"\t" <<peak_pixel_y <<flush;
			}
		    out <<endl;
		}

	}

    else
	{
	    throw invalid_argument("Invalid method selection for find_peaks");
	}

}


















 


