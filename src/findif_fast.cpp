/***
    Class providing very precise and fast finite differencing routines
    for unmasked grids and up to second order. The derivative coefficients
    are based on Fongberg88.

    Julian Merten
    JPL / Caltech 
    jmerten@caltech.edu
    Dec. 2011

***/

#include <saw/findif_fast.h>

findif_grid_fast::findif_grid_fast(int x, int y)
{

  //Checking if the input makes sense

  if(x <= 0 || y <= 0)
    {
      throw invalid_argument("FINDIF_GRID: Grid dimensions must be positive and > 0");
    }


  dim.resize(2);
  order.resize(2);
  dim[0] = x;
  dim[1] = y;
  order[0] = 1;
  order[1] = 8;

  grid_scale = dim[0];

  // Setting vector sizes


  coeff_c1_8.resize(9);
  coeff_c2_8.resize(9);
  coeff_f1_8.resize(9);
  coeff_f2_8.resize(9);

  coeff_c1_6.resize(7);
  coeff_c2_6.resize(7);
  coeff_c3_6.resize(9);
  coeff_f1_6.resize(7);
  coeff_f2_6.resize(8);
  coeff_f3_6.resize(9);

  coeff_c1_4.resize(5);
  coeff_c2_4.resize(5);
  coeff_c3_4.resize(7);
  coeff_f1_4.resize(5);
  coeff_f2_4.resize(6);
  coeff_f3_4.resize(7);

  coeff_c1_2.resize(3);
  coeff_c2_2.resize(3);
  coeff_c3_2.resize(5);
  coeff_f1_2.resize(3);
  coeff_f2_2.resize(4);
  coeff_f3_2.resize(5);

  // Setting first order coefficients

  coeff_c1_8[0] = 1./280.;
  coeff_c1_8[1] = -4./105.;
  coeff_c1_8[2] = 1./5.;
  coeff_c1_8[3] = -4./5.;
  coeff_c1_8[4] = 0.;
  coeff_c1_8[5] = 4./5.;
  coeff_c1_8[6] = -1./5.;
  coeff_c1_8[7] = 4./105.;
  coeff_c1_8[8] = -1./280.;

  coeff_f1_8[0] = -761./280.;
  coeff_f1_8[1] = 8.;
  coeff_f1_8[2] = -14.;
  coeff_f1_8[3] = 56./3.;
  coeff_f1_8[4] = -35./2.;
  coeff_f1_8[5] = 56./5.;
  coeff_f1_8[6] = -14./3.;
  coeff_f1_8[7] = 8./7.;
  coeff_f1_8[8] = -1./8.;

  coeff_c1_6[0] = -1./60.;
  coeff_c1_6[1] = 3./20.;
  coeff_c1_6[2] = -3/4.;
  coeff_c1_6[3] = 0.;
  coeff_c1_6[4] = 3./4.;
  coeff_c1_6[5] = -3/20.;
  coeff_c1_6[6] = 1./60.;

  coeff_f1_6[0] = -49./20.;
  coeff_f1_6[1] = 6.;
  coeff_f1_6[2] = -15./2.;
  coeff_f1_6[3] = 20./3.;
  coeff_f1_6[4] = -15./4.;
  coeff_f1_6[5] = 6./5.;
  coeff_f1_6[6] = -1./6.;

  coeff_c1_4[0] = 1./12.;
  coeff_c1_4[1] = -2./3.;
  coeff_c1_4[2] = 0.;
  coeff_c1_4[3] = 2./3.;
  coeff_c1_4[4] = -1./12.;
  
  coeff_f1_4[0] = -25./12.;
  coeff_f1_4[1] = 4.;
  coeff_f1_4[2] = -3.;
  coeff_f1_4[3] = 4./3.;
  coeff_f1_4[4] = -1./4.;

  coeff_c1_2[0] = -1./2.;
  coeff_c1_2[1] = 0.;
  coeff_c1_2[2] = 1./2.;
  
  coeff_f1_2[0] = -3./2.;
  coeff_f1_2[1] = 2.;
  coeff_f1_2[2] = -1./2.;

  //Second order derivatives

  coeff_c2_8[0] = -1./560.;
  coeff_c2_8[1] = 8./315.;
  coeff_c2_8[2] = -1./5.;
  coeff_c2_8[3] = 8./5.;
  coeff_c2_8[4] = -205./72.;
  coeff_c2_8[5] = 8./5.;
  coeff_c2_8[6] = -1./5.;
  coeff_c2_8[7] = 8./315.;
  coeff_c2_8[8] = -1./560.;
  
  coeff_f2_8[0] = 29531./5040.;
  coeff_f2_8[1] = -962./35.;
  coeff_f2_8[2] = 621./10.;
  coeff_f2_8[3] = -4006./45.;
  coeff_f2_8[4] = 691./8.;
  coeff_f2_8[5] = -282./5.;
  coeff_f2_8[6] = 2143./90.;
  coeff_f2_8[7] = -206./35.;
  coeff_f2_8[8] = 363./560.;

  coeff_c2_6[0] = 1./90.;
  coeff_c2_6[1] = -3./20.;
  coeff_c2_6[2] = 3./2.;
  coeff_c2_6[3] = -49./18.;
  coeff_c2_6[4] = 3./2.;
  coeff_c2_6[5] = -3./20.;
  coeff_c2_6[6] = 1./90.;
  
  coeff_f2_6[0] = 469./90.;
  coeff_f2_6[1] = -223./10.;
  coeff_f2_6[2] = 879./20.;
  coeff_f2_6[3] = -949./18.;
  coeff_f2_6[4] = 41.;
  coeff_f2_6[5] = -201./10.;
  coeff_f2_6[6] = 1019./180.;
  coeff_f2_6[7] = -7./10.;

  coeff_c2_4[0] = -1./12.;
  coeff_c2_4[1] = 4./3.;
  coeff_c2_4[2] = -5./2.;
  coeff_c2_4[3] = 4./3.;
  coeff_c2_4[4] = -1./12.;
  
  coeff_f2_4[0] = 15./4.;
  coeff_f2_4[1] = -77./6.;
  coeff_f2_4[2] = 107./6.;
  coeff_f2_4[3] = -13.;
  coeff_f2_4[4] = 61./12.;
  coeff_f2_4[5] = -5./6.;

  coeff_c2_2[0] = 1.;
  coeff_c2_2[1] = -2.;
  coeff_c2_2[2] = 1.;
  
  coeff_f2_2[0] = 2.;
  coeff_f2_2[1] = -5.;
  coeff_f2_2[2] = 4.;
  coeff_f2_2[3] = -1.;

  //Setting third derivative order coefficients

  coeff_c3_6[0] = -7./240.;
  coeff_c3_6[1] = 3./10.;
  coeff_c3_6[2] = -169./120.;
  coeff_c3_6[3] = 61./30.;
  coeff_c3_6[4] = 0.;
  coeff_c3_6[5] = -61./30.;
  coeff_c3_6[6] = 169./120.;
  coeff_c3_6[7] = -3./10.;
  coeff_c3_6[8] = 7./240.;
  
  coeff_f3_6[0] = -801./80.;
  coeff_f3_6[1] = 349./6.;
  coeff_f3_6[2] = -18353./120.;
  coeff_f3_6[3] = 2391./10.;
  coeff_f3_6[4] = -1457./6.;
  coeff_f3_6[5] = 4891./30.;
  coeff_f3_6[6] = -561./8.;
  coeff_f3_6[7] = 527./30.;
  coeff_f3_6[8] = -469./240.;

  coeff_c3_4[0] = 1./8.;
  coeff_c3_4[1] = -1.;
  coeff_c3_4[2] = 13./8.;
  coeff_c3_4[3] = 0.;
  coeff_c3_4[4] = -13./8.;
  coeff_c3_4[5] = 1.;
  coeff_c3_4[6] = -1./8.;
  
  coeff_f3_4[0] = -49./8.;
  coeff_f3_4[1] = 29.;
  coeff_f3_4[2] = -461./8.;
  coeff_f3_4[3] = 62.;
  coeff_f3_4[4] = -307./8.;
  coeff_f3_4[5] = 13.;
  coeff_f3_4[6] = -15./8.;

  coeff_c3_2[0] = -1./2.;
  coeff_c3_2[1] = 1.;
  coeff_c3_2[2] = 0.;
  coeff_c3_2[3] = -1.;
  coeff_c3_2[4] = 1./2.;
  
  coeff_f3_2[0] = -5./2.;
  coeff_f3_2[1] = 9.;
  coeff_f3_2[2] = -12.;
  coeff_f3_2[3] = 7.;
  coeff_f3_2[4] = -3./2.;

 //Normalising the differences before the fun begins

  for(int i = 0; i < coeff_c1_8.size(); i++)
    {
      coeff_c1_8[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_c1_6.size(); i++)
    {
      coeff_c1_6[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_c1_4.size(); i++)
    {
      coeff_c1_4[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_c1_2.size(); i++)
    {
      coeff_c1_2[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_f1_8.size(); i++)
    {
      coeff_f1_8[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_f1_6.size(); i++)
    {
      coeff_f1_6[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_f1_4.size(); i++)
    {
      coeff_f1_4[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_f1_2.size(); i++)
    {
      coeff_f1_2[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_c2_8.size(); i++)
    {
      coeff_c2_8[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_c2_6.size(); i++)
    {
      coeff_c2_6[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_c2_4.size(); i++)
    {
      coeff_c2_4[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_c2_2.size(); i++)
    {
      coeff_c2_2[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_8.size(); i++)
    {
      coeff_f2_8[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_6.size(); i++)
    {
      coeff_f2_6[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_6.size(); i++)
    {
      coeff_f2_6[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_4.size(); i++)
    {
      coeff_f2_4[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_2.size(); i++)
    {
      coeff_f2_2[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_c3_6.size(); i++)
    {
      coeff_c3_6[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_c3_4.size(); i++)
    {
      coeff_c3_4[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_c3_2.size(); i++)
    {
      coeff_c3_2[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_f3_6.size(); i++)
    {
      coeff_f3_6[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_f3_4.size(); i++)
    {
      coeff_f3_4[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_f3_2.size(); i++)
    {
      coeff_f3_2[i] *= pow(grid_scale,3.);
    }
}



findif_grid_fast::~findif_grid_fast()
{
  coeff_c1_8.clear();
  coeff_c1_6.clear();
  coeff_c1_4.clear();
  coeff_c1_2.clear();
  coeff_f1_8.clear();
  coeff_f1_6.clear();
  coeff_f1_4.clear();
  coeff_f1_2.clear();
  coeff_c2_8.clear();
  coeff_c2_6.clear();
  coeff_c2_4.clear();
  coeff_c2_2.clear();
  coeff_f2_8.clear();
  coeff_f2_6.clear();
  coeff_f2_4.clear();
  coeff_f2_2.clear();
  coeff_c3_6.clear();
  coeff_c3_4.clear();
  coeff_c3_2.clear();
  coeff_f3_6.clear();
  coeff_f3_4.clear();
  coeff_f3_2.clear();
}

int findif_grid_fast::show_dim(string selection)
{

  if(selection == "x")
    {
      return dim[0];
    }

  else if(selection == "y")
    {
      return dim[1];
    }
  else
    {
      throw invalid_argument("FINDIF_GRID: Invalid selection for show_dim");
    }
}

int findif_grid_fast::show_order(string selection)
{

  if(selection == "derivative")
    {
      return order[0];
    }

  else if(selection == "accuracy")
    {
      return order[1];
    }
  else
    {
      throw invalid_argument("FINDIF_GRID: Invalid selection for show_order");
    }
}

int findif_grid_fast::define_type(int index, string selection)
{
  if(index < 0 && index >= dim[0]*dim[1])
    {
      throw invalid_argument("FINDIF_GRID: bad_number index out of range");
    }
  
  int type = 0;
  int size = dim[0]*dim[1];

  int x,y;
  show_xy(dim[0],dim[1],index,&x,&y);

  if(selection == "x")
    {     
      if(x == 0)
	{
	  type = 1;
	}
      else if(x == 1)
	{
	  type = 2;
	}
      else if(x == dim[0]-2)
	{
	  type = 5;
	}
      else if(x == 2 || x == dim[0]-3)
	{
	  type = 3;
	}
      else if(x == 3 || x == dim[0]-4)
	{
	  type = 4;
	}
      else if(x == dim[0]-1)
	{
	  type = 8;
	}
    }


  else if(selection == "y")
    {
      if(y == 0)
	{
	  type = 1;
	}
      else if(y == 1)
	{
	  type = 2;
	}
      else if(y == dim[1]-2)
	{
	  type = 5;
	}
      else if(y == 2 || y == dim[1]-3)
	{
	  type = 3;
	}
      else if(y == 3 || y == dim[1]-4)
	{
	  type = 4;
	}
      else if(y == dim[1]-1)
	{
	  type = 8;
	}
    }
  
  else
    {
      throw invalid_argument("FINDIF_GRID: Invalid selection for type_definition");
    }
  
  return type;
}

void findif_grid_fast::switch_order(string selection)
{

  if(selection == "first")
    {
      order[0] = 1 ;
    }
  else if(selection == "second")
    {
      order[0] = 2;
    }
  else if(selection == "third")
    {
      order[0] = 3;
    }
  else
    {
      throw invalid_argument("FINDIF_GRID: Invalid selection for switch_order");
    }
}

void findif_grid_fast::set_scale(double scale)
{

  //Getting old normlisation out
  for(int i = 0; i < coeff_c1_8.size(); i++)
    {
      coeff_c1_8[i] *= 1./grid_scale;
    }
  for(int i = 0; i < coeff_c1_6.size(); i++)
    {
      coeff_c1_6[i] *= 1./grid_scale;
    }
  for(int i = 0; i < coeff_c1_4.size(); i++)
    {
      coeff_c1_4[i] *= 1./grid_scale;
    }
  for(int i = 0; i < coeff_c1_2.size(); i++)
    {
      coeff_c1_2[i] *= 1./grid_scale;
    }
  for(int i = 0; i < coeff_f1_8.size(); i++)
    {
      coeff_f1_8[i] *= 1./grid_scale;
    }
  for(int i = 0; i < coeff_f1_6.size(); i++)
    {
      coeff_f1_6[i] *= 1./grid_scale;
    }
  for(int i = 0; i < coeff_f1_4.size(); i++)
    {
      coeff_f1_4[i] *= 1./grid_scale;
    }
  for(int i = 0; i < coeff_f1_2.size(); i++)
    {
      coeff_f1_2[i] *= 1./grid_scale;
    }
  for(int i = 0; i < coeff_c2_8.size(); i++)
    {
      coeff_c2_8[i] *= pow(1./grid_scale,2.);
    }
  for(int i = 0; i < coeff_c2_6.size(); i++)
    {
      coeff_c2_6[i] *= pow(1./grid_scale,2.);
    }
  for(int i = 0; i < coeff_c2_4.size(); i++)
    {
      coeff_c2_4[i] *= pow(1./grid_scale,2.);
    }
  for(int i = 0; i < coeff_c2_2.size(); i++)
    {
      coeff_c2_2[i] *= pow(1./grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_8.size(); i++)
    {
      coeff_f2_8[i] *= pow(1./grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_6.size(); i++)
    {
      coeff_f2_6[i] *= pow(1./grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_6.size(); i++)
    {
      coeff_f2_6[i] *= pow(1./grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_4.size(); i++)
    {
      coeff_f2_4[i] *= pow(1./grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_2.size(); i++)
    {
      coeff_f2_2[i] *= pow(1./grid_scale,2.);
    }
  for(int i = 0; i < coeff_c3_6.size(); i++)
    {
      coeff_c3_6[i] *= pow(1./grid_scale,3.);
    }
  for(int i = 0; i < coeff_c3_4.size(); i++)
    {
      coeff_c3_4[i] *= pow(1./grid_scale,3.);
    }
  for(int i = 0; i < coeff_c3_2.size(); i++)
    {
      coeff_c3_2[i] *= pow(1./grid_scale,3.);
    }
  for(int i = 0; i < coeff_f3_6.size(); i++)
    {
      coeff_f3_6[i] *= pow(1./grid_scale,3.);
    }
  for(int i = 0; i < coeff_f3_4.size(); i++)
    {
      coeff_f3_4[i] *= pow(1./grid_scale,3.);
    }
  for(int i = 0; i < coeff_f3_2.size(); i++)
    {
      coeff_f3_2[i] *= pow(1./grid_scale,3.);
    }

  grid_scale = dim[0]/scale;

 //Setting the new scale

  for(int i = 0; i < coeff_c1_8.size(); i++)
    {
      coeff_c1_8[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_c1_6.size(); i++)
    {
      coeff_c1_6[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_c1_4.size(); i++)
    {
      coeff_c1_4[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_c1_2.size(); i++)
    {
      coeff_c1_2[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_f1_8.size(); i++)
    {
      coeff_f1_8[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_f1_6.size(); i++)
    {
      coeff_f1_6[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_f1_4.size(); i++)
    {
      coeff_f1_4[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_f1_2.size(); i++)
    {
      coeff_f1_2[i] *= grid_scale;
    }
  for(int i = 0; i < coeff_c2_8.size(); i++)
    {
      coeff_c2_8[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_c2_6.size(); i++)
    {
      coeff_c2_6[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_c2_4.size(); i++)
    {
      coeff_c2_4[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_c2_2.size(); i++)
    {
      coeff_c2_2[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_8.size(); i++)
    {
      coeff_f2_8[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_6.size(); i++)
    {
      coeff_f2_6[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_6.size(); i++)
    {
      coeff_f2_6[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_4.size(); i++)
    {
      coeff_f2_4[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_f2_2.size(); i++)
    {
      coeff_f2_2[i] *= pow(grid_scale,2.);
    }
  for(int i = 0; i < coeff_c3_6.size(); i++)
    {
      coeff_c3_6[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_c3_4.size(); i++)
    {
      coeff_c3_4[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_c3_2.size(); i++)
    {
      coeff_c3_2[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_f3_6.size(); i++)
    {
      coeff_f3_6[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_f3_4.size(); i++)
    {
      coeff_f3_4[i] *= pow(grid_scale,3.);
    }
  for(int i = 0; i < coeff_f3_2.size(); i++)
    {
      coeff_f3_2[i] *= pow(grid_scale,3.);
    }

}

void findif_grid_fast::differentiate(gsl_vector *input, string selection)
{

  int size = dim[0]*dim[1];

  if(input->size != size)
    {
      throw invalid_argument("FINDIF_GRID: Input vector does not match grid dimensions");
    }

  gsl_vector *dummy = gsl_vector_calloc(size);
    
  double value;

  for(int i = 0; i < size; i++)
    {	  
      value = 0.;
      if(selection == "x")
	{
	  if(define_type(i,"x") == 1)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += coeff_f1_2[j]*gsl_vector_get(input,i+j);
		    }
		}
	      
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += coeff_f2_2[j]*gsl_vector_get(input,i+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += coeff_f3_2[j]*gsl_vector_get(input,i+j);
		    }
		  
		}
	    }
	  else if(define_type(i,"x") == 8)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += -coeff_f1_2[j]*gsl_vector_get(input,i-j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += -coeff_f2_2[j]*gsl_vector_get(input,i-j);
		    }
		  value *= -1.;
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += -coeff_f3_2[j]*gsl_vector_get(input,i-j);
		    }
		}
	    }
	  else if(define_type(i,"x") == 2 || define_type(i,"x") == 5)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_2.size(); j++)
		    {
		      value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_2.size(); j++)
		    {
		      value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
		    }
		}
	      else
		{
		  if(define_type(i,"x") == 2)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i+j);
			}
		    }
		  else if(define_type(i,"x") == 5)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i-j);
			}
		    }
		}
	    }
	  else if(define_type(i,"x") == 3)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_4.size(); j++)
		    {
		      value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_4.size(); j++)
		    {
		      value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_2.size(); j++)
		    {
		      value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)+j);
		    }
		}
	    }
	  else if(define_type(i,"x") == 4)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_6.size(); j++)
		    {
		      value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_6.size(); j++)
		    {
		      value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_4.size(); j++)
		    {
		      value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)+j);
		    }
		}
	    }
	  else
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_8.size(); j++)
		    {
		      value += coeff_c1_8[j]*gsl_vector_get(input,i-((coeff_c1_8.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_8.size(); j++)
		    {
		      value += coeff_c2_8[j]*gsl_vector_get(input,i-((coeff_c2_8.size()-1)/2)+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_6.size(); j++)
		    {
		      value += coeff_c3_6[j]*gsl_vector_get(input,i-((coeff_c3_6.size()-1)/2)+j);
		    }
		}
	    }
	}
      
      else if(selection == "y")
	{
	  if(define_type(i,"y") == 1)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += coeff_f1_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		}
	      
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += coeff_f2_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		}
	      else
		{

		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += coeff_f3_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		  
		}
	    }
	  else if(define_type(i,"y") == 8)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += -coeff_f1_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += -coeff_f2_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		  value *= -1.;
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += -coeff_f3_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		}
	    }
	  else if(define_type(i,"y") == 2 || define_type(i,"y") == 5)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_2.size(); j++)
		    {
		      value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_2.size(); j++)
		    {
		      value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  if(define_type(i,"y") == 2)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i+j*dim[0]);
			}

		    }
		  else if(define_type(i,"y") == 5)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i-j*dim[0]);
			}
		    }

		}
	    }
	  else if(define_type(i,"y") == 3)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_4.size(); j++)
		    {
		      value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_4.size(); j++)
		    {
		      value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_2.size(); j++)
		    {
		      value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	    }
	  else if(define_type(i,"y") == 4)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_6.size(); j++)
		    {
		      value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_6.size(); j++)
		    {
		      value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_4.size(); j++)
		    {
		      value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	    }
	  else
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_8.size(); j++)
		    {
		      value += coeff_c1_8[j]*gsl_vector_get(input,i-((coeff_c1_8.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_8.size(); j++)
		    {
		      value += coeff_c2_8[j]*gsl_vector_get(input,i-((coeff_c2_8.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_6.size(); j++)
		    {
		      value += coeff_c3_6[j]*gsl_vector_get(input,i-((coeff_c3_6.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	    }

	}
    
      else
	{
	  throw invalid_argument("FINDIF_GRID: Invalid selection for differentiate");
	}
      
      gsl_vector_set(dummy,i,value);
    }
  gsl_vector_memcpy(input,dummy);
  gsl_vector_free(dummy);

}

void findif_grid_fast::differentiate(gsl_vector *input, gsl_vector *output, string selection)
{

  int size = dim[0]*dim[1];

  if(input->size != size)
    {
      throw invalid_argument("FINDIF_GRID: Input vector does not match grid dimensions");
    }
    
  double value;

  for(int i = 0; i < size; i++)
    {
      value = 0.;
      if(selection == "x")
	{
	  if(define_type(i,"x") == 1)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += coeff_f1_2[j]*gsl_vector_get(input,i+j);
		    }
		}
	      
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += coeff_f2_2[j]*gsl_vector_get(input,i+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += coeff_f3_2[j]*gsl_vector_get(input,i+j);
		    }
		  
		}
	    }
	  else if(define_type(i,"x") == 8)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += -coeff_f1_2[j]*gsl_vector_get(input,i-j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += -coeff_f2_2[j]*gsl_vector_get(input,i-j);
		    }
		  value *= -1.;
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += -coeff_f3_2[j]*gsl_vector_get(input,i-j);
		    }
		}
	    }
	  else if(define_type(i,"x") == 2 || define_type(i,"x") == 5)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_2.size(); j++)
		    {
		      value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_2.size(); j++)
		    {
		      value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
		    }
		}
	      else
		{
		  if(define_type(i,"x") == 2)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i+j);
			}
		    }
		  else if(define_type(i,"x") == 5)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i-j);
			}
		    }
		}
	    }
	  else if(define_type(i,"x") == 3)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_4.size(); j++)
		    {
		      value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_4.size(); j++)
		    {
		      value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_2.size(); j++)
		    {
		      value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)+j);
		    }
		}
	    }
	  else if(define_type(i,"x") == 4)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_6.size(); j++)
		    {
		      value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_6.size(); j++)
		    {
		      value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_4.size(); j++)
		    {
		      value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)+j);
		    }
		}
	    }
	  else
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_8.size(); j++)
		    {
		      value += coeff_c1_8[j]*gsl_vector_get(input,i-((coeff_c1_8.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_8.size(); j++)
		    {
		      value += coeff_c2_8[j]*gsl_vector_get(input,i-((coeff_c2_8.size()-1)/2)+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_6.size(); j++)
		    {
		      value += coeff_c3_6[j]*gsl_vector_get(input,i-((coeff_c3_6.size()-1)/2)+j);
		    }
		}
	    }
	}
      
      else if(selection == "y")
	{
	  if(define_type(i,"y") == 1)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += coeff_f1_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		}
	      
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += coeff_f2_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		}
	      else
		{

		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += coeff_f3_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		  
		}
	    }
	  else if(define_type(i,"y") == 8)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += -coeff_f1_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += -coeff_f2_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		  value *= -1.;
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += -coeff_f3_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		}
	    }
	  else if(define_type(i,"y") == 2 || define_type(i,"y") == 5)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_2.size(); j++)
		    {
		      value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_2.size(); j++)
		    {
		      value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  if(define_type(i,"y") == 2)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i+j*dim[0]);
			}

		    }
		  else if(define_type(i,"y") == 5)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i-j*dim[0]);
			}
		    }

		}
	    }
	  else if(define_type(i,"y") == 3)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_4.size(); j++)
		    {
		      value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_4.size(); j++)
		    {
		      value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_2.size(); j++)
		    {
		      value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	    }
	  else if(define_type(i,"y") == 4)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_6.size(); j++)
		    {
		      value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_6.size(); j++)
		    {
		      value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_4.size(); j++)
		    {
		      value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	    }
	  else
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_8.size(); j++)
		    {
		      value += coeff_c1_8[j]*gsl_vector_get(input,i-((coeff_c1_8.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_8.size(); j++)
		    {
		      value += coeff_c2_8[j]*gsl_vector_get(input,i-((coeff_c2_8.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_6.size(); j++)
		    {
		      value += coeff_c3_6[j]*gsl_vector_get(input,i-((coeff_c3_6.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	    }

	}
    
      else
	{
	  throw invalid_argument("FINDIF_GRID: Invalid selection for differentiate");
	}
      
      gsl_vector_set(output,i,value);
    }
}

void findif_grid_fast::differentiate(gsl_vector *input, string selection, int acc_order)
{

  if(acc_order != 8 && acc_order != 6 && acc_order != 4 && acc_order != 2)
    {
      throw invalid_argument("FINDIF_GRID: Chosen accuracy order is invalid");
    } 

  int size = dim[0]*dim[1];

  if(input->size != size)
    {
      throw invalid_argument("FINDIF_GRID: Input vector does not match grid dimensions");
    }

  gsl_vector *dummy = gsl_vector_calloc(size);
    
  double value;

  for(int i = 0; i < size; i++)
    {
      value = 0.;
      if(selection == "x")
	{
	  if(define_type(i,"x") == 1)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += coeff_f1_2[j]*gsl_vector_get(input,i+j);
		    }
		}
	      
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += coeff_f2_2[j]*gsl_vector_get(input,i+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += coeff_f3_2[j]*gsl_vector_get(input,i+j);
		    }
		  
		}
	    }
	  else if(define_type(i,"x") == 8)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += -coeff_f1_2[j]*gsl_vector_get(input,i-j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += -coeff_f2_2[j]*gsl_vector_get(input,i-j);
		    }
		  value *= -1.;
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += -coeff_f3_2[j]*gsl_vector_get(input,i-j);
		    }
		}
	    }
	  else if(define_type(i,"x") == 2 || define_type(i,"x") == 5)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_2.size(); j++)
		    {
		      value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_2.size(); j++)
		    {
		      value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
		    }
		}
	      else
		{
		  if(define_type(i,"x") == 2)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i+j);
			}
		    }
		  else if(define_type(i,"x") == 5)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i-j);
			}
		    }
		}
	    }
	  else if(define_type(i,"x") == 3)
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)+j);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)+j);
			}
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_2.size(); j++)
		    {
		      value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)+j);
		    }
		}
	    }
	  else if(define_type(i,"x") == 4)
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_6.size(); j++)
			{
			  value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)+j);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_6.size(); j++)
			{
			  value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)+j);
			}
		    }
		}
	      else
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c3_2.size(); j++)
			{
			  value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c3_4.size(); j++)
			{
			  value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)+j);
			}
		    }
		}
	    }
	  else
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 8)
		    {
		      for(int j = 0; j < coeff_c1_6.size(); j++)
			{
			  value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_8.size(); j++)
			{
			  value += coeff_c1_8[j]*gsl_vector_get(input,i-((coeff_c1_8.size()-1)/2)+j);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 8)
		    {
		      for(int j = 0; j < coeff_c2_6.size(); j++)
			{
			  value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_8.size(); j++)
			{
			  value += coeff_c2_8[j]*gsl_vector_get(input,i-((coeff_c2_8.size()-1)/2)+j);
			}
		    }
		}
	      else
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c3_2.size(); j++)
			{
			  value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c3_4.size(); j++)
			{
			  value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c3_6.size(); j++)
			{
			  value += coeff_c3_6[j]*gsl_vector_get(input,i-((coeff_c3_6.size()-1)/2)+j);
			}
		    }
		}
	    }
	}
      
      else if(selection == "y")
	{
	  if(define_type(i,"y") == 1)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += coeff_f1_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		}
	      
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += coeff_f2_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		}
	      else
		{

		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += coeff_f3_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		  
		}
	    }
	  else if(define_type(i,"y") == 8)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += -coeff_f1_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += -coeff_f2_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		  value *= -1.;
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += -coeff_f3_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		}
	    }
	  else if(define_type(i,"y") == 2 || define_type(i,"y") == 5)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_2.size(); j++)
		    {
		      value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_2.size(); j++)
		    {
		      value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  if(define_type(i,"y") == 2)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i+j*dim[0]);
			}

		    }
		  else if(define_type(i,"y") == 5)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i-j*dim[0]);
			}
		    }

		}
	    }
	  else if(define_type(i,"y") == 3)
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_2.size(); j++)
		    {
		      value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	    }
	  else if(define_type(i,"y") == 4)
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_6.size(); j++)
			{
			  value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_6.size(); j++)
			{
			  value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c3_2.size(); j++)
			{
			  value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c3_4.size(); j++)
			{
			  value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	    }
	  else
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 8)
		    {
		      for(int j = 0; j < coeff_c1_6.size(); j++)
			{
			  value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_8.size(); j++)
			{
			  value += coeff_c1_8[j]*gsl_vector_get(input,i-((coeff_c1_8.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 8)
		    {
		      for(int j = 0; j < coeff_c2_6.size(); j++)
			{
			  value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_8.size(); j++)
			{
			  value += coeff_c2_8[j]*gsl_vector_get(input,i-((coeff_c2_8.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c3_2.size(); j++)
			{
			  value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c3_4.size(); j++)
			{
			  value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c3_6.size(); j++)
			{
			  value += coeff_c3_6[j]*gsl_vector_get(input,i-((coeff_c3_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	    }

	}
    
      else
	{
	  throw invalid_argument("FINDIF_GRID: Invalid selection for differentiate");
	}
      
      gsl_vector_set(dummy,i,value);
    }
      gsl_vector_memcpy(input,dummy);
      gsl_vector_free(dummy);
}

void findif_grid_fast::differentiate(gsl_vector *input, gsl_vector *output, string selection, int acc_order)
{

  if(acc_order != 8 && acc_order != 6 && acc_order != 4 && acc_order != 2)
    {
      throw invalid_argument("FINDIF_GRID: Chosen accuracy order is invalid");
    } 

  int size = dim[0]*dim[1];

  if(input->size != size)
    {
      throw invalid_argument("FINDIF_GRID: Input vector does not match grid dimensions");
    }
    
  double value;

  for(int i = 0; i < size; i++)
    {
      value = 0.;
      if(selection == "x")
	{
	  if(define_type(i,"x") == 1)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += coeff_f1_2[j]*gsl_vector_get(input,i+j);
		    }
		}
	      
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += coeff_f2_2[j]*gsl_vector_get(input,i+j);
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += coeff_f3_2[j]*gsl_vector_get(input,i+j);
		    }
		  
		}
	    }
	  else if(define_type(i,"x") == 8)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += -coeff_f1_2[j]*gsl_vector_get(input,i-j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += -coeff_f2_2[j]*gsl_vector_get(input,i-j);
		    }
		  value *= -1.;
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += -coeff_f3_2[j]*gsl_vector_get(input,i-j);
		    }
		}
	    }
	  else if(define_type(i,"x") == 2 || define_type(i,"x") == 5)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_2.size(); j++)
		    {
		      value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_2.size(); j++)
		    {
		      value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
		    }
		}
	      else
		{
		  if(define_type(i,"x") == 2)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i+j);
			}
		    }
		  else if(define_type(i,"x") == 5)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i-j);
			}
		    }
		}
	    }
	  else if(define_type(i,"x") == 3)
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)+j);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)+j);
			}
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_2.size(); j++)
		    {
		      value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)+j);
		    }
		}
	    }
	  else if(define_type(i,"x") == 4)
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_6.size(); j++)
			{
			  value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)+j);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_6.size(); j++)
			{
			  value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)+j);
			}
		    }
		}
	      else
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c3_2.size(); j++)
			{
			  value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c3_4.size(); j++)
			{
			  value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)+j);
			}
		    }
		}
	    }
	  else
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 8)
		    {
		      for(int j = 0; j < coeff_c1_6.size(); j++)
			{
			  value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_8.size(); j++)
			{
			  value += coeff_c1_8[j]*gsl_vector_get(input,i-((coeff_c1_8.size()-1)/2)+j);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 8)
		    {
		      for(int j = 0; j < coeff_c2_6.size(); j++)
			{
			  value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_8.size(); j++)
			{
			  value += coeff_c2_8[j]*gsl_vector_get(input,i-((coeff_c2_8.size()-1)/2)+j);
			}
		    }
		}
	      else
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c3_2.size(); j++)
			{
			  value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)+j);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c3_4.size(); j++)
			{
			  value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)+j);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c3_6.size(); j++)
			{
			  value += coeff_c3_6[j]*gsl_vector_get(input,i-((coeff_c3_6.size()-1)/2)+j);
			}
		    }
		}
	    }
	}
      
      else if(selection == "y")
	{
	  if(define_type(i,"y") == 1)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += coeff_f1_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		}
	      
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += coeff_f2_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		}
	      else
		{

		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += coeff_f3_2[j]*gsl_vector_get(input,i+j*dim[0]);
		    }
		  
		}
	    }
	  else if(define_type(i,"y") == 8)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_f1_2.size(); j++)
		    {
		      value += -coeff_f1_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_f2_2.size(); j++)
		    {
		      value += -coeff_f2_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		  value *= -1.;
		}
	      else
		{
		  for(int j = 0; j < coeff_f3_2.size(); j++)
		    {
		      value += -coeff_f3_2[j]*gsl_vector_get(input,i-j*dim[0]);
		    }
		}
	    }
	  else if(define_type(i,"y") == 2 || define_type(i,"y") == 5)
	    {
	      if(order[0] == 1)
		{
		  for(int j = 0; j < coeff_c1_2.size(); j++)
		    {
		      value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else if(order[0] == 2)
		{
		  for(int j = 0; j < coeff_c2_2.size(); j++)
		    {
		      value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	      else
		{
		  if(define_type(i,"y") == 2)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i+j*dim[0]);
			}

		    }
		  else if(define_type(i,"y") == 5)
		    {
		      for(int j = 0; j < coeff_f3_2.size(); j++)
			{
			  value += -coeff_f3_2[j]*gsl_vector_get(input,i-j*dim[0]);
			}
		    }

		}
	    }
	  else if(define_type(i,"y") == 3)
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else
		{
		  for(int j = 0; j < coeff_c3_2.size(); j++)
		    {
		      value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)*dim[0]+j*dim[0]);
		    }
		}
	    }
	  else if(define_type(i,"y") == 4)
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_6.size(); j++)
			{
			  value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_6.size(); j++)
			{
			  value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c3_2.size(); j++)
			{
			  value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c3_4.size(); j++)
			{
			  value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	    }
	  else
	    {
	      if(order[0] == 1)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c1_2.size(); j++)
			{
			  value += coeff_c1_2[j]*gsl_vector_get(input,i-((coeff_c1_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c1_4.size(); j++)
			{
			  value += coeff_c1_4[j]*gsl_vector_get(input,i-((coeff_c1_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 8)
		    {
		      for(int j = 0; j < coeff_c1_6.size(); j++)
			{
			  value += coeff_c1_6[j]*gsl_vector_get(input,i-((coeff_c1_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c1_8.size(); j++)
			{
			  value += coeff_c1_8[j]*gsl_vector_get(input,i-((coeff_c1_8.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else if(order[0] == 2)
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c2_2.size(); j++)
			{
			  value += coeff_c2_2[j]*gsl_vector_get(input,i-((coeff_c2_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c2_4.size(); j++)
			{
			  value += coeff_c2_4[j]*gsl_vector_get(input,i-((coeff_c2_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 8)
		    {
		      for(int j = 0; j < coeff_c2_6.size(); j++)
			{
			  value += coeff_c2_6[j]*gsl_vector_get(input,i-((coeff_c2_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c2_8.size(); j++)
			{
			  value += coeff_c2_8[j]*gsl_vector_get(input,i-((coeff_c2_8.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	      else
		{
		  if(acc_order < 4)
		    {
		      for(int j = 0; j < coeff_c3_2.size(); j++)
			{
			  value += coeff_c3_2[j]*gsl_vector_get(input,i-((coeff_c3_2.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else if(acc_order < 6)
		    {
		      for(int j = 0; j < coeff_c3_4.size(); j++)
			{
			  value += coeff_c3_4[j]*gsl_vector_get(input,i-((coeff_c3_4.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		  else
		    {
		      for(int j = 0; j < coeff_c3_6.size(); j++)
			{
			  value += coeff_c3_6[j]*gsl_vector_get(input,i-((coeff_c3_6.size()-1)/2)*dim[0]+j*dim[0]);
			}
		    }
		}
	    }

	}
    
      else
	{
	  throw invalid_argument("FINDIF_GRID: Invalid selection for differentiate");
	}
      
      gsl_vector_set(output,i,value);
    }
}


void findif_grid_fast::derivative_parser(gsl_vector *input, gsl_vector *output, string selection)
{

  vector<double> scaling;
  vector<std::string> ordername;
  vector<std::string> dir;
  vector<std::string> action;

  string subselection;

  int size = dim[0]*dim[1];
  if(input->size != size)
    {
      throw invalid_argument("FINDIF_GRID: Wrong length of input vector.");
    }
  if(output->size != size)
    {
      throw invalid_argument("FINDIF_GRID: Wrong length of output vector.");
    }

  gsl_vector *currentresult = gsl_vector_calloc(size);
  gsl_vector *currentresult2 = gsl_vector_calloc(size);


  while(read_word(selection,subselection,"#") && selection.size() > 2)
    {
      double sc;

      stringstream mystream (stringstream::in | stringstream::out);
      mystream << read_word(subselection,"%");
      mystream >>sc;
      scaling.push_back(sc);
      ordername.push_back(read_word(subselection,"%"));
      dir.push_back(read_word(subselection,"%"));
      action.push_back(read_word(subselection,"%"));
    }

  for(int i = 0; i < scaling.size(); i++)
    {
      if(i == 0)
	{
	  switch_order(ordername[0]);
	  differentiate(input,currentresult,dir[0]);
	  gsl_vector_scale(currentresult,scaling[0]);
	}
      else
	{
	  switch_order(ordername[i]);
	  if(action[i] == "multiply")
	    {
	      differentiate(currentresult,dir[i]);
	      gsl_vector_scale(currentresult,scaling[0]);
		}
	  else
	    {
	      differentiate(input,currentresult2,dir[i]);
	      gsl_vector_scale(currentresult2,scaling[0]);
		if(action[i] == "add")
		  {
		    gsl_vector_add(currentresult,currentresult2);
		  }
		else
		  {
		    gsl_vector_sub(currentresult,currentresult2);
		  }
	    }
	}
    }
  gsl_vector_memcpy(output,currentresult);
  gsl_vector_free(currentresult);
  gsl_vector_free(currentresult2);
    
}










 
