LIBNAMESTAT 	= libsaw.a
LIBNAMEDYN 	= libsaw.so
SAWSRC		= ./src
SAWOBS		= ./obs
SAWINC 		= ./include/	
DRIVER 		= add_WCS
CC 		= g++
CCFLAGS		= -fPIC
GSLINC 		= /usr/local/include
ASTROINC 	= /home/merten-l/include
FFTWINC		= /usr/local/include
CFITSIOINC	= /usr/local/include
CCFITSINC	= /usr/local/include
ADDINC 		= -I$(SAWINC) -I$(GSLINC) -I$(ASTROINC) -I$(FFTWINC) -I$(CFITSIOINC) -I$(CCFITSINC)
SRC 		= $(wildcard $(SAWSRC)/*.cpp)
OBJ		= $(SRC:$(SAWSRC)/%.cpp=$(SAWOBS)/%.o)

#SRC 		= cat_reader.cpp cat_writer.cpp coordinate_grid.cpp findif.cpp interpol.cpp lensing_kernel.cpp lensmodel.cpp map_analysis.cpp rw_fits.cpp smoothing.cpp timing.cpp util.cpp  

.PHONY: clean distclean doc	


$(SAWOBS)/%.o: $(SAWSRC)/%.cpp 
	$(CC) $(CCFLAGS) -c -o $@  $< $(ADDINC)

$(LIBNAMESTAT): $(OBJ)
	ar rv $(LIBNAMESTAT) $(OBJ)

$(LIBNAMEDYN): $(OBJ)	
	$(CC) $(CCFLAGS) -shared -o $(LIBNAMEDYN) $(OBJ) 	



default: $(LIBNAMESTAT)

all:	 $(LIBNAMESTAT)
install: $(LIBNAMESTAT)
shared: $(LIBNAMEDYN)

cat_reader.o: util.o  
cat_writer.o: cat_reader.o
coordinate_grid.o: rw_fits.o util.o 
findif.o:
interpol.o: util.o
lensing_kernel.o: rw_fits.o
lensmodel.o: findif.o rw_fits.o
map_analysis.o: 
rw_fits.o: util.o
smoothing.o: 
timing.o: 
util.o:
