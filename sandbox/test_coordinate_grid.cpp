#include <iostream>
#include <gsl/gsl_matrix.h>
#include <coordinate_grid.h>

using namespace std;

int main()
{

  int x_dim = 250;
  int y_dim = 250;

  coordinate_grid grid1(x_dim,y_dim);


  grid1.add_WCS(125.,125.,181.55061,-8.80092,-0.001666667,0.,0.,0.001666667,true);
  grid1.add_masks("./tests/ds9_2.reg");

  grid1.visualise("./tests/c_grid1_2.fits");
}
