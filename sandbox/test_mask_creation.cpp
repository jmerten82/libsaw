#include <iostream>
#include <gsl/gsl_matrix.h>
#include <rw_fits.h>
#include <util.h>

using namespace std;

int main()
{

  int x = 250;
  int y = 250;


  gsl_matrix_int *map = gsl_matrix_int_calloc(y,x);

  add_mask(map,1,200,50,20);
  add_mask(map,1,0,249,115,133);
  add_mask(map,1,0,249,115,133,15);
  add_mask(map,1,0,249,115,133,30);
  add_mask(map,1,0,249,115,133,45);
  add_mask(map,1,0,249,115,133,60);
  add_mask(map,1,0,249,115,133,75);
  add_mask(map,1,0,249,115,133,90);
  add_mask(map,1,0,249,115,133,105);
  add_mask(map,1,0,249,115,133,120);
  add_mask(map,1,0,249,115,133,135);
  add_mask(map,1,0,249,115,133,150);
  add_mask(map,1,0,249,115,133,165);
  add_mask(map,1,0,249,115,133,180);

  write_pimgint("./masks.fits",map);

  return 0;


}

