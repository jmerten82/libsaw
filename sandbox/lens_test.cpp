#include <iostream>
#include <rw_fits.h>
#include <findif.h>
#include <lensmodel.h>
#include <vector>

using namespace std;

int main()
{

  int x, y;
  x = 32;
  y = 32;

  gsl_matrix_int *mask1 = gsl_matrix_int_calloc(y,x);
  gsl_matrix_int *mask2 = gsl_matrix_int_calloc(y,x);
  read_pimgint("./mask.fits",mask1);
  read_imgeint("./mask.fits","mask2",mask2);

  gsl_vector *potential = gsl_vector_calloc(x*y);
  gsl_matrix *potential_map = gsl_matrix_calloc(y,x);
  read_pimg("./potential.fits", potential);
  read_pimg("./potential.fits", potential_map);

  FinDifGrid griddy(x,y,1.0,3);
  FinDifGrid punkgriddy(mask1,1.0,3);
  gsl_vector *potential2 = gsl_vector_calloc(punkgriddy.show_int("numpix"));
  punkgriddy.convert(potential_map,potential2);
  FinDifGrid extremepunkgriddy(mask2,1.0,3);
  gsl_vector *potential3 = gsl_vector_calloc(extremepunkgriddy.show_int("numpix"));
 extremepunkgriddy.convert(potential_map,potential3);

  string file1 = "./cluster_no_mask_nocoord_verbose0.fits";
  string file2 = "./cluster_no_mask_nocoord_verbose3.fits";
  string file3 = "./cluster_mask_nocoord_verbose2.fits";
  string file4 = "./cluster_mask_lincoord_verbose1.fits";
  string file5 = "./cluster_mask_J2000coord_verbose3.fits";
  string file6 = "./cluster_mask_coord_rescaled_verbose3.fits";
  
  LensModel lens1(&griddy);
  lens1.read_vector("pot",potential);
  lens1.build_lens_pot();
  lens1.write_fits(file1,0);
  
  LensModel lens2(&griddy,potential);
  lens2.write_fits(file2,3);

  LensModel lens3(&punkgriddy,potential2);
  lens3.write_fits(file3,2);

  vector<double> linorigin;
  linorigin.push_back(10.0);
  linorigin.push_back(15.0);
  linorigin.push_back(0.0);
  linorigin.push_back(0.0);
  vector<double> linscale;
  linscale.push_back(3.125);
  LensModel lens4(&extremepunkgriddy,potential3,linorigin,linscale,false);
  lens4.write_fits(file4,0);
  vector<double> linorigin2;
  linorigin2.push_back(10.0);
  linorigin2.push_back(15.0);
  linorigin2.push_back(-80.0);
  linorigin2.push_back(64.0);
  vector<double> linscale2;
  linscale2.push_back(0.009);
  LensModel lens5(&extremepunkgriddy,potential3,linorigin2,linscale2,true);
  lens5.write_fits(file5,3);
  gsl_matrix *z = gsl_matrix_calloc(y,x);
  gsl_matrix_set_all(z,1.3);
  lens5.rescale(z,0.3,2.5);
  lens5.write_fits(file6,3);

  return 0;

}

