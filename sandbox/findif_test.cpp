#include <iostream>
#include <rw_fits.h>
#include <findif.h>

using namespace std;

int main()
{

  int x, y;
  x = 32;
  y = 32;

  gsl_matrix_int *type = gsl_matrix_int_calloc(32,32);
  gsl_vector *potential = gsl_vector_calloc(32*32);
  gsl_vector *result = gsl_vector_calloc(32*32);
  read_pimg("./potential.fits", potential);

  FinDifGrid griddy(x,y,1.0,1);
  type = griddy.show_data("typemap");
  write_pimgint("./findif_mult_result.fits",type);
  griddy.mult_vec("a1",potential,result);
  write_imge("./findif_mult_result.fits","a1",32,32,result);
  griddy.mult_vec("a2",potential,result);
  write_imge("./findif_mult_result.fits","a2",32,32,result);
  /*
  griddy.mult_vec("s1",potential,result);
  write_imge("./findif_mult_result.fits","s1",32,32,result);
  griddy.mult_vec("s2",potential,result);
  write_imge("./findif_mult_result.fits","s2",32,32,result);
  griddy.mult_vec("c",potential,result);
  write_imge("./findif_mult_result.fits","c",32,32,result);
  griddy.mult_vec("f1",potential,result);
  write_imge("./findif_mult_result.fits","f1",32,32,result);
  griddy.mult_vec("f2",potential,result);
  write_imge("./findif_mult_result.fits","f2",32,32,result);
  griddy.mult_vec("g1",potential,result);
  write_imge("./findif_mult_result.fits","g1",32,32,result);
  griddy.mult_vec("g2",potential,result);
  write_imge("./findif_mult_result.fits","g2",32,32,result);
  */
  gsl_matrix_int *mask1 = gsl_matrix_int_calloc(y,x);
  gsl_matrix_int *mask2 = gsl_matrix_int_calloc(y,x);
  read_pimgint("./mask.fits",mask1);
  read_imgeint("./mask.fits","mask2",mask2);

  FinDifGrid punkgriddy(mask1,1.0,3);
  FinDifGrid extremepunkgriddy(mask2,1.0,3);

  type = punkgriddy.show_data("typemap");
  write_pimgint("./findif_masked_mult_result.fits",type);

  type = extremepunkgriddy.show_data("typemap");
  write_imgeint("./findif_masked_mult_result.fits","more_complicated",type);

  type = punkgriddy.show_data("indexmap");
  write_imgeint("./findif_masked_mult_result.fits","indexmap",type);

  gsl_matrix *mpotential = gsl_matrix_calloc(y,x);
  read_pimg("./potential.fits",mpotential);

  gsl_vector *cutpot1 = gsl_vector_calloc(punkgriddy.show_int("numpix"));
  gsl_vector *cutpot2 = gsl_vector_calloc(extremepunkgriddy.show_int("numpix"));
  gsl_vector *result1 = gsl_vector_calloc(punkgriddy.show_int("numpix"));
  gsl_vector *result2 = gsl_vector_calloc(extremepunkgriddy.show_int("numpix"));

  punkgriddy.convert(mpotential,cutpot1);
  extremepunkgriddy.convert(mpotential,cutpot2);

  punkgriddy.mult_vec("a1",cutpot1,result1);
  punkgriddy.convert(result1,mpotential);
  write_imge("./findif_masked_mult_result.fits","a1_easy",mpotential);
  extremepunkgriddy.mult_vec("a1",cutpot2,result2);
  extremepunkgriddy.convert(result2,mpotential);
  write_imge("./findif_masked_mult_result.fits","a1_hard",mpotential);

  punkgriddy.mult_vec("a2",cutpot1,result1);
  punkgriddy.convert(result1,mpotential);
  write_imge("./findif_masked_mult_result.fits","a2_easy",mpotential);
  extremepunkgriddy.mult_vec("a2",cutpot2,result2);
  extremepunkgriddy.convert(result2,mpotential);
  write_imge("./findif_masked_mult_result.fits","a2_hard",mpotential);

  punkgriddy.mult_vec("s1",cutpot1,result1);
  punkgriddy.convert(result1,mpotential);
  write_imge("./findif_masked_mult_result.fits","s1_easy",mpotential);
  extremepunkgriddy.mult_vec("s1",cutpot2,result2);
  extremepunkgriddy.convert(result2,mpotential);
  write_imge("./findif_masked_mult_result.fits","s1_hard",mpotential);

  punkgriddy.mult_vec("s2",cutpot1,result1);
  punkgriddy.convert(result1,mpotential);
  write_imge("./findif_masked_mult_result.fits","s2_easy",mpotential);
  extremepunkgriddy.mult_vec("s2",cutpot2,result2);
  extremepunkgriddy.convert(result2,mpotential);
  write_imge("./findif_masked_mult_result.fits","s2_hard",mpotential);

  punkgriddy.mult_vec("c",cutpot1,result1);
  punkgriddy.convert(result1,mpotential);
  write_imge("./findif_masked_mult_result.fits","c_easy",mpotential);
  extremepunkgriddy.mult_vec("c",cutpot2,result2);
  extremepunkgriddy.convert(result2,mpotential);
  write_imge("./findif_masked_mult_result.fits","c_hard",mpotential);

  punkgriddy.mult_vec("f1",cutpot1,result1);
  punkgriddy.convert(result1,mpotential);
  write_imge("./findif_masked_mult_result.fits","f1_easy",mpotential);
  extremepunkgriddy.mult_vec("f1",cutpot2,result2);
  extremepunkgriddy.convert(result2,mpotential);
  write_imge("./findif_masked_mult_result.fits","f1_hard",mpotential);

  punkgriddy.mult_vec("f2",cutpot1,result1);
  punkgriddy.convert(result1,mpotential);
  write_imge("./findif_masked_mult_result.fits","f2_easy",mpotential);
  extremepunkgriddy.mult_vec("f2",cutpot2,result2);
  extremepunkgriddy.convert(result2,mpotential);
  write_imge("./findif_masked_mult_result.fits","f2_hard",mpotential);

  punkgriddy.mult_vec("g1",cutpot1,result1);
  punkgriddy.convert(result1,mpotential);
  write_imge("./findif_masked_mult_result.fits","g1_easy",mpotential);
  extremepunkgriddy.mult_vec("g1",cutpot2,result2);
  extremepunkgriddy.convert(result2,mpotential);
  write_imge("./findif_masked_mult_result.fits","g1_hard",mpotential);

  punkgriddy.mult_vec("g2",cutpot1,result1);
  punkgriddy.convert(result1,mpotential);
  write_imge("./findif_masked_mult_result.fits","g2_easy",mpotential);
  extremepunkgriddy.mult_vec("g2",cutpot2,result2);
  extremepunkgriddy.convert(result2,mpotential);
  write_imge("./findif_masked_mult_result.fits","g2_hard",mpotential);

  return 0;

}
