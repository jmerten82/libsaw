#include <iostream>
#include <gsl/gsl_matrix.h>
#include <rw_fits.h>
#include <map_analysis.h>

using namespace std;

int main()
{

    string filename = "/home/jmerten/science/a2744/final_rec/analysis/smooth_maps/stack_highres_coordJ2000.fits";
    string outname = "./oversampled_map.fits";

    int x_dim, y_dim;
    int sampling = 10;

    x_dim = read_intheader(filename,"NAXIS1");
    y_dim = read_intheader(filename,"NAXIS2");

    gsl_matrix *cmap = gsl_matrix_calloc(y_dim,x_dim);
    gsl_matrix *zmap = gsl_matrix_calloc(y_dim,x_dim);

    gsl_matrix *cmapout = gsl_matrix_calloc(y_dim*sampling, x_dim*sampling);
    gsl_matrix *zmapout = gsl_matrix_calloc(y_dim*sampling, x_dim*sampling); 
    gsl_matrix *densitymapout = gsl_matrix_calloc(y_dim*sampling, x_dim*sampling); 
    

    read_pimg(filename,cmap);
    read_imge(filename,"redshifts",zmap);

    cosmology cosmo1(0.3,0.7,0.7,-1.0,0.04);

    Convergence_Map cmap1(cmap,600.0,zmap,0.308,35.5,35.5,10,&cosmo1);
    //cmap1.masssheet_normalise("border",0.05);
    gsl_matrix_memcpy(cmapout,cmap1.data("overs_convergence"));
    gsl_matrix_memcpy(zmapout,cmap1.data("overs_redshifts"));
    gsl_matrix_memcpy(densitymapout,cmap1.data("density"));

    //cmap1.density_profile("arcsec",0.0,0.0,580.0,1000,"first_table.dat"); 

    //cout <<cmap1.x_c("arcsec_centre",350) <<"\t"  <<cmap1.x_c("arcsec_centre",350) <<endl;

    cout <<cmap1.mass("arcsec",170.7,-29.1,55.4) <<endl; 
    //cout <<cmap1.mass("arcsec",-8.058,-8.058,288.0,188.0) <<endl; 
  
/*  
    cout <<cmap1.double_data("pixel_size") <<endl;
    cout <<cmap1.double_data("physical_pixel_size") <<endl;
    cout <<cmap1.mass() <<endl;
*/
    write_pimg(outname,cmapout);
    write_imge(outname,"redshifts",zmapout);
    write_imge(outname,"density",densitymapout);
		      
		      


    return 0;

}
